/**
 */
package nl.utwente.jbcmm.tests;

import junit.textui.TestRunner;

import nl.utwente.jbcmm.DreturnInstruction;
import nl.utwente.jbcmm.JbcmmFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Dreturn Instruction</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class DreturnInstructionTest extends SimpleInstructionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(DreturnInstructionTest.class);
	}

	/**
	 * Constructs a new Dreturn Instruction test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DreturnInstructionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Dreturn Instruction test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected DreturnInstruction getFixture() {
		return (DreturnInstruction)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(JbcmmFactory.eINSTANCE.createDreturnInstruction());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //DreturnInstructionTest
