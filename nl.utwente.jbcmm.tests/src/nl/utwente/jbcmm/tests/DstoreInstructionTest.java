/**
 */
package nl.utwente.jbcmm.tests;

import junit.textui.TestRunner;

import nl.utwente.jbcmm.DstoreInstruction;
import nl.utwente.jbcmm.JbcmmFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Dstore Instruction</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class DstoreInstructionTest extends VarInstructionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(DstoreInstructionTest.class);
	}

	/**
	 * Constructs a new Dstore Instruction test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DstoreInstructionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Dstore Instruction test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected DstoreInstruction getFixture() {
		return (DstoreInstruction)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(JbcmmFactory.eINSTANCE.createDstoreInstruction());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //DstoreInstructionTest
