/**
 */
package nl.utwente.jbcmm.tests;

import junit.textui.TestRunner;

import nl.utwente.jbcmm.AastoreInstruction;
import nl.utwente.jbcmm.JbcmmFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Aastore Instruction</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class AastoreInstructionTest extends SimpleInstructionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(AastoreInstructionTest.class);
	}

	/**
	 * Constructs a new Aastore Instruction test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AastoreInstructionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Aastore Instruction test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected AastoreInstruction getFixture() {
		return (AastoreInstruction)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(JbcmmFactory.eINSTANCE.createAastoreInstruction());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //AastoreInstructionTest
