/**
 */
package nl.utwente.jbcmm.tests;

import junit.textui.TestRunner;

import nl.utwente.jbcmm.JbcmmFactory;
import nl.utwente.jbcmm.PutstaticInstruction;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Putstatic Instruction</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class PutstaticInstructionTest extends FieldInstructionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(PutstaticInstructionTest.class);
	}

	/**
	 * Constructs a new Putstatic Instruction test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PutstaticInstructionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Putstatic Instruction test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected PutstaticInstruction getFixture() {
		return (PutstaticInstruction)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(JbcmmFactory.eINSTANCE.createPutstaticInstruction());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //PutstaticInstructionTest
