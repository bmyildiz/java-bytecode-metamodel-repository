/**
 */
package nl.utwente.jbcmm.tests;

import nl.utwente.jbcmm.IntInstruction;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Int Instruction</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class IntInstructionTest extends InstructionTest {

	/**
	 * Constructs a new Int Instruction test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntInstructionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Int Instruction test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected IntInstruction getFixture() {
		return (IntInstruction)fixture;
	}

} //IntInstructionTest
