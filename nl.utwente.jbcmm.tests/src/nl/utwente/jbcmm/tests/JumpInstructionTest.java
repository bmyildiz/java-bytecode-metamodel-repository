/**
 */
package nl.utwente.jbcmm.tests;

import nl.utwente.jbcmm.JumpInstruction;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Jump Instruction</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class JumpInstructionTest extends InstructionTest {

	/**
	 * Constructs a new Jump Instruction test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JumpInstructionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Jump Instruction test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected JumpInstruction getFixture() {
		return (JumpInstruction)fixture;
	}

} //JumpInstructionTest
