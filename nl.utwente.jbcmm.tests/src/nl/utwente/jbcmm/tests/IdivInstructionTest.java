/**
 */
package nl.utwente.jbcmm.tests;

import junit.textui.TestRunner;

import nl.utwente.jbcmm.IdivInstruction;
import nl.utwente.jbcmm.JbcmmFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Idiv Instruction</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class IdivInstructionTest extends SimpleInstructionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(IdivInstructionTest.class);
	}

	/**
	 * Constructs a new Idiv Instruction test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdivInstructionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Idiv Instruction test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected IdivInstruction getFixture() {
		return (IdivInstruction)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(JbcmmFactory.eINSTANCE.createIdivInstruction());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //IdivInstructionTest
