/**
 */
package nl.utwente.jbcmm.tests;

import junit.textui.TestRunner;

import nl.utwente.jbcmm.JbcmmFactory;
import nl.utwente.jbcmm.ShortValue;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Short Value</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ShortValueTest extends ElementaryValueTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ShortValueTest.class);
	}

	/**
	 * Constructs a new Short Value test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShortValueTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Short Value test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ShortValue getFixture() {
		return (ShortValue)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(JbcmmFactory.eINSTANCE.createShortValue());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ShortValueTest
