/**
 */
package nl.utwente.jbcmm.tests;

import junit.textui.TestRunner;

import nl.utwente.jbcmm.JbcmmFactory;
import nl.utwente.jbcmm.LdcTypeInstruction;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ldc Type Instruction</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class LdcTypeInstructionTest extends LdcInstructionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(LdcTypeInstructionTest.class);
	}

	/**
	 * Constructs a new Ldc Type Instruction test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LdcTypeInstructionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Ldc Type Instruction test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected LdcTypeInstruction getFixture() {
		return (LdcTypeInstruction)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(JbcmmFactory.eINSTANCE.createLdcTypeInstruction());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //LdcTypeInstructionTest
