/**
 */
package nl.utwente.jbcmm.tests;

import nl.utwente.jbcmm.FieldInstruction;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Field Instruction</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class FieldInstructionTest extends InstructionTest {

	/**
	 * Constructs a new Field Instruction test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FieldInstructionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Field Instruction test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected FieldInstruction getFixture() {
		return (FieldInstruction)fixture;
	}

} //FieldInstructionTest
