/**
 */
package nl.utwente.jbcmm.tests;

import junit.textui.TestRunner;

import nl.utwente.jbcmm.JbcmmFactory;
import nl.utwente.jbcmm.LastoreInstruction;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Lastore Instruction</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class LastoreInstructionTest extends SimpleInstructionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(LastoreInstructionTest.class);
	}

	/**
	 * Constructs a new Lastore Instruction test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LastoreInstructionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Lastore Instruction test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected LastoreInstruction getFixture() {
		return (LastoreInstruction)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(JbcmmFactory.eINSTANCE.createLastoreInstruction());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //LastoreInstructionTest
