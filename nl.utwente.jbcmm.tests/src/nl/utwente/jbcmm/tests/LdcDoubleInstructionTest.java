/**
 */
package nl.utwente.jbcmm.tests;

import junit.textui.TestRunner;

import nl.utwente.jbcmm.JbcmmFactory;
import nl.utwente.jbcmm.LdcDoubleInstruction;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ldc Double Instruction</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class LdcDoubleInstructionTest extends LdcInstructionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(LdcDoubleInstructionTest.class);
	}

	/**
	 * Constructs a new Ldc Double Instruction test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LdcDoubleInstructionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Ldc Double Instruction test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected LdcDoubleInstruction getFixture() {
		return (LdcDoubleInstruction)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(JbcmmFactory.eINSTANCE.createLdcDoubleInstruction());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //LdcDoubleInstructionTest
