/**
 */
package nl.utwente.jbcmm.tests;

import nl.utwente.jbcmm.Instruction;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Instruction</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class InstructionTest extends IdentifiableTest {

	/**
	 * Constructs a new Instruction test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InstructionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Instruction test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Instruction getFixture() {
		return (Instruction)fixture;
	}

} //InstructionTest
