/**
 */
package nl.utwente.jbcmm.tests;

import junit.textui.TestRunner;

import nl.utwente.jbcmm.JbcmmFactory;
import nl.utwente.jbcmm.SwitchDefaultEdge;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Switch Default Edge</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class SwitchDefaultEdgeTest extends ControlFlowEdgeTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(SwitchDefaultEdgeTest.class);
	}

	/**
	 * Constructs a new Switch Default Edge test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SwitchDefaultEdgeTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Switch Default Edge test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected SwitchDefaultEdge getFixture() {
		return (SwitchDefaultEdge)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(JbcmmFactory.eINSTANCE.createSwitchDefaultEdge());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //SwitchDefaultEdgeTest
