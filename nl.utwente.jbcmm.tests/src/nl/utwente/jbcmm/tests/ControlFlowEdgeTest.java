/**
 */
package nl.utwente.jbcmm.tests;

import nl.utwente.jbcmm.ControlFlowEdge;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Control Flow Edge</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ControlFlowEdgeTest extends IdentifiableTest {

	/**
	 * Constructs a new Control Flow Edge test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlFlowEdgeTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Control Flow Edge test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ControlFlowEdge getFixture() {
		return (ControlFlowEdge)fixture;
	}

} //ControlFlowEdgeTest
