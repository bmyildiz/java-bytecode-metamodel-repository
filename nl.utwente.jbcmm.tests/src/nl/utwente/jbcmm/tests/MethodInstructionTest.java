/**
 */
package nl.utwente.jbcmm.tests;

import nl.utwente.jbcmm.MethodInstruction;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Method Instruction</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class MethodInstructionTest extends InstructionTest {

	/**
	 * Constructs a new Method Instruction test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MethodInstructionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Method Instruction test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected MethodInstruction getFixture() {
		return (MethodInstruction)fixture;
	}

} //MethodInstructionTest
