/**
 */
package nl.utwente.jbcmm.tests;

import nl.utwente.jbcmm.SimpleInstruction;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Simple Instruction</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class SimpleInstructionTest extends InstructionTest {

	/**
	 * Constructs a new Simple Instruction test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleInstructionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Simple Instruction test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected SimpleInstruction getFixture() {
		return (SimpleInstruction)fixture;
	}

} //SimpleInstructionTest
