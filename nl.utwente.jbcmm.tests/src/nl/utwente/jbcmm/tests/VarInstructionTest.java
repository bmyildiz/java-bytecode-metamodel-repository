/**
 */
package nl.utwente.jbcmm.tests;

import nl.utwente.jbcmm.VarInstruction;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Var Instruction</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class VarInstructionTest extends InstructionTest {

	/**
	 * Constructs a new Var Instruction test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VarInstructionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Var Instruction test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected VarInstruction getFixture() {
		return (VarInstruction)fixture;
	}

} //VarInstructionTest
