/**
 */
package nl.utwente.jbcmm.tests;

import nl.utwente.jbcmm.LdcInstruction;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ldc Instruction</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class LdcInstructionTest extends InstructionTest {

	/**
	 * Constructs a new Ldc Instruction test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LdcInstructionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Ldc Instruction test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected LdcInstruction getFixture() {
		return (LdcInstruction)fixture;
	}

} //LdcInstructionTest
