/**
 */
package nl.utwente.jbcmm.tests;

import junit.textui.TestRunner;

import nl.utwente.jbcmm.Fconst_1Instruction;
import nl.utwente.jbcmm.JbcmmFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Fconst 1Instruction</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class Fconst_1InstructionTest extends SimpleInstructionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(Fconst_1InstructionTest.class);
	}

	/**
	 * Constructs a new Fconst 1Instruction test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Fconst_1InstructionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Fconst 1Instruction test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Fconst_1Instruction getFixture() {
		return (Fconst_1Instruction)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(JbcmmFactory.eINSTANCE.createFconst_1Instruction());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //Fconst_1InstructionTest
