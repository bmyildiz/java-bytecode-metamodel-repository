/**
 */
package nl.utwente.jbcmm.tests;

import junit.textui.TestRunner;

import nl.utwente.jbcmm.JbcmmFactory;
import nl.utwente.jbcmm.NopInstruction;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Nop Instruction</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class NopInstructionTest extends SimpleInstructionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(NopInstructionTest.class);
	}

	/**
	 * Constructs a new Nop Instruction test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NopInstructionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Nop Instruction test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected NopInstruction getFixture() {
		return (NopInstruction)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(JbcmmFactory.eINSTANCE.createNopInstruction());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //NopInstructionTest
