/**
 */
package nl.utwente.jbcmm.tests;

import nl.utwente.jbcmm.TypeInstruction;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Type Instruction</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class TypeInstructionTest extends InstructionTest {

	/**
	 * Constructs a new Type Instruction test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeInstructionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Type Instruction test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected TypeInstruction getFixture() {
		return (TypeInstruction)fixture;
	}

} //TypeInstructionTest
