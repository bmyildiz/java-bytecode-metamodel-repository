package nl.utwente.jbcmm.ui.handlers;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.filesystem.URIUtil;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.progress.IProgressService;

import nl.utwente.jbcmm.Project;
import nl.utwente.jbcmm.importer.Extractor;
import nl.utwente.jbcmm.importer.IExtractionProgressMonitor;
import nl.utwente.jbcmm.ui.Activator;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * 
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class ExtractJbcmm extends AbstractHandler {
	
	private static class TimedSubProgressMonitor extends SubProgressMonitor {
		
		private class Entry {
			public final String name;
			public final long time;

			public Entry(String name, long time) {
				this.name = name;
				this.time = time;	
			}
			
			@Override
			public String toString() {
				return Long.toString(time);
			}
		}

		private List<Entry> taskTimes = new ArrayList<ExtractJbcmm.TimedSubProgressMonitor.Entry>(); 
		private String currentTask;
		private long currentBeginTime;
		
		public TimedSubProgressMonitor(IProgressMonitor monitor, int ticks) {
			super(monitor, ticks);
		}
		
		@Override
		public void beginTask(String name, int totalWork) {
			super.beginTask(name, totalWork);
			currentTask = name;
			currentBeginTime = System.currentTimeMillis();
		}
		
		@Override
		public void done() {
			long totalTime = System.currentTimeMillis() - currentBeginTime;
			taskTimes.add(new Entry(currentTask, totalTime));
		}
		
		public String reportTaskTimes() {
			if (taskTimes.isEmpty())
				return "";
			
			Iterator<Entry> it = taskTimes.iterator();
			StringBuilder res = new StringBuilder(it.next().toString());
			
			while (it.hasNext()) {
				res.append("\n").append(it.next());
			}
			return res.toString();
		}
	}
	
	/**
	 * The constructor.
	 */
	public ExtractJbcmm() {
	}
	
	public class ExtractionProgressMonitor implements IExtractionProgressMonitor {

		private IProgressMonitor monitor;
		private String name;
		
		public ExtractionProgressMonitor(IProgressMonitor monitor, String name) {
			this.monitor = monitor;
			this.name = name;
		}

		@Override
		public void beginTask(int amount) {
			monitor.beginTask(name, amount);
		}

		@Override
		public void worked(int amount) {
			monitor.worked(amount);
		}

		@Override
		public void done() {
			monitor.done();
		}

		@Override
		public void beginTask() {
			monitor.beginTask(name, IProgressMonitor.UNKNOWN);
		}

		@Override
		public void beginTask(int amount, String info) {
			monitor.beginTask(name + "[" + info + "]", amount);
		}

		@Override
		public void beginTask(String info) {
			monitor.beginTask(name + "[" + info + "]", IProgressMonitor.UNKNOWN);
		}
		
	}
	
	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection structuredSelection = (IStructuredSelection) selection;
			Object selected = structuredSelection.getFirstElement();
			if (selected instanceof IType) {
				try {
					final IType selectedType = (IType) selected;

					String typeName = selectedType.getFullyQualifiedName();
					IPackageFragmentRoot[] roots = selectedType.getJavaProject().getPackageFragmentRoots();
					List<URL> cpSourceElementsURL = new ArrayList<URL>();
					List<URL> cpBinaryElementsURL = new ArrayList<URL>();

					String message = "main type: " + typeName
							+ "\n\n classpath:\n";
					for (int i = 0; i < roots.length; i++) {
						List<URL> listForAddition;
						IPath location;
						if (roots[i].getResolvedClasspathEntry().getEntryKind() == IClasspathEntry.CPE_SOURCE) {
							
							location = roots[i].getResolvedClasspathEntry().getOutputLocation();
							if (location == null)
								location = selectedType.getJavaProject()
										.getOutputLocation();
							location = underlyingResource(location.makeRelativeTo(selectedType.getJavaProject().getPath()), selectedType.getJavaProject().getProject()).getLocation();
							listForAddition = cpSourceElementsURL;
							
						} else {
							if (roots[i].isExternal())
								location = roots[i].getPath();
							else 
								location = roots[i].getUnderlyingResource().getLocation();
							listForAddition = cpBinaryElementsURL;
						}
						location.makeAbsolute();
						message += location.toOSString() + "\n";
						listForAddition.add(location.toFile().toURI().toURL());
					}

					final URL[] cpSourceElementsURLArray = new URL[cpSourceElementsURL.size()];
					cpSourceElementsURL.toArray(cpSourceElementsURLArray);
					final URL[] cpBinaryElementsURLArray = new URL[cpBinaryElementsURL.size()];
					cpBinaryElementsURL.toArray(cpBinaryElementsURLArray);
					
					final IWorkbenchWindow window = HandlerUtil
							.getActiveWorkbenchWindowChecked(event);
					MessageDialog.openInformation(window.getShell(),
							"Java Bytceode ++ UI", message);

					IWorkbench wb = PlatformUI.getWorkbench();
					IProgressService ps = wb.getProgressService();
					try {
						ps.busyCursorWhile(new IRunnableWithProgress() {
							
							@Override
							public void run(IProgressMonitor monitor) throws InvocationTargetException,
									InterruptedException {
								try {
									monitor.beginTask("Extract Java Bytecode ++", 10);
									
									TimedSubProgressMonitor typeHierarchyProgress = new TimedSubProgressMonitor(monitor, 5);
									TimedSubProgressMonitor extractionProgress = new TimedSubProgressMonitor(monitor, 4);
									
									Project project = new Extractor().extract(selectedType.getFullyQualifiedName(), cpSourceElementsURLArray, cpBinaryElementsURLArray,
											new ExtractionProgressMonitor(typeHierarchyProgress, "Extracting type hierarchy."),
											new ExtractionProgressMonitor(extractionProgress, "Extracting Java Bytecode++"));
									
									TimedSubProgressMonitor savingProgress = new TimedSubProgressMonitor(monitor, 1);
									try {
										savingProgress.beginTask("Saving Java Bytecode++ model", 3);
										Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE; 
									    Map<String, Object> m = reg.getExtensionToFactoryMap();
									    m.put("jbcmm", new XMIResourceFactoryImpl());
		
									    // Obtain a new resource set
									    ResourceSet resSet = new ResourceSetImpl();
		
									    IPath outputFile = selectedType.getJavaProject().getProject().getLocation().append(selectedType.getFullyQualifiedName().replace(".",  "_") + ".jbcmm");
									    
									    // create a resource
									    Resource resource = resSet.createResource(
									    		URI.createURI(URIUtil.toURI(outputFile).toString()));

									    Activator.logInfo("Write model to URI: " + resource.getURI() + ".");
									    // Get the first model element and cast it to the right type, in my
									    // example everything is hierarchical included in this first node
									    resource.getContents().add(project);
									    savingProgress.worked(1);
		
									    // now save the content.
									    try {
									    	resource.save(Collections.singletonMap(XMLResource.OPTION_XML_VERSION, "1.1"));
									    	selectedType.getJavaProject().getProject().refreshLocal(IResource.DEPTH_ONE, new SubProgressMonitor(savingProgress, 0));
									    	savingProgress.worked(1);
//											provideVisualizationSupport(outputFile);
											savingProgress.worked(1);
									    } catch (IOException e) {
									    	throw new RuntimeException(e);
									    }
									}
									finally {
										savingProgress.done();
									}
									final StringBuilder timeReport = new StringBuilder();
									timeReport.append("hierarchy time [ms]; extraction time [ms]; saving time [ms]; #processedMethods; #methodInstructions; #localVariables; #labels; #lineNumbers; #frames; #instructions; #jumpInstructions; #switchInstructions; #switchTargets; #typesInHierarchy; #classesLoaded\n");
									timeReport.append(typeHierarchyProgress.reportTaskTimes()).append(";");
									timeReport.append(extractionProgress.reportTaskTimes()).append(";");
									timeReport.append(savingProgress.reportTaskTimes()).append(";");
									timeReport.append(Extractor.PROFILE.processedMethods).append(";");
									timeReport.append(Extractor.PROFILE.methodInstructions).append(";");
									timeReport.append(Extractor.PROFILE.localVariables).append(";");
									timeReport.append(Extractor.PROFILE.labels).append(";");
									timeReport.append(Extractor.PROFILE.lineNumbers).append(";");
									timeReport.append(Extractor.PROFILE.frames).append(";");
									timeReport.append(Extractor.PROFILE.instructions).append(";");
									timeReport.append(Extractor.PROFILE.jumpInstructions).append(";");
									timeReport.append(Extractor.PROFILE.switchInstructions).append(";");
									timeReport.append(Extractor.PROFILE.switchTargets).append(";");
									timeReport.append(Extractor.PROFILE.typesInHierarchy).append(";");
									timeReport.append(Extractor.PROFILE.classesLoaded).append("\n");
									
									Display.getDefault().syncExec(new Runnable() {
										
										@Override
										public void run() {
											new InputDialog(window.getShell(), "Java Bytceode ++ UI", "PROFILE:", timeReport.toString(), null).open();
//											MessageDialog.openInformation(window.getShell(), "Java Bytceode ++ UI",
//													timeReport);
										}
									});

								}
								catch (Throwable thr) {
									Activator.logThrowable(thr);
									throw new RuntimeException(thr);
								}
								finally {
									monitor.done();
								}
							}
						});
					} catch (Exception e) {
						throw new RuntimeException(e);
					}
					return null;
				} catch (JavaModelException e) {
					Activator.logThrowable(e);
					throw new ExecutionException("", e);
				} catch (MalformedURLException e) {
					Activator.logThrowable(e);
					throw new ExecutionException("", e);
				} catch (Throwable thr) {
					Activator.logThrowable(thr);
					throw new RuntimeException("", thr);
				}
			}
		}
	
		IWorkbenchWindow window = HandlerUtil
				.getActiveWorkbenchWindowChecked(event);
		MessageDialog.openInformation(window.getShell(), "Java Bytceode ++ UI",
				"Must select exactly one Java type.");
		return null;
	}
	
//	private void provideVisualizationSupport(IPath modelPath) {
//		File modelFile = modelPath.toFile();
//		String modelFileString = modelFile.toString();
//		String modelFolder = modelFileString.substring(0, modelFileString.lastIndexOf(File.separatorChar));
//		String modelFileName = modelFileString.substring(modelFileString.lastIndexOf(File.separatorChar) + 1, modelFileString.lastIndexOf('.'));
//		
//		String targetGraphdescFileName = modelFolder + File.separatorChar + "jbcpp.graphdesc";
//		InputStream sourceFile = ExtractJbcpp.class.getClassLoader().getResourceAsStream("jbcpp.graphdesc");
//		Activator.logInfo("Attempt to copy graphdesc file to: " + targetGraphdescFileName);
//
//		try {
//			OutputStream targetFile = new FileOutputStream(targetGraphdescFileName);
//			byte[] buffer = new byte[1024];
//			while (sourceFile.available() > buffer.length) {
//				sourceFile.read(buffer);
//				targetFile.write(buffer);
//			}
//			if (sourceFile.available() > 0) {
//				int bytes = sourceFile.available();
//				sourceFile.read(buffer, 0, bytes);
//				targetFile.write(buffer, 0, bytes);
//			}
//			targetFile.flush();
//			targetFile.close();
//			sourceFile.close();
//		} catch (IOException e) {
//			Activator.logThrowable(e);
//			throw new RuntimeException("", e);
//		}
//	}

	public IResource underlyingResource(IPath path, IProject project) {
		IContainer folder = (IContainer) project;
		String[] segs = path.segments();
		for (int i = 0; i < segs.length; ++i) {
			IResource child = folder.findMember(segs[i]);
			if (child == null || child.getType() != IResource.FOLDER) {
				throw new RuntimeException("Not found: " + path + " in " + project);
			}
			folder = (IFolder) child;
		}
		return folder;

	}
}
