package nl.utwente.jbcmm.ui.popup.actions;

import java.awt.SecondaryLoop;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.filesystem.URIUtil;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceImpl;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceFactoryImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import nl.utwente.jbcmm.Project;
import nl.utwente.jbcmm.importer.Exporter;
import nl.utwente.jbcmm.ui.Activator;

public class ToClassfiles implements IObjectActionDelegate {

	private Shell shell;
	private IFile jbcmmFile;
	
	/**
	 * Constructor for Action1.
	 */
	public ToClassfiles() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		long start = System.currentTimeMillis();
	    // Obtain a new resource set
	    ResourceSet resSet = new ResourceSetImpl();
	    Resource resource = resSet.createResource(URI.createURI(URIUtil.toURI(jbcmmFile.getLocation()).toString()));
	    try {
	    	Map<Object, Object> loadOptions = ((XMLResourceImpl) resource).getDefaultLoadOptions();
	    	loadOptions.put(XMLResource.OPTION_DEFER_ATTACHMENT, Boolean.TRUE);
	    	loadOptions.put(XMLResource.OPTION_DEFER_IDREF_RESOLUTION, Boolean.TRUE);
	    	loadOptions.put(XMLResource.OPTION_USE_DEPRECATED_METHODS, Boolean.TRUE);	    	
	    	loadOptions.put(XMLResource.OPTION_USE_XML_NAME_TO_FEATURE_MAP, new HashMap<Object, Object>());
	    	((ResourceImpl)resource).setIntrinsicIDToEObjectMap(new HashMap<String, EObject>());
			resource.load(loadOptions);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	    
	    
	    // get resource
	    //Resource resource = resSet.getResource(URI.createURI(URIUtil.toURI(jbcmmFile.getLocation()).toString()), true);	    
	    
	    EList<EObject> contents = resource.getContents();
	    if (contents.size() != 1) {
	    	MessageDialog.openInformation(
	    			shell,
	    			"JBCMM User Interface",
	    			"Did not find exactly one root in " + jbcmmFile);
	    	return;
	    }
	    
	    Project project = (Project) contents.get(0);
	    long readModelMillis = System.currentTimeMillis() - start;
	    Exporter exporter = new Exporter();
	    exporter.export(project, jbcmmFile.getProject().getLocation().toOSString());
	    long totalMillis = System.currentTimeMillis() - start;
	    
		Display.getDefault().syncExec(new Runnable() {
			
			@Override
			public void run() {
				new InputDialog(shell, "Java Bytceode ++ UI", "Export PROFILE:", exporter.reportTime(totalMillis, readModelMillis), null).open();
//				MessageDialog.openInformation(window.getShell(), "Java Bytceode ++ UI",
//						timeReport);
			}
		});

	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
		this.jbcmmFile = null;
		if (selection instanceof IStructuredSelection) {
			Object selectedItem = ((IStructuredSelection) selection).getFirstElement();
			if (selectedItem instanceof IFile) {
				IFile selectedFile = (IFile) selectedItem;
				if (selectedFile.getFileExtension().equalsIgnoreCase("jbcmm")) {
					this.jbcmmFile = selectedFile;
				}
			}
		}
	}

}
