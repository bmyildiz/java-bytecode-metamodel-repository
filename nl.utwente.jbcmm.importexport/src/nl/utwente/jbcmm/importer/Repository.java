package nl.utwente.jbcmm.importer;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;

public class Repository {

	private Map<String, ClassNode> typedescriptor2classnode = new HashMap<String, ClassNode>();
	private Map<String, Boolean> typedescriptor2isSource = new HashMap<String, Boolean>();
	
	private Map<String, Set<String>> type2subtypes = new HashMap<String, Set<String>>();
	private final URLClassLoader sourceCL;
	private URLClassLoader binaryCL;

	public Repository(URL[] cpSourceElements, URL[] cpBinaryElements, IExtractionProgressMonitor typeHierarchyMonitor) {
		try {
			List<URL> urls = new ArrayList<URL>();
			urls.addAll(Arrays.asList(cpSourceElements));
			urls.addAll(Arrays.asList(cpBinaryElements));
			
			sourceCL = new URLClassLoader(cpSourceElements) {
				@Override
				public URL getResource(String name) {
					return findResource(name);
				}
			};
			binaryCL = new URLClassLoader(cpBinaryElements);
			
			typeHierarchyMonitor.beginTask(urls.size());
			for (URL url : urls) {
				File file;
				try {
					file = new File(url.toURI());
					if (file.isDirectory()) {
						findClassesInDirectory(file);
					}
					else if (file.getPath().endsWith(".jar") || file.getPath().endsWith(".zip")) {
						findClassesInJar(file);
					}
					else {
						throw new IllegalArgumentException("Unsupported class path entry: " + file);
					}
				} catch (Exception e) {
					throw new IllegalArgumentException("Unsupported class path entry: " + url, e);
				}
				typeHierarchyMonitor.worked(1);
			}
		}
		finally {
			typeHierarchyMonitor.done();
		}
	}
	
	public Map<String, ClassNode> collectReachableClasses(String typeDescriptor, String methodID) {
		Map<String, ClassNode> result = new HashMap<String, ClassNode>();
		Set<String> processedMethods = new HashSet<String>();
		process(typeDescriptor, methodID, result, processedMethods);
		return result;
	}
	
	/**
	 * 
	 * @param this
	 * @param typeDescriptor
	 * @param methodID
	 * @param reachableClasses
	 * @param processedMethods 
	 * @return true if a concrete method was found and could be processed. false
	 *         otherwise
	 */
	private boolean process(String typeDescriptor, String methodID,
			Map<String, ClassNode> reachableClasses, Set<String> processedMethods) {
		try {
			// was the method already processed?
			String methodFQN = typeDescriptor + " " + methodID; 
			if (processedMethods.contains(methodFQN))
				return true;
			processedMethods.add(methodFQN);

			// attempt to read the method from the class path
			// (this may fail if there is an I/O error, e.g., if the class that
			// contains the method is not on the classpath)
			MethodNode methodNode;
			try {
				methodNode = this.getMethodNode(typeDescriptor, methodID);
			} catch (Exception e) {
//				System.err
//						.println("Cannot find method implementation " + methodID + " in type " + typeDescriptor + ".");
				return false;
			}

			// TODO (exclusion criteria)
			// possibly exclude method from processing in order not to bloat the
			// model
			if (!this.isSource(typeDescriptor))
				return false;
			if (Extractor.PROFILING)
				Extractor.PROFILE.processedMethods++;

			reachableClasses.put(typeDescriptor, this.getClassNode(typeDescriptor));

			// // create the model for the method and add it to the repository
			// repository.putModel(typeDescriptor, methodID,
			// mapMethod(typeDescriptor, methodNode));

			// scan the method for called method and continue processing them
			InsnList insnList = methodNode.instructions;
			AbstractInsnNode currentInstruction = insnList.getFirst();
			if (currentInstruction == null) { // i.e., if the method is abstract
				return false;
			}
			do {
				if (currentInstruction instanceof MethodInsnNode) {
					if (Extractor.PROFILING)
						Extractor.PROFILE.methodInstructions++;
					MethodInsnNode methodInsn = (MethodInsnNode) currentInstruction;
					String ownerTypedescriptor = methodInsn.owner;
					String targetMethodID = methodInsn.name + methodInsn.desc;

					String[] polymorphicOwnerTypeDescrptors = new String[] {};
					String[] orderedOwnerSuperClasses = new String[] {};
					if (methodInsn.getOpcode() == Opcodes.INVOKEVIRTUAL
							|| methodInsn.getOpcode() == Opcodes.INVOKEINTERFACE) {
						polymorphicOwnerTypeDescrptors = this.getSubtypes(ownerTypedescriptor);
					} else if (methodInsn.getOpcode() == Opcodes.INVOKESPECIAL) {
						orderedOwnerSuperClasses = this.getSuperClasses(ownerTypedescriptor);
					} else {
						polymorphicOwnerTypeDescrptors = new String[] { ownerTypedescriptor };
					}

					for (String superTypeDescriptor : orderedOwnerSuperClasses) {
						// try to process a method until one with a definition
						// implementation is found
						if (process(superTypeDescriptor, targetMethodID, reachableClasses, processedMethods))
							break;
					}
					for (int i = 0; i < polymorphicOwnerTypeDescrptors.length; i++) {
						process(polymorphicOwnerTypeDescrptors[i], targetMethodID, reachableClasses, processedMethods);
					}
				}
				currentInstruction = currentInstruction.getNext();
			} while (currentInstruction != null);

		} finally {
			Extractor.extractionMonitor.worked(1);
		}
		return true;
	}


	private void findClassesInJar(File file) throws IOException {
		JarFile jarFile = new JarFile(file);
		for (JarEntry entry : Collections.list(jarFile.entries())) {
			if (!entry.isDirectory() && entry.getName().endsWith(".class")) {
				extractTypeHierarchy(jarFile.getInputStream(entry));
			}
		}
	}

	private void findClassesInDirectory(File file) throws IOException {
		if (file.isDirectory()) {
			for (File subfile : file.listFiles()) {
				findClassesInDirectory(subfile);
			}
		}
		else if (file.getPath().endsWith(".class")) {
			extractTypeHierarchy(new FileInputStream(file));
		}
		else {
			// ignore
		}
	}

	private void extractTypeHierarchy(InputStream stream) throws IOException {
		ClassReader cr = new ClassReader(stream);
		cr.accept(new ClassVisitor(Opcodes.ASM5) {
			@Override
			public void visit(int version, int access, String name,
					String signature, String superName, String[] interfaces) {
				addSubclass(superName, name);
				for (String interfaceName : interfaces) {
					addSubclass(interfaceName, name);
				}
			}
		}, 0);
	}

	private void addSubclass(String superName, String subName) {
		Set<String> subNames = type2subtypes.get(superName);
		if (subNames == null) {
			subNames = new HashSet<String>();
			type2subtypes.put(superName, subNames);
		}
		subNames.add(subName);
		
		if (Extractor.PROFILING)
			Extractor.PROFILE.typesInHierarchy++;
	}

//	public boolean hasModel(String typedescriptor, String methoddescriptor) {
//		Map<String, Method> methoddescriptor2model = typedescriptor2methodid2model.get(typedescriptor);
//		if (methoddescriptor2model == null)
//			return false;
//
//		return methoddescriptor2model.containsKey(methoddescriptor);
//	}

//	public void putModel(String typedescriptor, String methodID, Method methodModel) {
//		Map<String, Method> methodid2model = typedescriptor2methodid2model.get(typedescriptor);
//		if (methodid2model == null) {
//			methodid2model = new HashMap<String, Method>();
//			typedescriptor2methodid2model.put(typedescriptor, methodid2model);
//		}
//
//		methodid2model.put(methodID, methodModel);
//
//		Clazz classmodel = getClassModel(typedescriptor);
//		classmodel.getMethods().add(methodModel);
//	}
//
//	public Clazz getClassModel(String typedescriptor) {
//		Clazz classmodel = typedescriptor2classmodel.get(typedescriptor);
//		if (classmodel == null) {
//			classmodel = JbcmmFactory.eINSTANCE.createClazz();
//			classmodel.setName(typedescriptor);
//			Set<String> subtypes = type2subtypes.get(typedescriptor);
//			if (subtypes != null) {
//				for (String subtype : subtypes) {
//					classmodel.getSubclasses().add(getClassModel(subtype));
//				}
//			}
//			typedescriptor2classmodel.put(typedescriptor, classmodel);
//		}
//		return classmodel;
//	}

//	public Collection<Clazz> getClassModels() {
//		return typedescriptor2classmodel.values();
//	}

	public MethodNode getMethodNode(String typeDescriptor, String methodID) {
		ClassNode cn = getClassNode(typeDescriptor);
		for (MethodNode methodNode : (List<MethodNode>) cn.methods) {
			if ((methodNode.name + methodNode.desc).equals(methodID))
				return methodNode;
		}
		throw new IllegalArgumentException("Method " + methodID + " in type " + typeDescriptor + " does not exist.");
	}
	
	public boolean isSource(String typeDescriptor) {
		getClassNode(typeDescriptor);
		return typedescriptor2isSource.get(typeDescriptor);
	}

	public ClassNode getClassNode(String typeDescriptor) {
		ClassNode result = typedescriptor2classnode.get(typeDescriptor);
		if (result == null) {
			try {
				boolean isSource = true;
				URL url = sourceCL.getResource(typeDescriptor + ".class");
				if (url == null) {
					isSource = false;
					url = binaryCL.getResource(typeDescriptor + ".class");
				}
				InputStream stream;
				try {
					 stream = url.openStream();
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
				ClassReader cr = new ClassReader(stream);
				result = new ClassNode();
				cr.accept(result, 0);
				typedescriptor2classnode.put(typeDescriptor, result);
				typedescriptor2isSource.put(typeDescriptor, isSource);
				if (Extractor.PROFILING)
					Extractor.PROFILE.classesLoaded++;
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
		return result;
	}

	public String[] getSubtypes(String typedescriptor) {
		Set<String> subtypes = new HashSet<String>();
		collectSubtypes(subtypes, typedescriptor);
		return (String[]) subtypes.toArray(new String[subtypes.size()]);
	}
	
	public Set<String> getChildren(String typedescriptor) {
		Set<String> result = type2subtypes.get(typedescriptor);
		if (result == null)
			return Collections.emptySet();
		else
			return result;
	}

	private void collectSubtypes(Set<String> subtypes, String typedescriptor) {
		subtypes.add(typedescriptor);
		if (!type2subtypes.containsKey(typedescriptor))
			return;
		for (String subtype : type2subtypes.get(typedescriptor)) {
			subtypes.add(subtype);
			collectSubtypes(subtypes, subtype);
		}
	}

//	public List<Method> getCFGs() {
//		List<Method> result = new ArrayList<Method>();
//		for (Map<String, Method> map : typedescriptor2methodid2model.values()) {
//			result.addAll(map.values());
//		}
//		return result;
//	}

	public String[] getSuperClasses(String typedescriptor) {
		List<String> resultList = new ArrayList<String>();
		String currentTypedescriptor = typedescriptor;
		do {
			resultList.add(currentTypedescriptor);
			if (!typedescriptor2classnode.containsKey(currentTypedescriptor))
				break;
			currentTypedescriptor = typedescriptor2classnode.get(currentTypedescriptor).superName;
		} while (currentTypedescriptor != null);
		return resultList.toArray(new String[resultList.size()]);
	}

}
