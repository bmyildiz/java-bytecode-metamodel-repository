package nl.utwente.jbcmm.importer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Modifier;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.swing.LookAndFeel;
import javax.xml.stream.events.EntityDeclaration;

import org.apache.commons.lang.StringEscapeUtils;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.InvokeDynamicInsnNode;
import org.objectweb.asm.tree.JumpInsnNode;
import org.objectweb.asm.util.CheckClassAdapter;
import org.objectweb.asm.util.Printer;

import nl.utwente.jbcmm.BooleanValue;
import nl.utwente.jbcmm.ByteValue;
import nl.utwente.jbcmm.CharValue;
import nl.utwente.jbcmm.Clazz;
import nl.utwente.jbcmm.ConditionalEdge;
import nl.utwente.jbcmm.ControlFlowEdge;
import nl.utwente.jbcmm.DoubleValue;
import nl.utwente.jbcmm.ElementaryValue;
import nl.utwente.jbcmm.ExceptionTableEntry;
import nl.utwente.jbcmm.ExceptionalEdge;
import nl.utwente.jbcmm.Field;
import nl.utwente.jbcmm.FieldInstruction;
import nl.utwente.jbcmm.FieldReference;
import nl.utwente.jbcmm.FloatValue;
import nl.utwente.jbcmm.GotoInstruction;
import nl.utwente.jbcmm.IincInstruction;
import nl.utwente.jbcmm.Instruction;
import nl.utwente.jbcmm.IntInstruction;
import nl.utwente.jbcmm.IntValue;
import nl.utwente.jbcmm.JbcmmFactory;
import nl.utwente.jbcmm.JbcmmPackage;
import nl.utwente.jbcmm.JsrInstruction;
import nl.utwente.jbcmm.JumpInstruction;
import nl.utwente.jbcmm.LdcDoubleInstruction;
import nl.utwente.jbcmm.LdcFloatInstruction;
import nl.utwente.jbcmm.LdcInstruction;
import nl.utwente.jbcmm.LdcIntInstruction;
import nl.utwente.jbcmm.LdcLongInstruction;
import nl.utwente.jbcmm.LdcStringInstruction;
import nl.utwente.jbcmm.LdcTypeInstruction;
import nl.utwente.jbcmm.LocalVariableTableEntry;
import nl.utwente.jbcmm.LongValue;
import nl.utwente.jbcmm.Method;
import nl.utwente.jbcmm.MethodInstruction;
import nl.utwente.jbcmm.MethodReference;
import nl.utwente.jbcmm.MultianewarrayInstruction;
import nl.utwente.jbcmm.Project;
import nl.utwente.jbcmm.ShortValue;
import nl.utwente.jbcmm.StringValue;
import nl.utwente.jbcmm.SwitchCaseEdge;
import nl.utwente.jbcmm.SwitchDefaultEdge;
import nl.utwente.jbcmm.SwitchInstruction;
import nl.utwente.jbcmm.TypeInstruction;
import nl.utwente.jbcmm.TypeReference;
import nl.utwente.jbcmm.UnconditionalEdge;
import nl.utwente.jbcmm.VarInstruction;

public class Exporter {

	private static final Map<String, Integer> MNEMONIC_TO_OPCODE = new HashMap<>();
	
	private long ioMillis;
	private long verificationMillis;
	private long classAssemblyMillis;
	
	static {
		for (int i = 0; i < Printer.OPCODES.length; i++) {
			if (!Printer.OPCODES[i].equals(""))
				MNEMONIC_TO_OPCODE.put(Printer.OPCODES[i], i);
		}
	}
	
	public void export(Project project, String projectRootDir) {
		File baseFolder = new File(projectRootDir, "jbcmm-gen");
		baseFolder.mkdirs();
		for (Clazz clazz : project.getClasses()) {
			long start = System.currentTimeMillis();
			String className = clazz.getName();
			File classFile;
			if (className.contains("/")) {
				String packageName = className.substring(0, className.lastIndexOf("/"));
				File packageDir = new File(baseFolder, packageName.replace('/', File.separatorChar));
				packageDir.mkdirs();
				classFile = new File(packageDir, className.substring(className.lastIndexOf("/") + 1) + ".class");
			}
			else {
				classFile = new File(baseFolder, className + ".class");
			}
			try {
				classFile.createNewFile();
				ioMillis += System.currentTimeMillis() - start;

				export(clazz, classFile);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}

	private void export(Clazz clazz, File classFile) throws IOException {
		long startAssembly = System.currentTimeMillis();
		int flags = ClassWriter.COMPUTE_MAXS;
		if (clazz.getMajorVersion() >= 50) // Java 6
			flags = ClassWriter.COMPUTE_FRAMES;
			
		ClassWriter classWriter = new ClassWriter(flags);
		CheckClassAdapter checkedClassWriter = new CheckClassAdapter(classWriter, false);

		assemble(checkedClassWriter, clazz);
		
		classAssemblyMillis += System.currentTimeMillis() - startAssembly;
		
		long startVerification = System.currentTimeMillis();
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		byte[] bytecode = classWriter.toByteArray();
		
		try {
			CheckClassAdapter.verify(new ClassReader(bytecode), true, pw);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(sw.toString());
		}
		
		verificationMillis += System.currentTimeMillis() - startVerification;
		
		long startIO = System.currentTimeMillis();
		try (FileOutputStream fos = new FileOutputStream(classFile)) {
			fos.write(bytecode);
		}
		ioMillis += System.currentTimeMillis() - startIO;
	}

	private void assemble(ClassVisitor classVisitor, Clazz clazz) {
		int access = 0;
		if (clazz.isSuper())
			access |= Opcodes.ACC_SUPER;
		if (clazz.isAbstract())
			access |= Opcodes.ACC_ABSTRACT;
		if (clazz.isAnnotation())
			access |= Opcodes.ACC_ANNOTATION;
		if (clazz.isDeprecated())
			access |= Opcodes.ACC_DEPRECATED;
		if (clazz.isEnum())
			access |= Opcodes.ACC_ENUM;
		if (clazz.isFinal())
			access |= Opcodes.ACC_FINAL;
		if (clazz.isInterface())
			access |= Opcodes.ACC_INTERFACE;
		if (clazz.isPrivate())
			access |= Opcodes.ACC_PRIVATE;
		if (clazz.isProtected())
			access |= Opcodes.ACC_PROTECTED;
		if (clazz.isPublic())
			access |= Opcodes.ACC_PUBLIC;
		if (clazz.isSynthetic())
			access |= Opcodes.ACC_SYNTHETIC;

		String[] interfaces = new String[clazz.getInterfaces().size()];
		for (int i = 0; i < clazz.getInterfaces().size(); i++) {
			interfaces[i] = Type.getType(clazz.getInterfaces().get(i).getTypeDescriptor()).getInternalName();
		}
		String signature = null;
		if (clazz.getSignature() != null) 
			signature = clazz.getSignature().getSignature();
		String superName = null;
		if (clazz.getSuperClass() != null)
			superName = Type.getType(clazz.getSuperClass().getTypeDescriptor()).getInternalName();
		
		classVisitor.visit(
				clazz.getMajorVersion() << 16 | clazz.getMinorVersion(),
				access,
				clazz.getName(),
				signature,
				superName,
				interfaces);

		classVisitor.visitSource(clazz.getSourceFileName(), clazz.getSourceDebugExtension());
		if (clazz.getEnclosingMethod() != null) {
			Type resultType = Type.getType(clazz.getEnclosingMethod().getDescriptor().getResultType().getTypeDescriptor());
			Type[] argumentTypes = new Type[clazz.getEnclosingMethod().getDescriptor().getParameterTypes().size()];
			for (int i = 0; i < clazz.getEnclosingMethod().getDescriptor().getParameterTypes().size(); i++) {
				argumentTypes[i] = Type.getType(clazz.getEnclosingMethod().getDescriptor().getParameterTypes().get(i).getTypeDescriptor());
			}
			
			classVisitor.visitOuterClass(
					Type.getType(clazz.getEnclosingMethod().getDeclaringClass().getTypeDescriptor()).getInternalName(),
					clazz.getEnclosingMethod().getName(),
					Type.getMethodDescriptor(resultType, argumentTypes));
		}
		else if (clazz.getOuterClass() != null) {
			classVisitor.visitOuterClass(Type.getType(clazz.getOuterClass().getTypeDescriptor()).getInternalName(), null, null);
		}
		
//		classNode.visitAnnotation(desc, visible);
//		classNode.visitTypeAnnotation(typeRef, typePath, desc, visible);
//		classNode.visitAttribute(attr);
//		classNode.visitInnerClass(name, outerName, innerName, access);
		
		assembleFields(classVisitor, clazz);
		assembleMethods(classVisitor, clazz);
		classVisitor.visitEnd();
	}

	private void assembleMethods(ClassVisitor classVisitor, Clazz clazz) {
		for (Method method : clazz.getMethods()) {
			int methodAccess = 0;
			if (method.isDeprecated())
				methodAccess |= Opcodes.ACC_DEPRECATED;
			if (method.isDeprecated())
				methodAccess |= Opcodes.ACC_DEPRECATED;
			if (method.isBridge())
				methodAccess |= Opcodes.ACC_BRIDGE;
			if (method.isFinal())
				methodAccess |= Opcodes.ACC_FINAL;
			if (method.isNative())
				methodAccess |= Opcodes.ACC_NATIVE;
			if (method.isPrivate())
				methodAccess |= Opcodes.ACC_PRIVATE;
			if (method.isProtected())
				methodAccess |= Opcodes.ACC_PROTECTED;
			if (method.isPublic())
				methodAccess |= Opcodes.ACC_PUBLIC;
			if (method.isStatic())
				methodAccess |= Opcodes.ACC_STATIC;
			if (method.isStrict())
				methodAccess |= Opcodes.ACC_STRICT;
			if (method.isSynchronized())
				methodAccess |= Opcodes.ACC_SYNCHRONIZED;
			if (method.isVarArgs())
				methodAccess |= Opcodes.ACC_VARARGS;
			
			String methodSignature = null;
			if (method.getSignature() != null)
				methodSignature = method.getSignature().getSignature();
			
			Type resultType = Type.getType(method.getDescriptor().getResultType().getTypeDescriptor());
			Type[] argumentTypes = new Type[method.getDescriptor().getParameterTypes().size()];
			for (int i = 0; i < method.getDescriptor().getParameterTypes().size(); i++) {
				 argumentTypes[i] = Type.getType(method.getDescriptor().getParameterTypes().get(i).getTypeDescriptor());
			}
			String methodDescriptor = Type.getMethodDescriptor(resultType, argumentTypes);
			
			String[] exceptionTypes = new String[method.getExceptionsCanBeThrown().size()];
			for (int i = 0; i < method.getExceptionsCanBeThrown().size(); i++) {
				exceptionTypes[i] = Type.getType(method.getExceptionsCanBeThrown().get(i).getTypeDescriptor()).getInternalName();
			}
			
			MethodVisitor methodVisitor = classVisitor.visitMethod(
					methodAccess,
					method.getName(),
					methodDescriptor,
					methodSignature,
					exceptionTypes);
			assembleMethod(methodVisitor, method);
//			annotations
		}
	}

	private void assembleFields(ClassVisitor classVisitor, Clazz clazz) {
		for (Field field : clazz.getFields()) {
			int fieldAccess = 0;
			if (field.isDeprecated())
				fieldAccess |= Opcodes.ACC_DEPRECATED;
			if (field.isEnum())
				fieldAccess |= Opcodes.ACC_ENUM;
			if (field.isFinal())
				fieldAccess |= Opcodes.ACC_FINAL;
			if (field.isPrivate())
				fieldAccess |= Opcodes.ACC_PRIVATE;
			if (field.isProtected())
				fieldAccess |= Opcodes.ACC_PROTECTED;
			if (field.isPublic())
				fieldAccess |= Opcodes.ACC_PUBLIC;
			if (field.isStatic())
				fieldAccess |= Opcodes.ACC_STATIC;
			if (field.isSynthetic())
				fieldAccess |= Opcodes.ACC_SYNTHETIC;
			if (field.isTransient())
				fieldAccess |= Opcodes.ACC_TRANSIENT;
			if (field.isVolatile())
				fieldAccess |= Opcodes.ACC_VOLATILE;
			
			String fieldSignature = null;
			if (field.getSignature() != null)
				fieldSignature = field.getSignature().getSignature();
			
			FieldVisitor fieldVisitor = classVisitor.visitField(fieldAccess, 
					field.getName(), 
					Type.getType(field.getDescriptor().getTypeDescriptor()).getDescriptor(), 
					fieldSignature, 
					asObjectValue(field.getConstantValue()));
//			fieldVisitor.visitAnnotation(desc, visible);
//			fieldVisitor.visitTypeAnnotation(typeRef, typePath, desc, visible);
//			fieldVisitor.visitAttribute(attr);
			fieldVisitor.visitEnd();
		}
	}

	private static final EStructuralFeature LINENUMBER_FEATURE = JbcmmPackage.eINSTANCE.getInstruction().getEStructuralFeature(JbcmmPackage.INSTRUCTION__LINENUMBER);
	private void assembleMethod(MethodVisitor methodVisitor, Method method) {
//		methodVisitor.visitParameter(name, access);
//		methodVisitor.visitAnnotationDefault();
//		methodVisitor.visitAnnotation(desc, visible);
//		methodVisitor.visitParameterAnnotation(parameter, desc, visible);
//		methodVisitor.visitAttribute(attr);
		
		if (!method.isAbstract()) {
			methodVisitor.visitCode();
			
			Map<Instruction, Label> jumpTargets = new HashMap<>();
			Label terminalLabel = new Label();

			for (ExceptionTableEntry exceptionTableEntry : method.getExceptionTable()) {
				Label startLabel = getLabel(jumpTargets, exceptionTableEntry.getStartInstruction());
				Label endLabel = getLabel(jumpTargets, exceptionTableEntry.getEndInstruction());
				Label handlerLabel = getLabel(jumpTargets, exceptionTableEntry.getHandlerInstruction());
				methodVisitor.visitTryCatchBlock(startLabel,
						endLabel,
						handlerLabel,
						exceptionTableEntry.getCatchType() == null ? null : Type.getType(exceptionTableEntry.getCatchType().getTypeDescriptor()).getInternalName());
			}
//			methodVisitor.visitTryCatchAnnotation(typeRef, typePath, desc, visible)

			Instruction instruction = method.getFirstInstruction();
			int previousLineNumer = -1;
			
			while (instruction  != null) {
				
					Label label = getLabel(jumpTargets, instruction);
					methodVisitor.visitLabel(label);
					
					int opcode = getOpcode(instruction.getOpcode());
					
					if (instruction.eIsSet(LINENUMBER_FEATURE) && instruction.getLinenumber() != previousLineNumer) {
						Label lineNumberStartLabel = new Label();
						methodVisitor.visitLabel(lineNumberStartLabel);
						methodVisitor.visitLineNumber(instruction.getLinenumber(), lineNumberStartLabel);
						previousLineNumer = instruction.getLinenumber();
					}

					if (instruction instanceof FieldInstruction) {
						FieldInstruction fieldInstruction = (FieldInstruction) instruction;
						FieldReference fieldReference = fieldInstruction.getFieldReference();
						methodVisitor.visitFieldInsn(opcode, 
								Type.getType(fieldReference.getDeclaringClass().getTypeDescriptor()).getInternalName(),
								fieldReference.getName(),
								Type.getType(fieldReference.getDescriptor().getTypeDescriptor()).getDescriptor());
					}
					else if (instruction instanceof MethodInstruction) {
						MethodInstruction methodInstruction = (MethodInstruction) instruction;
						MethodReference methodReference = methodInstruction.getMethodReference();

						Type resultType = Type.getType(methodReference.getDescriptor().getResultType().getTypeDescriptor());
						Type[] argumentTypes = new Type[methodReference.getDescriptor().getParameterTypes().size()];
						for (int i = 0; i < methodReference.getDescriptor().getParameterTypes().size(); i++) {
							 argumentTypes[i] = Type.getType(methodReference.getDescriptor().getParameterTypes().get(i).getTypeDescriptor());
						}
						String methodDescriptor = Type.getMethodDescriptor(resultType, argumentTypes);
						
						if (getOpcode(methodInstruction.getOpcode()) == Opcodes.INVOKEDYNAMIC) {
							throw new RuntimeException("unsupported instruction " + methodInstruction);
						}
						else {
							methodVisitor.visitMethodInsn(opcode,
									Type.getType(methodReference.getDeclaringClass().getTypeDescriptor()).getInternalName(),
									methodReference.getName(),
									methodDescriptor);
						}
					}
					else if (instruction instanceof TypeInstruction) {
						TypeInstruction typeInstruction = (TypeInstruction) instruction;
						methodVisitor.visitTypeInsn(opcode, Type.getType(typeInstruction.getTypeReference().getTypeDescriptor()).getInternalName());
					}
					else if (instruction instanceof MultianewarrayInstruction) {
						MultianewarrayInstruction multianewarrayInstruction = (MultianewarrayInstruction) instruction;
						methodVisitor.visitMultiANewArrayInsn(Type.getType(multianewarrayInstruction.getTypeReference().getTypeDescriptor()).getDescriptor(), multianewarrayInstruction.getDims());
					}
					else if (instruction instanceof SwitchInstruction) {
						SwitchInstruction switchInstruction = (SwitchInstruction) instruction;
						Label defaultLabel = null;
						int minimum = 0;
						boolean canUseTable = true;
						List<Label> caseLabelsList = new ArrayList<>();
						List<Integer> keysList = new ArrayList<>();
						for (ControlFlowEdge edge : switchInstruction.getOutEdges()) {
							if (edge instanceof SwitchDefaultEdge) {
								defaultLabel = getLabel(jumpTargets, edge.getEnd());
							} else if (edge instanceof SwitchCaseEdge) {
								if (caseLabelsList.isEmpty()) {
									minimum = ((SwitchCaseEdge) edge).getCondition();
								}
								Label caseLabel = getLabel(jumpTargets, edge.getEnd());
								caseLabelsList.add(caseLabel);
								keysList.add(((SwitchCaseEdge) edge).getCondition());
								if (minimum + caseLabelsList.size() - 1 != ((SwitchCaseEdge) edge).getCondition())
									canUseTable = false;
							}
						}
						Label[] caseLabels = new Label[caseLabelsList.size()];
						caseLabels = caseLabelsList.toArray(caseLabels);
						if (canUseTable) {
							methodVisitor.visitTableSwitchInsn(minimum, minimum + caseLabelsList.size() - 1, defaultLabel, caseLabels);
						}
						else {
							int[] keys = new int[keysList.size()];
							for (int i = 0; i < keysList.size(); i++) {
								keys[i] = keysList.get(i);
							}
							methodVisitor.visitLookupSwitchInsn(defaultLabel, keys, caseLabels);
						}
					}
					else if (instruction instanceof LdcIntInstruction) {
						methodVisitor.visitLdcInsn(((LdcIntInstruction) instruction).getConstant());
					}
					else if (instruction instanceof LdcLongInstruction) {
						methodVisitor.visitLdcInsn(((LdcLongInstruction) instruction).getConstant());
					}
					else if (instruction instanceof LdcFloatInstruction) {
						methodVisitor.visitLdcInsn(((LdcFloatInstruction) instruction).getConstant());
					}
					else if (instruction instanceof LdcDoubleInstruction) {
						methodVisitor.visitLdcInsn(((LdcDoubleInstruction) instruction).getConstant());
					}
					else if (instruction instanceof LdcStringInstruction) {
						String stringConstant = ((LdcStringInstruction) instruction).getConstant();
						StringWriter writer = new StringWriter(stringConstant.length());
						try {
							StringEscapeUtils.unescapeJava(writer, stringConstant);
							methodVisitor.visitLdcInsn(writer.toString());
						} catch (IOException e) {
							throw new RuntimeException(e);
						}
					}
					else if (instruction instanceof LdcTypeInstruction) {
						methodVisitor.visitLdcInsn(Type.getType(((LdcTypeInstruction) instruction).getConstant().getTypeDescriptor()));
					}
					else if (instruction instanceof IntInstruction) {
						methodVisitor.visitIntInsn(opcode, ((IntInstruction) instruction).getOperand()); 
					}
					else if (instruction instanceof JumpInstruction) {
						Label target = null;
						for (ControlFlowEdge edge : ((JumpInstruction) instruction).getOutEdges()) {
							if (edge instanceof ConditionalEdge && ((ConditionalEdge) edge).isCondition() == true) {
								target = getLabel(jumpTargets, edge.getEnd());
							}
							else if (edge instanceof UnconditionalEdge && (instruction instanceof GotoInstruction || instruction instanceof JsrInstruction)) {
								target = getLabel(jumpTargets, edge.getEnd());
							}
						}
						methodVisitor.visitJumpInsn(opcode, target);
					}
					else if (instruction instanceof VarInstruction) {
						if (instruction instanceof IincInstruction) {
							IincInstruction iincInstruction = (IincInstruction) instruction;
							methodVisitor.visitIincInsn(iincInstruction.getVarIndex(), iincInstruction.getIncr()); 
						}
						else {
							methodVisitor.visitVarInsn(opcode, ((VarInstruction) instruction).getVarIndex()); 
						}
					}
					else {
						methodVisitor.visitInsn(opcode);
					}
				
				
					instruction = instruction.getNextInCodeOrder();
			}
			methodVisitor.visitLabel(terminalLabel);
			
			for (LocalVariableTableEntry entry : method.getLocalVariableTable()) {
				Instruction endInstruction = entry.getEndInstruction().getNextInCodeOrder();
				Label endLabel;
				if (endInstruction == null)
					endLabel = terminalLabel;
				else
					endLabel = jumpTargets.get(endInstruction);
				methodVisitor.visitLocalVariable(entry.getName(),
						Type.getType(entry.getDescriptor().getTypeDescriptor()).getDescriptor(),
						null, // TODO
						jumpTargets.get(entry.getStartInstruction()),
						endLabel,
						entry.getIndex());
			}
			
			methodVisitor.visitMaxs(0, 0);
		}
		methodVisitor.visitEnd();
		
	}

	private Label getLabel(Map<Instruction, Label> jumpTargets, Instruction instruction) {
		Label label = jumpTargets.get(instruction);
		if (label == null) {
			label = new Label();
			jumpTargets.put(instruction, label);
		}
		return label;
	}

	
	
	private int getOpcode(String mnemonic) {
		return MNEMONIC_TO_OPCODE.get(mnemonic);
	}

//	private void createSequence(Instruction firstInstruction, List<Instruction> instructionSequence,
//			Map<Instruction, Label> jumpTargets) {
//
//		Deque<Instruction> nextInstructions = new ArrayDeque<>();
//		
//		nextInstructions.push(firstInstruction);
//
//		while (!nextInstructions.isEmpty()) {
//			Instruction currentInstruction = nextInstructions.pop();
//			if (instructionSequence.contains(currentInstruction))
//				continue;
//			
//			instructionSequence.add(currentInstruction);
//			
//			List<ControlFlowEdge> outEdges = currentInstruction.getOutEdges();
//			if (outEdges.isEmpty()) {
//				continue;
//			}
//			else {
//				ControlFlowEdge unconditionalEdge = null;
//				List<ControlFlowEdge> exceptionalEdges = new ArrayList<>();
//				List<ControlFlowEdge> conditionalEdges = new ArrayList<>();
//				List<ControlFlowEdge> switchCaseEdges = new ArrayList<>();
//				ControlFlowEdge switchDefaultEdge = null;
//				for (ControlFlowEdge edge : currentInstruction.getOutEdges()) {
//					if (edge instanceof UnconditionalEdge)
//						unconditionalEdge = edge;
//					else if (edge instanceof ExceptionalEdge)
//						exceptionalEdges.add(edge);
//					else if (edge instanceof ConditionalEdge)
//						conditionalEdges.add(edge);
//					else if (edge instanceof SwitchCaseEdge)
//						switchCaseEdges.add(edge);
//					else if (edge instanceof SwitchDefaultEdge)
//						switchDefaultEdge = edge;
//					else
//						throw new RuntimeException("Unsupported control flow edge " + edge);
//				}
//
//				for (ControlFlowEdge controlFlowEdge : exceptionalEdges) {
//					nextInstructions.push(controlFlowEdge.getEnd());
//					jumpTargets.put(controlFlowEdge.getEnd(), null);
//				}
//				if (switchDefaultEdge != null) {
//					nextInstructions.push(switchDefaultEdge.getEnd());
//					jumpTargets.put(switchDefaultEdge.getEnd(), null);
//				}
//				Collections.reverse(switchCaseEdges);
//				for (ControlFlowEdge controlFlowEdge : switchCaseEdges) {
//					nextInstructions.push(controlFlowEdge.getEnd());
//					jumpTargets.put(controlFlowEdge.getEnd(), null);
//				}
//				for (ControlFlowEdge controlFlowEdge : conditionalEdges) {
//					nextInstructions.push(controlFlowEdge.getEnd());
//					jumpTargets.put(controlFlowEdge.getEnd(), null);
//				}
//				if (currentInstruction instanceof GotoInstruction || currentInstruction instanceof JsrInstruction) {
//					nextInstructions.addFirst(unconditionalEdge.getEnd());
//					jumpTargets.put(unconditionalEdge.getEnd(), null);
//				} else {
//					if (unconditionalEdge != null)
//						nextInstructions.push(unconditionalEdge.getEnd());
//				}
//			}
//		}
//	}

	private Object asObjectValue(ElementaryValue value) {
		if (value == null)
			return null;
		else {
			if (value instanceof BooleanValue)
				return ((BooleanValue) value).isValue();
			else if (value instanceof CharValue) {
				String escaped = ((CharValue) value).getValue();
				if (escaped.equals("\\\'"))
					return '\'';
				else
					return StringEscapeUtils.unescapeJava(escaped).charAt(0);
			}
			else if (value instanceof ByteValue)
				return ((ByteValue) value).getValue();
			else if (value instanceof ShortValue)
				return ((ShortValue) value).getValue();
			else if (value instanceof IntValue)
				return ((IntValue) value).getValue();
			else if (value instanceof LongValue)
				return ((LongValue) value).getValue();
			else if (value instanceof FloatValue)
				return ((FloatValue) value).getValue();
			else if (value instanceof DoubleValue)
				return ((DoubleValue) value).getValue();
			else if (value instanceof StringValue) {
				String escaped = ((StringValue) value).getValue();
				return StringEscapeUtils.unescapeJava(escaped);
			}
			else
				throw new RuntimeException("Unsupported elementary value " + value);
		}
	}

	public String reportTime(long totalMillis, long readModelMillis) {
		StringBuilder report = new StringBuilder("total[ms];reading model[ms];assembling classes[ms];verifying classes[ms];writing class files [ms]\n");
		report.append(totalMillis).append(';');
		report.append(readModelMillis).append(';');
		report.append(classAssemblyMillis).append(';');
		report.append(verificationMillis).append(';');
		report.append(ioMillis).append('\n');
		return report.toString();
	}

}
