package nl.utwente.jbcmm.importer;

public interface IExtractionProgressMonitor {
	
	public void beginTask(int amount, String info);
	public void beginTask(String info);
	public void beginTask(int amount);
	public void beginTask();
	public void worked(int amount);
	public void done();

}
