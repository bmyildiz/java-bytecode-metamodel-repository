/**
 */
package nl.utwente.jbcmm.provider;


import java.util.Collection;
import java.util.List;

import nl.utwente.jbcmm.Clazz;
import nl.utwente.jbcmm.JbcmmFactory;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link nl.utwente.jbcmm.Clazz} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ClazzItemProvider 
	extends IdentifiableItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClazzItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addSubclassesPropertyDescriptor(object);
			addNamePropertyDescriptor(object);
			addMinorVersionPropertyDescriptor(object);
			addMajorVersionPropertyDescriptor(object);
			addPublicPropertyDescriptor(object);
			addFinalPropertyDescriptor(object);
			addSuperPropertyDescriptor(object);
			addInterfacePropertyDescriptor(object);
			addAbstractPropertyDescriptor(object);
			addSyntheticPropertyDescriptor(object);
			addAnnotationPropertyDescriptor(object);
			addEnumPropertyDescriptor(object);
			addSourceFileNamePropertyDescriptor(object);
			addDeprecatedPropertyDescriptor(object);
			addSourceDebugExtensionPropertyDescriptor(object);
			addSuperClassPropertyDescriptor(object);
			addInterfacesPropertyDescriptor(object);
			addPrivatePropertyDescriptor(object);
			addProtectedPropertyDescriptor(object);
			addOuterClassPropertyDescriptor(object);
			addEnclosingMethodPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Subclasses feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSubclassesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Clazz_subclasses_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Clazz_subclasses_feature", "_UI_Clazz_type"),
				 JbcmmPackage.eINSTANCE.getClazz_Subclasses(),
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Clazz_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Clazz_name_feature", "_UI_Clazz_type"),
				 JbcmmPackage.eINSTANCE.getClazz_Name(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Minor Version feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMinorVersionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Clazz_minorVersion_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Clazz_minorVersion_feature", "_UI_Clazz_type"),
				 JbcmmPackage.eINSTANCE.getClazz_MinorVersion(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Major Version feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMajorVersionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Clazz_majorVersion_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Clazz_majorVersion_feature", "_UI_Clazz_type"),
				 JbcmmPackage.eINSTANCE.getClazz_MajorVersion(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Public feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPublicPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Clazz_public_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Clazz_public_feature", "_UI_Clazz_type"),
				 JbcmmPackage.eINSTANCE.getClazz_Public(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Final feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFinalPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Clazz_final_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Clazz_final_feature", "_UI_Clazz_type"),
				 JbcmmPackage.eINSTANCE.getClazz_Final(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Super feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSuperPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Clazz_super_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Clazz_super_feature", "_UI_Clazz_type"),
				 JbcmmPackage.eINSTANCE.getClazz_Super(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Interface feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInterfacePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Clazz_interface_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Clazz_interface_feature", "_UI_Clazz_type"),
				 JbcmmPackage.eINSTANCE.getClazz_Interface(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Abstract feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAbstractPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Clazz_abstract_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Clazz_abstract_feature", "_UI_Clazz_type"),
				 JbcmmPackage.eINSTANCE.getClazz_Abstract(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Synthetic feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSyntheticPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Clazz_synthetic_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Clazz_synthetic_feature", "_UI_Clazz_type"),
				 JbcmmPackage.eINSTANCE.getClazz_Synthetic(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Annotation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAnnotationPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Clazz_annotation_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Clazz_annotation_feature", "_UI_Clazz_type"),
				 JbcmmPackage.eINSTANCE.getClazz_Annotation(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Enum feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEnumPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Clazz_enum_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Clazz_enum_feature", "_UI_Clazz_type"),
				 JbcmmPackage.eINSTANCE.getClazz_Enum(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Source File Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSourceFileNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Clazz_sourceFileName_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Clazz_sourceFileName_feature", "_UI_Clazz_type"),
				 JbcmmPackage.eINSTANCE.getClazz_SourceFileName(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Deprecated feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDeprecatedPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Clazz_deprecated_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Clazz_deprecated_feature", "_UI_Clazz_type"),
				 JbcmmPackage.eINSTANCE.getClazz_Deprecated(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Source Debug Extension feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSourceDebugExtensionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Clazz_sourceDebugExtension_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Clazz_sourceDebugExtension_feature", "_UI_Clazz_type"),
				 JbcmmPackage.eINSTANCE.getClazz_SourceDebugExtension(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Super Class feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSuperClassPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Clazz_superClass_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Clazz_superClass_feature", "_UI_Clazz_type"),
				 JbcmmPackage.eINSTANCE.getClazz_SuperClass(),
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Interfaces feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInterfacesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Clazz_interfaces_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Clazz_interfaces_feature", "_UI_Clazz_type"),
				 JbcmmPackage.eINSTANCE.getClazz_Interfaces(),
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Private feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPrivatePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Clazz_private_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Clazz_private_feature", "_UI_Clazz_type"),
				 JbcmmPackage.eINSTANCE.getClazz_Private(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Protected feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addProtectedPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Clazz_protected_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Clazz_protected_feature", "_UI_Clazz_type"),
				 JbcmmPackage.eINSTANCE.getClazz_Protected(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Outer Class feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOuterClassPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Clazz_outerClass_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Clazz_outerClass_feature", "_UI_Clazz_type"),
				 JbcmmPackage.eINSTANCE.getClazz_OuterClass(),
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Enclosing Method feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEnclosingMethodPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Clazz_enclosingMethod_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Clazz_enclosingMethod_feature", "_UI_Clazz_type"),
				 JbcmmPackage.eINSTANCE.getClazz_EnclosingMethod(),
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(JbcmmPackage.eINSTANCE.getClazz_Methods());
			childrenFeatures.add(JbcmmPackage.eINSTANCE.getClazz_RuntimeInvisibleAnnotations());
			childrenFeatures.add(JbcmmPackage.eINSTANCE.getClazz_RuntimeVisibleAnnotations());
			childrenFeatures.add(JbcmmPackage.eINSTANCE.getClazz_Fields());
			childrenFeatures.add(JbcmmPackage.eINSTANCE.getClazz_Signature());
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Clazz.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Clazz"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Clazz)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_Clazz_type") :
			getString("_UI_Clazz_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Clazz.class)) {
			case JbcmmPackage.CLAZZ__NAME:
			case JbcmmPackage.CLAZZ__MINOR_VERSION:
			case JbcmmPackage.CLAZZ__MAJOR_VERSION:
			case JbcmmPackage.CLAZZ__PUBLIC:
			case JbcmmPackage.CLAZZ__FINAL:
			case JbcmmPackage.CLAZZ__SUPER:
			case JbcmmPackage.CLAZZ__INTERFACE:
			case JbcmmPackage.CLAZZ__ABSTRACT:
			case JbcmmPackage.CLAZZ__SYNTHETIC:
			case JbcmmPackage.CLAZZ__ANNOTATION:
			case JbcmmPackage.CLAZZ__ENUM:
			case JbcmmPackage.CLAZZ__SOURCE_FILE_NAME:
			case JbcmmPackage.CLAZZ__DEPRECATED:
			case JbcmmPackage.CLAZZ__SOURCE_DEBUG_EXTENSION:
			case JbcmmPackage.CLAZZ__PRIVATE:
			case JbcmmPackage.CLAZZ__PROTECTED:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case JbcmmPackage.CLAZZ__METHODS:
			case JbcmmPackage.CLAZZ__RUNTIME_INVISIBLE_ANNOTATIONS:
			case JbcmmPackage.CLAZZ__RUNTIME_VISIBLE_ANNOTATIONS:
			case JbcmmPackage.CLAZZ__FIELDS:
			case JbcmmPackage.CLAZZ__SIGNATURE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getClazz_Methods(),
				 JbcmmFactory.eINSTANCE.createMethod()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getClazz_RuntimeInvisibleAnnotations(),
				 JbcmmFactory.eINSTANCE.createAnnotation()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getClazz_RuntimeVisibleAnnotations(),
				 JbcmmFactory.eINSTANCE.createAnnotation()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getClazz_Fields(),
				 JbcmmFactory.eINSTANCE.createField()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getClazz_Signature(),
				 JbcmmFactory.eINSTANCE.createClassSignature()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == JbcmmPackage.eINSTANCE.getClazz_RuntimeInvisibleAnnotations() ||
			childFeature == JbcmmPackage.eINSTANCE.getClazz_RuntimeVisibleAnnotations();

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
