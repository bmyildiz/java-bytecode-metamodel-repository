/**
 */
package nl.utwente.jbcmm.provider;


import java.util.Collection;
import java.util.List;

import nl.utwente.jbcmm.ElementValue;
import nl.utwente.jbcmm.JbcmmFactory;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link nl.utwente.jbcmm.ElementValue} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ElementValueItemProvider 
	extends IdentifiableItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementValueItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(JbcmmPackage.eINSTANCE.getElementValue_ConstantValue());
			childrenFeatures.add(JbcmmPackage.eINSTANCE.getElementValue_EnumValue());
			childrenFeatures.add(JbcmmPackage.eINSTANCE.getElementValue_AnnotationValue());
			childrenFeatures.add(JbcmmPackage.eINSTANCE.getElementValue_ArrayValue());
			childrenFeatures.add(JbcmmPackage.eINSTANCE.getElementValue_ClassValue());
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ElementValue.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ElementValue"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ElementValue)object).getUuid();
		return label == null || label.length() == 0 ?
			getString("_UI_ElementValue_type") :
			getString("_UI_ElementValue_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ElementValue.class)) {
			case JbcmmPackage.ELEMENT_VALUE__CONSTANT_VALUE:
			case JbcmmPackage.ELEMENT_VALUE__ENUM_VALUE:
			case JbcmmPackage.ELEMENT_VALUE__ANNOTATION_VALUE:
			case JbcmmPackage.ELEMENT_VALUE__ARRAY_VALUE:
			case JbcmmPackage.ELEMENT_VALUE__CLASS_VALUE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getElementValue_ConstantValue(),
				 JbcmmFactory.eINSTANCE.createElementaryValue()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getElementValue_ConstantValue(),
				 JbcmmFactory.eINSTANCE.createBooleanValue()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getElementValue_ConstantValue(),
				 JbcmmFactory.eINSTANCE.createCharValue()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getElementValue_ConstantValue(),
				 JbcmmFactory.eINSTANCE.createByteValue()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getElementValue_ConstantValue(),
				 JbcmmFactory.eINSTANCE.createShortValue()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getElementValue_ConstantValue(),
				 JbcmmFactory.eINSTANCE.createIntValue()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getElementValue_ConstantValue(),
				 JbcmmFactory.eINSTANCE.createLongValue()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getElementValue_ConstantValue(),
				 JbcmmFactory.eINSTANCE.createFloatValue()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getElementValue_ConstantValue(),
				 JbcmmFactory.eINSTANCE.createDoubleValue()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getElementValue_ConstantValue(),
				 JbcmmFactory.eINSTANCE.createStringValue()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getElementValue_EnumValue(),
				 JbcmmFactory.eINSTANCE.createEnumValue()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getElementValue_AnnotationValue(),
				 JbcmmFactory.eINSTANCE.createAnnotation()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getElementValue_ArrayValue(),
				 JbcmmFactory.eINSTANCE.createElementValue()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getElementValue_ClassValue(),
				 JbcmmFactory.eINSTANCE.createTypeReference()));
	}

}
