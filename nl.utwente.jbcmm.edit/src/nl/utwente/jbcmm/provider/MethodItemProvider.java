/**
 */
package nl.utwente.jbcmm.provider;


import java.util.Collection;
import java.util.List;

import nl.utwente.jbcmm.JbcmmFactory;
import nl.utwente.jbcmm.JbcmmPackage;
import nl.utwente.jbcmm.Method;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link nl.utwente.jbcmm.Method} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MethodItemProvider 
	extends IdentifiableItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MethodItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addNamePropertyDescriptor(object);
			addFirstInstructionPropertyDescriptor(object);
			addPublicPropertyDescriptor(object);
			addPrivatePropertyDescriptor(object);
			addProtectedPropertyDescriptor(object);
			addStaticPropertyDescriptor(object);
			addFinalPropertyDescriptor(object);
			addSyntheticPropertyDescriptor(object);
			addSynchronizedPropertyDescriptor(object);
			addBridgePropertyDescriptor(object);
			addVarArgsPropertyDescriptor(object);
			addNativePropertyDescriptor(object);
			addAbstractPropertyDescriptor(object);
			addStrictPropertyDescriptor(object);
			addDeprecatedPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Method_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Method_name_feature", "_UI_Method_type"),
				 JbcmmPackage.eINSTANCE.getMethod_Name(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the First Instruction feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFirstInstructionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Method_firstInstruction_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Method_firstInstruction_feature", "_UI_Method_type"),
				 JbcmmPackage.eINSTANCE.getMethod_FirstInstruction(),
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Public feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPublicPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Method_public_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Method_public_feature", "_UI_Method_type"),
				 JbcmmPackage.eINSTANCE.getMethod_Public(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Private feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPrivatePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Method_private_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Method_private_feature", "_UI_Method_type"),
				 JbcmmPackage.eINSTANCE.getMethod_Private(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Protected feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addProtectedPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Method_protected_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Method_protected_feature", "_UI_Method_type"),
				 JbcmmPackage.eINSTANCE.getMethod_Protected(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Static feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStaticPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Method_static_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Method_static_feature", "_UI_Method_type"),
				 JbcmmPackage.eINSTANCE.getMethod_Static(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Final feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFinalPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Method_final_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Method_final_feature", "_UI_Method_type"),
				 JbcmmPackage.eINSTANCE.getMethod_Final(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Synthetic feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSyntheticPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Method_synthetic_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Method_synthetic_feature", "_UI_Method_type"),
				 JbcmmPackage.eINSTANCE.getMethod_Synthetic(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Synchronized feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSynchronizedPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Method_synchronized_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Method_synchronized_feature", "_UI_Method_type"),
				 JbcmmPackage.eINSTANCE.getMethod_Synchronized(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Bridge feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBridgePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Method_bridge_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Method_bridge_feature", "_UI_Method_type"),
				 JbcmmPackage.eINSTANCE.getMethod_Bridge(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Var Args feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVarArgsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Method_varArgs_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Method_varArgs_feature", "_UI_Method_type"),
				 JbcmmPackage.eINSTANCE.getMethod_VarArgs(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Native feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNativePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Method_native_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Method_native_feature", "_UI_Method_type"),
				 JbcmmPackage.eINSTANCE.getMethod_Native(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Abstract feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAbstractPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Method_abstract_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Method_abstract_feature", "_UI_Method_type"),
				 JbcmmPackage.eINSTANCE.getMethod_Abstract(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Strict feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStrictPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Method_strict_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Method_strict_feature", "_UI_Method_type"),
				 JbcmmPackage.eINSTANCE.getMethod_Strict(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Deprecated feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDeprecatedPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Method_deprecated_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Method_deprecated_feature", "_UI_Method_type"),
				 JbcmmPackage.eINSTANCE.getMethod_Deprecated(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(JbcmmPackage.eINSTANCE.getMethod_Instructions());
			childrenFeatures.add(JbcmmPackage.eINSTANCE.getMethod_RuntimeInvisibleAnnotations());
			childrenFeatures.add(JbcmmPackage.eINSTANCE.getMethod_RuntimeVisibleAnnotations());
			childrenFeatures.add(JbcmmPackage.eINSTANCE.getMethod_AnnotationDefaultValue());
			childrenFeatures.add(JbcmmPackage.eINSTANCE.getMethod_Descriptor());
			childrenFeatures.add(JbcmmPackage.eINSTANCE.getMethod_Signature());
			childrenFeatures.add(JbcmmPackage.eINSTANCE.getMethod_ExceptionsCanBeThrown());
			childrenFeatures.add(JbcmmPackage.eINSTANCE.getMethod_ExceptionTable());
			childrenFeatures.add(JbcmmPackage.eINSTANCE.getMethod_LocalVariableTable());
			childrenFeatures.add(JbcmmPackage.eINSTANCE.getMethod_RuntimeInvisibleParameterAnnotations());
			childrenFeatures.add(JbcmmPackage.eINSTANCE.getMethod_RuntimeVisibleParameterAnnotations());
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Method.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Method"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Method)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_Method_type") :
			getString("_UI_Method_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Method.class)) {
			case JbcmmPackage.METHOD__NAME:
			case JbcmmPackage.METHOD__PUBLIC:
			case JbcmmPackage.METHOD__PRIVATE:
			case JbcmmPackage.METHOD__PROTECTED:
			case JbcmmPackage.METHOD__STATIC:
			case JbcmmPackage.METHOD__FINAL:
			case JbcmmPackage.METHOD__SYNTHETIC:
			case JbcmmPackage.METHOD__SYNCHRONIZED:
			case JbcmmPackage.METHOD__BRIDGE:
			case JbcmmPackage.METHOD__VAR_ARGS:
			case JbcmmPackage.METHOD__NATIVE:
			case JbcmmPackage.METHOD__ABSTRACT:
			case JbcmmPackage.METHOD__STRICT:
			case JbcmmPackage.METHOD__DEPRECATED:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case JbcmmPackage.METHOD__INSTRUCTIONS:
			case JbcmmPackage.METHOD__RUNTIME_INVISIBLE_ANNOTATIONS:
			case JbcmmPackage.METHOD__RUNTIME_VISIBLE_ANNOTATIONS:
			case JbcmmPackage.METHOD__ANNOTATION_DEFAULT_VALUE:
			case JbcmmPackage.METHOD__DESCRIPTOR:
			case JbcmmPackage.METHOD__SIGNATURE:
			case JbcmmPackage.METHOD__EXCEPTIONS_CAN_BE_THROWN:
			case JbcmmPackage.METHOD__EXCEPTION_TABLE:
			case JbcmmPackage.METHOD__LOCAL_VARIABLE_TABLE:
			case JbcmmPackage.METHOD__RUNTIME_INVISIBLE_PARAMETER_ANNOTATIONS:
			case JbcmmPackage.METHOD__RUNTIME_VISIBLE_PARAMETER_ANNOTATIONS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIincInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createMultianewarrayInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createSwitchInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createLdcIntInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createLdcLongInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createLdcFloatInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createLdcDoubleInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createLdcStringInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createLdcTypeInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createGetstaticInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createPutstaticInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createGetfieldInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createPutfieldInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createNopInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createAconst_nullInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIconst_m1Instruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIconst_0Instruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIconst_1Instruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIconst_2Instruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIconst_3Instruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIconst_4Instruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIconst_5Instruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createLconst_0Instruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createLconst_1Instruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createFconst_0Instruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createFconst_1Instruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createFconst_2Instruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createDconst_0Instruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createDconst_1Instruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIaloadInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createLaloadInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createFaloadInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createDaloadInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createAaloadInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createBaloadInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createCaloadInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createSaloadInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIastoreInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createLastoreInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createFastoreInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createDastoreInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createAastoreInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createBastoreInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createCastoreInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createSastoreInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createPopInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createPop2Instruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createDupInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createDup_x1Instruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createDup_x2Instruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createDup2Instruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createDup2_x1Instruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createDup2_x2Instruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createSwapInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIaddInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createLaddInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createFaddInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createDaddInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIsubInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createLsubInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createFsubInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createDsubInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createImulInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createLmulInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createFmulInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createDmulInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIdivInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createLdivInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createFdivInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createDdivInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIremInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createLremInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createFremInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createDremInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createInegInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createLnegInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createFnegInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createDnegInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIshlInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createLshlInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIshrInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createLshrInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIushrInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createLushrInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIandInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createLandInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIorInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createLorInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIxorInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createLxorInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createI2lInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createI2fInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createI2dInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createL2iInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createL2fInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createL2dInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createF2iInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createF2lInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createF2dInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createD2iInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createD2lInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createD2fInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createI2bInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createI2cInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createI2sInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createLcmpInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createFcmplInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createFcmpgInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createDcmplInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createDcmpgInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIreturnInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createLreturnInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createFreturnInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createDreturnInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createAreturnInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createReturnInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createArraylengthInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createAthrowInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createMonitorenterInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createMonitorexitInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createBipushInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createSipushInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createNewarrayInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIfeqInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIfneInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIfltInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIfgeInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIfgtInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIfleInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIf_icmpeqInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIf_icmpneInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIf_icmpltInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIf_icmpgeInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIf_icmpgtInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIf_icmpleInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIf_acmpeqInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIf_acmpneInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createGotoInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createJsrInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIfnullInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIfnonnullInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createInvokevirtualInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createInvokespecialInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createInvokestaticInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createInvokeinterfaceInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createNewInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createAnewarrayInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createCheckcastInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createInstanceofInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIloadInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createLloadInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createFloadInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createDloadInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createAloadInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createIstoreInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createLstoreInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createFstoreInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createDstoreInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createAstoreInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Instructions(),
				 JbcmmFactory.eINSTANCE.createRetInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_RuntimeInvisibleAnnotations(),
				 JbcmmFactory.eINSTANCE.createAnnotation()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_RuntimeVisibleAnnotations(),
				 JbcmmFactory.eINSTANCE.createAnnotation()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_AnnotationDefaultValue(),
				 JbcmmFactory.eINSTANCE.createElementValue()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Descriptor(),
				 JbcmmFactory.eINSTANCE.createMethodDescriptor()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_Signature(),
				 JbcmmFactory.eINSTANCE.createMethodSignature()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_ExceptionsCanBeThrown(),
				 JbcmmFactory.eINSTANCE.createTypeReference()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_ExceptionTable(),
				 JbcmmFactory.eINSTANCE.createExceptionTableEntry()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_LocalVariableTable(),
				 JbcmmFactory.eINSTANCE.createLocalVariableTableEntry()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_RuntimeInvisibleParameterAnnotations(),
				 JbcmmFactory.eINSTANCE.createAnnotation()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getMethod_RuntimeVisibleParameterAnnotations(),
				 JbcmmFactory.eINSTANCE.createAnnotation()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == JbcmmPackage.eINSTANCE.getMethod_RuntimeInvisibleAnnotations() ||
			childFeature == JbcmmPackage.eINSTANCE.getMethod_RuntimeVisibleAnnotations() ||
			childFeature == JbcmmPackage.eINSTANCE.getMethod_RuntimeInvisibleParameterAnnotations() ||
			childFeature == JbcmmPackage.eINSTANCE.getMethod_RuntimeVisibleParameterAnnotations();

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
