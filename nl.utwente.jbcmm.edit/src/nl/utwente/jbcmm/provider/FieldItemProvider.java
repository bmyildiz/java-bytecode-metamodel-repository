/**
 */
package nl.utwente.jbcmm.provider;


import java.util.Collection;
import java.util.List;

import nl.utwente.jbcmm.Field;
import nl.utwente.jbcmm.JbcmmFactory;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link nl.utwente.jbcmm.Field} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class FieldItemProvider 
	extends IdentifiableItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FieldItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addPublicPropertyDescriptor(object);
			addPrivatePropertyDescriptor(object);
			addProtectedPropertyDescriptor(object);
			addStaticPropertyDescriptor(object);
			addFinalPropertyDescriptor(object);
			addVolatilePropertyDescriptor(object);
			addTransientPropertyDescriptor(object);
			addSyntheticPropertyDescriptor(object);
			addEnumPropertyDescriptor(object);
			addNamePropertyDescriptor(object);
			addDeprecatedPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Public feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPublicPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Field_public_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Field_public_feature", "_UI_Field_type"),
				 JbcmmPackage.eINSTANCE.getField_Public(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Private feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPrivatePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Field_private_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Field_private_feature", "_UI_Field_type"),
				 JbcmmPackage.eINSTANCE.getField_Private(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Protected feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addProtectedPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Field_protected_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Field_protected_feature", "_UI_Field_type"),
				 JbcmmPackage.eINSTANCE.getField_Protected(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Static feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStaticPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Field_static_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Field_static_feature", "_UI_Field_type"),
				 JbcmmPackage.eINSTANCE.getField_Static(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Final feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFinalPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Field_final_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Field_final_feature", "_UI_Field_type"),
				 JbcmmPackage.eINSTANCE.getField_Final(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Volatile feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVolatilePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Field_volatile_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Field_volatile_feature", "_UI_Field_type"),
				 JbcmmPackage.eINSTANCE.getField_Volatile(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Transient feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTransientPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Field_transient_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Field_transient_feature", "_UI_Field_type"),
				 JbcmmPackage.eINSTANCE.getField_Transient(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Synthetic feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSyntheticPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Field_synthetic_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Field_synthetic_feature", "_UI_Field_type"),
				 JbcmmPackage.eINSTANCE.getField_Synthetic(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Enum feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEnumPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Field_enum_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Field_enum_feature", "_UI_Field_type"),
				 JbcmmPackage.eINSTANCE.getField_Enum(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Field_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Field_name_feature", "_UI_Field_type"),
				 JbcmmPackage.eINSTANCE.getField_Name(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Deprecated feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDeprecatedPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Field_deprecated_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Field_deprecated_feature", "_UI_Field_type"),
				 JbcmmPackage.eINSTANCE.getField_Deprecated(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(JbcmmPackage.eINSTANCE.getField_ConstantValue());
			childrenFeatures.add(JbcmmPackage.eINSTANCE.getField_RuntimeInvisibleAnnotations());
			childrenFeatures.add(JbcmmPackage.eINSTANCE.getField_RuntimeVisibleAnnotations());
			childrenFeatures.add(JbcmmPackage.eINSTANCE.getField_Descriptor());
			childrenFeatures.add(JbcmmPackage.eINSTANCE.getField_Signature());
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Field.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Field"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Field)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_Field_type") :
			getString("_UI_Field_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Field.class)) {
			case JbcmmPackage.FIELD__PUBLIC:
			case JbcmmPackage.FIELD__PRIVATE:
			case JbcmmPackage.FIELD__PROTECTED:
			case JbcmmPackage.FIELD__STATIC:
			case JbcmmPackage.FIELD__FINAL:
			case JbcmmPackage.FIELD__VOLATILE:
			case JbcmmPackage.FIELD__TRANSIENT:
			case JbcmmPackage.FIELD__SYNTHETIC:
			case JbcmmPackage.FIELD__ENUM:
			case JbcmmPackage.FIELD__NAME:
			case JbcmmPackage.FIELD__DEPRECATED:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case JbcmmPackage.FIELD__CONSTANT_VALUE:
			case JbcmmPackage.FIELD__RUNTIME_INVISIBLE_ANNOTATIONS:
			case JbcmmPackage.FIELD__RUNTIME_VISIBLE_ANNOTATIONS:
			case JbcmmPackage.FIELD__DESCRIPTOR:
			case JbcmmPackage.FIELD__SIGNATURE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getField_ConstantValue(),
				 JbcmmFactory.eINSTANCE.createElementaryValue()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getField_ConstantValue(),
				 JbcmmFactory.eINSTANCE.createBooleanValue()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getField_ConstantValue(),
				 JbcmmFactory.eINSTANCE.createCharValue()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getField_ConstantValue(),
				 JbcmmFactory.eINSTANCE.createByteValue()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getField_ConstantValue(),
				 JbcmmFactory.eINSTANCE.createShortValue()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getField_ConstantValue(),
				 JbcmmFactory.eINSTANCE.createIntValue()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getField_ConstantValue(),
				 JbcmmFactory.eINSTANCE.createLongValue()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getField_ConstantValue(),
				 JbcmmFactory.eINSTANCE.createFloatValue()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getField_ConstantValue(),
				 JbcmmFactory.eINSTANCE.createDoubleValue()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getField_ConstantValue(),
				 JbcmmFactory.eINSTANCE.createStringValue()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getField_RuntimeInvisibleAnnotations(),
				 JbcmmFactory.eINSTANCE.createAnnotation()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getField_RuntimeVisibleAnnotations(),
				 JbcmmFactory.eINSTANCE.createAnnotation()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getField_Descriptor(),
				 JbcmmFactory.eINSTANCE.createTypeReference()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getField_Signature(),
				 JbcmmFactory.eINSTANCE.createFieldTypeSignature()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == JbcmmPackage.eINSTANCE.getField_RuntimeInvisibleAnnotations() ||
			childFeature == JbcmmPackage.eINSTANCE.getField_RuntimeVisibleAnnotations();

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
