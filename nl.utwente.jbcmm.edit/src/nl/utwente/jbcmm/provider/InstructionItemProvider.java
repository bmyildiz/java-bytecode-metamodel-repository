/**
 */
package nl.utwente.jbcmm.provider;


import java.util.Collection;
import java.util.List;

import nl.utwente.jbcmm.Instruction;
import nl.utwente.jbcmm.JbcmmFactory;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link nl.utwente.jbcmm.Instruction} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class InstructionItemProvider 
	extends IdentifiableItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InstructionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addNextInCodeOrderPropertyDescriptor(object);
			addPreviousInCodeOrderPropertyDescriptor(object);
			addLinenumberPropertyDescriptor(object);
			addIndexPropertyDescriptor(object);
			addOpcodePropertyDescriptor(object);
			addHumanReadablePropertyDescriptor(object);
			addInEdgesPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Next In Code Order feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNextInCodeOrderPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Instruction_nextInCodeOrder_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Instruction_nextInCodeOrder_feature", "_UI_Instruction_type"),
				 JbcmmPackage.eINSTANCE.getInstruction_NextInCodeOrder(),
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Previous In Code Order feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPreviousInCodeOrderPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Instruction_previousInCodeOrder_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Instruction_previousInCodeOrder_feature", "_UI_Instruction_type"),
				 JbcmmPackage.eINSTANCE.getInstruction_PreviousInCodeOrder(),
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Linenumber feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLinenumberPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Instruction_linenumber_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Instruction_linenumber_feature", "_UI_Instruction_type"),
				 JbcmmPackage.eINSTANCE.getInstruction_Linenumber(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Index feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIndexPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Instruction_index_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Instruction_index_feature", "_UI_Instruction_type"),
				 JbcmmPackage.eINSTANCE.getInstruction_Index(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Opcode feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOpcodePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Instruction_opcode_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Instruction_opcode_feature", "_UI_Instruction_type"),
				 JbcmmPackage.eINSTANCE.getInstruction_Opcode(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Human Readable feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addHumanReadablePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Instruction_humanReadable_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Instruction_humanReadable_feature", "_UI_Instruction_type"),
				 JbcmmPackage.eINSTANCE.getInstruction_HumanReadable(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the In Edges feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInEdgesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Instruction_inEdges_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Instruction_inEdges_feature", "_UI_Instruction_type"),
				 JbcmmPackage.eINSTANCE.getInstruction_InEdges(),
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(JbcmmPackage.eINSTANCE.getInstruction_OutEdges());
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Instruction)object).getUuid();
		return label == null || label.length() == 0 ?
			getString("_UI_Instruction_type") :
			getString("_UI_Instruction_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Instruction.class)) {
			case JbcmmPackage.INSTRUCTION__LINENUMBER:
			case JbcmmPackage.INSTRUCTION__INDEX:
			case JbcmmPackage.INSTRUCTION__OPCODE:
			case JbcmmPackage.INSTRUCTION__HUMAN_READABLE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case JbcmmPackage.INSTRUCTION__OUT_EDGES:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getInstruction_OutEdges(),
				 JbcmmFactory.eINSTANCE.createUnconditionalEdge()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getInstruction_OutEdges(),
				 JbcmmFactory.eINSTANCE.createConditionalEdge()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getInstruction_OutEdges(),
				 JbcmmFactory.eINSTANCE.createSwitchCaseEdge()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getInstruction_OutEdges(),
				 JbcmmFactory.eINSTANCE.createSwitchDefaultEdge()));

		newChildDescriptors.add
			(createChildParameter
				(JbcmmPackage.eINSTANCE.getInstruction_OutEdges(),
				 JbcmmFactory.eINSTANCE.createExceptionalEdge()));
	}

}
