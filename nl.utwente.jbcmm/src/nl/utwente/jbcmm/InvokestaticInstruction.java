/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Invokestatic Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getInvokestaticInstruction()
 * @model
 * @generated
 */
public interface InvokestaticInstruction extends MethodInstruction {
} // InvokestaticInstruction
