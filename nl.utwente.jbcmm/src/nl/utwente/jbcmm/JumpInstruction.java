/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Jump Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getJumpInstruction()
 * @model abstract="true"
 * @generated
 */
public interface JumpInstruction extends Instruction {
} // JumpInstruction
