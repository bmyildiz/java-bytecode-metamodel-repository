/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ldiv Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getLdivInstruction()
 * @model
 * @generated
 */
public interface LdivInstruction extends SimpleInstruction {
} // LdivInstruction
