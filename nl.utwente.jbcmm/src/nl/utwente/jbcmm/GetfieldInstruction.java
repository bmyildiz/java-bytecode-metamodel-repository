/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Getfield Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getGetfieldInstruction()
 * @model
 * @generated
 */
public interface GetfieldInstruction extends FieldInstruction {
} // GetfieldInstruction
