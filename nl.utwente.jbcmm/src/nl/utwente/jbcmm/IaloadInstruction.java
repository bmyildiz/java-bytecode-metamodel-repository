/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Iaload Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIaloadInstruction()
 * @model
 * @generated
 */
public interface IaloadInstruction extends SimpleInstruction {
} // IaloadInstruction
