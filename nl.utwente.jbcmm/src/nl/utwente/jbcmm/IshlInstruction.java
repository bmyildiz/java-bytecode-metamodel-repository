/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ishl Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIshlInstruction()
 * @model
 * @generated
 */
public interface IshlInstruction extends SimpleInstruction {
} // IshlInstruction
