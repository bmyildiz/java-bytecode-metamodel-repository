/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dastore Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getDastoreInstruction()
 * @model
 * @generated
 */
public interface DastoreInstruction extends SimpleInstruction {
} // DastoreInstruction
