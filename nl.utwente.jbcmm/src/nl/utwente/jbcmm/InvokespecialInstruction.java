/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Invokespecial Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getInvokespecialInstruction()
 * @model
 * @generated
 */
public interface InvokespecialInstruction extends MethodInstruction {
} // InvokespecialInstruction
