/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lor Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getLorInstruction()
 * @model
 * @generated
 */
public interface LorInstruction extends SimpleInstruction {
} // LorInstruction
