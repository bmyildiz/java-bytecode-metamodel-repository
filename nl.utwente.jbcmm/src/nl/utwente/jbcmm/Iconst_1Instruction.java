/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Iconst 1Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIconst_1Instruction()
 * @model
 * @generated
 */
public interface Iconst_1Instruction extends SimpleInstruction {
} // Iconst_1Instruction
