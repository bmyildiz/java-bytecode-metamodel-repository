/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lneg Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getLnegInstruction()
 * @model
 * @generated
 */
public interface LnegInstruction extends SimpleInstruction {
} // LnegInstruction
