/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Idiv Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIdivInstruction()
 * @model
 * @generated
 */
public interface IdivInstruction extends SimpleInstruction {
} // IdivInstruction
