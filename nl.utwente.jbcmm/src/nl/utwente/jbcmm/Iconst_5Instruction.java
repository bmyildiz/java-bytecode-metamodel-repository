/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Iconst 5Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIconst_5Instruction()
 * @model
 * @generated
 */
public interface Iconst_5Instruction extends SimpleInstruction {
} // Iconst_5Instruction
