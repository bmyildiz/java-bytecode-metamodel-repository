/**
 */
package nl.utwente.jbcmm;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Project</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.Project#getClasses <em>Classes</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Project#getMainClass <em>Main Class</em>}</li>
 * </ul>
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getProject()
 * @model
 * @generated
 */
public interface Project extends Identifiable {
	/**
	 * Returns the value of the '<em><b>Classes</b></em>' containment reference list.
	 * The list contents are of type {@link nl.utwente.jbcmm.Clazz}.
	 * It is bidirectional and its opposite is '{@link nl.utwente.jbcmm.Clazz#getProject <em>Project</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Classes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Classes</em>' containment reference list.
	 * @see nl.utwente.jbcmm.JbcmmPackage#getProject_Classes()
	 * @see nl.utwente.jbcmm.Clazz#getProject
	 * @model opposite="project" containment="true" required="true"
	 * @generated
	 */
	EList<Clazz> getClasses();

	/**
	 * Returns the value of the '<em><b>Main Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Main Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Main Class</em>' reference.
	 * @see #setMainClass(Clazz)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getProject_MainClass()
	 * @model required="true"
	 * @generated
	 */
	Clazz getMainClass();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Project#getMainClass <em>Main Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Main Class</em>' reference.
	 * @see #getMainClass()
	 * @generated
	 */
	void setMainClass(Clazz value);

} // Project
