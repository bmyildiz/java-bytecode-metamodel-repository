/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aaload Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getAaloadInstruction()
 * @model
 * @generated
 */
public interface AaloadInstruction extends SimpleInstruction {
} // AaloadInstruction
