/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Pop2 Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getPop2Instruction()
 * @model
 * @generated
 */
public interface Pop2Instruction extends SimpleInstruction {
} // Pop2Instruction
