/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fneg Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getFnegInstruction()
 * @model
 * @generated
 */
public interface FnegInstruction extends SimpleInstruction {
} // FnegInstruction
