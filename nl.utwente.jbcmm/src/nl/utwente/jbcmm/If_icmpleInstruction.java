/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>If icmple Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIf_icmpleInstruction()
 * @model
 * @generated
 */
public interface If_icmpleInstruction extends JumpInstruction {
} // If_icmpleInstruction
