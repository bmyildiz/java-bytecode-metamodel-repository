/**
 */
package nl.utwente.jbcmm;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.TypeReference#getReferencedClass <em>Referenced Class</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.TypeReference#getTypeDescriptor <em>Type Descriptor</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.TypeReference#getRuntimeInvisibleAnnotations <em>Runtime Invisible Annotations</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.TypeReference#getRuntimeVisibleAnnotations <em>Runtime Visible Annotations</em>}</li>
 * </ul>
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getTypeReference()
 * @model
 * @generated
 */
public interface TypeReference extends Identifiable {
	/**
	 * Returns the value of the '<em><b>Referenced Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Referenced Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Referenced Class</em>' reference.
	 * @see #setReferencedClass(Clazz)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getTypeReference_ReferencedClass()
	 * @model
	 * @generated
	 */
	Clazz getReferencedClass();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.TypeReference#getReferencedClass <em>Referenced Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Referenced Class</em>' reference.
	 * @see #getReferencedClass()
	 * @generated
	 */
	void setReferencedClass(Clazz value);

	/**
	 * Returns the value of the '<em><b>Type Descriptor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Descriptor</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Descriptor</em>' attribute.
	 * @see #setTypeDescriptor(String)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getTypeReference_TypeDescriptor()
	 * @model required="true"
	 * @generated
	 */
	String getTypeDescriptor();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.TypeReference#getTypeDescriptor <em>Type Descriptor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type Descriptor</em>' attribute.
	 * @see #getTypeDescriptor()
	 * @generated
	 */
	void setTypeDescriptor(String value);

	/**
	 * Returns the value of the '<em><b>Runtime Invisible Annotations</b></em>' containment reference list.
	 * The list contents are of type {@link nl.utwente.jbcmm.Annotation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Runtime Invisible Annotations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Runtime Invisible Annotations</em>' containment reference list.
	 * @see nl.utwente.jbcmm.JbcmmPackage#getTypeReference_RuntimeInvisibleAnnotations()
	 * @model containment="true"
	 * @generated
	 */
	EList<Annotation> getRuntimeInvisibleAnnotations();

	/**
	 * Returns the value of the '<em><b>Runtime Visible Annotations</b></em>' containment reference list.
	 * The list contents are of type {@link nl.utwente.jbcmm.Annotation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Runtime Visible Annotations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Runtime Visible Annotations</em>' containment reference list.
	 * @see nl.utwente.jbcmm.JbcmmPackage#getTypeReference_RuntimeVisibleAnnotations()
	 * @model containment="true"
	 * @generated
	 */
	EList<Annotation> getRuntimeVisibleAnnotations();

} // TypeReference
