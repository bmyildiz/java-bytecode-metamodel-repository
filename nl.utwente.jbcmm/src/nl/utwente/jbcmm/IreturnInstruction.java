/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ireturn Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIreturnInstruction()
 * @model
 * @generated
 */
public interface IreturnInstruction extends SimpleInstruction {
} // IreturnInstruction
