/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Goto Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getGotoInstruction()
 * @model
 * @generated
 */
public interface GotoInstruction extends JumpInstruction {
} // GotoInstruction
