/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Freturn Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getFreturnInstruction()
 * @model
 * @generated
 */
public interface FreturnInstruction extends SimpleInstruction {
} // FreturnInstruction
