/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>D2i Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getD2iInstruction()
 * @model
 * @generated
 */
public interface D2iInstruction extends SimpleInstruction {
} // D2iInstruction
