/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Baload Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getBaloadInstruction()
 * @model
 * @generated
 */
public interface BaloadInstruction extends SimpleInstruction {
} // BaloadInstruction
