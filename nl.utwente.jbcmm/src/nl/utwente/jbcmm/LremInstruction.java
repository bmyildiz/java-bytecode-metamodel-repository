/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lrem Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getLremInstruction()
 * @model
 * @generated
 */
public interface LremInstruction extends SimpleInstruction {
} // LremInstruction
