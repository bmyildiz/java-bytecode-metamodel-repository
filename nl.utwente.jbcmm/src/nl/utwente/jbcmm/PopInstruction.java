/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Pop Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getPopInstruction()
 * @model
 * @generated
 */
public interface PopInstruction extends SimpleInstruction {
} // PopInstruction
