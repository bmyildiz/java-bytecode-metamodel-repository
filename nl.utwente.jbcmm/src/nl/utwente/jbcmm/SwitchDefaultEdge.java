/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Switch Default Edge</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getSwitchDefaultEdge()
 * @model
 * @generated
 */
public interface SwitchDefaultEdge extends ControlFlowEdge {
} // SwitchDefaultEdge
