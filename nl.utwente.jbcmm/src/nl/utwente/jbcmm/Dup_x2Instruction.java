/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dup x2 Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getDup_x2Instruction()
 * @model
 * @generated
 */
public interface Dup_x2Instruction extends SimpleInstruction {
} // Dup_x2Instruction
