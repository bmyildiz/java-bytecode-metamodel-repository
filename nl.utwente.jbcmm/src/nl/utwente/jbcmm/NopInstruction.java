/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Nop Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getNopInstruction()
 * @model
 * @generated
 */
public interface NopInstruction extends SimpleInstruction {
} // NopInstruction
