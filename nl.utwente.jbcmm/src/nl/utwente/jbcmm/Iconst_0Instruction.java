/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Iconst 0Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIconst_0Instruction()
 * @model
 * @generated
 */
public interface Iconst_0Instruction extends SimpleInstruction {
} // Iconst_0Instruction
