/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>I2f Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getI2fInstruction()
 * @model
 * @generated
 */
public interface I2fInstruction extends SimpleInstruction {
} // I2fInstruction
