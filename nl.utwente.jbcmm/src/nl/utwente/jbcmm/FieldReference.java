/**
 */
package nl.utwente.jbcmm;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Field Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.FieldReference#getDeclaringClass <em>Declaring Class</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.FieldReference#getName <em>Name</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.FieldReference#getDescriptor <em>Descriptor</em>}</li>
 * </ul>
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getFieldReference()
 * @model
 * @generated
 */
public interface FieldReference extends Identifiable {
	/**
	 * Returns the value of the '<em><b>Declaring Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Declaring Class</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Declaring Class</em>' containment reference.
	 * @see #setDeclaringClass(TypeReference)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getFieldReference_DeclaringClass()
	 * @model containment="true" required="true"
	 * @generated
	 */
	TypeReference getDeclaringClass();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.FieldReference#getDeclaringClass <em>Declaring Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Declaring Class</em>' containment reference.
	 * @see #getDeclaringClass()
	 * @generated
	 */
	void setDeclaringClass(TypeReference value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getFieldReference_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.FieldReference#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Descriptor</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Descriptor</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Descriptor</em>' containment reference.
	 * @see #setDescriptor(TypeReference)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getFieldReference_Descriptor()
	 * @model containment="true" required="true"
	 * @generated
	 */
	TypeReference getDescriptor();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.FieldReference#getDescriptor <em>Descriptor</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Descriptor</em>' containment reference.
	 * @see #getDescriptor()
	 * @generated
	 */
	void setDescriptor(TypeReference value);

} // FieldReference
