/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>D2l Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getD2lInstruction()
 * @model
 * @generated
 */
public interface D2lInstruction extends SimpleInstruction {
} // D2lInstruction
