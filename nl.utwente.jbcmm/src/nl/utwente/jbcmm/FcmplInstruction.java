/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fcmpl Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getFcmplInstruction()
 * @model
 * @generated
 */
public interface FcmplInstruction extends SimpleInstruction {
} // FcmplInstruction
