/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fastore Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getFastoreInstruction()
 * @model
 * @generated
 */
public interface FastoreInstruction extends SimpleInstruction {
} // FastoreInstruction
