/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dadd Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getDaddInstruction()
 * @model
 * @generated
 */
public interface DaddInstruction extends SimpleInstruction {
} // DaddInstruction
