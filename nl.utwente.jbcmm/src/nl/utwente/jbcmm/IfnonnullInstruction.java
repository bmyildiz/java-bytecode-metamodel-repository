/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ifnonnull Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIfnonnullInstruction()
 * @model
 * @generated
 */
public interface IfnonnullInstruction extends JumpInstruction {
} // IfnonnullInstruction
