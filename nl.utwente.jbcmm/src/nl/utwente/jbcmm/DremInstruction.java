/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Drem Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getDremInstruction()
 * @model
 * @generated
 */
public interface DremInstruction extends SimpleInstruction {
} // DremInstruction
