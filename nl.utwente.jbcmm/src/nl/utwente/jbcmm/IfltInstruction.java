/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Iflt Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIfltInstruction()
 * @model
 * @generated
 */
public interface IfltInstruction extends JumpInstruction {
} // IfltInstruction
