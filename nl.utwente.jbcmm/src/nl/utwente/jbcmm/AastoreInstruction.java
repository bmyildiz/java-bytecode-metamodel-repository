/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aastore Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getAastoreInstruction()
 * @model
 * @generated
 */
public interface AastoreInstruction extends SimpleInstruction {
} // AastoreInstruction
