/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Iload Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIloadInstruction()
 * @model
 * @generated
 */
public interface IloadInstruction extends VarInstruction {
} // IloadInstruction
