/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ldc Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getLdcInstruction()
 * @model abstract="true"
 * @generated
 */
public interface LdcInstruction extends Instruction {
} // LdcInstruction
