/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.Dconst_1Instruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dconst 1Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class Dconst_1InstructionImpl extends SimpleInstructionImpl implements Dconst_1Instruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Dconst_1InstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getDconst_1Instruction();
	}

} //Dconst_1InstructionImpl
