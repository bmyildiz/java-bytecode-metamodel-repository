/**
 */
package nl.utwente.jbcmm.impl;

import java.util.Collection;

import nl.utwente.jbcmm.Annotation;
import nl.utwente.jbcmm.ElementValue;
import nl.utwente.jbcmm.ElementaryValue;
import nl.utwente.jbcmm.EnumValue;
import nl.utwente.jbcmm.JbcmmPackage;
import nl.utwente.jbcmm.TypeReference;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Element Value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.impl.ElementValueImpl#getConstantValue <em>Constant Value</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ElementValueImpl#getEnumValue <em>Enum Value</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ElementValueImpl#getAnnotationValue <em>Annotation Value</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ElementValueImpl#getArrayValue <em>Array Value</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ElementValueImpl#getClassValue <em>Class Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ElementValueImpl extends IdentifiableImpl implements ElementValue {
	/**
	 * The cached value of the '{@link #getConstantValue() <em>Constant Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstantValue()
	 * @generated
	 * @ordered
	 */
	protected ElementaryValue constantValue;

	/**
	 * The cached value of the '{@link #getEnumValue() <em>Enum Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnumValue()
	 * @generated
	 * @ordered
	 */
	protected EnumValue enumValue;

	/**
	 * The cached value of the '{@link #getAnnotationValue() <em>Annotation Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotationValue()
	 * @generated
	 * @ordered
	 */
	protected Annotation annotationValue;

	/**
	 * The cached value of the '{@link #getArrayValue() <em>Array Value</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArrayValue()
	 * @generated
	 * @ordered
	 */
	protected EList<ElementValue> arrayValue;

	/**
	 * The cached value of the '{@link #getClassValue() <em>Class Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassValue()
	 * @generated
	 * @ordered
	 */
	protected TypeReference classValue;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ElementValueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getElementValue();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementaryValue getConstantValue() {
		return constantValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConstantValue(ElementaryValue newConstantValue, NotificationChain msgs) {
		ElementaryValue oldConstantValue = constantValue;
		constantValue = newConstantValue;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JbcmmPackage.ELEMENT_VALUE__CONSTANT_VALUE, oldConstantValue, newConstantValue);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstantValue(ElementaryValue newConstantValue) {
		if (newConstantValue != constantValue) {
			NotificationChain msgs = null;
			if (constantValue != null)
				msgs = ((InternalEObject)constantValue).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.ELEMENT_VALUE__CONSTANT_VALUE, null, msgs);
			if (newConstantValue != null)
				msgs = ((InternalEObject)newConstantValue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.ELEMENT_VALUE__CONSTANT_VALUE, null, msgs);
			msgs = basicSetConstantValue(newConstantValue, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.ELEMENT_VALUE__CONSTANT_VALUE, newConstantValue, newConstantValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumValue getEnumValue() {
		return enumValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEnumValue(EnumValue newEnumValue, NotificationChain msgs) {
		EnumValue oldEnumValue = enumValue;
		enumValue = newEnumValue;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JbcmmPackage.ELEMENT_VALUE__ENUM_VALUE, oldEnumValue, newEnumValue);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnumValue(EnumValue newEnumValue) {
		if (newEnumValue != enumValue) {
			NotificationChain msgs = null;
			if (enumValue != null)
				msgs = ((InternalEObject)enumValue).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.ELEMENT_VALUE__ENUM_VALUE, null, msgs);
			if (newEnumValue != null)
				msgs = ((InternalEObject)newEnumValue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.ELEMENT_VALUE__ENUM_VALUE, null, msgs);
			msgs = basicSetEnumValue(newEnumValue, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.ELEMENT_VALUE__ENUM_VALUE, newEnumValue, newEnumValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Annotation getAnnotationValue() {
		return annotationValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnnotationValue(Annotation newAnnotationValue, NotificationChain msgs) {
		Annotation oldAnnotationValue = annotationValue;
		annotationValue = newAnnotationValue;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JbcmmPackage.ELEMENT_VALUE__ANNOTATION_VALUE, oldAnnotationValue, newAnnotationValue);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnnotationValue(Annotation newAnnotationValue) {
		if (newAnnotationValue != annotationValue) {
			NotificationChain msgs = null;
			if (annotationValue != null)
				msgs = ((InternalEObject)annotationValue).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.ELEMENT_VALUE__ANNOTATION_VALUE, null, msgs);
			if (newAnnotationValue != null)
				msgs = ((InternalEObject)newAnnotationValue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.ELEMENT_VALUE__ANNOTATION_VALUE, null, msgs);
			msgs = basicSetAnnotationValue(newAnnotationValue, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.ELEMENT_VALUE__ANNOTATION_VALUE, newAnnotationValue, newAnnotationValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ElementValue> getArrayValue() {
		if (arrayValue == null) {
			arrayValue = new EObjectContainmentEList<ElementValue>(ElementValue.class, this, JbcmmPackage.ELEMENT_VALUE__ARRAY_VALUE);
		}
		return arrayValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeReference getClassValue() {
		return classValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetClassValue(TypeReference newClassValue, NotificationChain msgs) {
		TypeReference oldClassValue = classValue;
		classValue = newClassValue;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JbcmmPackage.ELEMENT_VALUE__CLASS_VALUE, oldClassValue, newClassValue);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClassValue(TypeReference newClassValue) {
		if (newClassValue != classValue) {
			NotificationChain msgs = null;
			if (classValue != null)
				msgs = ((InternalEObject)classValue).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.ELEMENT_VALUE__CLASS_VALUE, null, msgs);
			if (newClassValue != null)
				msgs = ((InternalEObject)newClassValue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.ELEMENT_VALUE__CLASS_VALUE, null, msgs);
			msgs = basicSetClassValue(newClassValue, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.ELEMENT_VALUE__CLASS_VALUE, newClassValue, newClassValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case JbcmmPackage.ELEMENT_VALUE__CONSTANT_VALUE:
				return basicSetConstantValue(null, msgs);
			case JbcmmPackage.ELEMENT_VALUE__ENUM_VALUE:
				return basicSetEnumValue(null, msgs);
			case JbcmmPackage.ELEMENT_VALUE__ANNOTATION_VALUE:
				return basicSetAnnotationValue(null, msgs);
			case JbcmmPackage.ELEMENT_VALUE__ARRAY_VALUE:
				return ((InternalEList<?>)getArrayValue()).basicRemove(otherEnd, msgs);
			case JbcmmPackage.ELEMENT_VALUE__CLASS_VALUE:
				return basicSetClassValue(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case JbcmmPackage.ELEMENT_VALUE__CONSTANT_VALUE:
				return getConstantValue();
			case JbcmmPackage.ELEMENT_VALUE__ENUM_VALUE:
				return getEnumValue();
			case JbcmmPackage.ELEMENT_VALUE__ANNOTATION_VALUE:
				return getAnnotationValue();
			case JbcmmPackage.ELEMENT_VALUE__ARRAY_VALUE:
				return getArrayValue();
			case JbcmmPackage.ELEMENT_VALUE__CLASS_VALUE:
				return getClassValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case JbcmmPackage.ELEMENT_VALUE__CONSTANT_VALUE:
				setConstantValue((ElementaryValue)newValue);
				return;
			case JbcmmPackage.ELEMENT_VALUE__ENUM_VALUE:
				setEnumValue((EnumValue)newValue);
				return;
			case JbcmmPackage.ELEMENT_VALUE__ANNOTATION_VALUE:
				setAnnotationValue((Annotation)newValue);
				return;
			case JbcmmPackage.ELEMENT_VALUE__ARRAY_VALUE:
				getArrayValue().clear();
				getArrayValue().addAll((Collection<? extends ElementValue>)newValue);
				return;
			case JbcmmPackage.ELEMENT_VALUE__CLASS_VALUE:
				setClassValue((TypeReference)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case JbcmmPackage.ELEMENT_VALUE__CONSTANT_VALUE:
				setConstantValue((ElementaryValue)null);
				return;
			case JbcmmPackage.ELEMENT_VALUE__ENUM_VALUE:
				setEnumValue((EnumValue)null);
				return;
			case JbcmmPackage.ELEMENT_VALUE__ANNOTATION_VALUE:
				setAnnotationValue((Annotation)null);
				return;
			case JbcmmPackage.ELEMENT_VALUE__ARRAY_VALUE:
				getArrayValue().clear();
				return;
			case JbcmmPackage.ELEMENT_VALUE__CLASS_VALUE:
				setClassValue((TypeReference)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case JbcmmPackage.ELEMENT_VALUE__CONSTANT_VALUE:
				return constantValue != null;
			case JbcmmPackage.ELEMENT_VALUE__ENUM_VALUE:
				return enumValue != null;
			case JbcmmPackage.ELEMENT_VALUE__ANNOTATION_VALUE:
				return annotationValue != null;
			case JbcmmPackage.ELEMENT_VALUE__ARRAY_VALUE:
				return arrayValue != null && !arrayValue.isEmpty();
			case JbcmmPackage.ELEMENT_VALUE__CLASS_VALUE:
				return classValue != null;
		}
		return super.eIsSet(featureID);
	}

} //ElementValueImpl
