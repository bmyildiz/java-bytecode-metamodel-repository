/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.Fconst_2Instruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Fconst 2Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class Fconst_2InstructionImpl extends SimpleInstructionImpl implements Fconst_2Instruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Fconst_2InstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getFconst_2Instruction();
	}

} //Fconst_2InstructionImpl
