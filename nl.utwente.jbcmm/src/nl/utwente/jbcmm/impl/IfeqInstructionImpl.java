/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.IfeqInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ifeq Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IfeqInstructionImpl extends JumpInstructionImpl implements IfeqInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IfeqInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIfeqInstruction();
	}

} //IfeqInstructionImpl
