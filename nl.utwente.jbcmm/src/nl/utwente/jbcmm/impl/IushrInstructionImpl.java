/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.IushrInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Iushr Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IushrInstructionImpl extends SimpleInstructionImpl implements IushrInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IushrInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIushrInstruction();
	}

} //IushrInstructionImpl
