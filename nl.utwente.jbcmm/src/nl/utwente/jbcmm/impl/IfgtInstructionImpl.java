/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.IfgtInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ifgt Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IfgtInstructionImpl extends JumpInstructionImpl implements IfgtInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IfgtInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIfgtInstruction();
	}

} //IfgtInstructionImpl
