/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.JbcmmPackage;
import nl.utwente.jbcmm.NopInstruction;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Nop Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class NopInstructionImpl extends SimpleInstructionImpl implements NopInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NopInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getNopInstruction();
	}

} //NopInstructionImpl
