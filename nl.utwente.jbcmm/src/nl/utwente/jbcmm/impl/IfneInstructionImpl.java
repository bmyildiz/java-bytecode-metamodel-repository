/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.IfneInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ifne Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IfneInstructionImpl extends JumpInstructionImpl implements IfneInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IfneInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIfneInstruction();
	}

} //IfneInstructionImpl
