/**
 */
package nl.utwente.jbcmm.impl;

import java.util.Collection;

import nl.utwente.jbcmm.Annotation;
import nl.utwente.jbcmm.Clazz;
import nl.utwente.jbcmm.ElementValue;
import nl.utwente.jbcmm.ExceptionTableEntry;
import nl.utwente.jbcmm.Instruction;
import nl.utwente.jbcmm.JbcmmPackage;
import nl.utwente.jbcmm.LocalVariableTableEntry;
import nl.utwente.jbcmm.Method;
import nl.utwente.jbcmm.MethodDescriptor;
import nl.utwente.jbcmm.MethodSignature;
import nl.utwente.jbcmm.TypeReference;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Method</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.impl.MethodImpl#getClass_ <em>Class</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.MethodImpl#getName <em>Name</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.MethodImpl#getInstructions <em>Instructions</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.MethodImpl#getFirstInstruction <em>First Instruction</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.MethodImpl#isPublic <em>Public</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.MethodImpl#isPrivate <em>Private</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.MethodImpl#isProtected <em>Protected</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.MethodImpl#isStatic <em>Static</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.MethodImpl#isFinal <em>Final</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.MethodImpl#isSynthetic <em>Synthetic</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.MethodImpl#isSynchronized <em>Synchronized</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.MethodImpl#isBridge <em>Bridge</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.MethodImpl#isVarArgs <em>Var Args</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.MethodImpl#isNative <em>Native</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.MethodImpl#isAbstract <em>Abstract</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.MethodImpl#isStrict <em>Strict</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.MethodImpl#isDeprecated <em>Deprecated</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.MethodImpl#getRuntimeInvisibleAnnotations <em>Runtime Invisible Annotations</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.MethodImpl#getRuntimeVisibleAnnotations <em>Runtime Visible Annotations</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.MethodImpl#getAnnotationDefaultValue <em>Annotation Default Value</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.MethodImpl#getDescriptor <em>Descriptor</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.MethodImpl#getSignature <em>Signature</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.MethodImpl#getExceptionsCanBeThrown <em>Exceptions Can Be Thrown</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.MethodImpl#getExceptionTable <em>Exception Table</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.MethodImpl#getLocalVariableTable <em>Local Variable Table</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.MethodImpl#getRuntimeInvisibleParameterAnnotations <em>Runtime Invisible Parameter Annotations</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.MethodImpl#getRuntimeVisibleParameterAnnotations <em>Runtime Visible Parameter Annotations</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MethodImpl extends IdentifiableImpl implements Method {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getInstructions() <em>Instructions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstructions()
	 * @generated
	 * @ordered
	 */
	protected EList<Instruction> instructions;

	/**
	 * The cached value of the '{@link #getFirstInstruction() <em>First Instruction</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirstInstruction()
	 * @generated
	 * @ordered
	 */
	protected Instruction firstInstruction;

	/**
	 * The default value of the '{@link #isPublic() <em>Public</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPublic()
	 * @generated
	 * @ordered
	 */
	protected static final boolean PUBLIC_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isPublic() <em>Public</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPublic()
	 * @generated
	 * @ordered
	 */
	protected boolean public_ = PUBLIC_EDEFAULT;

	/**
	 * The default value of the '{@link #isPrivate() <em>Private</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPrivate()
	 * @generated
	 * @ordered
	 */
	protected static final boolean PRIVATE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isPrivate() <em>Private</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPrivate()
	 * @generated
	 * @ordered
	 */
	protected boolean private_ = PRIVATE_EDEFAULT;

	/**
	 * The default value of the '{@link #isProtected() <em>Protected</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isProtected()
	 * @generated
	 * @ordered
	 */
	protected static final boolean PROTECTED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isProtected() <em>Protected</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isProtected()
	 * @generated
	 * @ordered
	 */
	protected boolean protected_ = PROTECTED_EDEFAULT;

	/**
	 * The default value of the '{@link #isStatic() <em>Static</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isStatic()
	 * @generated
	 * @ordered
	 */
	protected static final boolean STATIC_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isStatic() <em>Static</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isStatic()
	 * @generated
	 * @ordered
	 */
	protected boolean static_ = STATIC_EDEFAULT;

	/**
	 * The default value of the '{@link #isFinal() <em>Final</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isFinal()
	 * @generated
	 * @ordered
	 */
	protected static final boolean FINAL_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isFinal() <em>Final</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isFinal()
	 * @generated
	 * @ordered
	 */
	protected boolean final_ = FINAL_EDEFAULT;

	/**
	 * The default value of the '{@link #isSynthetic() <em>Synthetic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSynthetic()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SYNTHETIC_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSynthetic() <em>Synthetic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSynthetic()
	 * @generated
	 * @ordered
	 */
	protected boolean synthetic = SYNTHETIC_EDEFAULT;

	/**
	 * The default value of the '{@link #isSynchronized() <em>Synchronized</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSynchronized()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SYNCHRONIZED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSynchronized() <em>Synchronized</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSynchronized()
	 * @generated
	 * @ordered
	 */
	protected boolean synchronized_ = SYNCHRONIZED_EDEFAULT;

	/**
	 * The default value of the '{@link #isBridge() <em>Bridge</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isBridge()
	 * @generated
	 * @ordered
	 */
	protected static final boolean BRIDGE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isBridge() <em>Bridge</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isBridge()
	 * @generated
	 * @ordered
	 */
	protected boolean bridge = BRIDGE_EDEFAULT;

	/**
	 * The default value of the '{@link #isVarArgs() <em>Var Args</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isVarArgs()
	 * @generated
	 * @ordered
	 */
	protected static final boolean VAR_ARGS_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isVarArgs() <em>Var Args</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isVarArgs()
	 * @generated
	 * @ordered
	 */
	protected boolean varArgs = VAR_ARGS_EDEFAULT;

	/**
	 * The default value of the '{@link #isNative() <em>Native</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isNative()
	 * @generated
	 * @ordered
	 */
	protected static final boolean NATIVE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isNative() <em>Native</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isNative()
	 * @generated
	 * @ordered
	 */
	protected boolean native_ = NATIVE_EDEFAULT;

	/**
	 * The default value of the '{@link #isAbstract() <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAbstract()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ABSTRACT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isAbstract() <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAbstract()
	 * @generated
	 * @ordered
	 */
	protected boolean abstract_ = ABSTRACT_EDEFAULT;

	/**
	 * The default value of the '{@link #isStrict() <em>Strict</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isStrict()
	 * @generated
	 * @ordered
	 */
	protected static final boolean STRICT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isStrict() <em>Strict</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isStrict()
	 * @generated
	 * @ordered
	 */
	protected boolean strict = STRICT_EDEFAULT;

	/**
	 * The default value of the '{@link #isDeprecated() <em>Deprecated</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDeprecated()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DEPRECATED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isDeprecated() <em>Deprecated</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDeprecated()
	 * @generated
	 * @ordered
	 */
	protected boolean deprecated = DEPRECATED_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRuntimeInvisibleAnnotations() <em>Runtime Invisible Annotations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRuntimeInvisibleAnnotations()
	 * @generated
	 * @ordered
	 */
	protected EList<Annotation> runtimeInvisibleAnnotations;

	/**
	 * The cached value of the '{@link #getRuntimeVisibleAnnotations() <em>Runtime Visible Annotations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRuntimeVisibleAnnotations()
	 * @generated
	 * @ordered
	 */
	protected EList<Annotation> runtimeVisibleAnnotations;

	/**
	 * The cached value of the '{@link #getAnnotationDefaultValue() <em>Annotation Default Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotationDefaultValue()
	 * @generated
	 * @ordered
	 */
	protected ElementValue annotationDefaultValue;

	/**
	 * The cached value of the '{@link #getDescriptor() <em>Descriptor</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescriptor()
	 * @generated
	 * @ordered
	 */
	protected MethodDescriptor descriptor;

	/**
	 * The cached value of the '{@link #getSignature() <em>Signature</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSignature()
	 * @generated
	 * @ordered
	 */
	protected MethodSignature signature;

	/**
	 * The cached value of the '{@link #getExceptionsCanBeThrown() <em>Exceptions Can Be Thrown</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExceptionsCanBeThrown()
	 * @generated
	 * @ordered
	 */
	protected EList<TypeReference> exceptionsCanBeThrown;

	/**
	 * The cached value of the '{@link #getExceptionTable() <em>Exception Table</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExceptionTable()
	 * @generated
	 * @ordered
	 */
	protected EList<ExceptionTableEntry> exceptionTable;

	/**
	 * The cached value of the '{@link #getLocalVariableTable() <em>Local Variable Table</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalVariableTable()
	 * @generated
	 * @ordered
	 */
	protected EList<LocalVariableTableEntry> localVariableTable;

	/**
	 * The cached value of the '{@link #getRuntimeInvisibleParameterAnnotations() <em>Runtime Invisible Parameter Annotations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRuntimeInvisibleParameterAnnotations()
	 * @generated
	 * @ordered
	 */
	protected EList<Annotation> runtimeInvisibleParameterAnnotations;

	/**
	 * The cached value of the '{@link #getRuntimeVisibleParameterAnnotations() <em>Runtime Visible Parameter Annotations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRuntimeVisibleParameterAnnotations()
	 * @generated
	 * @ordered
	 */
	protected EList<Annotation> runtimeVisibleParameterAnnotations;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MethodImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getMethod();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Clazz getClass_() {
		if (eContainerFeatureID() != JbcmmPackage.METHOD__CLASS) return null;
		return (Clazz)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetClass(Clazz newClass, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newClass, JbcmmPackage.METHOD__CLASS, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClass(Clazz newClass) {
		if (newClass != eInternalContainer() || (eContainerFeatureID() != JbcmmPackage.METHOD__CLASS && newClass != null)) {
			if (EcoreUtil.isAncestor(this, newClass))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newClass != null)
				msgs = ((InternalEObject)newClass).eInverseAdd(this, JbcmmPackage.CLAZZ__METHODS, Clazz.class, msgs);
			msgs = basicSetClass(newClass, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.METHOD__CLASS, newClass, newClass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.METHOD__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Instruction> getInstructions() {
		if (instructions == null) {
			instructions = new EObjectContainmentWithInverseEList<Instruction>(Instruction.class, this, JbcmmPackage.METHOD__INSTRUCTIONS, JbcmmPackage.INSTRUCTION__METHOD);
		}
		return instructions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getFirstInstruction() {
		if (firstInstruction != null && firstInstruction.eIsProxy()) {
			InternalEObject oldFirstInstruction = (InternalEObject)firstInstruction;
			firstInstruction = (Instruction)eResolveProxy(oldFirstInstruction);
			if (firstInstruction != oldFirstInstruction) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, JbcmmPackage.METHOD__FIRST_INSTRUCTION, oldFirstInstruction, firstInstruction));
			}
		}
		return firstInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction basicGetFirstInstruction() {
		return firstInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFirstInstruction(Instruction newFirstInstruction) {
		Instruction oldFirstInstruction = firstInstruction;
		firstInstruction = newFirstInstruction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.METHOD__FIRST_INSTRUCTION, oldFirstInstruction, firstInstruction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isPublic() {
		return public_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPublic(boolean newPublic) {
		boolean oldPublic = public_;
		public_ = newPublic;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.METHOD__PUBLIC, oldPublic, public_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isPrivate() {
		return private_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrivate(boolean newPrivate) {
		boolean oldPrivate = private_;
		private_ = newPrivate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.METHOD__PRIVATE, oldPrivate, private_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isProtected() {
		return protected_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProtected(boolean newProtected) {
		boolean oldProtected = protected_;
		protected_ = newProtected;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.METHOD__PROTECTED, oldProtected, protected_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isStatic() {
		return static_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStatic(boolean newStatic) {
		boolean oldStatic = static_;
		static_ = newStatic;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.METHOD__STATIC, oldStatic, static_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isFinal() {
		return final_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFinal(boolean newFinal) {
		boolean oldFinal = final_;
		final_ = newFinal;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.METHOD__FINAL, oldFinal, final_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSynthetic() {
		return synthetic;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSynthetic(boolean newSynthetic) {
		boolean oldSynthetic = synthetic;
		synthetic = newSynthetic;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.METHOD__SYNTHETIC, oldSynthetic, synthetic));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSynchronized() {
		return synchronized_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSynchronized(boolean newSynchronized) {
		boolean oldSynchronized = synchronized_;
		synchronized_ = newSynchronized;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.METHOD__SYNCHRONIZED, oldSynchronized, synchronized_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isBridge() {
		return bridge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBridge(boolean newBridge) {
		boolean oldBridge = bridge;
		bridge = newBridge;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.METHOD__BRIDGE, oldBridge, bridge));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isVarArgs() {
		return varArgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVarArgs(boolean newVarArgs) {
		boolean oldVarArgs = varArgs;
		varArgs = newVarArgs;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.METHOD__VAR_ARGS, oldVarArgs, varArgs));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isNative() {
		return native_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNative(boolean newNative) {
		boolean oldNative = native_;
		native_ = newNative;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.METHOD__NATIVE, oldNative, native_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isAbstract() {
		return abstract_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbstract(boolean newAbstract) {
		boolean oldAbstract = abstract_;
		abstract_ = newAbstract;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.METHOD__ABSTRACT, oldAbstract, abstract_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isStrict() {
		return strict;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStrict(boolean newStrict) {
		boolean oldStrict = strict;
		strict = newStrict;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.METHOD__STRICT, oldStrict, strict));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isDeprecated() {
		return deprecated;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeprecated(boolean newDeprecated) {
		boolean oldDeprecated = deprecated;
		deprecated = newDeprecated;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.METHOD__DEPRECATED, oldDeprecated, deprecated));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Annotation> getRuntimeInvisibleAnnotations() {
		if (runtimeInvisibleAnnotations == null) {
			runtimeInvisibleAnnotations = new EObjectContainmentEList<Annotation>(Annotation.class, this, JbcmmPackage.METHOD__RUNTIME_INVISIBLE_ANNOTATIONS);
		}
		return runtimeInvisibleAnnotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Annotation> getRuntimeVisibleAnnotations() {
		if (runtimeVisibleAnnotations == null) {
			runtimeVisibleAnnotations = new EObjectContainmentEList<Annotation>(Annotation.class, this, JbcmmPackage.METHOD__RUNTIME_VISIBLE_ANNOTATIONS);
		}
		return runtimeVisibleAnnotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementValue getAnnotationDefaultValue() {
		return annotationDefaultValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnnotationDefaultValue(ElementValue newAnnotationDefaultValue, NotificationChain msgs) {
		ElementValue oldAnnotationDefaultValue = annotationDefaultValue;
		annotationDefaultValue = newAnnotationDefaultValue;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JbcmmPackage.METHOD__ANNOTATION_DEFAULT_VALUE, oldAnnotationDefaultValue, newAnnotationDefaultValue);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnnotationDefaultValue(ElementValue newAnnotationDefaultValue) {
		if (newAnnotationDefaultValue != annotationDefaultValue) {
			NotificationChain msgs = null;
			if (annotationDefaultValue != null)
				msgs = ((InternalEObject)annotationDefaultValue).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.METHOD__ANNOTATION_DEFAULT_VALUE, null, msgs);
			if (newAnnotationDefaultValue != null)
				msgs = ((InternalEObject)newAnnotationDefaultValue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.METHOD__ANNOTATION_DEFAULT_VALUE, null, msgs);
			msgs = basicSetAnnotationDefaultValue(newAnnotationDefaultValue, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.METHOD__ANNOTATION_DEFAULT_VALUE, newAnnotationDefaultValue, newAnnotationDefaultValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MethodDescriptor getDescriptor() {
		return descriptor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDescriptor(MethodDescriptor newDescriptor, NotificationChain msgs) {
		MethodDescriptor oldDescriptor = descriptor;
		descriptor = newDescriptor;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JbcmmPackage.METHOD__DESCRIPTOR, oldDescriptor, newDescriptor);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescriptor(MethodDescriptor newDescriptor) {
		if (newDescriptor != descriptor) {
			NotificationChain msgs = null;
			if (descriptor != null)
				msgs = ((InternalEObject)descriptor).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.METHOD__DESCRIPTOR, null, msgs);
			if (newDescriptor != null)
				msgs = ((InternalEObject)newDescriptor).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.METHOD__DESCRIPTOR, null, msgs);
			msgs = basicSetDescriptor(newDescriptor, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.METHOD__DESCRIPTOR, newDescriptor, newDescriptor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MethodSignature getSignature() {
		return signature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSignature(MethodSignature newSignature, NotificationChain msgs) {
		MethodSignature oldSignature = signature;
		signature = newSignature;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JbcmmPackage.METHOD__SIGNATURE, oldSignature, newSignature);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSignature(MethodSignature newSignature) {
		if (newSignature != signature) {
			NotificationChain msgs = null;
			if (signature != null)
				msgs = ((InternalEObject)signature).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.METHOD__SIGNATURE, null, msgs);
			if (newSignature != null)
				msgs = ((InternalEObject)newSignature).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.METHOD__SIGNATURE, null, msgs);
			msgs = basicSetSignature(newSignature, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.METHOD__SIGNATURE, newSignature, newSignature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TypeReference> getExceptionsCanBeThrown() {
		if (exceptionsCanBeThrown == null) {
			exceptionsCanBeThrown = new EObjectContainmentEList<TypeReference>(TypeReference.class, this, JbcmmPackage.METHOD__EXCEPTIONS_CAN_BE_THROWN);
		}
		return exceptionsCanBeThrown;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExceptionTableEntry> getExceptionTable() {
		if (exceptionTable == null) {
			exceptionTable = new EObjectContainmentWithInverseEList<ExceptionTableEntry>(ExceptionTableEntry.class, this, JbcmmPackage.METHOD__EXCEPTION_TABLE, JbcmmPackage.EXCEPTION_TABLE_ENTRY__METHOD);
		}
		return exceptionTable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LocalVariableTableEntry> getLocalVariableTable() {
		if (localVariableTable == null) {
			localVariableTable = new EObjectContainmentWithInverseEList<LocalVariableTableEntry>(LocalVariableTableEntry.class, this, JbcmmPackage.METHOD__LOCAL_VARIABLE_TABLE, JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__METHOD);
		}
		return localVariableTable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Annotation> getRuntimeInvisibleParameterAnnotations() {
		if (runtimeInvisibleParameterAnnotations == null) {
			runtimeInvisibleParameterAnnotations = new EObjectContainmentEList<Annotation>(Annotation.class, this, JbcmmPackage.METHOD__RUNTIME_INVISIBLE_PARAMETER_ANNOTATIONS);
		}
		return runtimeInvisibleParameterAnnotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Annotation> getRuntimeVisibleParameterAnnotations() {
		if (runtimeVisibleParameterAnnotations == null) {
			runtimeVisibleParameterAnnotations = new EObjectContainmentEList<Annotation>(Annotation.class, this, JbcmmPackage.METHOD__RUNTIME_VISIBLE_PARAMETER_ANNOTATIONS);
		}
		return runtimeVisibleParameterAnnotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case JbcmmPackage.METHOD__CLASS:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetClass((Clazz)otherEnd, msgs);
			case JbcmmPackage.METHOD__INSTRUCTIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getInstructions()).basicAdd(otherEnd, msgs);
			case JbcmmPackage.METHOD__EXCEPTION_TABLE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getExceptionTable()).basicAdd(otherEnd, msgs);
			case JbcmmPackage.METHOD__LOCAL_VARIABLE_TABLE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getLocalVariableTable()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case JbcmmPackage.METHOD__CLASS:
				return basicSetClass(null, msgs);
			case JbcmmPackage.METHOD__INSTRUCTIONS:
				return ((InternalEList<?>)getInstructions()).basicRemove(otherEnd, msgs);
			case JbcmmPackage.METHOD__RUNTIME_INVISIBLE_ANNOTATIONS:
				return ((InternalEList<?>)getRuntimeInvisibleAnnotations()).basicRemove(otherEnd, msgs);
			case JbcmmPackage.METHOD__RUNTIME_VISIBLE_ANNOTATIONS:
				return ((InternalEList<?>)getRuntimeVisibleAnnotations()).basicRemove(otherEnd, msgs);
			case JbcmmPackage.METHOD__ANNOTATION_DEFAULT_VALUE:
				return basicSetAnnotationDefaultValue(null, msgs);
			case JbcmmPackage.METHOD__DESCRIPTOR:
				return basicSetDescriptor(null, msgs);
			case JbcmmPackage.METHOD__SIGNATURE:
				return basicSetSignature(null, msgs);
			case JbcmmPackage.METHOD__EXCEPTIONS_CAN_BE_THROWN:
				return ((InternalEList<?>)getExceptionsCanBeThrown()).basicRemove(otherEnd, msgs);
			case JbcmmPackage.METHOD__EXCEPTION_TABLE:
				return ((InternalEList<?>)getExceptionTable()).basicRemove(otherEnd, msgs);
			case JbcmmPackage.METHOD__LOCAL_VARIABLE_TABLE:
				return ((InternalEList<?>)getLocalVariableTable()).basicRemove(otherEnd, msgs);
			case JbcmmPackage.METHOD__RUNTIME_INVISIBLE_PARAMETER_ANNOTATIONS:
				return ((InternalEList<?>)getRuntimeInvisibleParameterAnnotations()).basicRemove(otherEnd, msgs);
			case JbcmmPackage.METHOD__RUNTIME_VISIBLE_PARAMETER_ANNOTATIONS:
				return ((InternalEList<?>)getRuntimeVisibleParameterAnnotations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case JbcmmPackage.METHOD__CLASS:
				return eInternalContainer().eInverseRemove(this, JbcmmPackage.CLAZZ__METHODS, Clazz.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case JbcmmPackage.METHOD__CLASS:
				return getClass_();
			case JbcmmPackage.METHOD__NAME:
				return getName();
			case JbcmmPackage.METHOD__INSTRUCTIONS:
				return getInstructions();
			case JbcmmPackage.METHOD__FIRST_INSTRUCTION:
				if (resolve) return getFirstInstruction();
				return basicGetFirstInstruction();
			case JbcmmPackage.METHOD__PUBLIC:
				return isPublic();
			case JbcmmPackage.METHOD__PRIVATE:
				return isPrivate();
			case JbcmmPackage.METHOD__PROTECTED:
				return isProtected();
			case JbcmmPackage.METHOD__STATIC:
				return isStatic();
			case JbcmmPackage.METHOD__FINAL:
				return isFinal();
			case JbcmmPackage.METHOD__SYNTHETIC:
				return isSynthetic();
			case JbcmmPackage.METHOD__SYNCHRONIZED:
				return isSynchronized();
			case JbcmmPackage.METHOD__BRIDGE:
				return isBridge();
			case JbcmmPackage.METHOD__VAR_ARGS:
				return isVarArgs();
			case JbcmmPackage.METHOD__NATIVE:
				return isNative();
			case JbcmmPackage.METHOD__ABSTRACT:
				return isAbstract();
			case JbcmmPackage.METHOD__STRICT:
				return isStrict();
			case JbcmmPackage.METHOD__DEPRECATED:
				return isDeprecated();
			case JbcmmPackage.METHOD__RUNTIME_INVISIBLE_ANNOTATIONS:
				return getRuntimeInvisibleAnnotations();
			case JbcmmPackage.METHOD__RUNTIME_VISIBLE_ANNOTATIONS:
				return getRuntimeVisibleAnnotations();
			case JbcmmPackage.METHOD__ANNOTATION_DEFAULT_VALUE:
				return getAnnotationDefaultValue();
			case JbcmmPackage.METHOD__DESCRIPTOR:
				return getDescriptor();
			case JbcmmPackage.METHOD__SIGNATURE:
				return getSignature();
			case JbcmmPackage.METHOD__EXCEPTIONS_CAN_BE_THROWN:
				return getExceptionsCanBeThrown();
			case JbcmmPackage.METHOD__EXCEPTION_TABLE:
				return getExceptionTable();
			case JbcmmPackage.METHOD__LOCAL_VARIABLE_TABLE:
				return getLocalVariableTable();
			case JbcmmPackage.METHOD__RUNTIME_INVISIBLE_PARAMETER_ANNOTATIONS:
				return getRuntimeInvisibleParameterAnnotations();
			case JbcmmPackage.METHOD__RUNTIME_VISIBLE_PARAMETER_ANNOTATIONS:
				return getRuntimeVisibleParameterAnnotations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case JbcmmPackage.METHOD__CLASS:
				setClass((Clazz)newValue);
				return;
			case JbcmmPackage.METHOD__NAME:
				setName((String)newValue);
				return;
			case JbcmmPackage.METHOD__INSTRUCTIONS:
				getInstructions().clear();
				getInstructions().addAll((Collection<? extends Instruction>)newValue);
				return;
			case JbcmmPackage.METHOD__FIRST_INSTRUCTION:
				setFirstInstruction((Instruction)newValue);
				return;
			case JbcmmPackage.METHOD__PUBLIC:
				setPublic((Boolean)newValue);
				return;
			case JbcmmPackage.METHOD__PRIVATE:
				setPrivate((Boolean)newValue);
				return;
			case JbcmmPackage.METHOD__PROTECTED:
				setProtected((Boolean)newValue);
				return;
			case JbcmmPackage.METHOD__STATIC:
				setStatic((Boolean)newValue);
				return;
			case JbcmmPackage.METHOD__FINAL:
				setFinal((Boolean)newValue);
				return;
			case JbcmmPackage.METHOD__SYNTHETIC:
				setSynthetic((Boolean)newValue);
				return;
			case JbcmmPackage.METHOD__SYNCHRONIZED:
				setSynchronized((Boolean)newValue);
				return;
			case JbcmmPackage.METHOD__BRIDGE:
				setBridge((Boolean)newValue);
				return;
			case JbcmmPackage.METHOD__VAR_ARGS:
				setVarArgs((Boolean)newValue);
				return;
			case JbcmmPackage.METHOD__NATIVE:
				setNative((Boolean)newValue);
				return;
			case JbcmmPackage.METHOD__ABSTRACT:
				setAbstract((Boolean)newValue);
				return;
			case JbcmmPackage.METHOD__STRICT:
				setStrict((Boolean)newValue);
				return;
			case JbcmmPackage.METHOD__DEPRECATED:
				setDeprecated((Boolean)newValue);
				return;
			case JbcmmPackage.METHOD__RUNTIME_INVISIBLE_ANNOTATIONS:
				getRuntimeInvisibleAnnotations().clear();
				getRuntimeInvisibleAnnotations().addAll((Collection<? extends Annotation>)newValue);
				return;
			case JbcmmPackage.METHOD__RUNTIME_VISIBLE_ANNOTATIONS:
				getRuntimeVisibleAnnotations().clear();
				getRuntimeVisibleAnnotations().addAll((Collection<? extends Annotation>)newValue);
				return;
			case JbcmmPackage.METHOD__ANNOTATION_DEFAULT_VALUE:
				setAnnotationDefaultValue((ElementValue)newValue);
				return;
			case JbcmmPackage.METHOD__DESCRIPTOR:
				setDescriptor((MethodDescriptor)newValue);
				return;
			case JbcmmPackage.METHOD__SIGNATURE:
				setSignature((MethodSignature)newValue);
				return;
			case JbcmmPackage.METHOD__EXCEPTIONS_CAN_BE_THROWN:
				getExceptionsCanBeThrown().clear();
				getExceptionsCanBeThrown().addAll((Collection<? extends TypeReference>)newValue);
				return;
			case JbcmmPackage.METHOD__EXCEPTION_TABLE:
				getExceptionTable().clear();
				getExceptionTable().addAll((Collection<? extends ExceptionTableEntry>)newValue);
				return;
			case JbcmmPackage.METHOD__LOCAL_VARIABLE_TABLE:
				getLocalVariableTable().clear();
				getLocalVariableTable().addAll((Collection<? extends LocalVariableTableEntry>)newValue);
				return;
			case JbcmmPackage.METHOD__RUNTIME_INVISIBLE_PARAMETER_ANNOTATIONS:
				getRuntimeInvisibleParameterAnnotations().clear();
				getRuntimeInvisibleParameterAnnotations().addAll((Collection<? extends Annotation>)newValue);
				return;
			case JbcmmPackage.METHOD__RUNTIME_VISIBLE_PARAMETER_ANNOTATIONS:
				getRuntimeVisibleParameterAnnotations().clear();
				getRuntimeVisibleParameterAnnotations().addAll((Collection<? extends Annotation>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case JbcmmPackage.METHOD__CLASS:
				setClass((Clazz)null);
				return;
			case JbcmmPackage.METHOD__NAME:
				setName(NAME_EDEFAULT);
				return;
			case JbcmmPackage.METHOD__INSTRUCTIONS:
				getInstructions().clear();
				return;
			case JbcmmPackage.METHOD__FIRST_INSTRUCTION:
				setFirstInstruction((Instruction)null);
				return;
			case JbcmmPackage.METHOD__PUBLIC:
				setPublic(PUBLIC_EDEFAULT);
				return;
			case JbcmmPackage.METHOD__PRIVATE:
				setPrivate(PRIVATE_EDEFAULT);
				return;
			case JbcmmPackage.METHOD__PROTECTED:
				setProtected(PROTECTED_EDEFAULT);
				return;
			case JbcmmPackage.METHOD__STATIC:
				setStatic(STATIC_EDEFAULT);
				return;
			case JbcmmPackage.METHOD__FINAL:
				setFinal(FINAL_EDEFAULT);
				return;
			case JbcmmPackage.METHOD__SYNTHETIC:
				setSynthetic(SYNTHETIC_EDEFAULT);
				return;
			case JbcmmPackage.METHOD__SYNCHRONIZED:
				setSynchronized(SYNCHRONIZED_EDEFAULT);
				return;
			case JbcmmPackage.METHOD__BRIDGE:
				setBridge(BRIDGE_EDEFAULT);
				return;
			case JbcmmPackage.METHOD__VAR_ARGS:
				setVarArgs(VAR_ARGS_EDEFAULT);
				return;
			case JbcmmPackage.METHOD__NATIVE:
				setNative(NATIVE_EDEFAULT);
				return;
			case JbcmmPackage.METHOD__ABSTRACT:
				setAbstract(ABSTRACT_EDEFAULT);
				return;
			case JbcmmPackage.METHOD__STRICT:
				setStrict(STRICT_EDEFAULT);
				return;
			case JbcmmPackage.METHOD__DEPRECATED:
				setDeprecated(DEPRECATED_EDEFAULT);
				return;
			case JbcmmPackage.METHOD__RUNTIME_INVISIBLE_ANNOTATIONS:
				getRuntimeInvisibleAnnotations().clear();
				return;
			case JbcmmPackage.METHOD__RUNTIME_VISIBLE_ANNOTATIONS:
				getRuntimeVisibleAnnotations().clear();
				return;
			case JbcmmPackage.METHOD__ANNOTATION_DEFAULT_VALUE:
				setAnnotationDefaultValue((ElementValue)null);
				return;
			case JbcmmPackage.METHOD__DESCRIPTOR:
				setDescriptor((MethodDescriptor)null);
				return;
			case JbcmmPackage.METHOD__SIGNATURE:
				setSignature((MethodSignature)null);
				return;
			case JbcmmPackage.METHOD__EXCEPTIONS_CAN_BE_THROWN:
				getExceptionsCanBeThrown().clear();
				return;
			case JbcmmPackage.METHOD__EXCEPTION_TABLE:
				getExceptionTable().clear();
				return;
			case JbcmmPackage.METHOD__LOCAL_VARIABLE_TABLE:
				getLocalVariableTable().clear();
				return;
			case JbcmmPackage.METHOD__RUNTIME_INVISIBLE_PARAMETER_ANNOTATIONS:
				getRuntimeInvisibleParameterAnnotations().clear();
				return;
			case JbcmmPackage.METHOD__RUNTIME_VISIBLE_PARAMETER_ANNOTATIONS:
				getRuntimeVisibleParameterAnnotations().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case JbcmmPackage.METHOD__CLASS:
				return getClass_() != null;
			case JbcmmPackage.METHOD__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case JbcmmPackage.METHOD__INSTRUCTIONS:
				return instructions != null && !instructions.isEmpty();
			case JbcmmPackage.METHOD__FIRST_INSTRUCTION:
				return firstInstruction != null;
			case JbcmmPackage.METHOD__PUBLIC:
				return public_ != PUBLIC_EDEFAULT;
			case JbcmmPackage.METHOD__PRIVATE:
				return private_ != PRIVATE_EDEFAULT;
			case JbcmmPackage.METHOD__PROTECTED:
				return protected_ != PROTECTED_EDEFAULT;
			case JbcmmPackage.METHOD__STATIC:
				return static_ != STATIC_EDEFAULT;
			case JbcmmPackage.METHOD__FINAL:
				return final_ != FINAL_EDEFAULT;
			case JbcmmPackage.METHOD__SYNTHETIC:
				return synthetic != SYNTHETIC_EDEFAULT;
			case JbcmmPackage.METHOD__SYNCHRONIZED:
				return synchronized_ != SYNCHRONIZED_EDEFAULT;
			case JbcmmPackage.METHOD__BRIDGE:
				return bridge != BRIDGE_EDEFAULT;
			case JbcmmPackage.METHOD__VAR_ARGS:
				return varArgs != VAR_ARGS_EDEFAULT;
			case JbcmmPackage.METHOD__NATIVE:
				return native_ != NATIVE_EDEFAULT;
			case JbcmmPackage.METHOD__ABSTRACT:
				return abstract_ != ABSTRACT_EDEFAULT;
			case JbcmmPackage.METHOD__STRICT:
				return strict != STRICT_EDEFAULT;
			case JbcmmPackage.METHOD__DEPRECATED:
				return deprecated != DEPRECATED_EDEFAULT;
			case JbcmmPackage.METHOD__RUNTIME_INVISIBLE_ANNOTATIONS:
				return runtimeInvisibleAnnotations != null && !runtimeInvisibleAnnotations.isEmpty();
			case JbcmmPackage.METHOD__RUNTIME_VISIBLE_ANNOTATIONS:
				return runtimeVisibleAnnotations != null && !runtimeVisibleAnnotations.isEmpty();
			case JbcmmPackage.METHOD__ANNOTATION_DEFAULT_VALUE:
				return annotationDefaultValue != null;
			case JbcmmPackage.METHOD__DESCRIPTOR:
				return descriptor != null;
			case JbcmmPackage.METHOD__SIGNATURE:
				return signature != null;
			case JbcmmPackage.METHOD__EXCEPTIONS_CAN_BE_THROWN:
				return exceptionsCanBeThrown != null && !exceptionsCanBeThrown.isEmpty();
			case JbcmmPackage.METHOD__EXCEPTION_TABLE:
				return exceptionTable != null && !exceptionTable.isEmpty();
			case JbcmmPackage.METHOD__LOCAL_VARIABLE_TABLE:
				return localVariableTable != null && !localVariableTable.isEmpty();
			case JbcmmPackage.METHOD__RUNTIME_INVISIBLE_PARAMETER_ANNOTATIONS:
				return runtimeInvisibleParameterAnnotations != null && !runtimeInvisibleParameterAnnotations.isEmpty();
			case JbcmmPackage.METHOD__RUNTIME_VISIBLE_PARAMETER_ANNOTATIONS:
				return runtimeVisibleParameterAnnotations != null && !runtimeVisibleParameterAnnotations.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", public: ");
		result.append(public_);
		result.append(", private: ");
		result.append(private_);
		result.append(", protected: ");
		result.append(protected_);
		result.append(", static: ");
		result.append(static_);
		result.append(", final: ");
		result.append(final_);
		result.append(", synthetic: ");
		result.append(synthetic);
		result.append(", synchronized: ");
		result.append(synchronized_);
		result.append(", bridge: ");
		result.append(bridge);
		result.append(", varArgs: ");
		result.append(varArgs);
		result.append(", native: ");
		result.append(native_);
		result.append(", abstract: ");
		result.append(abstract_);
		result.append(", strict: ");
		result.append(strict);
		result.append(", deprecated: ");
		result.append(deprecated);
		result.append(')');
		return result.toString();
	}

} //MethodImpl
