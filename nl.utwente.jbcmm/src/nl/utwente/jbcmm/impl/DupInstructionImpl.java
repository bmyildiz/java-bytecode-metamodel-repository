/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.DupInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dup Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DupInstructionImpl extends SimpleInstructionImpl implements DupInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DupInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getDupInstruction();
	}

} //DupInstructionImpl
