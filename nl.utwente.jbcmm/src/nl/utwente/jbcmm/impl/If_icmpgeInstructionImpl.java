/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.If_icmpgeInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>If icmpge Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class If_icmpgeInstructionImpl extends JumpInstructionImpl implements If_icmpgeInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected If_icmpgeInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIf_icmpgeInstruction();
	}

} //If_icmpgeInstructionImpl
