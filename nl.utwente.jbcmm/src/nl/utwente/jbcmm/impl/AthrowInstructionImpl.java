/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.AthrowInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Athrow Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AthrowInstructionImpl extends SimpleInstructionImpl implements AthrowInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AthrowInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getAthrowInstruction();
	}

} //AthrowInstructionImpl
