/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.AreturnInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Areturn Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AreturnInstructionImpl extends SimpleInstructionImpl implements AreturnInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AreturnInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getAreturnInstruction();
	}

} //AreturnInstructionImpl
