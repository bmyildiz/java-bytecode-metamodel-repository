/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.InegInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ineg Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class InegInstructionImpl extends SimpleInstructionImpl implements InegInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InegInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getInegInstruction();
	}

} //InegInstructionImpl
