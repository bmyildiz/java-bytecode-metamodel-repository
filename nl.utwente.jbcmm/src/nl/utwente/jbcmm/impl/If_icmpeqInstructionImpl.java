/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.If_icmpeqInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>If icmpeq Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class If_icmpeqInstructionImpl extends JumpInstructionImpl implements If_icmpeqInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected If_icmpeqInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIf_icmpeqInstruction();
	}

} //If_icmpeqInstructionImpl
