/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.FdivInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Fdiv Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FdivInstructionImpl extends SimpleInstructionImpl implements FdivInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FdivInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getFdivInstruction();
	}

} //FdivInstructionImpl
