/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.AnewarrayInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Anewarray Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AnewarrayInstructionImpl extends TypeInstructionImpl implements AnewarrayInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AnewarrayInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getAnewarrayInstruction();
	}

} //AnewarrayInstructionImpl
