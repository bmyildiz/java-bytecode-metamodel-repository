/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.Iconst_m1Instruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Iconst m1 Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class Iconst_m1InstructionImpl extends SimpleInstructionImpl implements Iconst_m1Instruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Iconst_m1InstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIconst_m1Instruction();
	}

} //Iconst_m1InstructionImpl
