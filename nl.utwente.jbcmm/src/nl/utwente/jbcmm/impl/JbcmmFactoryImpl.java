/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class JbcmmFactoryImpl extends EFactoryImpl implements JbcmmFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static JbcmmFactory init() {
		try {
			JbcmmFactory theJbcmmFactory = (JbcmmFactory)EPackage.Registry.INSTANCE.getEFactory(JbcmmPackage.eNS_URI);
			if (theJbcmmFactory != null) {
				return theJbcmmFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new JbcmmFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JbcmmFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case JbcmmPackage.IDENTIFIABLE: return createIdentifiable();
			case JbcmmPackage.PROJECT: return createProject();
			case JbcmmPackage.CLAZZ: return createClazz();
			case JbcmmPackage.CLASS_SIGNATURE: return createClassSignature();
			case JbcmmPackage.METHOD_SIGNATURE: return createMethodSignature();
			case JbcmmPackage.FIELD_TYPE_SIGNATURE: return createFieldTypeSignature();
			case JbcmmPackage.TYPE_REFERENCE: return createTypeReference();
			case JbcmmPackage.METHOD_DESCRIPTOR: return createMethodDescriptor();
			case JbcmmPackage.METHOD_REFERENCE: return createMethodReference();
			case JbcmmPackage.FIELD_REFERENCE: return createFieldReference();
			case JbcmmPackage.FIELD: return createField();
			case JbcmmPackage.ANNOTATION: return createAnnotation();
			case JbcmmPackage.ELEMENT_VALUE_PAIR: return createElementValuePair();
			case JbcmmPackage.ELEMENT_VALUE: return createElementValue();
			case JbcmmPackage.ELEMENTARY_VALUE: return createElementaryValue();
			case JbcmmPackage.BOOLEAN_VALUE: return createBooleanValue();
			case JbcmmPackage.CHAR_VALUE: return createCharValue();
			case JbcmmPackage.BYTE_VALUE: return createByteValue();
			case JbcmmPackage.SHORT_VALUE: return createShortValue();
			case JbcmmPackage.INT_VALUE: return createIntValue();
			case JbcmmPackage.LONG_VALUE: return createLongValue();
			case JbcmmPackage.FLOAT_VALUE: return createFloatValue();
			case JbcmmPackage.DOUBLE_VALUE: return createDoubleValue();
			case JbcmmPackage.STRING_VALUE: return createStringValue();
			case JbcmmPackage.ENUM_VALUE: return createEnumValue();
			case JbcmmPackage.METHOD: return createMethod();
			case JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY: return createLocalVariableTableEntry();
			case JbcmmPackage.EXCEPTION_TABLE_ENTRY: return createExceptionTableEntry();
			case JbcmmPackage.UNCONDITIONAL_EDGE: return createUnconditionalEdge();
			case JbcmmPackage.CONDITIONAL_EDGE: return createConditionalEdge();
			case JbcmmPackage.SWITCH_CASE_EDGE: return createSwitchCaseEdge();
			case JbcmmPackage.SWITCH_DEFAULT_EDGE: return createSwitchDefaultEdge();
			case JbcmmPackage.EXCEPTIONAL_EDGE: return createExceptionalEdge();
			case JbcmmPackage.IINC_INSTRUCTION: return createIincInstruction();
			case JbcmmPackage.MULTIANEWARRAY_INSTRUCTION: return createMultianewarrayInstruction();
			case JbcmmPackage.SWITCH_INSTRUCTION: return createSwitchInstruction();
			case JbcmmPackage.LDC_INT_INSTRUCTION: return createLdcIntInstruction();
			case JbcmmPackage.LDC_LONG_INSTRUCTION: return createLdcLongInstruction();
			case JbcmmPackage.LDC_FLOAT_INSTRUCTION: return createLdcFloatInstruction();
			case JbcmmPackage.LDC_DOUBLE_INSTRUCTION: return createLdcDoubleInstruction();
			case JbcmmPackage.LDC_STRING_INSTRUCTION: return createLdcStringInstruction();
			case JbcmmPackage.LDC_TYPE_INSTRUCTION: return createLdcTypeInstruction();
			case JbcmmPackage.GETSTATIC_INSTRUCTION: return createGetstaticInstruction();
			case JbcmmPackage.PUTSTATIC_INSTRUCTION: return createPutstaticInstruction();
			case JbcmmPackage.GETFIELD_INSTRUCTION: return createGetfieldInstruction();
			case JbcmmPackage.PUTFIELD_INSTRUCTION: return createPutfieldInstruction();
			case JbcmmPackage.NOP_INSTRUCTION: return createNopInstruction();
			case JbcmmPackage.ACONST_NULL_INSTRUCTION: return createAconst_nullInstruction();
			case JbcmmPackage.ICONST_M1_INSTRUCTION: return createIconst_m1Instruction();
			case JbcmmPackage.ICONST_0INSTRUCTION: return createIconst_0Instruction();
			case JbcmmPackage.ICONST_1INSTRUCTION: return createIconst_1Instruction();
			case JbcmmPackage.ICONST_2INSTRUCTION: return createIconst_2Instruction();
			case JbcmmPackage.ICONST_3INSTRUCTION: return createIconst_3Instruction();
			case JbcmmPackage.ICONST_4INSTRUCTION: return createIconst_4Instruction();
			case JbcmmPackage.ICONST_5INSTRUCTION: return createIconst_5Instruction();
			case JbcmmPackage.LCONST_0INSTRUCTION: return createLconst_0Instruction();
			case JbcmmPackage.LCONST_1INSTRUCTION: return createLconst_1Instruction();
			case JbcmmPackage.FCONST_0INSTRUCTION: return createFconst_0Instruction();
			case JbcmmPackage.FCONST_1INSTRUCTION: return createFconst_1Instruction();
			case JbcmmPackage.FCONST_2INSTRUCTION: return createFconst_2Instruction();
			case JbcmmPackage.DCONST_0INSTRUCTION: return createDconst_0Instruction();
			case JbcmmPackage.DCONST_1INSTRUCTION: return createDconst_1Instruction();
			case JbcmmPackage.IALOAD_INSTRUCTION: return createIaloadInstruction();
			case JbcmmPackage.LALOAD_INSTRUCTION: return createLaloadInstruction();
			case JbcmmPackage.FALOAD_INSTRUCTION: return createFaloadInstruction();
			case JbcmmPackage.DALOAD_INSTRUCTION: return createDaloadInstruction();
			case JbcmmPackage.AALOAD_INSTRUCTION: return createAaloadInstruction();
			case JbcmmPackage.BALOAD_INSTRUCTION: return createBaloadInstruction();
			case JbcmmPackage.CALOAD_INSTRUCTION: return createCaloadInstruction();
			case JbcmmPackage.SALOAD_INSTRUCTION: return createSaloadInstruction();
			case JbcmmPackage.IASTORE_INSTRUCTION: return createIastoreInstruction();
			case JbcmmPackage.LASTORE_INSTRUCTION: return createLastoreInstruction();
			case JbcmmPackage.FASTORE_INSTRUCTION: return createFastoreInstruction();
			case JbcmmPackage.DASTORE_INSTRUCTION: return createDastoreInstruction();
			case JbcmmPackage.AASTORE_INSTRUCTION: return createAastoreInstruction();
			case JbcmmPackage.BASTORE_INSTRUCTION: return createBastoreInstruction();
			case JbcmmPackage.CASTORE_INSTRUCTION: return createCastoreInstruction();
			case JbcmmPackage.SASTORE_INSTRUCTION: return createSastoreInstruction();
			case JbcmmPackage.POP_INSTRUCTION: return createPopInstruction();
			case JbcmmPackage.POP2_INSTRUCTION: return createPop2Instruction();
			case JbcmmPackage.DUP_INSTRUCTION: return createDupInstruction();
			case JbcmmPackage.DUP_X1_INSTRUCTION: return createDup_x1Instruction();
			case JbcmmPackage.DUP_X2_INSTRUCTION: return createDup_x2Instruction();
			case JbcmmPackage.DUP2_INSTRUCTION: return createDup2Instruction();
			case JbcmmPackage.DUP2_X1_INSTRUCTION: return createDup2_x1Instruction();
			case JbcmmPackage.DUP2_X2_INSTRUCTION: return createDup2_x2Instruction();
			case JbcmmPackage.SWAP_INSTRUCTION: return createSwapInstruction();
			case JbcmmPackage.IADD_INSTRUCTION: return createIaddInstruction();
			case JbcmmPackage.LADD_INSTRUCTION: return createLaddInstruction();
			case JbcmmPackage.FADD_INSTRUCTION: return createFaddInstruction();
			case JbcmmPackage.DADD_INSTRUCTION: return createDaddInstruction();
			case JbcmmPackage.ISUB_INSTRUCTION: return createIsubInstruction();
			case JbcmmPackage.LSUB_INSTRUCTION: return createLsubInstruction();
			case JbcmmPackage.FSUB_INSTRUCTION: return createFsubInstruction();
			case JbcmmPackage.DSUB_INSTRUCTION: return createDsubInstruction();
			case JbcmmPackage.IMUL_INSTRUCTION: return createImulInstruction();
			case JbcmmPackage.LMUL_INSTRUCTION: return createLmulInstruction();
			case JbcmmPackage.FMUL_INSTRUCTION: return createFmulInstruction();
			case JbcmmPackage.DMUL_INSTRUCTION: return createDmulInstruction();
			case JbcmmPackage.IDIV_INSTRUCTION: return createIdivInstruction();
			case JbcmmPackage.LDIV_INSTRUCTION: return createLdivInstruction();
			case JbcmmPackage.FDIV_INSTRUCTION: return createFdivInstruction();
			case JbcmmPackage.DDIV_INSTRUCTION: return createDdivInstruction();
			case JbcmmPackage.IREM_INSTRUCTION: return createIremInstruction();
			case JbcmmPackage.LREM_INSTRUCTION: return createLremInstruction();
			case JbcmmPackage.FREM_INSTRUCTION: return createFremInstruction();
			case JbcmmPackage.DREM_INSTRUCTION: return createDremInstruction();
			case JbcmmPackage.INEG_INSTRUCTION: return createInegInstruction();
			case JbcmmPackage.LNEG_INSTRUCTION: return createLnegInstruction();
			case JbcmmPackage.FNEG_INSTRUCTION: return createFnegInstruction();
			case JbcmmPackage.DNEG_INSTRUCTION: return createDnegInstruction();
			case JbcmmPackage.ISHL_INSTRUCTION: return createIshlInstruction();
			case JbcmmPackage.LSHL_INSTRUCTION: return createLshlInstruction();
			case JbcmmPackage.ISHR_INSTRUCTION: return createIshrInstruction();
			case JbcmmPackage.LSHR_INSTRUCTION: return createLshrInstruction();
			case JbcmmPackage.IUSHR_INSTRUCTION: return createIushrInstruction();
			case JbcmmPackage.LUSHR_INSTRUCTION: return createLushrInstruction();
			case JbcmmPackage.IAND_INSTRUCTION: return createIandInstruction();
			case JbcmmPackage.LAND_INSTRUCTION: return createLandInstruction();
			case JbcmmPackage.IOR_INSTRUCTION: return createIorInstruction();
			case JbcmmPackage.LOR_INSTRUCTION: return createLorInstruction();
			case JbcmmPackage.IXOR_INSTRUCTION: return createIxorInstruction();
			case JbcmmPackage.LXOR_INSTRUCTION: return createLxorInstruction();
			case JbcmmPackage.I2L_INSTRUCTION: return createI2lInstruction();
			case JbcmmPackage.I2F_INSTRUCTION: return createI2fInstruction();
			case JbcmmPackage.I2D_INSTRUCTION: return createI2dInstruction();
			case JbcmmPackage.L2I_INSTRUCTION: return createL2iInstruction();
			case JbcmmPackage.L2F_INSTRUCTION: return createL2fInstruction();
			case JbcmmPackage.L2D_INSTRUCTION: return createL2dInstruction();
			case JbcmmPackage.F2I_INSTRUCTION: return createF2iInstruction();
			case JbcmmPackage.F2L_INSTRUCTION: return createF2lInstruction();
			case JbcmmPackage.F2D_INSTRUCTION: return createF2dInstruction();
			case JbcmmPackage.D2I_INSTRUCTION: return createD2iInstruction();
			case JbcmmPackage.D2L_INSTRUCTION: return createD2lInstruction();
			case JbcmmPackage.D2F_INSTRUCTION: return createD2fInstruction();
			case JbcmmPackage.I2B_INSTRUCTION: return createI2bInstruction();
			case JbcmmPackage.I2C_INSTRUCTION: return createI2cInstruction();
			case JbcmmPackage.I2S_INSTRUCTION: return createI2sInstruction();
			case JbcmmPackage.LCMP_INSTRUCTION: return createLcmpInstruction();
			case JbcmmPackage.FCMPL_INSTRUCTION: return createFcmplInstruction();
			case JbcmmPackage.FCMPG_INSTRUCTION: return createFcmpgInstruction();
			case JbcmmPackage.DCMPL_INSTRUCTION: return createDcmplInstruction();
			case JbcmmPackage.DCMPG_INSTRUCTION: return createDcmpgInstruction();
			case JbcmmPackage.IRETURN_INSTRUCTION: return createIreturnInstruction();
			case JbcmmPackage.LRETURN_INSTRUCTION: return createLreturnInstruction();
			case JbcmmPackage.FRETURN_INSTRUCTION: return createFreturnInstruction();
			case JbcmmPackage.DRETURN_INSTRUCTION: return createDreturnInstruction();
			case JbcmmPackage.ARETURN_INSTRUCTION: return createAreturnInstruction();
			case JbcmmPackage.RETURN_INSTRUCTION: return createReturnInstruction();
			case JbcmmPackage.ARRAYLENGTH_INSTRUCTION: return createArraylengthInstruction();
			case JbcmmPackage.ATHROW_INSTRUCTION: return createAthrowInstruction();
			case JbcmmPackage.MONITORENTER_INSTRUCTION: return createMonitorenterInstruction();
			case JbcmmPackage.MONITOREXIT_INSTRUCTION: return createMonitorexitInstruction();
			case JbcmmPackage.BIPUSH_INSTRUCTION: return createBipushInstruction();
			case JbcmmPackage.SIPUSH_INSTRUCTION: return createSipushInstruction();
			case JbcmmPackage.NEWARRAY_INSTRUCTION: return createNewarrayInstruction();
			case JbcmmPackage.IFEQ_INSTRUCTION: return createIfeqInstruction();
			case JbcmmPackage.IFNE_INSTRUCTION: return createIfneInstruction();
			case JbcmmPackage.IFLT_INSTRUCTION: return createIfltInstruction();
			case JbcmmPackage.IFGE_INSTRUCTION: return createIfgeInstruction();
			case JbcmmPackage.IFGT_INSTRUCTION: return createIfgtInstruction();
			case JbcmmPackage.IFLE_INSTRUCTION: return createIfleInstruction();
			case JbcmmPackage.IF_ICMPEQ_INSTRUCTION: return createIf_icmpeqInstruction();
			case JbcmmPackage.IF_ICMPNE_INSTRUCTION: return createIf_icmpneInstruction();
			case JbcmmPackage.IF_ICMPLT_INSTRUCTION: return createIf_icmpltInstruction();
			case JbcmmPackage.IF_ICMPGE_INSTRUCTION: return createIf_icmpgeInstruction();
			case JbcmmPackage.IF_ICMPGT_INSTRUCTION: return createIf_icmpgtInstruction();
			case JbcmmPackage.IF_ICMPLE_INSTRUCTION: return createIf_icmpleInstruction();
			case JbcmmPackage.IF_ACMPEQ_INSTRUCTION: return createIf_acmpeqInstruction();
			case JbcmmPackage.IF_ACMPNE_INSTRUCTION: return createIf_acmpneInstruction();
			case JbcmmPackage.GOTO_INSTRUCTION: return createGotoInstruction();
			case JbcmmPackage.JSR_INSTRUCTION: return createJsrInstruction();
			case JbcmmPackage.IFNULL_INSTRUCTION: return createIfnullInstruction();
			case JbcmmPackage.IFNONNULL_INSTRUCTION: return createIfnonnullInstruction();
			case JbcmmPackage.INVOKEVIRTUAL_INSTRUCTION: return createInvokevirtualInstruction();
			case JbcmmPackage.INVOKESPECIAL_INSTRUCTION: return createInvokespecialInstruction();
			case JbcmmPackage.INVOKESTATIC_INSTRUCTION: return createInvokestaticInstruction();
			case JbcmmPackage.INVOKEINTERFACE_INSTRUCTION: return createInvokeinterfaceInstruction();
			case JbcmmPackage.NEW_INSTRUCTION: return createNewInstruction();
			case JbcmmPackage.ANEWARRAY_INSTRUCTION: return createAnewarrayInstruction();
			case JbcmmPackage.CHECKCAST_INSTRUCTION: return createCheckcastInstruction();
			case JbcmmPackage.INSTANCEOF_INSTRUCTION: return createInstanceofInstruction();
			case JbcmmPackage.ILOAD_INSTRUCTION: return createIloadInstruction();
			case JbcmmPackage.LLOAD_INSTRUCTION: return createLloadInstruction();
			case JbcmmPackage.FLOAD_INSTRUCTION: return createFloadInstruction();
			case JbcmmPackage.DLOAD_INSTRUCTION: return createDloadInstruction();
			case JbcmmPackage.ALOAD_INSTRUCTION: return createAloadInstruction();
			case JbcmmPackage.ISTORE_INSTRUCTION: return createIstoreInstruction();
			case JbcmmPackage.LSTORE_INSTRUCTION: return createLstoreInstruction();
			case JbcmmPackage.FSTORE_INSTRUCTION: return createFstoreInstruction();
			case JbcmmPackage.DSTORE_INSTRUCTION: return createDstoreInstruction();
			case JbcmmPackage.ASTORE_INSTRUCTION: return createAstoreInstruction();
			case JbcmmPackage.RET_INSTRUCTION: return createRetInstruction();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Identifiable createIdentifiable() {
		IdentifiableImpl identifiable = new IdentifiableImpl();
		return identifiable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Project createProject() {
		ProjectImpl project = new ProjectImpl();
		return project;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Clazz createClazz() {
		ClazzImpl clazz = new ClazzImpl();
		return clazz;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassSignature createClassSignature() {
		ClassSignatureImpl classSignature = new ClassSignatureImpl();
		return classSignature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MethodSignature createMethodSignature() {
		MethodSignatureImpl methodSignature = new MethodSignatureImpl();
		return methodSignature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FieldTypeSignature createFieldTypeSignature() {
		FieldTypeSignatureImpl fieldTypeSignature = new FieldTypeSignatureImpl();
		return fieldTypeSignature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeReference createTypeReference() {
		TypeReferenceImpl typeReference = new TypeReferenceImpl();
		return typeReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MethodDescriptor createMethodDescriptor() {
		MethodDescriptorImpl methodDescriptor = new MethodDescriptorImpl();
		return methodDescriptor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MethodReference createMethodReference() {
		MethodReferenceImpl methodReference = new MethodReferenceImpl();
		return methodReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FieldReference createFieldReference() {
		FieldReferenceImpl fieldReference = new FieldReferenceImpl();
		return fieldReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Field createField() {
		FieldImpl field = new FieldImpl();
		return field;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Annotation createAnnotation() {
		AnnotationImpl annotation = new AnnotationImpl();
		return annotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementValuePair createElementValuePair() {
		ElementValuePairImpl elementValuePair = new ElementValuePairImpl();
		return elementValuePair;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementValue createElementValue() {
		ElementValueImpl elementValue = new ElementValueImpl();
		return elementValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementaryValue createElementaryValue() {
		ElementaryValueImpl elementaryValue = new ElementaryValueImpl();
		return elementaryValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BooleanValue createBooleanValue() {
		BooleanValueImpl booleanValue = new BooleanValueImpl();
		return booleanValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CharValue createCharValue() {
		CharValueImpl charValue = new CharValueImpl();
		return charValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ByteValue createByteValue() {
		ByteValueImpl byteValue = new ByteValueImpl();
		return byteValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShortValue createShortValue() {
		ShortValueImpl shortValue = new ShortValueImpl();
		return shortValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntValue createIntValue() {
		IntValueImpl intValue = new IntValueImpl();
		return intValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LongValue createLongValue() {
		LongValueImpl longValue = new LongValueImpl();
		return longValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FloatValue createFloatValue() {
		FloatValueImpl floatValue = new FloatValueImpl();
		return floatValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DoubleValue createDoubleValue() {
		DoubleValueImpl doubleValue = new DoubleValueImpl();
		return doubleValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StringValue createStringValue() {
		StringValueImpl stringValue = new StringValueImpl();
		return stringValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumValue createEnumValue() {
		EnumValueImpl enumValue = new EnumValueImpl();
		return enumValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Method createMethod() {
		MethodImpl method = new MethodImpl();
		return method;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocalVariableTableEntry createLocalVariableTableEntry() {
		LocalVariableTableEntryImpl localVariableTableEntry = new LocalVariableTableEntryImpl();
		return localVariableTableEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExceptionTableEntry createExceptionTableEntry() {
		ExceptionTableEntryImpl exceptionTableEntry = new ExceptionTableEntryImpl();
		return exceptionTableEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnconditionalEdge createUnconditionalEdge() {
		UnconditionalEdgeImpl unconditionalEdge = new UnconditionalEdgeImpl();
		return unconditionalEdge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConditionalEdge createConditionalEdge() {
		ConditionalEdgeImpl conditionalEdge = new ConditionalEdgeImpl();
		return conditionalEdge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SwitchCaseEdge createSwitchCaseEdge() {
		SwitchCaseEdgeImpl switchCaseEdge = new SwitchCaseEdgeImpl();
		return switchCaseEdge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SwitchDefaultEdge createSwitchDefaultEdge() {
		SwitchDefaultEdgeImpl switchDefaultEdge = new SwitchDefaultEdgeImpl();
		return switchDefaultEdge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExceptionalEdge createExceptionalEdge() {
		ExceptionalEdgeImpl exceptionalEdge = new ExceptionalEdgeImpl();
		return exceptionalEdge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IincInstruction createIincInstruction() {
		IincInstructionImpl iincInstruction = new IincInstructionImpl();
		return iincInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MultianewarrayInstruction createMultianewarrayInstruction() {
		MultianewarrayInstructionImpl multianewarrayInstruction = new MultianewarrayInstructionImpl();
		return multianewarrayInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SwitchInstruction createSwitchInstruction() {
		SwitchInstructionImpl switchInstruction = new SwitchInstructionImpl();
		return switchInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LdcIntInstruction createLdcIntInstruction() {
		LdcIntInstructionImpl ldcIntInstruction = new LdcIntInstructionImpl();
		return ldcIntInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LdcLongInstruction createLdcLongInstruction() {
		LdcLongInstructionImpl ldcLongInstruction = new LdcLongInstructionImpl();
		return ldcLongInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LdcFloatInstruction createLdcFloatInstruction() {
		LdcFloatInstructionImpl ldcFloatInstruction = new LdcFloatInstructionImpl();
		return ldcFloatInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LdcDoubleInstruction createLdcDoubleInstruction() {
		LdcDoubleInstructionImpl ldcDoubleInstruction = new LdcDoubleInstructionImpl();
		return ldcDoubleInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LdcStringInstruction createLdcStringInstruction() {
		LdcStringInstructionImpl ldcStringInstruction = new LdcStringInstructionImpl();
		return ldcStringInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LdcTypeInstruction createLdcTypeInstruction() {
		LdcTypeInstructionImpl ldcTypeInstruction = new LdcTypeInstructionImpl();
		return ldcTypeInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetstaticInstruction createGetstaticInstruction() {
		GetstaticInstructionImpl getstaticInstruction = new GetstaticInstructionImpl();
		return getstaticInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PutstaticInstruction createPutstaticInstruction() {
		PutstaticInstructionImpl putstaticInstruction = new PutstaticInstructionImpl();
		return putstaticInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GetfieldInstruction createGetfieldInstruction() {
		GetfieldInstructionImpl getfieldInstruction = new GetfieldInstructionImpl();
		return getfieldInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PutfieldInstruction createPutfieldInstruction() {
		PutfieldInstructionImpl putfieldInstruction = new PutfieldInstructionImpl();
		return putfieldInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NopInstruction createNopInstruction() {
		NopInstructionImpl nopInstruction = new NopInstructionImpl();
		return nopInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Aconst_nullInstruction createAconst_nullInstruction() {
		Aconst_nullInstructionImpl aconst_nullInstruction = new Aconst_nullInstructionImpl();
		return aconst_nullInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Iconst_m1Instruction createIconst_m1Instruction() {
		Iconst_m1InstructionImpl iconst_m1Instruction = new Iconst_m1InstructionImpl();
		return iconst_m1Instruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Iconst_0Instruction createIconst_0Instruction() {
		Iconst_0InstructionImpl iconst_0Instruction = new Iconst_0InstructionImpl();
		return iconst_0Instruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Iconst_1Instruction createIconst_1Instruction() {
		Iconst_1InstructionImpl iconst_1Instruction = new Iconst_1InstructionImpl();
		return iconst_1Instruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Iconst_2Instruction createIconst_2Instruction() {
		Iconst_2InstructionImpl iconst_2Instruction = new Iconst_2InstructionImpl();
		return iconst_2Instruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Iconst_3Instruction createIconst_3Instruction() {
		Iconst_3InstructionImpl iconst_3Instruction = new Iconst_3InstructionImpl();
		return iconst_3Instruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Iconst_4Instruction createIconst_4Instruction() {
		Iconst_4InstructionImpl iconst_4Instruction = new Iconst_4InstructionImpl();
		return iconst_4Instruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Iconst_5Instruction createIconst_5Instruction() {
		Iconst_5InstructionImpl iconst_5Instruction = new Iconst_5InstructionImpl();
		return iconst_5Instruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Lconst_0Instruction createLconst_0Instruction() {
		Lconst_0InstructionImpl lconst_0Instruction = new Lconst_0InstructionImpl();
		return lconst_0Instruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Lconst_1Instruction createLconst_1Instruction() {
		Lconst_1InstructionImpl lconst_1Instruction = new Lconst_1InstructionImpl();
		return lconst_1Instruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Fconst_0Instruction createFconst_0Instruction() {
		Fconst_0InstructionImpl fconst_0Instruction = new Fconst_0InstructionImpl();
		return fconst_0Instruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Fconst_1Instruction createFconst_1Instruction() {
		Fconst_1InstructionImpl fconst_1Instruction = new Fconst_1InstructionImpl();
		return fconst_1Instruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Fconst_2Instruction createFconst_2Instruction() {
		Fconst_2InstructionImpl fconst_2Instruction = new Fconst_2InstructionImpl();
		return fconst_2Instruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dconst_0Instruction createDconst_0Instruction() {
		Dconst_0InstructionImpl dconst_0Instruction = new Dconst_0InstructionImpl();
		return dconst_0Instruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dconst_1Instruction createDconst_1Instruction() {
		Dconst_1InstructionImpl dconst_1Instruction = new Dconst_1InstructionImpl();
		return dconst_1Instruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IaloadInstruction createIaloadInstruction() {
		IaloadInstructionImpl ialoadInstruction = new IaloadInstructionImpl();
		return ialoadInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LaloadInstruction createLaloadInstruction() {
		LaloadInstructionImpl laloadInstruction = new LaloadInstructionImpl();
		return laloadInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FaloadInstruction createFaloadInstruction() {
		FaloadInstructionImpl faloadInstruction = new FaloadInstructionImpl();
		return faloadInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DaloadInstruction createDaloadInstruction() {
		DaloadInstructionImpl daloadInstruction = new DaloadInstructionImpl();
		return daloadInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AaloadInstruction createAaloadInstruction() {
		AaloadInstructionImpl aaloadInstruction = new AaloadInstructionImpl();
		return aaloadInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaloadInstruction createBaloadInstruction() {
		BaloadInstructionImpl baloadInstruction = new BaloadInstructionImpl();
		return baloadInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CaloadInstruction createCaloadInstruction() {
		CaloadInstructionImpl caloadInstruction = new CaloadInstructionImpl();
		return caloadInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SaloadInstruction createSaloadInstruction() {
		SaloadInstructionImpl saloadInstruction = new SaloadInstructionImpl();
		return saloadInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IastoreInstruction createIastoreInstruction() {
		IastoreInstructionImpl iastoreInstruction = new IastoreInstructionImpl();
		return iastoreInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LastoreInstruction createLastoreInstruction() {
		LastoreInstructionImpl lastoreInstruction = new LastoreInstructionImpl();
		return lastoreInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FastoreInstruction createFastoreInstruction() {
		FastoreInstructionImpl fastoreInstruction = new FastoreInstructionImpl();
		return fastoreInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DastoreInstruction createDastoreInstruction() {
		DastoreInstructionImpl dastoreInstruction = new DastoreInstructionImpl();
		return dastoreInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AastoreInstruction createAastoreInstruction() {
		AastoreInstructionImpl aastoreInstruction = new AastoreInstructionImpl();
		return aastoreInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BastoreInstruction createBastoreInstruction() {
		BastoreInstructionImpl bastoreInstruction = new BastoreInstructionImpl();
		return bastoreInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CastoreInstruction createCastoreInstruction() {
		CastoreInstructionImpl castoreInstruction = new CastoreInstructionImpl();
		return castoreInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SastoreInstruction createSastoreInstruction() {
		SastoreInstructionImpl sastoreInstruction = new SastoreInstructionImpl();
		return sastoreInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PopInstruction createPopInstruction() {
		PopInstructionImpl popInstruction = new PopInstructionImpl();
		return popInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Pop2Instruction createPop2Instruction() {
		Pop2InstructionImpl pop2Instruction = new Pop2InstructionImpl();
		return pop2Instruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DupInstruction createDupInstruction() {
		DupInstructionImpl dupInstruction = new DupInstructionImpl();
		return dupInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dup_x1Instruction createDup_x1Instruction() {
		Dup_x1InstructionImpl dup_x1Instruction = new Dup_x1InstructionImpl();
		return dup_x1Instruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dup_x2Instruction createDup_x2Instruction() {
		Dup_x2InstructionImpl dup_x2Instruction = new Dup_x2InstructionImpl();
		return dup_x2Instruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dup2Instruction createDup2Instruction() {
		Dup2InstructionImpl dup2Instruction = new Dup2InstructionImpl();
		return dup2Instruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dup2_x1Instruction createDup2_x1Instruction() {
		Dup2_x1InstructionImpl dup2_x1Instruction = new Dup2_x1InstructionImpl();
		return dup2_x1Instruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dup2_x2Instruction createDup2_x2Instruction() {
		Dup2_x2InstructionImpl dup2_x2Instruction = new Dup2_x2InstructionImpl();
		return dup2_x2Instruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SwapInstruction createSwapInstruction() {
		SwapInstructionImpl swapInstruction = new SwapInstructionImpl();
		return swapInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IaddInstruction createIaddInstruction() {
		IaddInstructionImpl iaddInstruction = new IaddInstructionImpl();
		return iaddInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LaddInstruction createLaddInstruction() {
		LaddInstructionImpl laddInstruction = new LaddInstructionImpl();
		return laddInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FaddInstruction createFaddInstruction() {
		FaddInstructionImpl faddInstruction = new FaddInstructionImpl();
		return faddInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DaddInstruction createDaddInstruction() {
		DaddInstructionImpl daddInstruction = new DaddInstructionImpl();
		return daddInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsubInstruction createIsubInstruction() {
		IsubInstructionImpl isubInstruction = new IsubInstructionImpl();
		return isubInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LsubInstruction createLsubInstruction() {
		LsubInstructionImpl lsubInstruction = new LsubInstructionImpl();
		return lsubInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FsubInstruction createFsubInstruction() {
		FsubInstructionImpl fsubInstruction = new FsubInstructionImpl();
		return fsubInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DsubInstruction createDsubInstruction() {
		DsubInstructionImpl dsubInstruction = new DsubInstructionImpl();
		return dsubInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImulInstruction createImulInstruction() {
		ImulInstructionImpl imulInstruction = new ImulInstructionImpl();
		return imulInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LmulInstruction createLmulInstruction() {
		LmulInstructionImpl lmulInstruction = new LmulInstructionImpl();
		return lmulInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FmulInstruction createFmulInstruction() {
		FmulInstructionImpl fmulInstruction = new FmulInstructionImpl();
		return fmulInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DmulInstruction createDmulInstruction() {
		DmulInstructionImpl dmulInstruction = new DmulInstructionImpl();
		return dmulInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdivInstruction createIdivInstruction() {
		IdivInstructionImpl idivInstruction = new IdivInstructionImpl();
		return idivInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LdivInstruction createLdivInstruction() {
		LdivInstructionImpl ldivInstruction = new LdivInstructionImpl();
		return ldivInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FdivInstruction createFdivInstruction() {
		FdivInstructionImpl fdivInstruction = new FdivInstructionImpl();
		return fdivInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DdivInstruction createDdivInstruction() {
		DdivInstructionImpl ddivInstruction = new DdivInstructionImpl();
		return ddivInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IremInstruction createIremInstruction() {
		IremInstructionImpl iremInstruction = new IremInstructionImpl();
		return iremInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LremInstruction createLremInstruction() {
		LremInstructionImpl lremInstruction = new LremInstructionImpl();
		return lremInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FremInstruction createFremInstruction() {
		FremInstructionImpl fremInstruction = new FremInstructionImpl();
		return fremInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DremInstruction createDremInstruction() {
		DremInstructionImpl dremInstruction = new DremInstructionImpl();
		return dremInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InegInstruction createInegInstruction() {
		InegInstructionImpl inegInstruction = new InegInstructionImpl();
		return inegInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LnegInstruction createLnegInstruction() {
		LnegInstructionImpl lnegInstruction = new LnegInstructionImpl();
		return lnegInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FnegInstruction createFnegInstruction() {
		FnegInstructionImpl fnegInstruction = new FnegInstructionImpl();
		return fnegInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DnegInstruction createDnegInstruction() {
		DnegInstructionImpl dnegInstruction = new DnegInstructionImpl();
		return dnegInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IshlInstruction createIshlInstruction() {
		IshlInstructionImpl ishlInstruction = new IshlInstructionImpl();
		return ishlInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LshlInstruction createLshlInstruction() {
		LshlInstructionImpl lshlInstruction = new LshlInstructionImpl();
		return lshlInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IshrInstruction createIshrInstruction() {
		IshrInstructionImpl ishrInstruction = new IshrInstructionImpl();
		return ishrInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LshrInstruction createLshrInstruction() {
		LshrInstructionImpl lshrInstruction = new LshrInstructionImpl();
		return lshrInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IushrInstruction createIushrInstruction() {
		IushrInstructionImpl iushrInstruction = new IushrInstructionImpl();
		return iushrInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LushrInstruction createLushrInstruction() {
		LushrInstructionImpl lushrInstruction = new LushrInstructionImpl();
		return lushrInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IandInstruction createIandInstruction() {
		IandInstructionImpl iandInstruction = new IandInstructionImpl();
		return iandInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LandInstruction createLandInstruction() {
		LandInstructionImpl landInstruction = new LandInstructionImpl();
		return landInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IorInstruction createIorInstruction() {
		IorInstructionImpl iorInstruction = new IorInstructionImpl();
		return iorInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LorInstruction createLorInstruction() {
		LorInstructionImpl lorInstruction = new LorInstructionImpl();
		return lorInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IxorInstruction createIxorInstruction() {
		IxorInstructionImpl ixorInstruction = new IxorInstructionImpl();
		return ixorInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LxorInstruction createLxorInstruction() {
		LxorInstructionImpl lxorInstruction = new LxorInstructionImpl();
		return lxorInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public I2lInstruction createI2lInstruction() {
		I2lInstructionImpl i2lInstruction = new I2lInstructionImpl();
		return i2lInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public I2fInstruction createI2fInstruction() {
		I2fInstructionImpl i2fInstruction = new I2fInstructionImpl();
		return i2fInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public I2dInstruction createI2dInstruction() {
		I2dInstructionImpl i2dInstruction = new I2dInstructionImpl();
		return i2dInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public L2iInstruction createL2iInstruction() {
		L2iInstructionImpl l2iInstruction = new L2iInstructionImpl();
		return l2iInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public L2fInstruction createL2fInstruction() {
		L2fInstructionImpl l2fInstruction = new L2fInstructionImpl();
		return l2fInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public L2dInstruction createL2dInstruction() {
		L2dInstructionImpl l2dInstruction = new L2dInstructionImpl();
		return l2dInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public F2iInstruction createF2iInstruction() {
		F2iInstructionImpl f2iInstruction = new F2iInstructionImpl();
		return f2iInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public F2lInstruction createF2lInstruction() {
		F2lInstructionImpl f2lInstruction = new F2lInstructionImpl();
		return f2lInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public F2dInstruction createF2dInstruction() {
		F2dInstructionImpl f2dInstruction = new F2dInstructionImpl();
		return f2dInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public D2iInstruction createD2iInstruction() {
		D2iInstructionImpl d2iInstruction = new D2iInstructionImpl();
		return d2iInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public D2lInstruction createD2lInstruction() {
		D2lInstructionImpl d2lInstruction = new D2lInstructionImpl();
		return d2lInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public D2fInstruction createD2fInstruction() {
		D2fInstructionImpl d2fInstruction = new D2fInstructionImpl();
		return d2fInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public I2bInstruction createI2bInstruction() {
		I2bInstructionImpl i2bInstruction = new I2bInstructionImpl();
		return i2bInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public I2cInstruction createI2cInstruction() {
		I2cInstructionImpl i2cInstruction = new I2cInstructionImpl();
		return i2cInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public I2sInstruction createI2sInstruction() {
		I2sInstructionImpl i2sInstruction = new I2sInstructionImpl();
		return i2sInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LcmpInstruction createLcmpInstruction() {
		LcmpInstructionImpl lcmpInstruction = new LcmpInstructionImpl();
		return lcmpInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FcmplInstruction createFcmplInstruction() {
		FcmplInstructionImpl fcmplInstruction = new FcmplInstructionImpl();
		return fcmplInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FcmpgInstruction createFcmpgInstruction() {
		FcmpgInstructionImpl fcmpgInstruction = new FcmpgInstructionImpl();
		return fcmpgInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DcmplInstruction createDcmplInstruction() {
		DcmplInstructionImpl dcmplInstruction = new DcmplInstructionImpl();
		return dcmplInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DcmpgInstruction createDcmpgInstruction() {
		DcmpgInstructionImpl dcmpgInstruction = new DcmpgInstructionImpl();
		return dcmpgInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IreturnInstruction createIreturnInstruction() {
		IreturnInstructionImpl ireturnInstruction = new IreturnInstructionImpl();
		return ireturnInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LreturnInstruction createLreturnInstruction() {
		LreturnInstructionImpl lreturnInstruction = new LreturnInstructionImpl();
		return lreturnInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FreturnInstruction createFreturnInstruction() {
		FreturnInstructionImpl freturnInstruction = new FreturnInstructionImpl();
		return freturnInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DreturnInstruction createDreturnInstruction() {
		DreturnInstructionImpl dreturnInstruction = new DreturnInstructionImpl();
		return dreturnInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AreturnInstruction createAreturnInstruction() {
		AreturnInstructionImpl areturnInstruction = new AreturnInstructionImpl();
		return areturnInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReturnInstruction createReturnInstruction() {
		ReturnInstructionImpl returnInstruction = new ReturnInstructionImpl();
		return returnInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArraylengthInstruction createArraylengthInstruction() {
		ArraylengthInstructionImpl arraylengthInstruction = new ArraylengthInstructionImpl();
		return arraylengthInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AthrowInstruction createAthrowInstruction() {
		AthrowInstructionImpl athrowInstruction = new AthrowInstructionImpl();
		return athrowInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MonitorenterInstruction createMonitorenterInstruction() {
		MonitorenterInstructionImpl monitorenterInstruction = new MonitorenterInstructionImpl();
		return monitorenterInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MonitorexitInstruction createMonitorexitInstruction() {
		MonitorexitInstructionImpl monitorexitInstruction = new MonitorexitInstructionImpl();
		return monitorexitInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BipushInstruction createBipushInstruction() {
		BipushInstructionImpl bipushInstruction = new BipushInstructionImpl();
		return bipushInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SipushInstruction createSipushInstruction() {
		SipushInstructionImpl sipushInstruction = new SipushInstructionImpl();
		return sipushInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NewarrayInstruction createNewarrayInstruction() {
		NewarrayInstructionImpl newarrayInstruction = new NewarrayInstructionImpl();
		return newarrayInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IfeqInstruction createIfeqInstruction() {
		IfeqInstructionImpl ifeqInstruction = new IfeqInstructionImpl();
		return ifeqInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IfneInstruction createIfneInstruction() {
		IfneInstructionImpl ifneInstruction = new IfneInstructionImpl();
		return ifneInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IfltInstruction createIfltInstruction() {
		IfltInstructionImpl ifltInstruction = new IfltInstructionImpl();
		return ifltInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IfgeInstruction createIfgeInstruction() {
		IfgeInstructionImpl ifgeInstruction = new IfgeInstructionImpl();
		return ifgeInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IfgtInstruction createIfgtInstruction() {
		IfgtInstructionImpl ifgtInstruction = new IfgtInstructionImpl();
		return ifgtInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IfleInstruction createIfleInstruction() {
		IfleInstructionImpl ifleInstruction = new IfleInstructionImpl();
		return ifleInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public If_icmpeqInstruction createIf_icmpeqInstruction() {
		If_icmpeqInstructionImpl if_icmpeqInstruction = new If_icmpeqInstructionImpl();
		return if_icmpeqInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public If_icmpneInstruction createIf_icmpneInstruction() {
		If_icmpneInstructionImpl if_icmpneInstruction = new If_icmpneInstructionImpl();
		return if_icmpneInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public If_icmpltInstruction createIf_icmpltInstruction() {
		If_icmpltInstructionImpl if_icmpltInstruction = new If_icmpltInstructionImpl();
		return if_icmpltInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public If_icmpgeInstruction createIf_icmpgeInstruction() {
		If_icmpgeInstructionImpl if_icmpgeInstruction = new If_icmpgeInstructionImpl();
		return if_icmpgeInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public If_icmpgtInstruction createIf_icmpgtInstruction() {
		If_icmpgtInstructionImpl if_icmpgtInstruction = new If_icmpgtInstructionImpl();
		return if_icmpgtInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public If_icmpleInstruction createIf_icmpleInstruction() {
		If_icmpleInstructionImpl if_icmpleInstruction = new If_icmpleInstructionImpl();
		return if_icmpleInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public If_acmpeqInstruction createIf_acmpeqInstruction() {
		If_acmpeqInstructionImpl if_acmpeqInstruction = new If_acmpeqInstructionImpl();
		return if_acmpeqInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public If_acmpneInstruction createIf_acmpneInstruction() {
		If_acmpneInstructionImpl if_acmpneInstruction = new If_acmpneInstructionImpl();
		return if_acmpneInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GotoInstruction createGotoInstruction() {
		GotoInstructionImpl gotoInstruction = new GotoInstructionImpl();
		return gotoInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JsrInstruction createJsrInstruction() {
		JsrInstructionImpl jsrInstruction = new JsrInstructionImpl();
		return jsrInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IfnullInstruction createIfnullInstruction() {
		IfnullInstructionImpl ifnullInstruction = new IfnullInstructionImpl();
		return ifnullInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IfnonnullInstruction createIfnonnullInstruction() {
		IfnonnullInstructionImpl ifnonnullInstruction = new IfnonnullInstructionImpl();
		return ifnonnullInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InvokevirtualInstruction createInvokevirtualInstruction() {
		InvokevirtualInstructionImpl invokevirtualInstruction = new InvokevirtualInstructionImpl();
		return invokevirtualInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InvokespecialInstruction createInvokespecialInstruction() {
		InvokespecialInstructionImpl invokespecialInstruction = new InvokespecialInstructionImpl();
		return invokespecialInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InvokestaticInstruction createInvokestaticInstruction() {
		InvokestaticInstructionImpl invokestaticInstruction = new InvokestaticInstructionImpl();
		return invokestaticInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InvokeinterfaceInstruction createInvokeinterfaceInstruction() {
		InvokeinterfaceInstructionImpl invokeinterfaceInstruction = new InvokeinterfaceInstructionImpl();
		return invokeinterfaceInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NewInstruction createNewInstruction() {
		NewInstructionImpl newInstruction = new NewInstructionImpl();
		return newInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnewarrayInstruction createAnewarrayInstruction() {
		AnewarrayInstructionImpl anewarrayInstruction = new AnewarrayInstructionImpl();
		return anewarrayInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CheckcastInstruction createCheckcastInstruction() {
		CheckcastInstructionImpl checkcastInstruction = new CheckcastInstructionImpl();
		return checkcastInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InstanceofInstruction createInstanceofInstruction() {
		InstanceofInstructionImpl instanceofInstruction = new InstanceofInstructionImpl();
		return instanceofInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IloadInstruction createIloadInstruction() {
		IloadInstructionImpl iloadInstruction = new IloadInstructionImpl();
		return iloadInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LloadInstruction createLloadInstruction() {
		LloadInstructionImpl lloadInstruction = new LloadInstructionImpl();
		return lloadInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FloadInstruction createFloadInstruction() {
		FloadInstructionImpl floadInstruction = new FloadInstructionImpl();
		return floadInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DloadInstruction createDloadInstruction() {
		DloadInstructionImpl dloadInstruction = new DloadInstructionImpl();
		return dloadInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AloadInstruction createAloadInstruction() {
		AloadInstructionImpl aloadInstruction = new AloadInstructionImpl();
		return aloadInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IstoreInstruction createIstoreInstruction() {
		IstoreInstructionImpl istoreInstruction = new IstoreInstructionImpl();
		return istoreInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LstoreInstruction createLstoreInstruction() {
		LstoreInstructionImpl lstoreInstruction = new LstoreInstructionImpl();
		return lstoreInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FstoreInstruction createFstoreInstruction() {
		FstoreInstructionImpl fstoreInstruction = new FstoreInstructionImpl();
		return fstoreInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DstoreInstruction createDstoreInstruction() {
		DstoreInstructionImpl dstoreInstruction = new DstoreInstructionImpl();
		return dstoreInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AstoreInstruction createAstoreInstruction() {
		AstoreInstructionImpl astoreInstruction = new AstoreInstructionImpl();
		return astoreInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RetInstruction createRetInstruction() {
		RetInstructionImpl retInstruction = new RetInstructionImpl();
		return retInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JbcmmPackage getJbcmmPackage() {
		return (JbcmmPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static JbcmmPackage getPackage() {
		return JbcmmPackage.eINSTANCE;
	}

} //JbcmmFactoryImpl
