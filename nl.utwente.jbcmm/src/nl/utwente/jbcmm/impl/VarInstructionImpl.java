/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.JbcmmPackage;
import nl.utwente.jbcmm.LocalVariableTableEntry;
import nl.utwente.jbcmm.VarInstruction;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Var Instruction</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.impl.VarInstructionImpl#getLocalVariable <em>Local Variable</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.VarInstructionImpl#getVarIndex <em>Var Index</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class VarInstructionImpl extends InstructionImpl implements VarInstruction {
	/**
	 * The cached value of the '{@link #getLocalVariable() <em>Local Variable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalVariable()
	 * @generated
	 * @ordered
	 */
	protected LocalVariableTableEntry localVariable;

	/**
	 * The default value of the '{@link #getVarIndex() <em>Var Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVarIndex()
	 * @generated
	 * @ordered
	 */
	protected static final int VAR_INDEX_EDEFAULT = 0;
	/**
	 * The cached value of the '{@link #getVarIndex() <em>Var Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVarIndex()
	 * @generated
	 * @ordered
	 */
	protected int varIndex = VAR_INDEX_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VarInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getVarInstruction();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocalVariableTableEntry getLocalVariable() {
		if (localVariable != null && localVariable.eIsProxy()) {
			InternalEObject oldLocalVariable = (InternalEObject)localVariable;
			localVariable = (LocalVariableTableEntry)eResolveProxy(oldLocalVariable);
			if (localVariable != oldLocalVariable) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, JbcmmPackage.VAR_INSTRUCTION__LOCAL_VARIABLE, oldLocalVariable, localVariable));
			}
		}
		return localVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocalVariableTableEntry basicGetLocalVariable() {
		return localVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocalVariable(LocalVariableTableEntry newLocalVariable) {
		LocalVariableTableEntry oldLocalVariable = localVariable;
		localVariable = newLocalVariable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.VAR_INSTRUCTION__LOCAL_VARIABLE, oldLocalVariable, localVariable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getVarIndex() {
		return varIndex;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVarIndex(int newVarIndex) {
		int oldVarIndex = varIndex;
		varIndex = newVarIndex;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.VAR_INSTRUCTION__VAR_INDEX, oldVarIndex, varIndex));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case JbcmmPackage.VAR_INSTRUCTION__LOCAL_VARIABLE:
				if (resolve) return getLocalVariable();
				return basicGetLocalVariable();
			case JbcmmPackage.VAR_INSTRUCTION__VAR_INDEX:
				return getVarIndex();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case JbcmmPackage.VAR_INSTRUCTION__LOCAL_VARIABLE:
				setLocalVariable((LocalVariableTableEntry)newValue);
				return;
			case JbcmmPackage.VAR_INSTRUCTION__VAR_INDEX:
				setVarIndex((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case JbcmmPackage.VAR_INSTRUCTION__LOCAL_VARIABLE:
				setLocalVariable((LocalVariableTableEntry)null);
				return;
			case JbcmmPackage.VAR_INSTRUCTION__VAR_INDEX:
				setVarIndex(VAR_INDEX_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case JbcmmPackage.VAR_INSTRUCTION__LOCAL_VARIABLE:
				return localVariable != null;
			case JbcmmPackage.VAR_INSTRUCTION__VAR_INDEX:
				return varIndex != VAR_INDEX_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (varIndex: ");
		result.append(varIndex);
		result.append(')');
		return result.toString();
	}

} //VarInstructionImpl
