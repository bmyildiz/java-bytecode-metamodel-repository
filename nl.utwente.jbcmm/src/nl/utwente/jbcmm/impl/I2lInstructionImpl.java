/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.I2lInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>I2l Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class I2lInstructionImpl extends SimpleInstructionImpl implements I2lInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected I2lInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getI2lInstruction();
	}

} //I2lInstructionImpl
