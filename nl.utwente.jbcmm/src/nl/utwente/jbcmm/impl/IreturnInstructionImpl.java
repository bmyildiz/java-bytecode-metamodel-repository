/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.IreturnInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ireturn Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IreturnInstructionImpl extends SimpleInstructionImpl implements IreturnInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IreturnInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIreturnInstruction();
	}

} //IreturnInstructionImpl
