/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.DnegInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dneg Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DnegInstructionImpl extends SimpleInstructionImpl implements DnegInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DnegInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getDnegInstruction();
	}

} //DnegInstructionImpl
