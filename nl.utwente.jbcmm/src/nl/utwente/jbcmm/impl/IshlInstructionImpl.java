/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.IshlInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ishl Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IshlInstructionImpl extends SimpleInstructionImpl implements IshlInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IshlInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIshlInstruction();
	}

} //IshlInstructionImpl
