/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.I2bInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>I2b Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class I2bInstructionImpl extends SimpleInstructionImpl implements I2bInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected I2bInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getI2bInstruction();
	}

} //I2bInstructionImpl
