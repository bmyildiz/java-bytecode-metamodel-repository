/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.F2iInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>F2i Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class F2iInstructionImpl extends SimpleInstructionImpl implements F2iInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected F2iInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getF2iInstruction();
	}

} //F2iInstructionImpl
