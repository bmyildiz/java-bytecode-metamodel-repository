/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.D2fInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>D2f Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class D2fInstructionImpl extends SimpleInstructionImpl implements D2fInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected D2fInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getD2fInstruction();
	}

} //D2fInstructionImpl
