/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.IaloadInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Iaload Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IaloadInstructionImpl extends SimpleInstructionImpl implements IaloadInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IaloadInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIaloadInstruction();
	}

} //IaloadInstructionImpl
