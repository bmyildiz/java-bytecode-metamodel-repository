/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.InvokestaticInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Invokestatic Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class InvokestaticInstructionImpl extends MethodInstructionImpl implements InvokestaticInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InvokestaticInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getInvokestaticInstruction();
	}

} //InvokestaticInstructionImpl
