/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.Iconst_2Instruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Iconst 2Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class Iconst_2InstructionImpl extends SimpleInstructionImpl implements Iconst_2Instruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Iconst_2InstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIconst_2Instruction();
	}

} //Iconst_2InstructionImpl
