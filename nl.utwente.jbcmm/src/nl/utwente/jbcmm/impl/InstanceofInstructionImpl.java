/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.InstanceofInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Instanceof Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class InstanceofInstructionImpl extends TypeInstructionImpl implements InstanceofInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InstanceofInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getInstanceofInstruction();
	}

} //InstanceofInstructionImpl
