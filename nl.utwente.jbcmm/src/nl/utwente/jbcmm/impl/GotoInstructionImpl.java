/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.GotoInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Goto Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class GotoInstructionImpl extends JumpInstructionImpl implements GotoInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GotoInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getGotoInstruction();
	}

} //GotoInstructionImpl
