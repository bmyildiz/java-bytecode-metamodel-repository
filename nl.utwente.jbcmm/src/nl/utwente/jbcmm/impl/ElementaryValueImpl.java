/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.ElementaryValue;
import nl.utwente.jbcmm.JbcmmPackage;
import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Elementary Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ElementaryValueImpl extends IdentifiableImpl implements ElementaryValue {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ElementaryValueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getElementaryValue();
	}

} //ElementaryValueImpl
