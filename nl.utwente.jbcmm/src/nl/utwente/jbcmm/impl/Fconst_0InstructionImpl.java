/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.Fconst_0Instruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Fconst 0Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class Fconst_0InstructionImpl extends SimpleInstructionImpl implements Fconst_0Instruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Fconst_0InstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getFconst_0Instruction();
	}

} //Fconst_0InstructionImpl
