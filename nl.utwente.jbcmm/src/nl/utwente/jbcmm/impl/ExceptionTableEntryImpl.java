/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.ExceptionTableEntry;
import nl.utwente.jbcmm.Instruction;
import nl.utwente.jbcmm.JbcmmPackage;
import nl.utwente.jbcmm.Method;
import nl.utwente.jbcmm.TypeReference;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Exception Table Entry</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.impl.ExceptionTableEntryImpl#getMethod <em>Method</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ExceptionTableEntryImpl#getStartInstruction <em>Start Instruction</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ExceptionTableEntryImpl#getEndInstruction <em>End Instruction</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ExceptionTableEntryImpl#getHandlerInstruction <em>Handler Instruction</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ExceptionTableEntryImpl#getCatchType <em>Catch Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ExceptionTableEntryImpl extends IdentifiableImpl implements ExceptionTableEntry {
	/**
	 * The cached value of the '{@link #getStartInstruction() <em>Start Instruction</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartInstruction()
	 * @generated
	 * @ordered
	 */
	protected Instruction startInstruction;

	/**
	 * The cached value of the '{@link #getEndInstruction() <em>End Instruction</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndInstruction()
	 * @generated
	 * @ordered
	 */
	protected Instruction endInstruction;

	/**
	 * The cached value of the '{@link #getHandlerInstruction() <em>Handler Instruction</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHandlerInstruction()
	 * @generated
	 * @ordered
	 */
	protected Instruction handlerInstruction;

	/**
	 * The cached value of the '{@link #getCatchType() <em>Catch Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCatchType()
	 * @generated
	 * @ordered
	 */
	protected TypeReference catchType;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExceptionTableEntryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getExceptionTableEntry();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Method getMethod() {
		if (eContainerFeatureID() != JbcmmPackage.EXCEPTION_TABLE_ENTRY__METHOD) return null;
		return (Method)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMethod(Method newMethod, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newMethod, JbcmmPackage.EXCEPTION_TABLE_ENTRY__METHOD, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMethod(Method newMethod) {
		if (newMethod != eInternalContainer() || (eContainerFeatureID() != JbcmmPackage.EXCEPTION_TABLE_ENTRY__METHOD && newMethod != null)) {
			if (EcoreUtil.isAncestor(this, newMethod))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newMethod != null)
				msgs = ((InternalEObject)newMethod).eInverseAdd(this, JbcmmPackage.METHOD__EXCEPTION_TABLE, Method.class, msgs);
			msgs = basicSetMethod(newMethod, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.EXCEPTION_TABLE_ENTRY__METHOD, newMethod, newMethod));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getStartInstruction() {
		if (startInstruction != null && startInstruction.eIsProxy()) {
			InternalEObject oldStartInstruction = (InternalEObject)startInstruction;
			startInstruction = (Instruction)eResolveProxy(oldStartInstruction);
			if (startInstruction != oldStartInstruction) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, JbcmmPackage.EXCEPTION_TABLE_ENTRY__START_INSTRUCTION, oldStartInstruction, startInstruction));
			}
		}
		return startInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction basicGetStartInstruction() {
		return startInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartInstruction(Instruction newStartInstruction) {
		Instruction oldStartInstruction = startInstruction;
		startInstruction = newStartInstruction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.EXCEPTION_TABLE_ENTRY__START_INSTRUCTION, oldStartInstruction, startInstruction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getEndInstruction() {
		if (endInstruction != null && endInstruction.eIsProxy()) {
			InternalEObject oldEndInstruction = (InternalEObject)endInstruction;
			endInstruction = (Instruction)eResolveProxy(oldEndInstruction);
			if (endInstruction != oldEndInstruction) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, JbcmmPackage.EXCEPTION_TABLE_ENTRY__END_INSTRUCTION, oldEndInstruction, endInstruction));
			}
		}
		return endInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction basicGetEndInstruction() {
		return endInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndInstruction(Instruction newEndInstruction) {
		Instruction oldEndInstruction = endInstruction;
		endInstruction = newEndInstruction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.EXCEPTION_TABLE_ENTRY__END_INSTRUCTION, oldEndInstruction, endInstruction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getHandlerInstruction() {
		if (handlerInstruction != null && handlerInstruction.eIsProxy()) {
			InternalEObject oldHandlerInstruction = (InternalEObject)handlerInstruction;
			handlerInstruction = (Instruction)eResolveProxy(oldHandlerInstruction);
			if (handlerInstruction != oldHandlerInstruction) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, JbcmmPackage.EXCEPTION_TABLE_ENTRY__HANDLER_INSTRUCTION, oldHandlerInstruction, handlerInstruction));
			}
		}
		return handlerInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction basicGetHandlerInstruction() {
		return handlerInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHandlerInstruction(Instruction newHandlerInstruction) {
		Instruction oldHandlerInstruction = handlerInstruction;
		handlerInstruction = newHandlerInstruction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.EXCEPTION_TABLE_ENTRY__HANDLER_INSTRUCTION, oldHandlerInstruction, handlerInstruction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeReference getCatchType() {
		return catchType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCatchType(TypeReference newCatchType, NotificationChain msgs) {
		TypeReference oldCatchType = catchType;
		catchType = newCatchType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JbcmmPackage.EXCEPTION_TABLE_ENTRY__CATCH_TYPE, oldCatchType, newCatchType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCatchType(TypeReference newCatchType) {
		if (newCatchType != catchType) {
			NotificationChain msgs = null;
			if (catchType != null)
				msgs = ((InternalEObject)catchType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.EXCEPTION_TABLE_ENTRY__CATCH_TYPE, null, msgs);
			if (newCatchType != null)
				msgs = ((InternalEObject)newCatchType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.EXCEPTION_TABLE_ENTRY__CATCH_TYPE, null, msgs);
			msgs = basicSetCatchType(newCatchType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.EXCEPTION_TABLE_ENTRY__CATCH_TYPE, newCatchType, newCatchType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case JbcmmPackage.EXCEPTION_TABLE_ENTRY__METHOD:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetMethod((Method)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case JbcmmPackage.EXCEPTION_TABLE_ENTRY__METHOD:
				return basicSetMethod(null, msgs);
			case JbcmmPackage.EXCEPTION_TABLE_ENTRY__CATCH_TYPE:
				return basicSetCatchType(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case JbcmmPackage.EXCEPTION_TABLE_ENTRY__METHOD:
				return eInternalContainer().eInverseRemove(this, JbcmmPackage.METHOD__EXCEPTION_TABLE, Method.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case JbcmmPackage.EXCEPTION_TABLE_ENTRY__METHOD:
				return getMethod();
			case JbcmmPackage.EXCEPTION_TABLE_ENTRY__START_INSTRUCTION:
				if (resolve) return getStartInstruction();
				return basicGetStartInstruction();
			case JbcmmPackage.EXCEPTION_TABLE_ENTRY__END_INSTRUCTION:
				if (resolve) return getEndInstruction();
				return basicGetEndInstruction();
			case JbcmmPackage.EXCEPTION_TABLE_ENTRY__HANDLER_INSTRUCTION:
				if (resolve) return getHandlerInstruction();
				return basicGetHandlerInstruction();
			case JbcmmPackage.EXCEPTION_TABLE_ENTRY__CATCH_TYPE:
				return getCatchType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case JbcmmPackage.EXCEPTION_TABLE_ENTRY__METHOD:
				setMethod((Method)newValue);
				return;
			case JbcmmPackage.EXCEPTION_TABLE_ENTRY__START_INSTRUCTION:
				setStartInstruction((Instruction)newValue);
				return;
			case JbcmmPackage.EXCEPTION_TABLE_ENTRY__END_INSTRUCTION:
				setEndInstruction((Instruction)newValue);
				return;
			case JbcmmPackage.EXCEPTION_TABLE_ENTRY__HANDLER_INSTRUCTION:
				setHandlerInstruction((Instruction)newValue);
				return;
			case JbcmmPackage.EXCEPTION_TABLE_ENTRY__CATCH_TYPE:
				setCatchType((TypeReference)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case JbcmmPackage.EXCEPTION_TABLE_ENTRY__METHOD:
				setMethod((Method)null);
				return;
			case JbcmmPackage.EXCEPTION_TABLE_ENTRY__START_INSTRUCTION:
				setStartInstruction((Instruction)null);
				return;
			case JbcmmPackage.EXCEPTION_TABLE_ENTRY__END_INSTRUCTION:
				setEndInstruction((Instruction)null);
				return;
			case JbcmmPackage.EXCEPTION_TABLE_ENTRY__HANDLER_INSTRUCTION:
				setHandlerInstruction((Instruction)null);
				return;
			case JbcmmPackage.EXCEPTION_TABLE_ENTRY__CATCH_TYPE:
				setCatchType((TypeReference)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case JbcmmPackage.EXCEPTION_TABLE_ENTRY__METHOD:
				return getMethod() != null;
			case JbcmmPackage.EXCEPTION_TABLE_ENTRY__START_INSTRUCTION:
				return startInstruction != null;
			case JbcmmPackage.EXCEPTION_TABLE_ENTRY__END_INSTRUCTION:
				return endInstruction != null;
			case JbcmmPackage.EXCEPTION_TABLE_ENTRY__HANDLER_INSTRUCTION:
				return handlerInstruction != null;
			case JbcmmPackage.EXCEPTION_TABLE_ENTRY__CATCH_TYPE:
				return catchType != null;
		}
		return super.eIsSet(featureID);
	}

} //ExceptionTableEntryImpl
