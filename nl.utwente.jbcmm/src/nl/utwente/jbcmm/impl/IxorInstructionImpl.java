/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.IxorInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ixor Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IxorInstructionImpl extends SimpleInstructionImpl implements IxorInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IxorInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIxorInstruction();
	}

} //IxorInstructionImpl
