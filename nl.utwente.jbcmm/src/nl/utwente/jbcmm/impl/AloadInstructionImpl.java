/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.AloadInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Aload Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AloadInstructionImpl extends VarInstructionImpl implements AloadInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AloadInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getAloadInstruction();
	}

} //AloadInstructionImpl
