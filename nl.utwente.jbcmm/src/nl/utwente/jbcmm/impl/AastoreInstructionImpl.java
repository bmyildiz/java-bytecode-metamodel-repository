/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.AastoreInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Aastore Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AastoreInstructionImpl extends SimpleInstructionImpl implements AastoreInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AastoreInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getAastoreInstruction();
	}

} //AastoreInstructionImpl
