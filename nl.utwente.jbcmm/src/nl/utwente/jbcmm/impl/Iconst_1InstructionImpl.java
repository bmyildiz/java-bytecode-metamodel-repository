/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.Iconst_1Instruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Iconst 1Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class Iconst_1InstructionImpl extends SimpleInstructionImpl implements Iconst_1Instruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Iconst_1InstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIconst_1Instruction();
	}

} //Iconst_1InstructionImpl
