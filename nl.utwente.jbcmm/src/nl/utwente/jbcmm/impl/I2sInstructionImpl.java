/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.I2sInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>I2s Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class I2sInstructionImpl extends SimpleInstructionImpl implements I2sInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected I2sInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getI2sInstruction();
	}

} //I2sInstructionImpl
