/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.DremInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Drem Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DremInstructionImpl extends SimpleInstructionImpl implements DremInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DremInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getDremInstruction();
	}

} //DremInstructionImpl
