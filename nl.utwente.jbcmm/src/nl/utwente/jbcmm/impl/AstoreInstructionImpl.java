/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.AstoreInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Astore Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AstoreInstructionImpl extends VarInstructionImpl implements AstoreInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AstoreInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getAstoreInstruction();
	}

} //AstoreInstructionImpl
