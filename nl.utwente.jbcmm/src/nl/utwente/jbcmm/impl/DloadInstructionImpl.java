/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.DloadInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dload Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DloadInstructionImpl extends VarInstructionImpl implements DloadInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DloadInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getDloadInstruction();
	}

} //DloadInstructionImpl
