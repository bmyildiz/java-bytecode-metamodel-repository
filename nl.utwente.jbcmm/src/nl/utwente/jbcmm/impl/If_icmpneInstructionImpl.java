/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.If_icmpneInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>If icmpne Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class If_icmpneInstructionImpl extends JumpInstructionImpl implements If_icmpneInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected If_icmpneInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIf_icmpneInstruction();
	}

} //If_icmpneInstructionImpl
