/**
 */
package nl.utwente.jbcmm.impl;

import java.util.Collection;

import nl.utwente.jbcmm.Annotation;
import nl.utwente.jbcmm.Clazz;
import nl.utwente.jbcmm.ElementaryValue;
import nl.utwente.jbcmm.Field;
import nl.utwente.jbcmm.FieldTypeSignature;
import nl.utwente.jbcmm.JbcmmPackage;
import nl.utwente.jbcmm.TypeReference;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Field</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.impl.FieldImpl#isPublic <em>Public</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.FieldImpl#isPrivate <em>Private</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.FieldImpl#isProtected <em>Protected</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.FieldImpl#isStatic <em>Static</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.FieldImpl#isFinal <em>Final</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.FieldImpl#isVolatile <em>Volatile</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.FieldImpl#isTransient <em>Transient</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.FieldImpl#isSynthetic <em>Synthetic</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.FieldImpl#isEnum <em>Enum</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.FieldImpl#getName <em>Name</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.FieldImpl#getConstantValue <em>Constant Value</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.FieldImpl#isDeprecated <em>Deprecated</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.FieldImpl#getRuntimeInvisibleAnnotations <em>Runtime Invisible Annotations</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.FieldImpl#getRuntimeVisibleAnnotations <em>Runtime Visible Annotations</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.FieldImpl#getDescriptor <em>Descriptor</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.FieldImpl#getSignature <em>Signature</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.FieldImpl#getClass_ <em>Class</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FieldImpl extends IdentifiableImpl implements Field {
	/**
	 * The default value of the '{@link #isPublic() <em>Public</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPublic()
	 * @generated
	 * @ordered
	 */
	protected static final boolean PUBLIC_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isPublic() <em>Public</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPublic()
	 * @generated
	 * @ordered
	 */
	protected boolean public_ = PUBLIC_EDEFAULT;

	/**
	 * The default value of the '{@link #isPrivate() <em>Private</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPrivate()
	 * @generated
	 * @ordered
	 */
	protected static final boolean PRIVATE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isPrivate() <em>Private</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPrivate()
	 * @generated
	 * @ordered
	 */
	protected boolean private_ = PRIVATE_EDEFAULT;

	/**
	 * The default value of the '{@link #isProtected() <em>Protected</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isProtected()
	 * @generated
	 * @ordered
	 */
	protected static final boolean PROTECTED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isProtected() <em>Protected</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isProtected()
	 * @generated
	 * @ordered
	 */
	protected boolean protected_ = PROTECTED_EDEFAULT;

	/**
	 * The default value of the '{@link #isStatic() <em>Static</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isStatic()
	 * @generated
	 * @ordered
	 */
	protected static final boolean STATIC_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isStatic() <em>Static</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isStatic()
	 * @generated
	 * @ordered
	 */
	protected boolean static_ = STATIC_EDEFAULT;

	/**
	 * The default value of the '{@link #isFinal() <em>Final</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isFinal()
	 * @generated
	 * @ordered
	 */
	protected static final boolean FINAL_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isFinal() <em>Final</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isFinal()
	 * @generated
	 * @ordered
	 */
	protected boolean final_ = FINAL_EDEFAULT;

	/**
	 * The default value of the '{@link #isVolatile() <em>Volatile</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isVolatile()
	 * @generated
	 * @ordered
	 */
	protected static final boolean VOLATILE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isVolatile() <em>Volatile</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isVolatile()
	 * @generated
	 * @ordered
	 */
	protected boolean volatile_ = VOLATILE_EDEFAULT;

	/**
	 * The default value of the '{@link #isTransient() <em>Transient</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTransient()
	 * @generated
	 * @ordered
	 */
	protected static final boolean TRANSIENT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isTransient() <em>Transient</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTransient()
	 * @generated
	 * @ordered
	 */
	protected boolean transient_ = TRANSIENT_EDEFAULT;

	/**
	 * The default value of the '{@link #isSynthetic() <em>Synthetic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSynthetic()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SYNTHETIC_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSynthetic() <em>Synthetic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSynthetic()
	 * @generated
	 * @ordered
	 */
	protected boolean synthetic = SYNTHETIC_EDEFAULT;

	/**
	 * The default value of the '{@link #isEnum() <em>Enum</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isEnum()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ENUM_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isEnum() <em>Enum</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isEnum()
	 * @generated
	 * @ordered
	 */
	protected boolean enum_ = ENUM_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getConstantValue() <em>Constant Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstantValue()
	 * @generated
	 * @ordered
	 */
	protected ElementaryValue constantValue;

	/**
	 * The default value of the '{@link #isDeprecated() <em>Deprecated</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDeprecated()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DEPRECATED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isDeprecated() <em>Deprecated</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDeprecated()
	 * @generated
	 * @ordered
	 */
	protected boolean deprecated = DEPRECATED_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRuntimeInvisibleAnnotations() <em>Runtime Invisible Annotations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRuntimeInvisibleAnnotations()
	 * @generated
	 * @ordered
	 */
	protected EList<Annotation> runtimeInvisibleAnnotations;

	/**
	 * The cached value of the '{@link #getRuntimeVisibleAnnotations() <em>Runtime Visible Annotations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRuntimeVisibleAnnotations()
	 * @generated
	 * @ordered
	 */
	protected EList<Annotation> runtimeVisibleAnnotations;

	/**
	 * The cached value of the '{@link #getDescriptor() <em>Descriptor</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescriptor()
	 * @generated
	 * @ordered
	 */
	protected TypeReference descriptor;

	/**
	 * The cached value of the '{@link #getSignature() <em>Signature</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSignature()
	 * @generated
	 * @ordered
	 */
	protected FieldTypeSignature signature;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FieldImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getField();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isPublic() {
		return public_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPublic(boolean newPublic) {
		boolean oldPublic = public_;
		public_ = newPublic;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.FIELD__PUBLIC, oldPublic, public_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isPrivate() {
		return private_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrivate(boolean newPrivate) {
		boolean oldPrivate = private_;
		private_ = newPrivate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.FIELD__PRIVATE, oldPrivate, private_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isProtected() {
		return protected_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProtected(boolean newProtected) {
		boolean oldProtected = protected_;
		protected_ = newProtected;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.FIELD__PROTECTED, oldProtected, protected_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isStatic() {
		return static_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStatic(boolean newStatic) {
		boolean oldStatic = static_;
		static_ = newStatic;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.FIELD__STATIC, oldStatic, static_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isFinal() {
		return final_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFinal(boolean newFinal) {
		boolean oldFinal = final_;
		final_ = newFinal;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.FIELD__FINAL, oldFinal, final_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isVolatile() {
		return volatile_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVolatile(boolean newVolatile) {
		boolean oldVolatile = volatile_;
		volatile_ = newVolatile;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.FIELD__VOLATILE, oldVolatile, volatile_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isTransient() {
		return transient_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTransient(boolean newTransient) {
		boolean oldTransient = transient_;
		transient_ = newTransient;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.FIELD__TRANSIENT, oldTransient, transient_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSynthetic() {
		return synthetic;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSynthetic(boolean newSynthetic) {
		boolean oldSynthetic = synthetic;
		synthetic = newSynthetic;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.FIELD__SYNTHETIC, oldSynthetic, synthetic));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEnum() {
		return enum_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnum(boolean newEnum) {
		boolean oldEnum = enum_;
		enum_ = newEnum;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.FIELD__ENUM, oldEnum, enum_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.FIELD__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementaryValue getConstantValue() {
		return constantValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConstantValue(ElementaryValue newConstantValue, NotificationChain msgs) {
		ElementaryValue oldConstantValue = constantValue;
		constantValue = newConstantValue;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JbcmmPackage.FIELD__CONSTANT_VALUE, oldConstantValue, newConstantValue);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstantValue(ElementaryValue newConstantValue) {
		if (newConstantValue != constantValue) {
			NotificationChain msgs = null;
			if (constantValue != null)
				msgs = ((InternalEObject)constantValue).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.FIELD__CONSTANT_VALUE, null, msgs);
			if (newConstantValue != null)
				msgs = ((InternalEObject)newConstantValue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.FIELD__CONSTANT_VALUE, null, msgs);
			msgs = basicSetConstantValue(newConstantValue, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.FIELD__CONSTANT_VALUE, newConstantValue, newConstantValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isDeprecated() {
		return deprecated;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeprecated(boolean newDeprecated) {
		boolean oldDeprecated = deprecated;
		deprecated = newDeprecated;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.FIELD__DEPRECATED, oldDeprecated, deprecated));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Annotation> getRuntimeInvisibleAnnotations() {
		if (runtimeInvisibleAnnotations == null) {
			runtimeInvisibleAnnotations = new EObjectContainmentEList<Annotation>(Annotation.class, this, JbcmmPackage.FIELD__RUNTIME_INVISIBLE_ANNOTATIONS);
		}
		return runtimeInvisibleAnnotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Annotation> getRuntimeVisibleAnnotations() {
		if (runtimeVisibleAnnotations == null) {
			runtimeVisibleAnnotations = new EObjectContainmentEList<Annotation>(Annotation.class, this, JbcmmPackage.FIELD__RUNTIME_VISIBLE_ANNOTATIONS);
		}
		return runtimeVisibleAnnotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeReference getDescriptor() {
		return descriptor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDescriptor(TypeReference newDescriptor, NotificationChain msgs) {
		TypeReference oldDescriptor = descriptor;
		descriptor = newDescriptor;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JbcmmPackage.FIELD__DESCRIPTOR, oldDescriptor, newDescriptor);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescriptor(TypeReference newDescriptor) {
		if (newDescriptor != descriptor) {
			NotificationChain msgs = null;
			if (descriptor != null)
				msgs = ((InternalEObject)descriptor).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.FIELD__DESCRIPTOR, null, msgs);
			if (newDescriptor != null)
				msgs = ((InternalEObject)newDescriptor).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.FIELD__DESCRIPTOR, null, msgs);
			msgs = basicSetDescriptor(newDescriptor, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.FIELD__DESCRIPTOR, newDescriptor, newDescriptor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FieldTypeSignature getSignature() {
		return signature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSignature(FieldTypeSignature newSignature, NotificationChain msgs) {
		FieldTypeSignature oldSignature = signature;
		signature = newSignature;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JbcmmPackage.FIELD__SIGNATURE, oldSignature, newSignature);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSignature(FieldTypeSignature newSignature) {
		if (newSignature != signature) {
			NotificationChain msgs = null;
			if (signature != null)
				msgs = ((InternalEObject)signature).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.FIELD__SIGNATURE, null, msgs);
			if (newSignature != null)
				msgs = ((InternalEObject)newSignature).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.FIELD__SIGNATURE, null, msgs);
			msgs = basicSetSignature(newSignature, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.FIELD__SIGNATURE, newSignature, newSignature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Clazz getClass_() {
		if (eContainerFeatureID() != JbcmmPackage.FIELD__CLASS) return null;
		return (Clazz)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetClass(Clazz newClass, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newClass, JbcmmPackage.FIELD__CLASS, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClass(Clazz newClass) {
		if (newClass != eInternalContainer() || (eContainerFeatureID() != JbcmmPackage.FIELD__CLASS && newClass != null)) {
			if (EcoreUtil.isAncestor(this, newClass))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newClass != null)
				msgs = ((InternalEObject)newClass).eInverseAdd(this, JbcmmPackage.CLAZZ__FIELDS, Clazz.class, msgs);
			msgs = basicSetClass(newClass, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.FIELD__CLASS, newClass, newClass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case JbcmmPackage.FIELD__CLASS:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetClass((Clazz)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case JbcmmPackage.FIELD__CONSTANT_VALUE:
				return basicSetConstantValue(null, msgs);
			case JbcmmPackage.FIELD__RUNTIME_INVISIBLE_ANNOTATIONS:
				return ((InternalEList<?>)getRuntimeInvisibleAnnotations()).basicRemove(otherEnd, msgs);
			case JbcmmPackage.FIELD__RUNTIME_VISIBLE_ANNOTATIONS:
				return ((InternalEList<?>)getRuntimeVisibleAnnotations()).basicRemove(otherEnd, msgs);
			case JbcmmPackage.FIELD__DESCRIPTOR:
				return basicSetDescriptor(null, msgs);
			case JbcmmPackage.FIELD__SIGNATURE:
				return basicSetSignature(null, msgs);
			case JbcmmPackage.FIELD__CLASS:
				return basicSetClass(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case JbcmmPackage.FIELD__CLASS:
				return eInternalContainer().eInverseRemove(this, JbcmmPackage.CLAZZ__FIELDS, Clazz.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case JbcmmPackage.FIELD__PUBLIC:
				return isPublic();
			case JbcmmPackage.FIELD__PRIVATE:
				return isPrivate();
			case JbcmmPackage.FIELD__PROTECTED:
				return isProtected();
			case JbcmmPackage.FIELD__STATIC:
				return isStatic();
			case JbcmmPackage.FIELD__FINAL:
				return isFinal();
			case JbcmmPackage.FIELD__VOLATILE:
				return isVolatile();
			case JbcmmPackage.FIELD__TRANSIENT:
				return isTransient();
			case JbcmmPackage.FIELD__SYNTHETIC:
				return isSynthetic();
			case JbcmmPackage.FIELD__ENUM:
				return isEnum();
			case JbcmmPackage.FIELD__NAME:
				return getName();
			case JbcmmPackage.FIELD__CONSTANT_VALUE:
				return getConstantValue();
			case JbcmmPackage.FIELD__DEPRECATED:
				return isDeprecated();
			case JbcmmPackage.FIELD__RUNTIME_INVISIBLE_ANNOTATIONS:
				return getRuntimeInvisibleAnnotations();
			case JbcmmPackage.FIELD__RUNTIME_VISIBLE_ANNOTATIONS:
				return getRuntimeVisibleAnnotations();
			case JbcmmPackage.FIELD__DESCRIPTOR:
				return getDescriptor();
			case JbcmmPackage.FIELD__SIGNATURE:
				return getSignature();
			case JbcmmPackage.FIELD__CLASS:
				return getClass_();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case JbcmmPackage.FIELD__PUBLIC:
				setPublic((Boolean)newValue);
				return;
			case JbcmmPackage.FIELD__PRIVATE:
				setPrivate((Boolean)newValue);
				return;
			case JbcmmPackage.FIELD__PROTECTED:
				setProtected((Boolean)newValue);
				return;
			case JbcmmPackage.FIELD__STATIC:
				setStatic((Boolean)newValue);
				return;
			case JbcmmPackage.FIELD__FINAL:
				setFinal((Boolean)newValue);
				return;
			case JbcmmPackage.FIELD__VOLATILE:
				setVolatile((Boolean)newValue);
				return;
			case JbcmmPackage.FIELD__TRANSIENT:
				setTransient((Boolean)newValue);
				return;
			case JbcmmPackage.FIELD__SYNTHETIC:
				setSynthetic((Boolean)newValue);
				return;
			case JbcmmPackage.FIELD__ENUM:
				setEnum((Boolean)newValue);
				return;
			case JbcmmPackage.FIELD__NAME:
				setName((String)newValue);
				return;
			case JbcmmPackage.FIELD__CONSTANT_VALUE:
				setConstantValue((ElementaryValue)newValue);
				return;
			case JbcmmPackage.FIELD__DEPRECATED:
				setDeprecated((Boolean)newValue);
				return;
			case JbcmmPackage.FIELD__RUNTIME_INVISIBLE_ANNOTATIONS:
				getRuntimeInvisibleAnnotations().clear();
				getRuntimeInvisibleAnnotations().addAll((Collection<? extends Annotation>)newValue);
				return;
			case JbcmmPackage.FIELD__RUNTIME_VISIBLE_ANNOTATIONS:
				getRuntimeVisibleAnnotations().clear();
				getRuntimeVisibleAnnotations().addAll((Collection<? extends Annotation>)newValue);
				return;
			case JbcmmPackage.FIELD__DESCRIPTOR:
				setDescriptor((TypeReference)newValue);
				return;
			case JbcmmPackage.FIELD__SIGNATURE:
				setSignature((FieldTypeSignature)newValue);
				return;
			case JbcmmPackage.FIELD__CLASS:
				setClass((Clazz)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case JbcmmPackage.FIELD__PUBLIC:
				setPublic(PUBLIC_EDEFAULT);
				return;
			case JbcmmPackage.FIELD__PRIVATE:
				setPrivate(PRIVATE_EDEFAULT);
				return;
			case JbcmmPackage.FIELD__PROTECTED:
				setProtected(PROTECTED_EDEFAULT);
				return;
			case JbcmmPackage.FIELD__STATIC:
				setStatic(STATIC_EDEFAULT);
				return;
			case JbcmmPackage.FIELD__FINAL:
				setFinal(FINAL_EDEFAULT);
				return;
			case JbcmmPackage.FIELD__VOLATILE:
				setVolatile(VOLATILE_EDEFAULT);
				return;
			case JbcmmPackage.FIELD__TRANSIENT:
				setTransient(TRANSIENT_EDEFAULT);
				return;
			case JbcmmPackage.FIELD__SYNTHETIC:
				setSynthetic(SYNTHETIC_EDEFAULT);
				return;
			case JbcmmPackage.FIELD__ENUM:
				setEnum(ENUM_EDEFAULT);
				return;
			case JbcmmPackage.FIELD__NAME:
				setName(NAME_EDEFAULT);
				return;
			case JbcmmPackage.FIELD__CONSTANT_VALUE:
				setConstantValue((ElementaryValue)null);
				return;
			case JbcmmPackage.FIELD__DEPRECATED:
				setDeprecated(DEPRECATED_EDEFAULT);
				return;
			case JbcmmPackage.FIELD__RUNTIME_INVISIBLE_ANNOTATIONS:
				getRuntimeInvisibleAnnotations().clear();
				return;
			case JbcmmPackage.FIELD__RUNTIME_VISIBLE_ANNOTATIONS:
				getRuntimeVisibleAnnotations().clear();
				return;
			case JbcmmPackage.FIELD__DESCRIPTOR:
				setDescriptor((TypeReference)null);
				return;
			case JbcmmPackage.FIELD__SIGNATURE:
				setSignature((FieldTypeSignature)null);
				return;
			case JbcmmPackage.FIELD__CLASS:
				setClass((Clazz)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case JbcmmPackage.FIELD__PUBLIC:
				return public_ != PUBLIC_EDEFAULT;
			case JbcmmPackage.FIELD__PRIVATE:
				return private_ != PRIVATE_EDEFAULT;
			case JbcmmPackage.FIELD__PROTECTED:
				return protected_ != PROTECTED_EDEFAULT;
			case JbcmmPackage.FIELD__STATIC:
				return static_ != STATIC_EDEFAULT;
			case JbcmmPackage.FIELD__FINAL:
				return final_ != FINAL_EDEFAULT;
			case JbcmmPackage.FIELD__VOLATILE:
				return volatile_ != VOLATILE_EDEFAULT;
			case JbcmmPackage.FIELD__TRANSIENT:
				return transient_ != TRANSIENT_EDEFAULT;
			case JbcmmPackage.FIELD__SYNTHETIC:
				return synthetic != SYNTHETIC_EDEFAULT;
			case JbcmmPackage.FIELD__ENUM:
				return enum_ != ENUM_EDEFAULT;
			case JbcmmPackage.FIELD__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case JbcmmPackage.FIELD__CONSTANT_VALUE:
				return constantValue != null;
			case JbcmmPackage.FIELD__DEPRECATED:
				return deprecated != DEPRECATED_EDEFAULT;
			case JbcmmPackage.FIELD__RUNTIME_INVISIBLE_ANNOTATIONS:
				return runtimeInvisibleAnnotations != null && !runtimeInvisibleAnnotations.isEmpty();
			case JbcmmPackage.FIELD__RUNTIME_VISIBLE_ANNOTATIONS:
				return runtimeVisibleAnnotations != null && !runtimeVisibleAnnotations.isEmpty();
			case JbcmmPackage.FIELD__DESCRIPTOR:
				return descriptor != null;
			case JbcmmPackage.FIELD__SIGNATURE:
				return signature != null;
			case JbcmmPackage.FIELD__CLASS:
				return getClass_() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (public: ");
		result.append(public_);
		result.append(", private: ");
		result.append(private_);
		result.append(", protected: ");
		result.append(protected_);
		result.append(", static: ");
		result.append(static_);
		result.append(", final: ");
		result.append(final_);
		result.append(", volatile: ");
		result.append(volatile_);
		result.append(", transient: ");
		result.append(transient_);
		result.append(", synthetic: ");
		result.append(synthetic);
		result.append(", enum: ");
		result.append(enum_);
		result.append(", name: ");
		result.append(name);
		result.append(", deprecated: ");
		result.append(deprecated);
		result.append(')');
		return result.toString();
	}

} //FieldImpl
