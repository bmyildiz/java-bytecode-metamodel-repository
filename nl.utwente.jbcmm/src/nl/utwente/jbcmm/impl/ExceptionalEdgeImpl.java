/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.ExceptionTableEntry;
import nl.utwente.jbcmm.ExceptionalEdge;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Exceptional Edge</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.impl.ExceptionalEdgeImpl#getExceptionTableEntry <em>Exception Table Entry</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ExceptionalEdgeImpl extends ControlFlowEdgeImpl implements ExceptionalEdge {
	/**
	 * The cached value of the '{@link #getExceptionTableEntry() <em>Exception Table Entry</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExceptionTableEntry()
	 * @generated
	 * @ordered
	 */
	protected ExceptionTableEntry exceptionTableEntry;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExceptionalEdgeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getExceptionalEdge();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExceptionTableEntry getExceptionTableEntry() {
		if (exceptionTableEntry != null && exceptionTableEntry.eIsProxy()) {
			InternalEObject oldExceptionTableEntry = (InternalEObject)exceptionTableEntry;
			exceptionTableEntry = (ExceptionTableEntry)eResolveProxy(oldExceptionTableEntry);
			if (exceptionTableEntry != oldExceptionTableEntry) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, JbcmmPackage.EXCEPTIONAL_EDGE__EXCEPTION_TABLE_ENTRY, oldExceptionTableEntry, exceptionTableEntry));
			}
		}
		return exceptionTableEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExceptionTableEntry basicGetExceptionTableEntry() {
		return exceptionTableEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExceptionTableEntry(ExceptionTableEntry newExceptionTableEntry) {
		ExceptionTableEntry oldExceptionTableEntry = exceptionTableEntry;
		exceptionTableEntry = newExceptionTableEntry;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.EXCEPTIONAL_EDGE__EXCEPTION_TABLE_ENTRY, oldExceptionTableEntry, exceptionTableEntry));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case JbcmmPackage.EXCEPTIONAL_EDGE__EXCEPTION_TABLE_ENTRY:
				if (resolve) return getExceptionTableEntry();
				return basicGetExceptionTableEntry();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case JbcmmPackage.EXCEPTIONAL_EDGE__EXCEPTION_TABLE_ENTRY:
				setExceptionTableEntry((ExceptionTableEntry)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case JbcmmPackage.EXCEPTIONAL_EDGE__EXCEPTION_TABLE_ENTRY:
				setExceptionTableEntry((ExceptionTableEntry)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case JbcmmPackage.EXCEPTIONAL_EDGE__EXCEPTION_TABLE_ENTRY:
				return exceptionTableEntry != null;
		}
		return super.eIsSet(featureID);
	}

} //ExceptionalEdgeImpl
