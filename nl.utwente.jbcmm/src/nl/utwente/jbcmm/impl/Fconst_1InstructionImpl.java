/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.Fconst_1Instruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Fconst 1Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class Fconst_1InstructionImpl extends SimpleInstructionImpl implements Fconst_1Instruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Fconst_1InstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getFconst_1Instruction();
	}

} //Fconst_1InstructionImpl
