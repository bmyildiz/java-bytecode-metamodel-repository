/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.IandInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Iand Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IandInstructionImpl extends SimpleInstructionImpl implements IandInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IandInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIandInstruction();
	}

} //IandInstructionImpl
