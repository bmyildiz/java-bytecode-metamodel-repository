/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.FsubInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Fsub Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FsubInstructionImpl extends SimpleInstructionImpl implements FsubInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FsubInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getFsubInstruction();
	}

} //FsubInstructionImpl
