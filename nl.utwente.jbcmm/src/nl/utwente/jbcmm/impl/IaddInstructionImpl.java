/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.IaddInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Iadd Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IaddInstructionImpl extends SimpleInstructionImpl implements IaddInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IaddInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIaddInstruction();
	}

} //IaddInstructionImpl
