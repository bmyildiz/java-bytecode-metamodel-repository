/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.JbcmmPackage;
import nl.utwente.jbcmm.MonitorexitInstruction;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Monitorexit Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class MonitorexitInstructionImpl extends SimpleInstructionImpl implements MonitorexitInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MonitorexitInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getMonitorexitInstruction();
	}

} //MonitorexitInstructionImpl
