/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.I2fInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>I2f Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class I2fInstructionImpl extends SimpleInstructionImpl implements I2fInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected I2fInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getI2fInstruction();
	}

} //I2fInstructionImpl
