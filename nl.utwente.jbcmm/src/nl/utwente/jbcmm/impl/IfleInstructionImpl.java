/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.IfleInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ifle Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IfleInstructionImpl extends JumpInstructionImpl implements IfleInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IfleInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIfleInstruction();
	}

} //IfleInstructionImpl
