/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.FmulInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Fmul Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FmulInstructionImpl extends SimpleInstructionImpl implements FmulInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FmulInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getFmulInstruction();
	}

} //FmulInstructionImpl
