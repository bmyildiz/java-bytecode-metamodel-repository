/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.InvokevirtualInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Invokevirtual Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class InvokevirtualInstructionImpl extends MethodInstructionImpl implements InvokevirtualInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InvokevirtualInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getInvokevirtualInstruction();
	}

} //InvokevirtualInstructionImpl
