/**
 */
package nl.utwente.jbcmm.impl;

import java.util.Collection;

import nl.utwente.jbcmm.Annotation;
import nl.utwente.jbcmm.ElementValuePair;
import nl.utwente.jbcmm.JbcmmPackage;
import nl.utwente.jbcmm.TypeReference;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Annotation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.impl.AnnotationImpl#getElementValuePairs <em>Element Value Pairs</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.AnnotationImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AnnotationImpl extends IdentifiableImpl implements Annotation {
	/**
	 * The cached value of the '{@link #getElementValuePairs() <em>Element Value Pairs</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElementValuePairs()
	 * @generated
	 * @ordered
	 */
	protected EList<ElementValuePair> elementValuePairs;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected TypeReference type;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AnnotationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getAnnotation();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ElementValuePair> getElementValuePairs() {
		if (elementValuePairs == null) {
			elementValuePairs = new EObjectContainmentEList<ElementValuePair>(ElementValuePair.class, this, JbcmmPackage.ANNOTATION__ELEMENT_VALUE_PAIRS);
		}
		return elementValuePairs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeReference getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetType(TypeReference newType, NotificationChain msgs) {
		TypeReference oldType = type;
		type = newType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JbcmmPackage.ANNOTATION__TYPE, oldType, newType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(TypeReference newType) {
		if (newType != type) {
			NotificationChain msgs = null;
			if (type != null)
				msgs = ((InternalEObject)type).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.ANNOTATION__TYPE, null, msgs);
			if (newType != null)
				msgs = ((InternalEObject)newType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.ANNOTATION__TYPE, null, msgs);
			msgs = basicSetType(newType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.ANNOTATION__TYPE, newType, newType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case JbcmmPackage.ANNOTATION__ELEMENT_VALUE_PAIRS:
				return ((InternalEList<?>)getElementValuePairs()).basicRemove(otherEnd, msgs);
			case JbcmmPackage.ANNOTATION__TYPE:
				return basicSetType(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case JbcmmPackage.ANNOTATION__ELEMENT_VALUE_PAIRS:
				return getElementValuePairs();
			case JbcmmPackage.ANNOTATION__TYPE:
				return getType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case JbcmmPackage.ANNOTATION__ELEMENT_VALUE_PAIRS:
				getElementValuePairs().clear();
				getElementValuePairs().addAll((Collection<? extends ElementValuePair>)newValue);
				return;
			case JbcmmPackage.ANNOTATION__TYPE:
				setType((TypeReference)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case JbcmmPackage.ANNOTATION__ELEMENT_VALUE_PAIRS:
				getElementValuePairs().clear();
				return;
			case JbcmmPackage.ANNOTATION__TYPE:
				setType((TypeReference)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case JbcmmPackage.ANNOTATION__ELEMENT_VALUE_PAIRS:
				return elementValuePairs != null && !elementValuePairs.isEmpty();
			case JbcmmPackage.ANNOTATION__TYPE:
				return type != null;
		}
		return super.eIsSet(featureID);
	}

} //AnnotationImpl
