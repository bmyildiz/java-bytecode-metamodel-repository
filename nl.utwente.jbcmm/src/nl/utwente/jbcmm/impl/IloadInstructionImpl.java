/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.IloadInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Iload Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IloadInstructionImpl extends VarInstructionImpl implements IloadInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IloadInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIloadInstruction();
	}

} //IloadInstructionImpl
