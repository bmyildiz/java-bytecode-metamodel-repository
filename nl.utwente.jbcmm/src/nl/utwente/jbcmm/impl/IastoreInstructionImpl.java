/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.IastoreInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Iastore Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IastoreInstructionImpl extends SimpleInstructionImpl implements IastoreInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IastoreInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIastoreInstruction();
	}

} //IastoreInstructionImpl
