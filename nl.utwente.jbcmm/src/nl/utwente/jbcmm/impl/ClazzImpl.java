/**
 */
package nl.utwente.jbcmm.impl;

import java.util.Collection;

import nl.utwente.jbcmm.Annotation;
import nl.utwente.jbcmm.ClassSignature;
import nl.utwente.jbcmm.Clazz;
import nl.utwente.jbcmm.Field;
import nl.utwente.jbcmm.JbcmmPackage;
import nl.utwente.jbcmm.Method;
import nl.utwente.jbcmm.MethodReference;
import nl.utwente.jbcmm.Project;
import nl.utwente.jbcmm.TypeReference;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Clazz</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.impl.ClazzImpl#getProject <em>Project</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ClazzImpl#getMethods <em>Methods</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ClazzImpl#getSubclasses <em>Subclasses</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ClazzImpl#getName <em>Name</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ClazzImpl#getMinorVersion <em>Minor Version</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ClazzImpl#getMajorVersion <em>Major Version</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ClazzImpl#isPublic <em>Public</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ClazzImpl#isFinal <em>Final</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ClazzImpl#isSuper <em>Super</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ClazzImpl#isInterface <em>Interface</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ClazzImpl#isAbstract <em>Abstract</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ClazzImpl#isSynthetic <em>Synthetic</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ClazzImpl#isAnnotation <em>Annotation</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ClazzImpl#isEnum <em>Enum</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ClazzImpl#getSourceFileName <em>Source File Name</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ClazzImpl#isDeprecated <em>Deprecated</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ClazzImpl#getSourceDebugExtension <em>Source Debug Extension</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ClazzImpl#getRuntimeInvisibleAnnotations <em>Runtime Invisible Annotations</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ClazzImpl#getRuntimeVisibleAnnotations <em>Runtime Visible Annotations</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ClazzImpl#getFields <em>Fields</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ClazzImpl#getSuperClass <em>Super Class</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ClazzImpl#getInterfaces <em>Interfaces</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ClazzImpl#getSignature <em>Signature</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ClazzImpl#isPrivate <em>Private</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ClazzImpl#isProtected <em>Protected</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ClazzImpl#getOuterClass <em>Outer Class</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ClazzImpl#getEnclosingMethod <em>Enclosing Method</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ClazzImpl extends IdentifiableImpl implements Clazz {
	/**
	 * The cached value of the '{@link #getMethods() <em>Methods</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMethods()
	 * @generated
	 * @ordered
	 */
	protected EList<Method> methods;

	/**
	 * The cached value of the '{@link #getSubclasses() <em>Subclasses</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubclasses()
	 * @generated
	 * @ordered
	 */
	protected EList<Clazz> subclasses;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getMinorVersion() <em>Minor Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinorVersion()
	 * @generated
	 * @ordered
	 */
	protected static final int MINOR_VERSION_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMinorVersion() <em>Minor Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinorVersion()
	 * @generated
	 * @ordered
	 */
	protected int minorVersion = MINOR_VERSION_EDEFAULT;

	/**
	 * The default value of the '{@link #getMajorVersion() <em>Major Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMajorVersion()
	 * @generated
	 * @ordered
	 */
	protected static final int MAJOR_VERSION_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMajorVersion() <em>Major Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMajorVersion()
	 * @generated
	 * @ordered
	 */
	protected int majorVersion = MAJOR_VERSION_EDEFAULT;

	/**
	 * The default value of the '{@link #isPublic() <em>Public</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPublic()
	 * @generated
	 * @ordered
	 */
	protected static final boolean PUBLIC_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isPublic() <em>Public</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPublic()
	 * @generated
	 * @ordered
	 */
	protected boolean public_ = PUBLIC_EDEFAULT;

	/**
	 * The default value of the '{@link #isFinal() <em>Final</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isFinal()
	 * @generated
	 * @ordered
	 */
	protected static final boolean FINAL_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isFinal() <em>Final</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isFinal()
	 * @generated
	 * @ordered
	 */
	protected boolean final_ = FINAL_EDEFAULT;

	/**
	 * The default value of the '{@link #isSuper() <em>Super</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSuper()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SUPER_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSuper() <em>Super</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSuper()
	 * @generated
	 * @ordered
	 */
	protected boolean super_ = SUPER_EDEFAULT;

	/**
	 * The default value of the '{@link #isInterface() <em>Interface</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isInterface()
	 * @generated
	 * @ordered
	 */
	protected static final boolean INTERFACE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isInterface() <em>Interface</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isInterface()
	 * @generated
	 * @ordered
	 */
	protected boolean interface_ = INTERFACE_EDEFAULT;

	/**
	 * The default value of the '{@link #isAbstract() <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAbstract()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ABSTRACT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isAbstract() <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAbstract()
	 * @generated
	 * @ordered
	 */
	protected boolean abstract_ = ABSTRACT_EDEFAULT;

	/**
	 * The default value of the '{@link #isSynthetic() <em>Synthetic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSynthetic()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SYNTHETIC_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSynthetic() <em>Synthetic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSynthetic()
	 * @generated
	 * @ordered
	 */
	protected boolean synthetic = SYNTHETIC_EDEFAULT;

	/**
	 * The default value of the '{@link #isAnnotation() <em>Annotation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAnnotation()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ANNOTATION_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isAnnotation() <em>Annotation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAnnotation()
	 * @generated
	 * @ordered
	 */
	protected boolean annotation = ANNOTATION_EDEFAULT;

	/**
	 * The default value of the '{@link #isEnum() <em>Enum</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isEnum()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ENUM_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isEnum() <em>Enum</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isEnum()
	 * @generated
	 * @ordered
	 */
	protected boolean enum_ = ENUM_EDEFAULT;

	/**
	 * The default value of the '{@link #getSourceFileName() <em>Source File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceFileName()
	 * @generated
	 * @ordered
	 */
	protected static final String SOURCE_FILE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSourceFileName() <em>Source File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceFileName()
	 * @generated
	 * @ordered
	 */
	protected String sourceFileName = SOURCE_FILE_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #isDeprecated() <em>Deprecated</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDeprecated()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DEPRECATED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isDeprecated() <em>Deprecated</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDeprecated()
	 * @generated
	 * @ordered
	 */
	protected boolean deprecated = DEPRECATED_EDEFAULT;

	/**
	 * The default value of the '{@link #getSourceDebugExtension() <em>Source Debug Extension</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceDebugExtension()
	 * @generated
	 * @ordered
	 */
	protected static final String SOURCE_DEBUG_EXTENSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSourceDebugExtension() <em>Source Debug Extension</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceDebugExtension()
	 * @generated
	 * @ordered
	 */
	protected String sourceDebugExtension = SOURCE_DEBUG_EXTENSION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRuntimeInvisibleAnnotations() <em>Runtime Invisible Annotations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRuntimeInvisibleAnnotations()
	 * @generated
	 * @ordered
	 */
	protected EList<Annotation> runtimeInvisibleAnnotations;

	/**
	 * The cached value of the '{@link #getRuntimeVisibleAnnotations() <em>Runtime Visible Annotations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRuntimeVisibleAnnotations()
	 * @generated
	 * @ordered
	 */
	protected EList<Annotation> runtimeVisibleAnnotations;

	/**
	 * The cached value of the '{@link #getFields() <em>Fields</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFields()
	 * @generated
	 * @ordered
	 */
	protected EList<Field> fields;

	/**
	 * The cached value of the '{@link #getSuperClass() <em>Super Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSuperClass()
	 * @generated
	 * @ordered
	 */
	protected TypeReference superClass;

	/**
	 * The cached value of the '{@link #getInterfaces() <em>Interfaces</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterfaces()
	 * @generated
	 * @ordered
	 */
	protected EList<TypeReference> interfaces;

	/**
	 * The cached value of the '{@link #getSignature() <em>Signature</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSignature()
	 * @generated
	 * @ordered
	 */
	protected ClassSignature signature;

	/**
	 * The default value of the '{@link #isPrivate() <em>Private</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPrivate()
	 * @generated
	 * @ordered
	 */
	protected static final boolean PRIVATE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isPrivate() <em>Private</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPrivate()
	 * @generated
	 * @ordered
	 */
	protected boolean private_ = PRIVATE_EDEFAULT;

	/**
	 * The default value of the '{@link #isProtected() <em>Protected</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isProtected()
	 * @generated
	 * @ordered
	 */
	protected static final boolean PROTECTED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isProtected() <em>Protected</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isProtected()
	 * @generated
	 * @ordered
	 */
	protected boolean protected_ = PROTECTED_EDEFAULT;

	/**
	 * The cached value of the '{@link #getOuterClass() <em>Outer Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOuterClass()
	 * @generated
	 * @ordered
	 */
	protected TypeReference outerClass;

	/**
	 * The cached value of the '{@link #getEnclosingMethod() <em>Enclosing Method</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnclosingMethod()
	 * @generated
	 * @ordered
	 */
	protected MethodReference enclosingMethod;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClazzImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getClazz();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Project getProject() {
		if (eContainerFeatureID() != JbcmmPackage.CLAZZ__PROJECT) return null;
		return (Project)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProject(Project newProject, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newProject, JbcmmPackage.CLAZZ__PROJECT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProject(Project newProject) {
		if (newProject != eInternalContainer() || (eContainerFeatureID() != JbcmmPackage.CLAZZ__PROJECT && newProject != null)) {
			if (EcoreUtil.isAncestor(this, newProject))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newProject != null)
				msgs = ((InternalEObject)newProject).eInverseAdd(this, JbcmmPackage.PROJECT__CLASSES, Project.class, msgs);
			msgs = basicSetProject(newProject, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.CLAZZ__PROJECT, newProject, newProject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Method> getMethods() {
		if (methods == null) {
			methods = new EObjectContainmentWithInverseEList<Method>(Method.class, this, JbcmmPackage.CLAZZ__METHODS, JbcmmPackage.METHOD__CLASS);
		}
		return methods;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Clazz> getSubclasses() {
		if (subclasses == null) {
			subclasses = new EObjectResolvingEList<Clazz>(Clazz.class, this, JbcmmPackage.CLAZZ__SUBCLASSES);
		}
		return subclasses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.CLAZZ__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMinorVersion() {
		return minorVersion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinorVersion(int newMinorVersion) {
		int oldMinorVersion = minorVersion;
		minorVersion = newMinorVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.CLAZZ__MINOR_VERSION, oldMinorVersion, minorVersion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMajorVersion() {
		return majorVersion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMajorVersion(int newMajorVersion) {
		int oldMajorVersion = majorVersion;
		majorVersion = newMajorVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.CLAZZ__MAJOR_VERSION, oldMajorVersion, majorVersion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isPublic() {
		return public_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPublic(boolean newPublic) {
		boolean oldPublic = public_;
		public_ = newPublic;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.CLAZZ__PUBLIC, oldPublic, public_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isFinal() {
		return final_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFinal(boolean newFinal) {
		boolean oldFinal = final_;
		final_ = newFinal;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.CLAZZ__FINAL, oldFinal, final_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSuper() {
		return super_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSuper(boolean newSuper) {
		boolean oldSuper = super_;
		super_ = newSuper;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.CLAZZ__SUPER, oldSuper, super_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isInterface() {
		return interface_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInterface(boolean newInterface) {
		boolean oldInterface = interface_;
		interface_ = newInterface;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.CLAZZ__INTERFACE, oldInterface, interface_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isAbstract() {
		return abstract_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbstract(boolean newAbstract) {
		boolean oldAbstract = abstract_;
		abstract_ = newAbstract;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.CLAZZ__ABSTRACT, oldAbstract, abstract_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSynthetic() {
		return synthetic;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSynthetic(boolean newSynthetic) {
		boolean oldSynthetic = synthetic;
		synthetic = newSynthetic;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.CLAZZ__SYNTHETIC, oldSynthetic, synthetic));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isAnnotation() {
		return annotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnnotation(boolean newAnnotation) {
		boolean oldAnnotation = annotation;
		annotation = newAnnotation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.CLAZZ__ANNOTATION, oldAnnotation, annotation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEnum() {
		return enum_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnum(boolean newEnum) {
		boolean oldEnum = enum_;
		enum_ = newEnum;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.CLAZZ__ENUM, oldEnum, enum_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSourceFileName() {
		return sourceFileName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceFileName(String newSourceFileName) {
		String oldSourceFileName = sourceFileName;
		sourceFileName = newSourceFileName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.CLAZZ__SOURCE_FILE_NAME, oldSourceFileName, sourceFileName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isDeprecated() {
		return deprecated;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeprecated(boolean newDeprecated) {
		boolean oldDeprecated = deprecated;
		deprecated = newDeprecated;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.CLAZZ__DEPRECATED, oldDeprecated, deprecated));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSourceDebugExtension() {
		return sourceDebugExtension;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceDebugExtension(String newSourceDebugExtension) {
		String oldSourceDebugExtension = sourceDebugExtension;
		sourceDebugExtension = newSourceDebugExtension;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.CLAZZ__SOURCE_DEBUG_EXTENSION, oldSourceDebugExtension, sourceDebugExtension));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Annotation> getRuntimeInvisibleAnnotations() {
		if (runtimeInvisibleAnnotations == null) {
			runtimeInvisibleAnnotations = new EObjectContainmentEList<Annotation>(Annotation.class, this, JbcmmPackage.CLAZZ__RUNTIME_INVISIBLE_ANNOTATIONS);
		}
		return runtimeInvisibleAnnotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Annotation> getRuntimeVisibleAnnotations() {
		if (runtimeVisibleAnnotations == null) {
			runtimeVisibleAnnotations = new EObjectContainmentEList<Annotation>(Annotation.class, this, JbcmmPackage.CLAZZ__RUNTIME_VISIBLE_ANNOTATIONS);
		}
		return runtimeVisibleAnnotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Field> getFields() {
		if (fields == null) {
			fields = new EObjectContainmentWithInverseEList<Field>(Field.class, this, JbcmmPackage.CLAZZ__FIELDS, JbcmmPackage.FIELD__CLASS);
		}
		return fields;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeReference getSuperClass() {
		return superClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSuperClass(TypeReference newSuperClass, NotificationChain msgs) {
		TypeReference oldSuperClass = superClass;
		superClass = newSuperClass;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JbcmmPackage.CLAZZ__SUPER_CLASS, oldSuperClass, newSuperClass);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSuperClass(TypeReference newSuperClass) {
		if (newSuperClass != superClass) {
			NotificationChain msgs = null;
			if (superClass != null)
				msgs = ((InternalEObject)superClass).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.CLAZZ__SUPER_CLASS, null, msgs);
			if (newSuperClass != null)
				msgs = ((InternalEObject)newSuperClass).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.CLAZZ__SUPER_CLASS, null, msgs);
			msgs = basicSetSuperClass(newSuperClass, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.CLAZZ__SUPER_CLASS, newSuperClass, newSuperClass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TypeReference> getInterfaces() {
		if (interfaces == null) {
			interfaces = new EObjectContainmentEList<TypeReference>(TypeReference.class, this, JbcmmPackage.CLAZZ__INTERFACES);
		}
		return interfaces;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassSignature getSignature() {
		return signature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSignature(ClassSignature newSignature, NotificationChain msgs) {
		ClassSignature oldSignature = signature;
		signature = newSignature;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JbcmmPackage.CLAZZ__SIGNATURE, oldSignature, newSignature);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSignature(ClassSignature newSignature) {
		if (newSignature != signature) {
			NotificationChain msgs = null;
			if (signature != null)
				msgs = ((InternalEObject)signature).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.CLAZZ__SIGNATURE, null, msgs);
			if (newSignature != null)
				msgs = ((InternalEObject)newSignature).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.CLAZZ__SIGNATURE, null, msgs);
			msgs = basicSetSignature(newSignature, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.CLAZZ__SIGNATURE, newSignature, newSignature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isPrivate() {
		return private_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrivate(boolean newPrivate) {
		boolean oldPrivate = private_;
		private_ = newPrivate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.CLAZZ__PRIVATE, oldPrivate, private_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isProtected() {
		return protected_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProtected(boolean newProtected) {
		boolean oldProtected = protected_;
		protected_ = newProtected;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.CLAZZ__PROTECTED, oldProtected, protected_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeReference getOuterClass() {
		return outerClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOuterClass(TypeReference newOuterClass, NotificationChain msgs) {
		TypeReference oldOuterClass = outerClass;
		outerClass = newOuterClass;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JbcmmPackage.CLAZZ__OUTER_CLASS, oldOuterClass, newOuterClass);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOuterClass(TypeReference newOuterClass) {
		if (newOuterClass != outerClass) {
			NotificationChain msgs = null;
			if (outerClass != null)
				msgs = ((InternalEObject)outerClass).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.CLAZZ__OUTER_CLASS, null, msgs);
			if (newOuterClass != null)
				msgs = ((InternalEObject)newOuterClass).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.CLAZZ__OUTER_CLASS, null, msgs);
			msgs = basicSetOuterClass(newOuterClass, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.CLAZZ__OUTER_CLASS, newOuterClass, newOuterClass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MethodReference getEnclosingMethod() {
		return enclosingMethod;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEnclosingMethod(MethodReference newEnclosingMethod, NotificationChain msgs) {
		MethodReference oldEnclosingMethod = enclosingMethod;
		enclosingMethod = newEnclosingMethod;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JbcmmPackage.CLAZZ__ENCLOSING_METHOD, oldEnclosingMethod, newEnclosingMethod);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnclosingMethod(MethodReference newEnclosingMethod) {
		if (newEnclosingMethod != enclosingMethod) {
			NotificationChain msgs = null;
			if (enclosingMethod != null)
				msgs = ((InternalEObject)enclosingMethod).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.CLAZZ__ENCLOSING_METHOD, null, msgs);
			if (newEnclosingMethod != null)
				msgs = ((InternalEObject)newEnclosingMethod).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.CLAZZ__ENCLOSING_METHOD, null, msgs);
			msgs = basicSetEnclosingMethod(newEnclosingMethod, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.CLAZZ__ENCLOSING_METHOD, newEnclosingMethod, newEnclosingMethod));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case JbcmmPackage.CLAZZ__PROJECT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetProject((Project)otherEnd, msgs);
			case JbcmmPackage.CLAZZ__METHODS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getMethods()).basicAdd(otherEnd, msgs);
			case JbcmmPackage.CLAZZ__FIELDS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getFields()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case JbcmmPackage.CLAZZ__PROJECT:
				return basicSetProject(null, msgs);
			case JbcmmPackage.CLAZZ__METHODS:
				return ((InternalEList<?>)getMethods()).basicRemove(otherEnd, msgs);
			case JbcmmPackage.CLAZZ__RUNTIME_INVISIBLE_ANNOTATIONS:
				return ((InternalEList<?>)getRuntimeInvisibleAnnotations()).basicRemove(otherEnd, msgs);
			case JbcmmPackage.CLAZZ__RUNTIME_VISIBLE_ANNOTATIONS:
				return ((InternalEList<?>)getRuntimeVisibleAnnotations()).basicRemove(otherEnd, msgs);
			case JbcmmPackage.CLAZZ__FIELDS:
				return ((InternalEList<?>)getFields()).basicRemove(otherEnd, msgs);
			case JbcmmPackage.CLAZZ__SUPER_CLASS:
				return basicSetSuperClass(null, msgs);
			case JbcmmPackage.CLAZZ__INTERFACES:
				return ((InternalEList<?>)getInterfaces()).basicRemove(otherEnd, msgs);
			case JbcmmPackage.CLAZZ__SIGNATURE:
				return basicSetSignature(null, msgs);
			case JbcmmPackage.CLAZZ__OUTER_CLASS:
				return basicSetOuterClass(null, msgs);
			case JbcmmPackage.CLAZZ__ENCLOSING_METHOD:
				return basicSetEnclosingMethod(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case JbcmmPackage.CLAZZ__PROJECT:
				return eInternalContainer().eInverseRemove(this, JbcmmPackage.PROJECT__CLASSES, Project.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case JbcmmPackage.CLAZZ__PROJECT:
				return getProject();
			case JbcmmPackage.CLAZZ__METHODS:
				return getMethods();
			case JbcmmPackage.CLAZZ__SUBCLASSES:
				return getSubclasses();
			case JbcmmPackage.CLAZZ__NAME:
				return getName();
			case JbcmmPackage.CLAZZ__MINOR_VERSION:
				return getMinorVersion();
			case JbcmmPackage.CLAZZ__MAJOR_VERSION:
				return getMajorVersion();
			case JbcmmPackage.CLAZZ__PUBLIC:
				return isPublic();
			case JbcmmPackage.CLAZZ__FINAL:
				return isFinal();
			case JbcmmPackage.CLAZZ__SUPER:
				return isSuper();
			case JbcmmPackage.CLAZZ__INTERFACE:
				return isInterface();
			case JbcmmPackage.CLAZZ__ABSTRACT:
				return isAbstract();
			case JbcmmPackage.CLAZZ__SYNTHETIC:
				return isSynthetic();
			case JbcmmPackage.CLAZZ__ANNOTATION:
				return isAnnotation();
			case JbcmmPackage.CLAZZ__ENUM:
				return isEnum();
			case JbcmmPackage.CLAZZ__SOURCE_FILE_NAME:
				return getSourceFileName();
			case JbcmmPackage.CLAZZ__DEPRECATED:
				return isDeprecated();
			case JbcmmPackage.CLAZZ__SOURCE_DEBUG_EXTENSION:
				return getSourceDebugExtension();
			case JbcmmPackage.CLAZZ__RUNTIME_INVISIBLE_ANNOTATIONS:
				return getRuntimeInvisibleAnnotations();
			case JbcmmPackage.CLAZZ__RUNTIME_VISIBLE_ANNOTATIONS:
				return getRuntimeVisibleAnnotations();
			case JbcmmPackage.CLAZZ__FIELDS:
				return getFields();
			case JbcmmPackage.CLAZZ__SUPER_CLASS:
				return getSuperClass();
			case JbcmmPackage.CLAZZ__INTERFACES:
				return getInterfaces();
			case JbcmmPackage.CLAZZ__SIGNATURE:
				return getSignature();
			case JbcmmPackage.CLAZZ__PRIVATE:
				return isPrivate();
			case JbcmmPackage.CLAZZ__PROTECTED:
				return isProtected();
			case JbcmmPackage.CLAZZ__OUTER_CLASS:
				return getOuterClass();
			case JbcmmPackage.CLAZZ__ENCLOSING_METHOD:
				return getEnclosingMethod();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case JbcmmPackage.CLAZZ__PROJECT:
				setProject((Project)newValue);
				return;
			case JbcmmPackage.CLAZZ__METHODS:
				getMethods().clear();
				getMethods().addAll((Collection<? extends Method>)newValue);
				return;
			case JbcmmPackage.CLAZZ__SUBCLASSES:
				getSubclasses().clear();
				getSubclasses().addAll((Collection<? extends Clazz>)newValue);
				return;
			case JbcmmPackage.CLAZZ__NAME:
				setName((String)newValue);
				return;
			case JbcmmPackage.CLAZZ__MINOR_VERSION:
				setMinorVersion((Integer)newValue);
				return;
			case JbcmmPackage.CLAZZ__MAJOR_VERSION:
				setMajorVersion((Integer)newValue);
				return;
			case JbcmmPackage.CLAZZ__PUBLIC:
				setPublic((Boolean)newValue);
				return;
			case JbcmmPackage.CLAZZ__FINAL:
				setFinal((Boolean)newValue);
				return;
			case JbcmmPackage.CLAZZ__SUPER:
				setSuper((Boolean)newValue);
				return;
			case JbcmmPackage.CLAZZ__INTERFACE:
				setInterface((Boolean)newValue);
				return;
			case JbcmmPackage.CLAZZ__ABSTRACT:
				setAbstract((Boolean)newValue);
				return;
			case JbcmmPackage.CLAZZ__SYNTHETIC:
				setSynthetic((Boolean)newValue);
				return;
			case JbcmmPackage.CLAZZ__ANNOTATION:
				setAnnotation((Boolean)newValue);
				return;
			case JbcmmPackage.CLAZZ__ENUM:
				setEnum((Boolean)newValue);
				return;
			case JbcmmPackage.CLAZZ__SOURCE_FILE_NAME:
				setSourceFileName((String)newValue);
				return;
			case JbcmmPackage.CLAZZ__DEPRECATED:
				setDeprecated((Boolean)newValue);
				return;
			case JbcmmPackage.CLAZZ__SOURCE_DEBUG_EXTENSION:
				setSourceDebugExtension((String)newValue);
				return;
			case JbcmmPackage.CLAZZ__RUNTIME_INVISIBLE_ANNOTATIONS:
				getRuntimeInvisibleAnnotations().clear();
				getRuntimeInvisibleAnnotations().addAll((Collection<? extends Annotation>)newValue);
				return;
			case JbcmmPackage.CLAZZ__RUNTIME_VISIBLE_ANNOTATIONS:
				getRuntimeVisibleAnnotations().clear();
				getRuntimeVisibleAnnotations().addAll((Collection<? extends Annotation>)newValue);
				return;
			case JbcmmPackage.CLAZZ__FIELDS:
				getFields().clear();
				getFields().addAll((Collection<? extends Field>)newValue);
				return;
			case JbcmmPackage.CLAZZ__SUPER_CLASS:
				setSuperClass((TypeReference)newValue);
				return;
			case JbcmmPackage.CLAZZ__INTERFACES:
				getInterfaces().clear();
				getInterfaces().addAll((Collection<? extends TypeReference>)newValue);
				return;
			case JbcmmPackage.CLAZZ__SIGNATURE:
				setSignature((ClassSignature)newValue);
				return;
			case JbcmmPackage.CLAZZ__PRIVATE:
				setPrivate((Boolean)newValue);
				return;
			case JbcmmPackage.CLAZZ__PROTECTED:
				setProtected((Boolean)newValue);
				return;
			case JbcmmPackage.CLAZZ__OUTER_CLASS:
				setOuterClass((TypeReference)newValue);
				return;
			case JbcmmPackage.CLAZZ__ENCLOSING_METHOD:
				setEnclosingMethod((MethodReference)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case JbcmmPackage.CLAZZ__PROJECT:
				setProject((Project)null);
				return;
			case JbcmmPackage.CLAZZ__METHODS:
				getMethods().clear();
				return;
			case JbcmmPackage.CLAZZ__SUBCLASSES:
				getSubclasses().clear();
				return;
			case JbcmmPackage.CLAZZ__NAME:
				setName(NAME_EDEFAULT);
				return;
			case JbcmmPackage.CLAZZ__MINOR_VERSION:
				setMinorVersion(MINOR_VERSION_EDEFAULT);
				return;
			case JbcmmPackage.CLAZZ__MAJOR_VERSION:
				setMajorVersion(MAJOR_VERSION_EDEFAULT);
				return;
			case JbcmmPackage.CLAZZ__PUBLIC:
				setPublic(PUBLIC_EDEFAULT);
				return;
			case JbcmmPackage.CLAZZ__FINAL:
				setFinal(FINAL_EDEFAULT);
				return;
			case JbcmmPackage.CLAZZ__SUPER:
				setSuper(SUPER_EDEFAULT);
				return;
			case JbcmmPackage.CLAZZ__INTERFACE:
				setInterface(INTERFACE_EDEFAULT);
				return;
			case JbcmmPackage.CLAZZ__ABSTRACT:
				setAbstract(ABSTRACT_EDEFAULT);
				return;
			case JbcmmPackage.CLAZZ__SYNTHETIC:
				setSynthetic(SYNTHETIC_EDEFAULT);
				return;
			case JbcmmPackage.CLAZZ__ANNOTATION:
				setAnnotation(ANNOTATION_EDEFAULT);
				return;
			case JbcmmPackage.CLAZZ__ENUM:
				setEnum(ENUM_EDEFAULT);
				return;
			case JbcmmPackage.CLAZZ__SOURCE_FILE_NAME:
				setSourceFileName(SOURCE_FILE_NAME_EDEFAULT);
				return;
			case JbcmmPackage.CLAZZ__DEPRECATED:
				setDeprecated(DEPRECATED_EDEFAULT);
				return;
			case JbcmmPackage.CLAZZ__SOURCE_DEBUG_EXTENSION:
				setSourceDebugExtension(SOURCE_DEBUG_EXTENSION_EDEFAULT);
				return;
			case JbcmmPackage.CLAZZ__RUNTIME_INVISIBLE_ANNOTATIONS:
				getRuntimeInvisibleAnnotations().clear();
				return;
			case JbcmmPackage.CLAZZ__RUNTIME_VISIBLE_ANNOTATIONS:
				getRuntimeVisibleAnnotations().clear();
				return;
			case JbcmmPackage.CLAZZ__FIELDS:
				getFields().clear();
				return;
			case JbcmmPackage.CLAZZ__SUPER_CLASS:
				setSuperClass((TypeReference)null);
				return;
			case JbcmmPackage.CLAZZ__INTERFACES:
				getInterfaces().clear();
				return;
			case JbcmmPackage.CLAZZ__SIGNATURE:
				setSignature((ClassSignature)null);
				return;
			case JbcmmPackage.CLAZZ__PRIVATE:
				setPrivate(PRIVATE_EDEFAULT);
				return;
			case JbcmmPackage.CLAZZ__PROTECTED:
				setProtected(PROTECTED_EDEFAULT);
				return;
			case JbcmmPackage.CLAZZ__OUTER_CLASS:
				setOuterClass((TypeReference)null);
				return;
			case JbcmmPackage.CLAZZ__ENCLOSING_METHOD:
				setEnclosingMethod((MethodReference)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case JbcmmPackage.CLAZZ__PROJECT:
				return getProject() != null;
			case JbcmmPackage.CLAZZ__METHODS:
				return methods != null && !methods.isEmpty();
			case JbcmmPackage.CLAZZ__SUBCLASSES:
				return subclasses != null && !subclasses.isEmpty();
			case JbcmmPackage.CLAZZ__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case JbcmmPackage.CLAZZ__MINOR_VERSION:
				return minorVersion != MINOR_VERSION_EDEFAULT;
			case JbcmmPackage.CLAZZ__MAJOR_VERSION:
				return majorVersion != MAJOR_VERSION_EDEFAULT;
			case JbcmmPackage.CLAZZ__PUBLIC:
				return public_ != PUBLIC_EDEFAULT;
			case JbcmmPackage.CLAZZ__FINAL:
				return final_ != FINAL_EDEFAULT;
			case JbcmmPackage.CLAZZ__SUPER:
				return super_ != SUPER_EDEFAULT;
			case JbcmmPackage.CLAZZ__INTERFACE:
				return interface_ != INTERFACE_EDEFAULT;
			case JbcmmPackage.CLAZZ__ABSTRACT:
				return abstract_ != ABSTRACT_EDEFAULT;
			case JbcmmPackage.CLAZZ__SYNTHETIC:
				return synthetic != SYNTHETIC_EDEFAULT;
			case JbcmmPackage.CLAZZ__ANNOTATION:
				return annotation != ANNOTATION_EDEFAULT;
			case JbcmmPackage.CLAZZ__ENUM:
				return enum_ != ENUM_EDEFAULT;
			case JbcmmPackage.CLAZZ__SOURCE_FILE_NAME:
				return SOURCE_FILE_NAME_EDEFAULT == null ? sourceFileName != null : !SOURCE_FILE_NAME_EDEFAULT.equals(sourceFileName);
			case JbcmmPackage.CLAZZ__DEPRECATED:
				return deprecated != DEPRECATED_EDEFAULT;
			case JbcmmPackage.CLAZZ__SOURCE_DEBUG_EXTENSION:
				return SOURCE_DEBUG_EXTENSION_EDEFAULT == null ? sourceDebugExtension != null : !SOURCE_DEBUG_EXTENSION_EDEFAULT.equals(sourceDebugExtension);
			case JbcmmPackage.CLAZZ__RUNTIME_INVISIBLE_ANNOTATIONS:
				return runtimeInvisibleAnnotations != null && !runtimeInvisibleAnnotations.isEmpty();
			case JbcmmPackage.CLAZZ__RUNTIME_VISIBLE_ANNOTATIONS:
				return runtimeVisibleAnnotations != null && !runtimeVisibleAnnotations.isEmpty();
			case JbcmmPackage.CLAZZ__FIELDS:
				return fields != null && !fields.isEmpty();
			case JbcmmPackage.CLAZZ__SUPER_CLASS:
				return superClass != null;
			case JbcmmPackage.CLAZZ__INTERFACES:
				return interfaces != null && !interfaces.isEmpty();
			case JbcmmPackage.CLAZZ__SIGNATURE:
				return signature != null;
			case JbcmmPackage.CLAZZ__PRIVATE:
				return private_ != PRIVATE_EDEFAULT;
			case JbcmmPackage.CLAZZ__PROTECTED:
				return protected_ != PROTECTED_EDEFAULT;
			case JbcmmPackage.CLAZZ__OUTER_CLASS:
				return outerClass != null;
			case JbcmmPackage.CLAZZ__ENCLOSING_METHOD:
				return enclosingMethod != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", minorVersion: ");
		result.append(minorVersion);
		result.append(", majorVersion: ");
		result.append(majorVersion);
		result.append(", public: ");
		result.append(public_);
		result.append(", final: ");
		result.append(final_);
		result.append(", super: ");
		result.append(super_);
		result.append(", interface: ");
		result.append(interface_);
		result.append(", abstract: ");
		result.append(abstract_);
		result.append(", synthetic: ");
		result.append(synthetic);
		result.append(", annotation: ");
		result.append(annotation);
		result.append(", enum: ");
		result.append(enum_);
		result.append(", sourceFileName: ");
		result.append(sourceFileName);
		result.append(", deprecated: ");
		result.append(deprecated);
		result.append(", sourceDebugExtension: ");
		result.append(sourceDebugExtension);
		result.append(", private: ");
		result.append(private_);
		result.append(", protected: ");
		result.append(protected_);
		result.append(')');
		return result.toString();
	}

} //ClazzImpl
