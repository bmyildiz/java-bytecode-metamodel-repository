/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.GetfieldInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Getfield Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class GetfieldInstructionImpl extends FieldInstructionImpl implements GetfieldInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GetfieldInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getGetfieldInstruction();
	}

} //GetfieldInstructionImpl
