/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.Iconst_3Instruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Iconst 3Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class Iconst_3InstructionImpl extends SimpleInstructionImpl implements Iconst_3Instruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Iconst_3InstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIconst_3Instruction();
	}

} //Iconst_3InstructionImpl
