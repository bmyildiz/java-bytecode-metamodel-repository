/**
 */
package nl.utwente.jbcmm.impl;

import java.io.IOException;

import java.net.URL;

import nl.utwente.jbcmm.JbcmmFactory;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.util.WrappedException;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class JbcmmPackageImpl extends EPackageImpl implements JbcmmPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected String packageFilename = "jbcmm.ecore";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass identifiableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass projectEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass clazzEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass signatureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass classSignatureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass methodSignatureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fieldTypeSignatureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass typeReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass methodDescriptorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass methodReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fieldReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fieldEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass annotationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass elementValuePairEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass elementValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass elementaryValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass booleanValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass charValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass byteValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass shortValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass intValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass longValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass floatValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass doubleValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass enumValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass methodEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass localVariableTableEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass exceptionTableEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass controlFlowEdgeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass unconditionalEdgeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass conditionalEdgeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass switchCaseEdgeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass switchDefaultEdgeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass exceptionalEdgeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass instructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fieldInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simpleInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass intInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass jumpInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ldcInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass methodInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass typeInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass varInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iincInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass multianewarrayInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass switchInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ldcIntInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ldcLongInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ldcFloatInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ldcDoubleInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ldcStringInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ldcTypeInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass getstaticInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass putstaticInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass getfieldInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass putfieldInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nopInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aconst_nullInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iconst_m1InstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iconst_0InstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iconst_1InstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iconst_2InstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iconst_3InstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iconst_4InstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iconst_5InstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lconst_0InstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lconst_1InstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fconst_0InstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fconst_1InstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fconst_2InstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dconst_0InstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dconst_1InstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ialoadInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass laloadInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass faloadInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass daloadInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aaloadInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baloadInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass caloadInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass saloadInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iastoreInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lastoreInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fastoreInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dastoreInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aastoreInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bastoreInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass castoreInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sastoreInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass popInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pop2InstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dupInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dup_x1InstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dup_x2InstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dup2InstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dup2_x1InstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dup2_x2InstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass swapInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iaddInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass laddInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass faddInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass daddInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass isubInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lsubInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsubInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dsubInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass imulInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lmulInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fmulInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dmulInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass idivInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ldivInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fdivInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ddivInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iremInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lremInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fremInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dremInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass inegInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lnegInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fnegInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dnegInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ishlInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lshlInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ishrInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lshrInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iushrInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lushrInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iandInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass landInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iorInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lorInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ixorInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lxorInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass i2lInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass i2fInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass i2dInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass l2iInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass l2fInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass l2dInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass f2iInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass f2lInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass f2dInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass d2iInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass d2lInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass d2fInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass i2bInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass i2cInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass i2sInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lcmpInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fcmplInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fcmpgInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dcmplInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dcmpgInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ireturnInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lreturnInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass freturnInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dreturnInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass areturnInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass returnInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass arraylengthInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass athrowInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass monitorenterInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass monitorexitInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bipushInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sipushInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass newarrayInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ifeqInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ifneInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ifltInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ifgeInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ifgtInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ifleInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass if_icmpeqInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass if_icmpneInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass if_icmpltInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass if_icmpgeInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass if_icmpgtInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass if_icmpleInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass if_acmpeqInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass if_acmpneInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass gotoInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass jsrInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ifnullInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ifnonnullInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass invokevirtualInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass invokespecialInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass invokestaticInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass invokeinterfaceInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass newInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass anewarrayInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass checkcastInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass instanceofInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iloadInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lloadInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass floadInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dloadInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aloadInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass istoreInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lstoreInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fstoreInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dstoreInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass astoreInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass retInstructionEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see nl.utwente.jbcmm.JbcmmPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private JbcmmPackageImpl() {
		super(eNS_URI, JbcmmFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link JbcmmPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @generated
	 */
	public static JbcmmPackage init() {
		if (isInited) return (JbcmmPackage)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI);

		// Obtain or create and register package
		JbcmmPackageImpl theJbcmmPackage = (JbcmmPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof JbcmmPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new JbcmmPackageImpl());

		isInited = true;

		// Load packages
		theJbcmmPackage.loadPackage();

		// Fix loaded packages
		theJbcmmPackage.fixPackageContents();

		// Mark meta-data to indicate it can't be changed
		theJbcmmPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(JbcmmPackage.eNS_URI, theJbcmmPackage);
		return theJbcmmPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIdentifiable() {
		if (identifiableEClass == null) {
			identifiableEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(0);
		}
		return identifiableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIdentifiable_Uuid() {
        return (EAttribute)getIdentifiable().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProject() {
		if (projectEClass == null) {
			projectEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(1);
		}
		return projectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProject_Classes() {
        return (EReference)getProject().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProject_MainClass() {
        return (EReference)getProject().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getClazz() {
		if (clazzEClass == null) {
			clazzEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(2);
		}
		return clazzEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClazz_Project() {
        return (EReference)getClazz().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClazz_Methods() {
        return (EReference)getClazz().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClazz_Subclasses() {
        return (EReference)getClazz().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClazz_Name() {
        return (EAttribute)getClazz().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClazz_MinorVersion() {
        return (EAttribute)getClazz().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClazz_MajorVersion() {
        return (EAttribute)getClazz().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClazz_Public() {
        return (EAttribute)getClazz().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClazz_Final() {
        return (EAttribute)getClazz().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClazz_Super() {
        return (EAttribute)getClazz().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClazz_Interface() {
        return (EAttribute)getClazz().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClazz_Abstract() {
        return (EAttribute)getClazz().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClazz_Synthetic() {
        return (EAttribute)getClazz().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClazz_Annotation() {
        return (EAttribute)getClazz().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClazz_Enum() {
        return (EAttribute)getClazz().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClazz_SourceFileName() {
        return (EAttribute)getClazz().getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClazz_Deprecated() {
        return (EAttribute)getClazz().getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClazz_SourceDebugExtension() {
        return (EAttribute)getClazz().getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClazz_RuntimeInvisibleAnnotations() {
        return (EReference)getClazz().getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClazz_RuntimeVisibleAnnotations() {
        return (EReference)getClazz().getEStructuralFeatures().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClazz_Fields() {
        return (EReference)getClazz().getEStructuralFeatures().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClazz_SuperClass() {
        return (EReference)getClazz().getEStructuralFeatures().get(20);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClazz_Interfaces() {
        return (EReference)getClazz().getEStructuralFeatures().get(21);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClazz_Signature() {
        return (EReference)getClazz().getEStructuralFeatures().get(22);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClazz_Private() {
        return (EAttribute)getClazz().getEStructuralFeatures().get(23);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClazz_Protected() {
        return (EAttribute)getClazz().getEStructuralFeatures().get(24);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClazz_OuterClass() {
        return (EReference)getClazz().getEStructuralFeatures().get(25);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClazz_EnclosingMethod() {
        return (EReference)getClazz().getEStructuralFeatures().get(26);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSignature() {
		if (signatureEClass == null) {
			signatureEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(3);
		}
		return signatureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSignature_Signature() {
        return (EAttribute)getSignature().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getClassSignature() {
		if (classSignatureEClass == null) {
			classSignatureEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(4);
		}
		return classSignatureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMethodSignature() {
		if (methodSignatureEClass == null) {
			methodSignatureEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(5);
		}
		return methodSignatureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFieldTypeSignature() {
		if (fieldTypeSignatureEClass == null) {
			fieldTypeSignatureEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(6);
		}
		return fieldTypeSignatureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTypeReference() {
		if (typeReferenceEClass == null) {
			typeReferenceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(7);
		}
		return typeReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTypeReference_ReferencedClass() {
        return (EReference)getTypeReference().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTypeReference_TypeDescriptor() {
        return (EAttribute)getTypeReference().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTypeReference_RuntimeInvisibleAnnotations() {
        return (EReference)getTypeReference().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTypeReference_RuntimeVisibleAnnotations() {
        return (EReference)getTypeReference().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMethodDescriptor() {
		if (methodDescriptorEClass == null) {
			methodDescriptorEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(8);
		}
		return methodDescriptorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMethodDescriptor_ParameterTypes() {
        return (EReference)getMethodDescriptor().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMethodDescriptor_ResultType() {
        return (EReference)getMethodDescriptor().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMethodReference() {
		if (methodReferenceEClass == null) {
			methodReferenceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(9);
		}
		return methodReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMethodReference_DeclaringClass() {
        return (EReference)getMethodReference().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMethodReference_Name() {
        return (EAttribute)getMethodReference().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMethodReference_Descriptor() {
        return (EReference)getMethodReference().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFieldReference() {
		if (fieldReferenceEClass == null) {
			fieldReferenceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(10);
		}
		return fieldReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFieldReference_DeclaringClass() {
        return (EReference)getFieldReference().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFieldReference_Name() {
        return (EAttribute)getFieldReference().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFieldReference_Descriptor() {
        return (EReference)getFieldReference().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getField() {
		if (fieldEClass == null) {
			fieldEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(11);
		}
		return fieldEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getField_Public() {
        return (EAttribute)getField().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getField_Private() {
        return (EAttribute)getField().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getField_Protected() {
        return (EAttribute)getField().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getField_Static() {
        return (EAttribute)getField().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getField_Final() {
        return (EAttribute)getField().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getField_Volatile() {
        return (EAttribute)getField().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getField_Transient() {
        return (EAttribute)getField().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getField_Synthetic() {
        return (EAttribute)getField().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getField_Enum() {
        return (EAttribute)getField().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getField_Name() {
        return (EAttribute)getField().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getField_ConstantValue() {
        return (EReference)getField().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getField_Deprecated() {
        return (EAttribute)getField().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getField_RuntimeInvisibleAnnotations() {
        return (EReference)getField().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getField_RuntimeVisibleAnnotations() {
        return (EReference)getField().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getField_Descriptor() {
        return (EReference)getField().getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getField_Signature() {
        return (EReference)getField().getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getField_Class() {
        return (EReference)getField().getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAnnotation() {
		if (annotationEClass == null) {
			annotationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(12);
		}
		return annotationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAnnotation_ElementValuePairs() {
        return (EReference)getAnnotation().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAnnotation_Type() {
        return (EReference)getAnnotation().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getElementValuePair() {
		if (elementValuePairEClass == null) {
			elementValuePairEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(13);
		}
		return elementValuePairEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementValuePair_Value() {
        return (EReference)getElementValuePair().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getElementValuePair_Name() {
        return (EAttribute)getElementValuePair().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getElementValue() {
		if (elementValueEClass == null) {
			elementValueEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(14);
		}
		return elementValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementValue_ConstantValue() {
        return (EReference)getElementValue().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementValue_EnumValue() {
        return (EReference)getElementValue().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementValue_AnnotationValue() {
        return (EReference)getElementValue().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementValue_ArrayValue() {
        return (EReference)getElementValue().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getElementValue_ClassValue() {
        return (EReference)getElementValue().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getElementaryValue() {
		if (elementaryValueEClass == null) {
			elementaryValueEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(15);
		}
		return elementaryValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBooleanValue() {
		if (booleanValueEClass == null) {
			booleanValueEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(16);
		}
		return booleanValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBooleanValue_Value() {
        return (EAttribute)getBooleanValue().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCharValue() {
		if (charValueEClass == null) {
			charValueEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(17);
		}
		return charValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCharValue_Value() {
        return (EAttribute)getCharValue().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getByteValue() {
		if (byteValueEClass == null) {
			byteValueEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(18);
		}
		return byteValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getByteValue_Value() {
        return (EAttribute)getByteValue().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getShortValue() {
		if (shortValueEClass == null) {
			shortValueEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(19);
		}
		return shortValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShortValue_Value() {
        return (EAttribute)getShortValue().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIntValue() {
		if (intValueEClass == null) {
			intValueEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(20);
		}
		return intValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIntValue_Value() {
        return (EAttribute)getIntValue().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLongValue() {
		if (longValueEClass == null) {
			longValueEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(21);
		}
		return longValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLongValue_Value() {
        return (EAttribute)getLongValue().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFloatValue() {
		if (floatValueEClass == null) {
			floatValueEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(22);
		}
		return floatValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFloatValue_Value() {
        return (EAttribute)getFloatValue().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDoubleValue() {
		if (doubleValueEClass == null) {
			doubleValueEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(23);
		}
		return doubleValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDoubleValue_Value() {
        return (EAttribute)getDoubleValue().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStringValue() {
		if (stringValueEClass == null) {
			stringValueEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(24);
		}
		return stringValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStringValue_Value() {
        return (EAttribute)getStringValue().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEnumValue() {
		if (enumValueEClass == null) {
			enumValueEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(25);
		}
		return enumValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEnumValue_ConstName() {
        return (EAttribute)getEnumValue().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEnumValue_Type() {
        return (EReference)getEnumValue().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMethod() {
		if (methodEClass == null) {
			methodEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(26);
		}
		return methodEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMethod_Class() {
        return (EReference)getMethod().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMethod_Name() {
        return (EAttribute)getMethod().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMethod_Instructions() {
        return (EReference)getMethod().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMethod_FirstInstruction() {
        return (EReference)getMethod().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMethod_Public() {
        return (EAttribute)getMethod().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMethod_Private() {
        return (EAttribute)getMethod().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMethod_Protected() {
        return (EAttribute)getMethod().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMethod_Static() {
        return (EAttribute)getMethod().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMethod_Final() {
        return (EAttribute)getMethod().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMethod_Synthetic() {
        return (EAttribute)getMethod().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMethod_Synchronized() {
        return (EAttribute)getMethod().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMethod_Bridge() {
        return (EAttribute)getMethod().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMethod_VarArgs() {
        return (EAttribute)getMethod().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMethod_Native() {
        return (EAttribute)getMethod().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMethod_Abstract() {
        return (EAttribute)getMethod().getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMethod_Strict() {
        return (EAttribute)getMethod().getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMethod_Deprecated() {
        return (EAttribute)getMethod().getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMethod_RuntimeInvisibleAnnotations() {
        return (EReference)getMethod().getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMethod_RuntimeVisibleAnnotations() {
        return (EReference)getMethod().getEStructuralFeatures().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMethod_AnnotationDefaultValue() {
        return (EReference)getMethod().getEStructuralFeatures().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMethod_Descriptor() {
        return (EReference)getMethod().getEStructuralFeatures().get(20);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMethod_Signature() {
        return (EReference)getMethod().getEStructuralFeatures().get(21);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMethod_ExceptionsCanBeThrown() {
        return (EReference)getMethod().getEStructuralFeatures().get(22);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMethod_ExceptionTable() {
        return (EReference)getMethod().getEStructuralFeatures().get(23);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMethod_LocalVariableTable() {
        return (EReference)getMethod().getEStructuralFeatures().get(24);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMethod_RuntimeInvisibleParameterAnnotations() {
        return (EReference)getMethod().getEStructuralFeatures().get(25);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMethod_RuntimeVisibleParameterAnnotations() {
        return (EReference)getMethod().getEStructuralFeatures().get(26);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLocalVariableTableEntry() {
		if (localVariableTableEntryEClass == null) {
			localVariableTableEntryEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(27);
		}
		return localVariableTableEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLocalVariableTableEntry_Method() {
        return (EReference)getLocalVariableTableEntry().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLocalVariableTableEntry_StartInstruction() {
        return (EReference)getLocalVariableTableEntry().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLocalVariableTableEntry_EndInstruction() {
        return (EReference)getLocalVariableTableEntry().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLocalVariableTableEntry_Name() {
        return (EAttribute)getLocalVariableTableEntry().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLocalVariableTableEntry_Index() {
        return (EAttribute)getLocalVariableTableEntry().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLocalVariableTableEntry_Signature() {
        return (EReference)getLocalVariableTableEntry().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLocalVariableTableEntry_Descriptor() {
        return (EReference)getLocalVariableTableEntry().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExceptionTableEntry() {
		if (exceptionTableEntryEClass == null) {
			exceptionTableEntryEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(28);
		}
		return exceptionTableEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExceptionTableEntry_Method() {
        return (EReference)getExceptionTableEntry().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExceptionTableEntry_StartInstruction() {
        return (EReference)getExceptionTableEntry().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExceptionTableEntry_EndInstruction() {
        return (EReference)getExceptionTableEntry().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExceptionTableEntry_HandlerInstruction() {
        return (EReference)getExceptionTableEntry().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExceptionTableEntry_CatchType() {
        return (EReference)getExceptionTableEntry().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getControlFlowEdge() {
		if (controlFlowEdgeEClass == null) {
			controlFlowEdgeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(29);
		}
		return controlFlowEdgeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getControlFlowEdge_Start() {
        return (EReference)getControlFlowEdge().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getControlFlowEdge_End() {
        return (EReference)getControlFlowEdge().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUnconditionalEdge() {
		if (unconditionalEdgeEClass == null) {
			unconditionalEdgeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(30);
		}
		return unconditionalEdgeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConditionalEdge() {
		if (conditionalEdgeEClass == null) {
			conditionalEdgeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(31);
		}
		return conditionalEdgeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getConditionalEdge_Condition() {
        return (EAttribute)getConditionalEdge().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSwitchCaseEdge() {
		if (switchCaseEdgeEClass == null) {
			switchCaseEdgeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(32);
		}
		return switchCaseEdgeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSwitchCaseEdge_Condition() {
        return (EAttribute)getSwitchCaseEdge().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSwitchDefaultEdge() {
		if (switchDefaultEdgeEClass == null) {
			switchDefaultEdgeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(33);
		}
		return switchDefaultEdgeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExceptionalEdge() {
		if (exceptionalEdgeEClass == null) {
			exceptionalEdgeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(34);
		}
		return exceptionalEdgeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExceptionalEdge_ExceptionTableEntry() {
        return (EReference)getExceptionalEdge().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInstruction() {
		if (instructionEClass == null) {
			instructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(35);
		}
		return instructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInstruction_Method() {
        return (EReference)getInstruction().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInstruction_NextInCodeOrder() {
        return (EReference)getInstruction().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInstruction_PreviousInCodeOrder() {
        return (EReference)getInstruction().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInstruction_Linenumber() {
        return (EAttribute)getInstruction().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInstruction_Index() {
        return (EAttribute)getInstruction().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInstruction_Opcode() {
        return (EAttribute)getInstruction().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInstruction_HumanReadable() {
        return (EAttribute)getInstruction().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInstruction_OutEdges() {
        return (EReference)getInstruction().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInstruction_InEdges() {
        return (EReference)getInstruction().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFieldInstruction() {
		if (fieldInstructionEClass == null) {
			fieldInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(36);
		}
		return fieldInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFieldInstruction_FieldReference() {
        return (EReference)getFieldInstruction().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSimpleInstruction() {
		if (simpleInstructionEClass == null) {
			simpleInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(37);
		}
		return simpleInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIntInstruction() {
		if (intInstructionEClass == null) {
			intInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(38);
		}
		return intInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIntInstruction_Operand() {
        return (EAttribute)getIntInstruction().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getJumpInstruction() {
		if (jumpInstructionEClass == null) {
			jumpInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(39);
		}
		return jumpInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLdcInstruction() {
		if (ldcInstructionEClass == null) {
			ldcInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(40);
		}
		return ldcInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMethodInstruction() {
		if (methodInstructionEClass == null) {
			methodInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(41);
		}
		return methodInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMethodInstruction_MethodReference() {
        return (EReference)getMethodInstruction().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTypeInstruction() {
		if (typeInstructionEClass == null) {
			typeInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(42);
		}
		return typeInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTypeInstruction_TypeReference() {
        return (EReference)getTypeInstruction().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVarInstruction() {
		if (varInstructionEClass == null) {
			varInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(43);
		}
		return varInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVarInstruction_LocalVariable() {
        return (EReference)getVarInstruction().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVarInstruction_VarIndex() {
        return (EAttribute)getVarInstruction().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIincInstruction() {
		if (iincInstructionEClass == null) {
			iincInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(44);
		}
		return iincInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIincInstruction_Incr() {
        return (EAttribute)getIincInstruction().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMultianewarrayInstruction() {
		if (multianewarrayInstructionEClass == null) {
			multianewarrayInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(45);
		}
		return multianewarrayInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMultianewarrayInstruction_TypeReference() {
        return (EReference)getMultianewarrayInstruction().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMultianewarrayInstruction_Dims() {
        return (EAttribute)getMultianewarrayInstruction().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSwitchInstruction() {
		if (switchInstructionEClass == null) {
			switchInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(46);
		}
		return switchInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLdcIntInstruction() {
		if (ldcIntInstructionEClass == null) {
			ldcIntInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(47);
		}
		return ldcIntInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLdcIntInstruction_Constant() {
        return (EAttribute)getLdcIntInstruction().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLdcLongInstruction() {
		if (ldcLongInstructionEClass == null) {
			ldcLongInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(48);
		}
		return ldcLongInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLdcLongInstruction_Constant() {
        return (EAttribute)getLdcLongInstruction().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLdcFloatInstruction() {
		if (ldcFloatInstructionEClass == null) {
			ldcFloatInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(49);
		}
		return ldcFloatInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLdcFloatInstruction_Constant() {
        return (EAttribute)getLdcFloatInstruction().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLdcDoubleInstruction() {
		if (ldcDoubleInstructionEClass == null) {
			ldcDoubleInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(50);
		}
		return ldcDoubleInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLdcDoubleInstruction_Constant() {
        return (EAttribute)getLdcDoubleInstruction().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLdcStringInstruction() {
		if (ldcStringInstructionEClass == null) {
			ldcStringInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(51);
		}
		return ldcStringInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLdcStringInstruction_Constant() {
        return (EAttribute)getLdcStringInstruction().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLdcTypeInstruction() {
		if (ldcTypeInstructionEClass == null) {
			ldcTypeInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(52);
		}
		return ldcTypeInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLdcTypeInstruction_Constant() {
        return (EReference)getLdcTypeInstruction().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGetstaticInstruction() {
		if (getstaticInstructionEClass == null) {
			getstaticInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(53);
		}
		return getstaticInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPutstaticInstruction() {
		if (putstaticInstructionEClass == null) {
			putstaticInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(54);
		}
		return putstaticInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGetfieldInstruction() {
		if (getfieldInstructionEClass == null) {
			getfieldInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(55);
		}
		return getfieldInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPutfieldInstruction() {
		if (putfieldInstructionEClass == null) {
			putfieldInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(56);
		}
		return putfieldInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNopInstruction() {
		if (nopInstructionEClass == null) {
			nopInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(57);
		}
		return nopInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAconst_nullInstruction() {
		if (aconst_nullInstructionEClass == null) {
			aconst_nullInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(58);
		}
		return aconst_nullInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIconst_m1Instruction() {
		if (iconst_m1InstructionEClass == null) {
			iconst_m1InstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(59);
		}
		return iconst_m1InstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIconst_0Instruction() {
		if (iconst_0InstructionEClass == null) {
			iconst_0InstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(60);
		}
		return iconst_0InstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIconst_1Instruction() {
		if (iconst_1InstructionEClass == null) {
			iconst_1InstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(61);
		}
		return iconst_1InstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIconst_2Instruction() {
		if (iconst_2InstructionEClass == null) {
			iconst_2InstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(62);
		}
		return iconst_2InstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIconst_3Instruction() {
		if (iconst_3InstructionEClass == null) {
			iconst_3InstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(63);
		}
		return iconst_3InstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIconst_4Instruction() {
		if (iconst_4InstructionEClass == null) {
			iconst_4InstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(64);
		}
		return iconst_4InstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIconst_5Instruction() {
		if (iconst_5InstructionEClass == null) {
			iconst_5InstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(65);
		}
		return iconst_5InstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLconst_0Instruction() {
		if (lconst_0InstructionEClass == null) {
			lconst_0InstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(66);
		}
		return lconst_0InstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLconst_1Instruction() {
		if (lconst_1InstructionEClass == null) {
			lconst_1InstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(67);
		}
		return lconst_1InstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFconst_0Instruction() {
		if (fconst_0InstructionEClass == null) {
			fconst_0InstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(68);
		}
		return fconst_0InstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFconst_1Instruction() {
		if (fconst_1InstructionEClass == null) {
			fconst_1InstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(69);
		}
		return fconst_1InstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFconst_2Instruction() {
		if (fconst_2InstructionEClass == null) {
			fconst_2InstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(70);
		}
		return fconst_2InstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDconst_0Instruction() {
		if (dconst_0InstructionEClass == null) {
			dconst_0InstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(71);
		}
		return dconst_0InstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDconst_1Instruction() {
		if (dconst_1InstructionEClass == null) {
			dconst_1InstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(72);
		}
		return dconst_1InstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIaloadInstruction() {
		if (ialoadInstructionEClass == null) {
			ialoadInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(73);
		}
		return ialoadInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLaloadInstruction() {
		if (laloadInstructionEClass == null) {
			laloadInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(74);
		}
		return laloadInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFaloadInstruction() {
		if (faloadInstructionEClass == null) {
			faloadInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(75);
		}
		return faloadInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDaloadInstruction() {
		if (daloadInstructionEClass == null) {
			daloadInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(76);
		}
		return daloadInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAaloadInstruction() {
		if (aaloadInstructionEClass == null) {
			aaloadInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(77);
		}
		return aaloadInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBaloadInstruction() {
		if (baloadInstructionEClass == null) {
			baloadInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(78);
		}
		return baloadInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCaloadInstruction() {
		if (caloadInstructionEClass == null) {
			caloadInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(79);
		}
		return caloadInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSaloadInstruction() {
		if (saloadInstructionEClass == null) {
			saloadInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(80);
		}
		return saloadInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIastoreInstruction() {
		if (iastoreInstructionEClass == null) {
			iastoreInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(81);
		}
		return iastoreInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLastoreInstruction() {
		if (lastoreInstructionEClass == null) {
			lastoreInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(82);
		}
		return lastoreInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFastoreInstruction() {
		if (fastoreInstructionEClass == null) {
			fastoreInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(83);
		}
		return fastoreInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDastoreInstruction() {
		if (dastoreInstructionEClass == null) {
			dastoreInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(84);
		}
		return dastoreInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAastoreInstruction() {
		if (aastoreInstructionEClass == null) {
			aastoreInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(85);
		}
		return aastoreInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBastoreInstruction() {
		if (bastoreInstructionEClass == null) {
			bastoreInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(86);
		}
		return bastoreInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCastoreInstruction() {
		if (castoreInstructionEClass == null) {
			castoreInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(87);
		}
		return castoreInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSastoreInstruction() {
		if (sastoreInstructionEClass == null) {
			sastoreInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(88);
		}
		return sastoreInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPopInstruction() {
		if (popInstructionEClass == null) {
			popInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(89);
		}
		return popInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPop2Instruction() {
		if (pop2InstructionEClass == null) {
			pop2InstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(90);
		}
		return pop2InstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDupInstruction() {
		if (dupInstructionEClass == null) {
			dupInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(91);
		}
		return dupInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDup_x1Instruction() {
		if (dup_x1InstructionEClass == null) {
			dup_x1InstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(92);
		}
		return dup_x1InstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDup_x2Instruction() {
		if (dup_x2InstructionEClass == null) {
			dup_x2InstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(93);
		}
		return dup_x2InstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDup2Instruction() {
		if (dup2InstructionEClass == null) {
			dup2InstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(94);
		}
		return dup2InstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDup2_x1Instruction() {
		if (dup2_x1InstructionEClass == null) {
			dup2_x1InstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(95);
		}
		return dup2_x1InstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDup2_x2Instruction() {
		if (dup2_x2InstructionEClass == null) {
			dup2_x2InstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(96);
		}
		return dup2_x2InstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSwapInstruction() {
		if (swapInstructionEClass == null) {
			swapInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(97);
		}
		return swapInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIaddInstruction() {
		if (iaddInstructionEClass == null) {
			iaddInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(98);
		}
		return iaddInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLaddInstruction() {
		if (laddInstructionEClass == null) {
			laddInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(99);
		}
		return laddInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFaddInstruction() {
		if (faddInstructionEClass == null) {
			faddInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(100);
		}
		return faddInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDaddInstruction() {
		if (daddInstructionEClass == null) {
			daddInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(101);
		}
		return daddInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIsubInstruction() {
		if (isubInstructionEClass == null) {
			isubInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(102);
		}
		return isubInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLsubInstruction() {
		if (lsubInstructionEClass == null) {
			lsubInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(103);
		}
		return lsubInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFsubInstruction() {
		if (fsubInstructionEClass == null) {
			fsubInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(104);
		}
		return fsubInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDsubInstruction() {
		if (dsubInstructionEClass == null) {
			dsubInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(105);
		}
		return dsubInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getImulInstruction() {
		if (imulInstructionEClass == null) {
			imulInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(106);
		}
		return imulInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLmulInstruction() {
		if (lmulInstructionEClass == null) {
			lmulInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(107);
		}
		return lmulInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFmulInstruction() {
		if (fmulInstructionEClass == null) {
			fmulInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(108);
		}
		return fmulInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDmulInstruction() {
		if (dmulInstructionEClass == null) {
			dmulInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(109);
		}
		return dmulInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIdivInstruction() {
		if (idivInstructionEClass == null) {
			idivInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(110);
		}
		return idivInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLdivInstruction() {
		if (ldivInstructionEClass == null) {
			ldivInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(111);
		}
		return ldivInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFdivInstruction() {
		if (fdivInstructionEClass == null) {
			fdivInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(112);
		}
		return fdivInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDdivInstruction() {
		if (ddivInstructionEClass == null) {
			ddivInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(113);
		}
		return ddivInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIremInstruction() {
		if (iremInstructionEClass == null) {
			iremInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(114);
		}
		return iremInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLremInstruction() {
		if (lremInstructionEClass == null) {
			lremInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(115);
		}
		return lremInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFremInstruction() {
		if (fremInstructionEClass == null) {
			fremInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(116);
		}
		return fremInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDremInstruction() {
		if (dremInstructionEClass == null) {
			dremInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(117);
		}
		return dremInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInegInstruction() {
		if (inegInstructionEClass == null) {
			inegInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(118);
		}
		return inegInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLnegInstruction() {
		if (lnegInstructionEClass == null) {
			lnegInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(119);
		}
		return lnegInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFnegInstruction() {
		if (fnegInstructionEClass == null) {
			fnegInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(120);
		}
		return fnegInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDnegInstruction() {
		if (dnegInstructionEClass == null) {
			dnegInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(121);
		}
		return dnegInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIshlInstruction() {
		if (ishlInstructionEClass == null) {
			ishlInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(122);
		}
		return ishlInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLshlInstruction() {
		if (lshlInstructionEClass == null) {
			lshlInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(123);
		}
		return lshlInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIshrInstruction() {
		if (ishrInstructionEClass == null) {
			ishrInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(124);
		}
		return ishrInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLshrInstruction() {
		if (lshrInstructionEClass == null) {
			lshrInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(125);
		}
		return lshrInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIushrInstruction() {
		if (iushrInstructionEClass == null) {
			iushrInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(126);
		}
		return iushrInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLushrInstruction() {
		if (lushrInstructionEClass == null) {
			lushrInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(127);
		}
		return lushrInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIandInstruction() {
		if (iandInstructionEClass == null) {
			iandInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(128);
		}
		return iandInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLandInstruction() {
		if (landInstructionEClass == null) {
			landInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(129);
		}
		return landInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIorInstruction() {
		if (iorInstructionEClass == null) {
			iorInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(130);
		}
		return iorInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLorInstruction() {
		if (lorInstructionEClass == null) {
			lorInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(131);
		}
		return lorInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIxorInstruction() {
		if (ixorInstructionEClass == null) {
			ixorInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(132);
		}
		return ixorInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLxorInstruction() {
		if (lxorInstructionEClass == null) {
			lxorInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(133);
		}
		return lxorInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getI2lInstruction() {
		if (i2lInstructionEClass == null) {
			i2lInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(134);
		}
		return i2lInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getI2fInstruction() {
		if (i2fInstructionEClass == null) {
			i2fInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(135);
		}
		return i2fInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getI2dInstruction() {
		if (i2dInstructionEClass == null) {
			i2dInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(136);
		}
		return i2dInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getL2iInstruction() {
		if (l2iInstructionEClass == null) {
			l2iInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(137);
		}
		return l2iInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getL2fInstruction() {
		if (l2fInstructionEClass == null) {
			l2fInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(138);
		}
		return l2fInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getL2dInstruction() {
		if (l2dInstructionEClass == null) {
			l2dInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(139);
		}
		return l2dInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getF2iInstruction() {
		if (f2iInstructionEClass == null) {
			f2iInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(140);
		}
		return f2iInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getF2lInstruction() {
		if (f2lInstructionEClass == null) {
			f2lInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(141);
		}
		return f2lInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getF2dInstruction() {
		if (f2dInstructionEClass == null) {
			f2dInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(142);
		}
		return f2dInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getD2iInstruction() {
		if (d2iInstructionEClass == null) {
			d2iInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(143);
		}
		return d2iInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getD2lInstruction() {
		if (d2lInstructionEClass == null) {
			d2lInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(144);
		}
		return d2lInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getD2fInstruction() {
		if (d2fInstructionEClass == null) {
			d2fInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(145);
		}
		return d2fInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getI2bInstruction() {
		if (i2bInstructionEClass == null) {
			i2bInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(146);
		}
		return i2bInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getI2cInstruction() {
		if (i2cInstructionEClass == null) {
			i2cInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(147);
		}
		return i2cInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getI2sInstruction() {
		if (i2sInstructionEClass == null) {
			i2sInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(148);
		}
		return i2sInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLcmpInstruction() {
		if (lcmpInstructionEClass == null) {
			lcmpInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(149);
		}
		return lcmpInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFcmplInstruction() {
		if (fcmplInstructionEClass == null) {
			fcmplInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(150);
		}
		return fcmplInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFcmpgInstruction() {
		if (fcmpgInstructionEClass == null) {
			fcmpgInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(151);
		}
		return fcmpgInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDcmplInstruction() {
		if (dcmplInstructionEClass == null) {
			dcmplInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(152);
		}
		return dcmplInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDcmpgInstruction() {
		if (dcmpgInstructionEClass == null) {
			dcmpgInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(153);
		}
		return dcmpgInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIreturnInstruction() {
		if (ireturnInstructionEClass == null) {
			ireturnInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(154);
		}
		return ireturnInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLreturnInstruction() {
		if (lreturnInstructionEClass == null) {
			lreturnInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(155);
		}
		return lreturnInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFreturnInstruction() {
		if (freturnInstructionEClass == null) {
			freturnInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(156);
		}
		return freturnInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDreturnInstruction() {
		if (dreturnInstructionEClass == null) {
			dreturnInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(157);
		}
		return dreturnInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAreturnInstruction() {
		if (areturnInstructionEClass == null) {
			areturnInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(158);
		}
		return areturnInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReturnInstruction() {
		if (returnInstructionEClass == null) {
			returnInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(159);
		}
		return returnInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getArraylengthInstruction() {
		if (arraylengthInstructionEClass == null) {
			arraylengthInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(160);
		}
		return arraylengthInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAthrowInstruction() {
		if (athrowInstructionEClass == null) {
			athrowInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(161);
		}
		return athrowInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMonitorenterInstruction() {
		if (monitorenterInstructionEClass == null) {
			monitorenterInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(162);
		}
		return monitorenterInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMonitorexitInstruction() {
		if (monitorexitInstructionEClass == null) {
			monitorexitInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(163);
		}
		return monitorexitInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBipushInstruction() {
		if (bipushInstructionEClass == null) {
			bipushInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(164);
		}
		return bipushInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSipushInstruction() {
		if (sipushInstructionEClass == null) {
			sipushInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(165);
		}
		return sipushInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNewarrayInstruction() {
		if (newarrayInstructionEClass == null) {
			newarrayInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(166);
		}
		return newarrayInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIfeqInstruction() {
		if (ifeqInstructionEClass == null) {
			ifeqInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(167);
		}
		return ifeqInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIfneInstruction() {
		if (ifneInstructionEClass == null) {
			ifneInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(168);
		}
		return ifneInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIfltInstruction() {
		if (ifltInstructionEClass == null) {
			ifltInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(169);
		}
		return ifltInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIfgeInstruction() {
		if (ifgeInstructionEClass == null) {
			ifgeInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(170);
		}
		return ifgeInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIfgtInstruction() {
		if (ifgtInstructionEClass == null) {
			ifgtInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(171);
		}
		return ifgtInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIfleInstruction() {
		if (ifleInstructionEClass == null) {
			ifleInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(172);
		}
		return ifleInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIf_icmpeqInstruction() {
		if (if_icmpeqInstructionEClass == null) {
			if_icmpeqInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(173);
		}
		return if_icmpeqInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIf_icmpneInstruction() {
		if (if_icmpneInstructionEClass == null) {
			if_icmpneInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(174);
		}
		return if_icmpneInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIf_icmpltInstruction() {
		if (if_icmpltInstructionEClass == null) {
			if_icmpltInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(175);
		}
		return if_icmpltInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIf_icmpgeInstruction() {
		if (if_icmpgeInstructionEClass == null) {
			if_icmpgeInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(176);
		}
		return if_icmpgeInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIf_icmpgtInstruction() {
		if (if_icmpgtInstructionEClass == null) {
			if_icmpgtInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(177);
		}
		return if_icmpgtInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIf_icmpleInstruction() {
		if (if_icmpleInstructionEClass == null) {
			if_icmpleInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(178);
		}
		return if_icmpleInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIf_acmpeqInstruction() {
		if (if_acmpeqInstructionEClass == null) {
			if_acmpeqInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(179);
		}
		return if_acmpeqInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIf_acmpneInstruction() {
		if (if_acmpneInstructionEClass == null) {
			if_acmpneInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(180);
		}
		return if_acmpneInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGotoInstruction() {
		if (gotoInstructionEClass == null) {
			gotoInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(181);
		}
		return gotoInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getJsrInstruction() {
		if (jsrInstructionEClass == null) {
			jsrInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(182);
		}
		return jsrInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIfnullInstruction() {
		if (ifnullInstructionEClass == null) {
			ifnullInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(183);
		}
		return ifnullInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIfnonnullInstruction() {
		if (ifnonnullInstructionEClass == null) {
			ifnonnullInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(184);
		}
		return ifnonnullInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInvokevirtualInstruction() {
		if (invokevirtualInstructionEClass == null) {
			invokevirtualInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(185);
		}
		return invokevirtualInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInvokespecialInstruction() {
		if (invokespecialInstructionEClass == null) {
			invokespecialInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(186);
		}
		return invokespecialInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInvokestaticInstruction() {
		if (invokestaticInstructionEClass == null) {
			invokestaticInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(187);
		}
		return invokestaticInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInvokeinterfaceInstruction() {
		if (invokeinterfaceInstructionEClass == null) {
			invokeinterfaceInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(188);
		}
		return invokeinterfaceInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNewInstruction() {
		if (newInstructionEClass == null) {
			newInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(189);
		}
		return newInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAnewarrayInstruction() {
		if (anewarrayInstructionEClass == null) {
			anewarrayInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(190);
		}
		return anewarrayInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCheckcastInstruction() {
		if (checkcastInstructionEClass == null) {
			checkcastInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(191);
		}
		return checkcastInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInstanceofInstruction() {
		if (instanceofInstructionEClass == null) {
			instanceofInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(192);
		}
		return instanceofInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIloadInstruction() {
		if (iloadInstructionEClass == null) {
			iloadInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(193);
		}
		return iloadInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLloadInstruction() {
		if (lloadInstructionEClass == null) {
			lloadInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(194);
		}
		return lloadInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFloadInstruction() {
		if (floadInstructionEClass == null) {
			floadInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(195);
		}
		return floadInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDloadInstruction() {
		if (dloadInstructionEClass == null) {
			dloadInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(196);
		}
		return dloadInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAloadInstruction() {
		if (aloadInstructionEClass == null) {
			aloadInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(197);
		}
		return aloadInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIstoreInstruction() {
		if (istoreInstructionEClass == null) {
			istoreInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(198);
		}
		return istoreInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLstoreInstruction() {
		if (lstoreInstructionEClass == null) {
			lstoreInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(199);
		}
		return lstoreInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFstoreInstruction() {
		if (fstoreInstructionEClass == null) {
			fstoreInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(200);
		}
		return fstoreInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDstoreInstruction() {
		if (dstoreInstructionEClass == null) {
			dstoreInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(201);
		}
		return dstoreInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAstoreInstruction() {
		if (astoreInstructionEClass == null) {
			astoreInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(202);
		}
		return astoreInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRetInstruction() {
		if (retInstructionEClass == null) {
			retInstructionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(JbcmmPackage.eNS_URI).getEClassifiers().get(203);
		}
		return retInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JbcmmFactory getJbcmmFactory() {
		return (JbcmmFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isLoaded = false;

	/**
	 * Laods the package and any sub-packages from their serialized form.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void loadPackage() {
		if (isLoaded) return;
		isLoaded = true;

		URL url = getClass().getResource(packageFilename);
		if (url == null) {
			throw new RuntimeException("Missing serialized package: " + packageFilename);
		}
		URI uri = URI.createURI(url.toString());
		Resource resource = new EcoreResourceFactoryImpl().createResource(uri);
		try {
			resource.load(null);
		}
		catch (IOException exception) {
			throw new WrappedException(exception);
		}
		initializeFromLoadedEPackage(this, (EPackage)resource.getContents().get(0));
		createResource(eNS_URI);
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isFixed = false;

	/**
	 * Fixes up the loaded package, to make it appear as if it had been programmatically built.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void fixPackageContents() {
		if (isFixed) return;
		isFixed = true;
		fixEClassifiers();
	}

	/**
	 * Sets the instance class on the given classifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void fixInstanceClass(EClassifier eClassifier) {
		if (eClassifier.getInstanceClassName() == null) {
			eClassifier.setInstanceClassName("nl.utwente.jbcmm." + eClassifier.getName());
			setGeneratedClassName(eClassifier);
		}
	}

} //JbcmmPackageImpl
