/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.FloadInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Fload Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FloadInstructionImpl extends VarInstructionImpl implements FloadInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FloadInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getFloadInstruction();
	}

} //FloadInstructionImpl
