/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.DreturnInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dreturn Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DreturnInstructionImpl extends SimpleInstructionImpl implements DreturnInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DreturnInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getDreturnInstruction();
	}

} //DreturnInstructionImpl
