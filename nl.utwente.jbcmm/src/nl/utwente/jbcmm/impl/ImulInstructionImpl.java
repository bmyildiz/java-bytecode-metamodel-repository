/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.ImulInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Imul Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ImulInstructionImpl extends SimpleInstructionImpl implements ImulInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ImulInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getImulInstruction();
	}

} //ImulInstructionImpl
