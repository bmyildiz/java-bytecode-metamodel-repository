/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.DaddInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dadd Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DaddInstructionImpl extends SimpleInstructionImpl implements DaddInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DaddInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getDaddInstruction();
	}

} //DaddInstructionImpl
