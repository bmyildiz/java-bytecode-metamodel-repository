/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.FstoreInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Fstore Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FstoreInstructionImpl extends VarInstructionImpl implements FstoreInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FstoreInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getFstoreInstruction();
	}

} //FstoreInstructionImpl
