/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.If_icmpgtInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>If icmpgt Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class If_icmpgtInstructionImpl extends JumpInstructionImpl implements If_icmpgtInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected If_icmpgtInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIf_icmpgtInstruction();
	}

} //If_icmpgtInstructionImpl
