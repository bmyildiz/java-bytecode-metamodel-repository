/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.ClassSignature;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Class Signature</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ClassSignatureImpl extends SignatureImpl implements ClassSignature {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClassSignatureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getClassSignature();
	}

} //ClassSignatureImpl
