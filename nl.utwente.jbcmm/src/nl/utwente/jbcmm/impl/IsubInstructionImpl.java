/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.IsubInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Isub Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IsubInstructionImpl extends SimpleInstructionImpl implements IsubInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IsubInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIsubInstruction();
	}

} //IsubInstructionImpl
