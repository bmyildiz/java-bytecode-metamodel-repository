/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.DdivInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ddiv Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DdivInstructionImpl extends SimpleInstructionImpl implements DdivInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DdivInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getDdivInstruction();
	}

} //DdivInstructionImpl
