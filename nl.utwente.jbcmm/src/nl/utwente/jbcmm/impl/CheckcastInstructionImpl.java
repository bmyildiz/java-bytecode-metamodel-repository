/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.CheckcastInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Checkcast Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CheckcastInstructionImpl extends TypeInstructionImpl implements CheckcastInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CheckcastInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getCheckcastInstruction();
	}

} //CheckcastInstructionImpl
