/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.BastoreInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Bastore Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BastoreInstructionImpl extends SimpleInstructionImpl implements BastoreInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BastoreInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getBastoreInstruction();
	}

} //BastoreInstructionImpl
