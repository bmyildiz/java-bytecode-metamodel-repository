/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.DsubInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dsub Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DsubInstructionImpl extends SimpleInstructionImpl implements DsubInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DsubInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getDsubInstruction();
	}

} //DsubInstructionImpl
