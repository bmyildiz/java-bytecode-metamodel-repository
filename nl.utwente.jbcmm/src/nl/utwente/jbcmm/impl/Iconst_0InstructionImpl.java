/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.Iconst_0Instruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Iconst 0Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class Iconst_0InstructionImpl extends SimpleInstructionImpl implements Iconst_0Instruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Iconst_0InstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIconst_0Instruction();
	}

} //Iconst_0InstructionImpl
