/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.IdivInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Idiv Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IdivInstructionImpl extends SimpleInstructionImpl implements IdivInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IdivInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIdivInstruction();
	}

} //IdivInstructionImpl
