/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.BipushInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Bipush Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BipushInstructionImpl extends IntInstructionImpl implements BipushInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BipushInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getBipushInstruction();
	}

} //BipushInstructionImpl
