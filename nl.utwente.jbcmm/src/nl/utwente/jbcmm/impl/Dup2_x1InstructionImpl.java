/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.Dup2_x1Instruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dup2 x1 Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class Dup2_x1InstructionImpl extends SimpleInstructionImpl implements Dup2_x1Instruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Dup2_x1InstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getDup2_x1Instruction();
	}

} //Dup2_x1InstructionImpl
