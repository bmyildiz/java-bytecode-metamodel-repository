/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.F2lInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>F2l Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class F2lInstructionImpl extends SimpleInstructionImpl implements F2lInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected F2lInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getF2lInstruction();
	}

} //F2lInstructionImpl
