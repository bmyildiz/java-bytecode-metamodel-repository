/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.DcmplInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dcmpl Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DcmplInstructionImpl extends SimpleInstructionImpl implements DcmplInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DcmplInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getDcmplInstruction();
	}

} //DcmplInstructionImpl
