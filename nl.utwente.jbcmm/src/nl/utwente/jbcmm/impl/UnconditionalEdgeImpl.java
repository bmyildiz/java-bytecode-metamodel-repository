/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.JbcmmPackage;
import nl.utwente.jbcmm.UnconditionalEdge;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Unconditional Edge</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class UnconditionalEdgeImpl extends ControlFlowEdgeImpl implements UnconditionalEdge {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UnconditionalEdgeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getUnconditionalEdge();
	}

} //UnconditionalEdgeImpl
