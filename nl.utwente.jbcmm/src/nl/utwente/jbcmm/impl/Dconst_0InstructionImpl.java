/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.Dconst_0Instruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dconst 0Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class Dconst_0InstructionImpl extends SimpleInstructionImpl implements Dconst_0Instruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Dconst_0InstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getDconst_0Instruction();
	}

} //Dconst_0InstructionImpl
