/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.IremInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Irem Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IremInstructionImpl extends SimpleInstructionImpl implements IremInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IremInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIremInstruction();
	}

} //IremInstructionImpl
