/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.FcmplInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Fcmpl Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FcmplInstructionImpl extends SimpleInstructionImpl implements FcmplInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FcmplInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getFcmplInstruction();
	}

} //FcmplInstructionImpl
