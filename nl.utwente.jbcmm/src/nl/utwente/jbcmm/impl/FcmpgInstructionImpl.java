/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.FcmpgInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Fcmpg Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FcmpgInstructionImpl extends SimpleInstructionImpl implements FcmpgInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FcmpgInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getFcmpgInstruction();
	}

} //FcmpgInstructionImpl
