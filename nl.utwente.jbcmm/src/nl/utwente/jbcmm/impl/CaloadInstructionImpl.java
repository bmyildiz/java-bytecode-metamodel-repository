/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.CaloadInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Caload Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CaloadInstructionImpl extends SimpleInstructionImpl implements CaloadInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CaloadInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getCaloadInstruction();
	}

} //CaloadInstructionImpl
