/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.JbcmmPackage;
import nl.utwente.jbcmm.SwitchDefaultEdge;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Switch Default Edge</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SwitchDefaultEdgeImpl extends ControlFlowEdgeImpl implements SwitchDefaultEdge {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SwitchDefaultEdgeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getSwitchDefaultEdge();
	}

} //SwitchDefaultEdgeImpl
