/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.If_acmpeqInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>If acmpeq Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class If_acmpeqInstructionImpl extends JumpInstructionImpl implements If_acmpeqInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected If_acmpeqInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIf_acmpeqInstruction();
	}

} //If_acmpeqInstructionImpl
