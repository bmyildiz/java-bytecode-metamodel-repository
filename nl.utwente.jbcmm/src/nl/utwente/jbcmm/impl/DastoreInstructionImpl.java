/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.DastoreInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dastore Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DastoreInstructionImpl extends SimpleInstructionImpl implements DastoreInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DastoreInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getDastoreInstruction();
	}

} //DastoreInstructionImpl
