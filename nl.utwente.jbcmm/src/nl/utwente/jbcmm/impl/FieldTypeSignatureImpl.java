/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.FieldTypeSignature;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Field Type Signature</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FieldTypeSignatureImpl extends SignatureImpl implements FieldTypeSignature {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FieldTypeSignatureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getFieldTypeSignature();
	}

} //FieldTypeSignatureImpl
