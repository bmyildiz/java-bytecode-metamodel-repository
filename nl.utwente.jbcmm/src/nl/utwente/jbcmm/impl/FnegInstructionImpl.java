/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.FnegInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Fneg Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FnegInstructionImpl extends SimpleInstructionImpl implements FnegInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FnegInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getFnegInstruction();
	}

} //FnegInstructionImpl
