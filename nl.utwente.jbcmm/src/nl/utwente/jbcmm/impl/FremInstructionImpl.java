/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.FremInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Frem Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FremInstructionImpl extends SimpleInstructionImpl implements FremInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FremInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getFremInstruction();
	}

} //FremInstructionImpl
