/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.CastoreInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Castore Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CastoreInstructionImpl extends SimpleInstructionImpl implements CastoreInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CastoreInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getCastoreInstruction();
	}

} //CastoreInstructionImpl
