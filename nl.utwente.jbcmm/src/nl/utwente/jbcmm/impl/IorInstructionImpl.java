/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.IorInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ior Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IorInstructionImpl extends SimpleInstructionImpl implements IorInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IorInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIorInstruction();
	}

} //IorInstructionImpl
