/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.IfnonnullInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ifnonnull Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IfnonnullInstructionImpl extends JumpInstructionImpl implements IfnonnullInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IfnonnullInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIfnonnullInstruction();
	}

} //IfnonnullInstructionImpl
