/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.DcmpgInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dcmpg Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DcmpgInstructionImpl extends SimpleInstructionImpl implements DcmpgInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DcmpgInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getDcmpgInstruction();
	}

} //DcmpgInstructionImpl
