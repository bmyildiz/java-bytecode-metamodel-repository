/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.Dup_x1Instruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dup x1 Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class Dup_x1InstructionImpl extends SimpleInstructionImpl implements Dup_x1Instruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Dup_x1InstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getDup_x1Instruction();
	}

} //Dup_x1InstructionImpl
