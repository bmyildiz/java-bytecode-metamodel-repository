/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.AaloadInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Aaload Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AaloadInstructionImpl extends SimpleInstructionImpl implements AaloadInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AaloadInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getAaloadInstruction();
	}

} //AaloadInstructionImpl
