/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.IfltInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Iflt Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IfltInstructionImpl extends JumpInstructionImpl implements IfltInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IfltInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIfltInstruction();
	}

} //IfltInstructionImpl
