/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.IshrInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ishr Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IshrInstructionImpl extends SimpleInstructionImpl implements IshrInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IshrInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIshrInstruction();
	}

} //IshrInstructionImpl
