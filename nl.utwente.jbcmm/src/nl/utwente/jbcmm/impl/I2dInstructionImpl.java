/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.I2dInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>I2d Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class I2dInstructionImpl extends SimpleInstructionImpl implements I2dInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected I2dInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getI2dInstruction();
	}

} //I2dInstructionImpl
