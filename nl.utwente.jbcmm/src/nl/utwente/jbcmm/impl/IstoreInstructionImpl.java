/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.IstoreInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Istore Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IstoreInstructionImpl extends VarInstructionImpl implements IstoreInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IstoreInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIstoreInstruction();
	}

} //IstoreInstructionImpl
