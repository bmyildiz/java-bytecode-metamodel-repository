/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.ControlFlowEdge;
import nl.utwente.jbcmm.Instruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Control Flow Edge</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.impl.ControlFlowEdgeImpl#getStart <em>Start</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.ControlFlowEdgeImpl#getEnd <em>End</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ControlFlowEdgeImpl extends IdentifiableImpl implements ControlFlowEdge {
	/**
	 * The cached value of the '{@link #getEnd() <em>End</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnd()
	 * @generated
	 * @ordered
	 */
	protected Instruction end;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ControlFlowEdgeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getControlFlowEdge();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getStart() {
		if (eContainerFeatureID() != JbcmmPackage.CONTROL_FLOW_EDGE__START) return null;
		return (Instruction)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStart(Instruction newStart, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newStart, JbcmmPackage.CONTROL_FLOW_EDGE__START, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStart(Instruction newStart) {
		if (newStart != eInternalContainer() || (eContainerFeatureID() != JbcmmPackage.CONTROL_FLOW_EDGE__START && newStart != null)) {
			if (EcoreUtil.isAncestor(this, newStart))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newStart != null)
				msgs = ((InternalEObject)newStart).eInverseAdd(this, JbcmmPackage.INSTRUCTION__OUT_EDGES, Instruction.class, msgs);
			msgs = basicSetStart(newStart, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.CONTROL_FLOW_EDGE__START, newStart, newStart));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getEnd() {
		if (end != null && end.eIsProxy()) {
			InternalEObject oldEnd = (InternalEObject)end;
			end = (Instruction)eResolveProxy(oldEnd);
			if (end != oldEnd) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, JbcmmPackage.CONTROL_FLOW_EDGE__END, oldEnd, end));
			}
		}
		return end;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction basicGetEnd() {
		return end;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEnd(Instruction newEnd, NotificationChain msgs) {
		Instruction oldEnd = end;
		end = newEnd;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JbcmmPackage.CONTROL_FLOW_EDGE__END, oldEnd, newEnd);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnd(Instruction newEnd) {
		if (newEnd != end) {
			NotificationChain msgs = null;
			if (end != null)
				msgs = ((InternalEObject)end).eInverseRemove(this, JbcmmPackage.INSTRUCTION__IN_EDGES, Instruction.class, msgs);
			if (newEnd != null)
				msgs = ((InternalEObject)newEnd).eInverseAdd(this, JbcmmPackage.INSTRUCTION__IN_EDGES, Instruction.class, msgs);
			msgs = basicSetEnd(newEnd, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.CONTROL_FLOW_EDGE__END, newEnd, newEnd));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case JbcmmPackage.CONTROL_FLOW_EDGE__START:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetStart((Instruction)otherEnd, msgs);
			case JbcmmPackage.CONTROL_FLOW_EDGE__END:
				if (end != null)
					msgs = ((InternalEObject)end).eInverseRemove(this, JbcmmPackage.INSTRUCTION__IN_EDGES, Instruction.class, msgs);
				return basicSetEnd((Instruction)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case JbcmmPackage.CONTROL_FLOW_EDGE__START:
				return basicSetStart(null, msgs);
			case JbcmmPackage.CONTROL_FLOW_EDGE__END:
				return basicSetEnd(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case JbcmmPackage.CONTROL_FLOW_EDGE__START:
				return eInternalContainer().eInverseRemove(this, JbcmmPackage.INSTRUCTION__OUT_EDGES, Instruction.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case JbcmmPackage.CONTROL_FLOW_EDGE__START:
				return getStart();
			case JbcmmPackage.CONTROL_FLOW_EDGE__END:
				if (resolve) return getEnd();
				return basicGetEnd();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case JbcmmPackage.CONTROL_FLOW_EDGE__START:
				setStart((Instruction)newValue);
				return;
			case JbcmmPackage.CONTROL_FLOW_EDGE__END:
				setEnd((Instruction)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case JbcmmPackage.CONTROL_FLOW_EDGE__START:
				setStart((Instruction)null);
				return;
			case JbcmmPackage.CONTROL_FLOW_EDGE__END:
				setEnd((Instruction)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case JbcmmPackage.CONTROL_FLOW_EDGE__START:
				return getStart() != null;
			case JbcmmPackage.CONTROL_FLOW_EDGE__END:
				return end != null;
		}
		return super.eIsSet(featureID);
	}

} //ControlFlowEdgeImpl
