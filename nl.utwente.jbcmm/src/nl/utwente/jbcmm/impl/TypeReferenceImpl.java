/**
 */
package nl.utwente.jbcmm.impl;

import java.util.Collection;

import nl.utwente.jbcmm.Annotation;
import nl.utwente.jbcmm.Clazz;
import nl.utwente.jbcmm.JbcmmPackage;
import nl.utwente.jbcmm.TypeReference;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Type Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.impl.TypeReferenceImpl#getReferencedClass <em>Referenced Class</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.TypeReferenceImpl#getTypeDescriptor <em>Type Descriptor</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.TypeReferenceImpl#getRuntimeInvisibleAnnotations <em>Runtime Invisible Annotations</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.TypeReferenceImpl#getRuntimeVisibleAnnotations <em>Runtime Visible Annotations</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TypeReferenceImpl extends IdentifiableImpl implements TypeReference {
	/**
	 * The cached value of the '{@link #getReferencedClass() <em>Referenced Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferencedClass()
	 * @generated
	 * @ordered
	 */
	protected Clazz referencedClass;

	/**
	 * The default value of the '{@link #getTypeDescriptor() <em>Type Descriptor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeDescriptor()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_DESCRIPTOR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTypeDescriptor() <em>Type Descriptor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeDescriptor()
	 * @generated
	 * @ordered
	 */
	protected String typeDescriptor = TYPE_DESCRIPTOR_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRuntimeInvisibleAnnotations() <em>Runtime Invisible Annotations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRuntimeInvisibleAnnotations()
	 * @generated
	 * @ordered
	 */
	protected EList<Annotation> runtimeInvisibleAnnotations;

	/**
	 * The cached value of the '{@link #getRuntimeVisibleAnnotations() <em>Runtime Visible Annotations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRuntimeVisibleAnnotations()
	 * @generated
	 * @ordered
	 */
	protected EList<Annotation> runtimeVisibleAnnotations;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TypeReferenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getTypeReference();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Clazz getReferencedClass() {
		if (referencedClass != null && referencedClass.eIsProxy()) {
			InternalEObject oldReferencedClass = (InternalEObject)referencedClass;
			referencedClass = (Clazz)eResolveProxy(oldReferencedClass);
			if (referencedClass != oldReferencedClass) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, JbcmmPackage.TYPE_REFERENCE__REFERENCED_CLASS, oldReferencedClass, referencedClass));
			}
		}
		return referencedClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Clazz basicGetReferencedClass() {
		return referencedClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReferencedClass(Clazz newReferencedClass) {
		Clazz oldReferencedClass = referencedClass;
		referencedClass = newReferencedClass;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.TYPE_REFERENCE__REFERENCED_CLASS, oldReferencedClass, referencedClass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTypeDescriptor() {
		return typeDescriptor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTypeDescriptor(String newTypeDescriptor) {
		String oldTypeDescriptor = typeDescriptor;
		typeDescriptor = newTypeDescriptor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.TYPE_REFERENCE__TYPE_DESCRIPTOR, oldTypeDescriptor, typeDescriptor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Annotation> getRuntimeInvisibleAnnotations() {
		if (runtimeInvisibleAnnotations == null) {
			runtimeInvisibleAnnotations = new EObjectContainmentEList<Annotation>(Annotation.class, this, JbcmmPackage.TYPE_REFERENCE__RUNTIME_INVISIBLE_ANNOTATIONS);
		}
		return runtimeInvisibleAnnotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Annotation> getRuntimeVisibleAnnotations() {
		if (runtimeVisibleAnnotations == null) {
			runtimeVisibleAnnotations = new EObjectContainmentEList<Annotation>(Annotation.class, this, JbcmmPackage.TYPE_REFERENCE__RUNTIME_VISIBLE_ANNOTATIONS);
		}
		return runtimeVisibleAnnotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case JbcmmPackage.TYPE_REFERENCE__RUNTIME_INVISIBLE_ANNOTATIONS:
				return ((InternalEList<?>)getRuntimeInvisibleAnnotations()).basicRemove(otherEnd, msgs);
			case JbcmmPackage.TYPE_REFERENCE__RUNTIME_VISIBLE_ANNOTATIONS:
				return ((InternalEList<?>)getRuntimeVisibleAnnotations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case JbcmmPackage.TYPE_REFERENCE__REFERENCED_CLASS:
				if (resolve) return getReferencedClass();
				return basicGetReferencedClass();
			case JbcmmPackage.TYPE_REFERENCE__TYPE_DESCRIPTOR:
				return getTypeDescriptor();
			case JbcmmPackage.TYPE_REFERENCE__RUNTIME_INVISIBLE_ANNOTATIONS:
				return getRuntimeInvisibleAnnotations();
			case JbcmmPackage.TYPE_REFERENCE__RUNTIME_VISIBLE_ANNOTATIONS:
				return getRuntimeVisibleAnnotations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case JbcmmPackage.TYPE_REFERENCE__REFERENCED_CLASS:
				setReferencedClass((Clazz)newValue);
				return;
			case JbcmmPackage.TYPE_REFERENCE__TYPE_DESCRIPTOR:
				setTypeDescriptor((String)newValue);
				return;
			case JbcmmPackage.TYPE_REFERENCE__RUNTIME_INVISIBLE_ANNOTATIONS:
				getRuntimeInvisibleAnnotations().clear();
				getRuntimeInvisibleAnnotations().addAll((Collection<? extends Annotation>)newValue);
				return;
			case JbcmmPackage.TYPE_REFERENCE__RUNTIME_VISIBLE_ANNOTATIONS:
				getRuntimeVisibleAnnotations().clear();
				getRuntimeVisibleAnnotations().addAll((Collection<? extends Annotation>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case JbcmmPackage.TYPE_REFERENCE__REFERENCED_CLASS:
				setReferencedClass((Clazz)null);
				return;
			case JbcmmPackage.TYPE_REFERENCE__TYPE_DESCRIPTOR:
				setTypeDescriptor(TYPE_DESCRIPTOR_EDEFAULT);
				return;
			case JbcmmPackage.TYPE_REFERENCE__RUNTIME_INVISIBLE_ANNOTATIONS:
				getRuntimeInvisibleAnnotations().clear();
				return;
			case JbcmmPackage.TYPE_REFERENCE__RUNTIME_VISIBLE_ANNOTATIONS:
				getRuntimeVisibleAnnotations().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case JbcmmPackage.TYPE_REFERENCE__REFERENCED_CLASS:
				return referencedClass != null;
			case JbcmmPackage.TYPE_REFERENCE__TYPE_DESCRIPTOR:
				return TYPE_DESCRIPTOR_EDEFAULT == null ? typeDescriptor != null : !TYPE_DESCRIPTOR_EDEFAULT.equals(typeDescriptor);
			case JbcmmPackage.TYPE_REFERENCE__RUNTIME_INVISIBLE_ANNOTATIONS:
				return runtimeInvisibleAnnotations != null && !runtimeInvisibleAnnotations.isEmpty();
			case JbcmmPackage.TYPE_REFERENCE__RUNTIME_VISIBLE_ANNOTATIONS:
				return runtimeVisibleAnnotations != null && !runtimeVisibleAnnotations.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (typeDescriptor: ");
		result.append(typeDescriptor);
		result.append(')');
		return result.toString();
	}

} //TypeReferenceImpl
