/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.F2dInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>F2d Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class F2dInstructionImpl extends SimpleInstructionImpl implements F2dInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected F2dInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getF2dInstruction();
	}

} //F2dInstructionImpl
