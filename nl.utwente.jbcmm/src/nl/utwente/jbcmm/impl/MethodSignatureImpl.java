/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.JbcmmPackage;
import nl.utwente.jbcmm.MethodSignature;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Method Signature</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class MethodSignatureImpl extends SignatureImpl implements MethodSignature {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MethodSignatureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getMethodSignature();
	}

} //MethodSignatureImpl
