/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.FreturnInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Freturn Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FreturnInstructionImpl extends SimpleInstructionImpl implements FreturnInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FreturnInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getFreturnInstruction();
	}

} //FreturnInstructionImpl
