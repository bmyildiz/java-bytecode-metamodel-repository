/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.If_icmpltInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>If icmplt Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class If_icmpltInstructionImpl extends JumpInstructionImpl implements If_icmpltInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected If_icmpltInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIf_icmpltInstruction();
	}

} //If_icmpltInstructionImpl
