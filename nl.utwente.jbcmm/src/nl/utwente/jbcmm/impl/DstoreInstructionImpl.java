/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.DstoreInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dstore Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DstoreInstructionImpl extends VarInstructionImpl implements DstoreInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DstoreInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getDstoreInstruction();
	}

} //DstoreInstructionImpl
