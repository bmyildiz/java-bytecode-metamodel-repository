/**
 */
package nl.utwente.jbcmm.impl;

import java.util.Collection;
import nl.utwente.jbcmm.JbcmmPackage;
import nl.utwente.jbcmm.MethodDescriptor;
import nl.utwente.jbcmm.TypeReference;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Method Descriptor</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.impl.MethodDescriptorImpl#getParameterTypes <em>Parameter Types</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.MethodDescriptorImpl#getResultType <em>Result Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MethodDescriptorImpl extends IdentifiableImpl implements MethodDescriptor {
	/**
	 * The cached value of the '{@link #getParameterTypes() <em>Parameter Types</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameterTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<TypeReference> parameterTypes;

	/**
	 * The cached value of the '{@link #getResultType() <em>Result Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResultType()
	 * @generated
	 * @ordered
	 */
	protected TypeReference resultType;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MethodDescriptorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getMethodDescriptor();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TypeReference> getParameterTypes() {
		if (parameterTypes == null) {
			parameterTypes = new EObjectContainmentEList<TypeReference>(TypeReference.class, this, JbcmmPackage.METHOD_DESCRIPTOR__PARAMETER_TYPES);
		}
		return parameterTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeReference getResultType() {
		return resultType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetResultType(TypeReference newResultType, NotificationChain msgs) {
		TypeReference oldResultType = resultType;
		resultType = newResultType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JbcmmPackage.METHOD_DESCRIPTOR__RESULT_TYPE, oldResultType, newResultType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResultType(TypeReference newResultType) {
		if (newResultType != resultType) {
			NotificationChain msgs = null;
			if (resultType != null)
				msgs = ((InternalEObject)resultType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.METHOD_DESCRIPTOR__RESULT_TYPE, null, msgs);
			if (newResultType != null)
				msgs = ((InternalEObject)newResultType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.METHOD_DESCRIPTOR__RESULT_TYPE, null, msgs);
			msgs = basicSetResultType(newResultType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.METHOD_DESCRIPTOR__RESULT_TYPE, newResultType, newResultType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case JbcmmPackage.METHOD_DESCRIPTOR__PARAMETER_TYPES:
				return ((InternalEList<?>)getParameterTypes()).basicRemove(otherEnd, msgs);
			case JbcmmPackage.METHOD_DESCRIPTOR__RESULT_TYPE:
				return basicSetResultType(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case JbcmmPackage.METHOD_DESCRIPTOR__PARAMETER_TYPES:
				return getParameterTypes();
			case JbcmmPackage.METHOD_DESCRIPTOR__RESULT_TYPE:
				return getResultType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case JbcmmPackage.METHOD_DESCRIPTOR__PARAMETER_TYPES:
				getParameterTypes().clear();
				getParameterTypes().addAll((Collection<? extends TypeReference>)newValue);
				return;
			case JbcmmPackage.METHOD_DESCRIPTOR__RESULT_TYPE:
				setResultType((TypeReference)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case JbcmmPackage.METHOD_DESCRIPTOR__PARAMETER_TYPES:
				getParameterTypes().clear();
				return;
			case JbcmmPackage.METHOD_DESCRIPTOR__RESULT_TYPE:
				setResultType((TypeReference)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case JbcmmPackage.METHOD_DESCRIPTOR__PARAMETER_TYPES:
				return parameterTypes != null && !parameterTypes.isEmpty();
			case JbcmmPackage.METHOD_DESCRIPTOR__RESULT_TYPE:
				return resultType != null;
		}
		return super.eIsSet(featureID);
	}

} //MethodDescriptorImpl
