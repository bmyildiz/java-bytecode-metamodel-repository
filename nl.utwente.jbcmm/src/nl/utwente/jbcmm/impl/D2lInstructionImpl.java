/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.D2lInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>D2l Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class D2lInstructionImpl extends SimpleInstructionImpl implements D2lInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected D2lInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getD2lInstruction();
	}

} //D2lInstructionImpl
