/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.IfgeInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ifge Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IfgeInstructionImpl extends JumpInstructionImpl implements IfgeInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IfgeInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIfgeInstruction();
	}

} //IfgeInstructionImpl
