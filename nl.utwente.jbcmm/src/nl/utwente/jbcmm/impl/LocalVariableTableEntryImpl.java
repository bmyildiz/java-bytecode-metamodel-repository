/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.FieldTypeSignature;
import nl.utwente.jbcmm.Instruction;
import nl.utwente.jbcmm.JbcmmPackage;
import nl.utwente.jbcmm.LocalVariableTableEntry;
import nl.utwente.jbcmm.Method;
import nl.utwente.jbcmm.TypeReference;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Local Variable Table Entry</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.impl.LocalVariableTableEntryImpl#getMethod <em>Method</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.LocalVariableTableEntryImpl#getStartInstruction <em>Start Instruction</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.LocalVariableTableEntryImpl#getEndInstruction <em>End Instruction</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.LocalVariableTableEntryImpl#getName <em>Name</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.LocalVariableTableEntryImpl#getIndex <em>Index</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.LocalVariableTableEntryImpl#getSignature <em>Signature</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.LocalVariableTableEntryImpl#getDescriptor <em>Descriptor</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LocalVariableTableEntryImpl extends IdentifiableImpl implements LocalVariableTableEntry {
	/**
	 * The cached value of the '{@link #getStartInstruction() <em>Start Instruction</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartInstruction()
	 * @generated
	 * @ordered
	 */
	protected Instruction startInstruction;

	/**
	 * The cached value of the '{@link #getEndInstruction() <em>End Instruction</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndInstruction()
	 * @generated
	 * @ordered
	 */
	protected Instruction endInstruction;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getIndex() <em>Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndex()
	 * @generated
	 * @ordered
	 */
	protected static final int INDEX_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getIndex() <em>Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndex()
	 * @generated
	 * @ordered
	 */
	protected int index = INDEX_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSignature() <em>Signature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSignature()
	 * @generated
	 * @ordered
	 */
	protected FieldTypeSignature signature;

	/**
	 * The cached value of the '{@link #getDescriptor() <em>Descriptor</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescriptor()
	 * @generated
	 * @ordered
	 */
	protected TypeReference descriptor;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LocalVariableTableEntryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getLocalVariableTableEntry();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Method getMethod() {
		if (eContainerFeatureID() != JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__METHOD) return null;
		return (Method)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMethod(Method newMethod, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newMethod, JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__METHOD, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMethod(Method newMethod) {
		if (newMethod != eInternalContainer() || (eContainerFeatureID() != JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__METHOD && newMethod != null)) {
			if (EcoreUtil.isAncestor(this, newMethod))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newMethod != null)
				msgs = ((InternalEObject)newMethod).eInverseAdd(this, JbcmmPackage.METHOD__LOCAL_VARIABLE_TABLE, Method.class, msgs);
			msgs = basicSetMethod(newMethod, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__METHOD, newMethod, newMethod));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getStartInstruction() {
		if (startInstruction != null && startInstruction.eIsProxy()) {
			InternalEObject oldStartInstruction = (InternalEObject)startInstruction;
			startInstruction = (Instruction)eResolveProxy(oldStartInstruction);
			if (startInstruction != oldStartInstruction) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__START_INSTRUCTION, oldStartInstruction, startInstruction));
			}
		}
		return startInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction basicGetStartInstruction() {
		return startInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartInstruction(Instruction newStartInstruction) {
		Instruction oldStartInstruction = startInstruction;
		startInstruction = newStartInstruction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__START_INSTRUCTION, oldStartInstruction, startInstruction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getEndInstruction() {
		if (endInstruction != null && endInstruction.eIsProxy()) {
			InternalEObject oldEndInstruction = (InternalEObject)endInstruction;
			endInstruction = (Instruction)eResolveProxy(oldEndInstruction);
			if (endInstruction != oldEndInstruction) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__END_INSTRUCTION, oldEndInstruction, endInstruction));
			}
		}
		return endInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction basicGetEndInstruction() {
		return endInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndInstruction(Instruction newEndInstruction) {
		Instruction oldEndInstruction = endInstruction;
		endInstruction = newEndInstruction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__END_INSTRUCTION, oldEndInstruction, endInstruction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIndex(int newIndex) {
		int oldIndex = index;
		index = newIndex;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__INDEX, oldIndex, index));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FieldTypeSignature getSignature() {
		if (signature != null && signature.eIsProxy()) {
			InternalEObject oldSignature = (InternalEObject)signature;
			signature = (FieldTypeSignature)eResolveProxy(oldSignature);
			if (signature != oldSignature) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__SIGNATURE, oldSignature, signature));
			}
		}
		return signature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FieldTypeSignature basicGetSignature() {
		return signature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSignature(FieldTypeSignature newSignature) {
		FieldTypeSignature oldSignature = signature;
		signature = newSignature;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__SIGNATURE, oldSignature, signature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeReference getDescriptor() {
		return descriptor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDescriptor(TypeReference newDescriptor, NotificationChain msgs) {
		TypeReference oldDescriptor = descriptor;
		descriptor = newDescriptor;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__DESCRIPTOR, oldDescriptor, newDescriptor);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescriptor(TypeReference newDescriptor) {
		if (newDescriptor != descriptor) {
			NotificationChain msgs = null;
			if (descriptor != null)
				msgs = ((InternalEObject)descriptor).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__DESCRIPTOR, null, msgs);
			if (newDescriptor != null)
				msgs = ((InternalEObject)newDescriptor).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__DESCRIPTOR, null, msgs);
			msgs = basicSetDescriptor(newDescriptor, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__DESCRIPTOR, newDescriptor, newDescriptor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__METHOD:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetMethod((Method)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__METHOD:
				return basicSetMethod(null, msgs);
			case JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__DESCRIPTOR:
				return basicSetDescriptor(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__METHOD:
				return eInternalContainer().eInverseRemove(this, JbcmmPackage.METHOD__LOCAL_VARIABLE_TABLE, Method.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__METHOD:
				return getMethod();
			case JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__START_INSTRUCTION:
				if (resolve) return getStartInstruction();
				return basicGetStartInstruction();
			case JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__END_INSTRUCTION:
				if (resolve) return getEndInstruction();
				return basicGetEndInstruction();
			case JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__NAME:
				return getName();
			case JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__INDEX:
				return getIndex();
			case JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__SIGNATURE:
				if (resolve) return getSignature();
				return basicGetSignature();
			case JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__DESCRIPTOR:
				return getDescriptor();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__METHOD:
				setMethod((Method)newValue);
				return;
			case JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__START_INSTRUCTION:
				setStartInstruction((Instruction)newValue);
				return;
			case JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__END_INSTRUCTION:
				setEndInstruction((Instruction)newValue);
				return;
			case JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__NAME:
				setName((String)newValue);
				return;
			case JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__INDEX:
				setIndex((Integer)newValue);
				return;
			case JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__SIGNATURE:
				setSignature((FieldTypeSignature)newValue);
				return;
			case JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__DESCRIPTOR:
				setDescriptor((TypeReference)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__METHOD:
				setMethod((Method)null);
				return;
			case JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__START_INSTRUCTION:
				setStartInstruction((Instruction)null);
				return;
			case JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__END_INSTRUCTION:
				setEndInstruction((Instruction)null);
				return;
			case JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__NAME:
				setName(NAME_EDEFAULT);
				return;
			case JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__INDEX:
				setIndex(INDEX_EDEFAULT);
				return;
			case JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__SIGNATURE:
				setSignature((FieldTypeSignature)null);
				return;
			case JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__DESCRIPTOR:
				setDescriptor((TypeReference)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__METHOD:
				return getMethod() != null;
			case JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__START_INSTRUCTION:
				return startInstruction != null;
			case JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__END_INSTRUCTION:
				return endInstruction != null;
			case JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__INDEX:
				return index != INDEX_EDEFAULT;
			case JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__SIGNATURE:
				return signature != null;
			case JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY__DESCRIPTOR:
				return descriptor != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", index: ");
		result.append(index);
		result.append(')');
		return result.toString();
	}

} //LocalVariableTableEntryImpl
