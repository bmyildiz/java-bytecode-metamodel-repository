/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.InvokeinterfaceInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Invokeinterface Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class InvokeinterfaceInstructionImpl extends MethodInstructionImpl implements InvokeinterfaceInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InvokeinterfaceInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getInvokeinterfaceInstruction();
	}

} //InvokeinterfaceInstructionImpl
