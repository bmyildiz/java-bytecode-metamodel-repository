/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.FaddInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Fadd Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FaddInstructionImpl extends SimpleInstructionImpl implements FaddInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FaddInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getFaddInstruction();
	}

} //FaddInstructionImpl
