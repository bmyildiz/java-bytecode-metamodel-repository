/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.Dup2Instruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dup2 Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class Dup2InstructionImpl extends SimpleInstructionImpl implements Dup2Instruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Dup2InstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getDup2Instruction();
	}

} //Dup2InstructionImpl
