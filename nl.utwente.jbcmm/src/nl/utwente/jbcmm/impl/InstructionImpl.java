/**
 */
package nl.utwente.jbcmm.impl;

import java.util.Collection;

import nl.utwente.jbcmm.ControlFlowEdge;
import nl.utwente.jbcmm.Instruction;
import nl.utwente.jbcmm.JbcmmPackage;
import nl.utwente.jbcmm.Method;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Instruction</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.impl.InstructionImpl#getMethod <em>Method</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.InstructionImpl#getNextInCodeOrder <em>Next In Code Order</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.InstructionImpl#getPreviousInCodeOrder <em>Previous In Code Order</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.InstructionImpl#getLinenumber <em>Linenumber</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.InstructionImpl#getIndex <em>Index</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.InstructionImpl#getOpcode <em>Opcode</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.InstructionImpl#getHumanReadable <em>Human Readable</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.InstructionImpl#getOutEdges <em>Out Edges</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.impl.InstructionImpl#getInEdges <em>In Edges</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class InstructionImpl extends IdentifiableImpl implements Instruction {
	/**
	 * The cached value of the '{@link #getNextInCodeOrder() <em>Next In Code Order</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNextInCodeOrder()
	 * @generated
	 * @ordered
	 */
	protected Instruction nextInCodeOrder;

	/**
	 * The cached value of the '{@link #getPreviousInCodeOrder() <em>Previous In Code Order</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreviousInCodeOrder()
	 * @generated
	 * @ordered
	 */
	protected Instruction previousInCodeOrder;

	/**
	 * The default value of the '{@link #getLinenumber() <em>Linenumber</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinenumber()
	 * @generated
	 * @ordered
	 */
	protected static final int LINENUMBER_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getLinenumber() <em>Linenumber</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinenumber()
	 * @generated
	 * @ordered
	 */
	protected int linenumber = LINENUMBER_EDEFAULT;

	/**
	 * The default value of the '{@link #getIndex() <em>Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndex()
	 * @generated
	 * @ordered
	 */
	protected static final int INDEX_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getIndex() <em>Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndex()
	 * @generated
	 * @ordered
	 */
	protected int index = INDEX_EDEFAULT;

	/**
	 * The default value of the '{@link #getOpcode() <em>Opcode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOpcode()
	 * @generated
	 * @ordered
	 */
	protected static final String OPCODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOpcode() <em>Opcode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOpcode()
	 * @generated
	 * @ordered
	 */
	protected String opcode = OPCODE_EDEFAULT;

	/**
	 * The default value of the '{@link #getHumanReadable() <em>Human Readable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHumanReadable()
	 * @generated
	 * @ordered
	 */
	protected static final String HUMAN_READABLE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHumanReadable() <em>Human Readable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHumanReadable()
	 * @generated
	 * @ordered
	 */
	protected String humanReadable = HUMAN_READABLE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getOutEdges() <em>Out Edges</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutEdges()
	 * @generated
	 * @ordered
	 */
	protected EList<ControlFlowEdge> outEdges;

	/**
	 * The cached value of the '{@link #getInEdges() <em>In Edges</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInEdges()
	 * @generated
	 * @ordered
	 */
	protected EList<ControlFlowEdge> inEdges;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getInstruction();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Method getMethod() {
		if (eContainerFeatureID() != JbcmmPackage.INSTRUCTION__METHOD) return null;
		return (Method)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMethod(Method newMethod, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newMethod, JbcmmPackage.INSTRUCTION__METHOD, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMethod(Method newMethod) {
		if (newMethod != eInternalContainer() || (eContainerFeatureID() != JbcmmPackage.INSTRUCTION__METHOD && newMethod != null)) {
			if (EcoreUtil.isAncestor(this, newMethod))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newMethod != null)
				msgs = ((InternalEObject)newMethod).eInverseAdd(this, JbcmmPackage.METHOD__INSTRUCTIONS, Method.class, msgs);
			msgs = basicSetMethod(newMethod, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.INSTRUCTION__METHOD, newMethod, newMethod));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getNextInCodeOrder() {
		if (nextInCodeOrder != null && nextInCodeOrder.eIsProxy()) {
			InternalEObject oldNextInCodeOrder = (InternalEObject)nextInCodeOrder;
			nextInCodeOrder = (Instruction)eResolveProxy(oldNextInCodeOrder);
			if (nextInCodeOrder != oldNextInCodeOrder) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, JbcmmPackage.INSTRUCTION__NEXT_IN_CODE_ORDER, oldNextInCodeOrder, nextInCodeOrder));
			}
		}
		return nextInCodeOrder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction basicGetNextInCodeOrder() {
		return nextInCodeOrder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNextInCodeOrder(Instruction newNextInCodeOrder, NotificationChain msgs) {
		Instruction oldNextInCodeOrder = nextInCodeOrder;
		nextInCodeOrder = newNextInCodeOrder;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JbcmmPackage.INSTRUCTION__NEXT_IN_CODE_ORDER, oldNextInCodeOrder, newNextInCodeOrder);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNextInCodeOrder(Instruction newNextInCodeOrder) {
		if (newNextInCodeOrder != nextInCodeOrder) {
			NotificationChain msgs = null;
			if (nextInCodeOrder != null)
				msgs = ((InternalEObject)nextInCodeOrder).eInverseRemove(this, JbcmmPackage.INSTRUCTION__PREVIOUS_IN_CODE_ORDER, Instruction.class, msgs);
			if (newNextInCodeOrder != null)
				msgs = ((InternalEObject)newNextInCodeOrder).eInverseAdd(this, JbcmmPackage.INSTRUCTION__PREVIOUS_IN_CODE_ORDER, Instruction.class, msgs);
			msgs = basicSetNextInCodeOrder(newNextInCodeOrder, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.INSTRUCTION__NEXT_IN_CODE_ORDER, newNextInCodeOrder, newNextInCodeOrder));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction getPreviousInCodeOrder() {
		if (previousInCodeOrder != null && previousInCodeOrder.eIsProxy()) {
			InternalEObject oldPreviousInCodeOrder = (InternalEObject)previousInCodeOrder;
			previousInCodeOrder = (Instruction)eResolveProxy(oldPreviousInCodeOrder);
			if (previousInCodeOrder != oldPreviousInCodeOrder) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, JbcmmPackage.INSTRUCTION__PREVIOUS_IN_CODE_ORDER, oldPreviousInCodeOrder, previousInCodeOrder));
			}
		}
		return previousInCodeOrder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instruction basicGetPreviousInCodeOrder() {
		return previousInCodeOrder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPreviousInCodeOrder(Instruction newPreviousInCodeOrder, NotificationChain msgs) {
		Instruction oldPreviousInCodeOrder = previousInCodeOrder;
		previousInCodeOrder = newPreviousInCodeOrder;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JbcmmPackage.INSTRUCTION__PREVIOUS_IN_CODE_ORDER, oldPreviousInCodeOrder, newPreviousInCodeOrder);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreviousInCodeOrder(Instruction newPreviousInCodeOrder) {
		if (newPreviousInCodeOrder != previousInCodeOrder) {
			NotificationChain msgs = null;
			if (previousInCodeOrder != null)
				msgs = ((InternalEObject)previousInCodeOrder).eInverseRemove(this, JbcmmPackage.INSTRUCTION__NEXT_IN_CODE_ORDER, Instruction.class, msgs);
			if (newPreviousInCodeOrder != null)
				msgs = ((InternalEObject)newPreviousInCodeOrder).eInverseAdd(this, JbcmmPackage.INSTRUCTION__NEXT_IN_CODE_ORDER, Instruction.class, msgs);
			msgs = basicSetPreviousInCodeOrder(newPreviousInCodeOrder, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.INSTRUCTION__PREVIOUS_IN_CODE_ORDER, newPreviousInCodeOrder, newPreviousInCodeOrder));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getLinenumber() {
		return linenumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLinenumber(int newLinenumber) {
		int oldLinenumber = linenumber;
		linenumber = newLinenumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.INSTRUCTION__LINENUMBER, oldLinenumber, linenumber));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIndex(int newIndex) {
		int oldIndex = index;
		index = newIndex;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.INSTRUCTION__INDEX, oldIndex, index));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getOpcode() {
		return opcode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOpcode(String newOpcode) {
		String oldOpcode = opcode;
		opcode = newOpcode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.INSTRUCTION__OPCODE, oldOpcode, opcode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getHumanReadable() {
		return humanReadable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHumanReadable(String newHumanReadable) {
		String oldHumanReadable = humanReadable;
		humanReadable = newHumanReadable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.INSTRUCTION__HUMAN_READABLE, oldHumanReadable, humanReadable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControlFlowEdge> getOutEdges() {
		if (outEdges == null) {
			outEdges = new EObjectContainmentWithInverseEList<ControlFlowEdge>(ControlFlowEdge.class, this, JbcmmPackage.INSTRUCTION__OUT_EDGES, JbcmmPackage.CONTROL_FLOW_EDGE__START);
		}
		return outEdges;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControlFlowEdge> getInEdges() {
		if (inEdges == null) {
			inEdges = new EObjectWithInverseResolvingEList<ControlFlowEdge>(ControlFlowEdge.class, this, JbcmmPackage.INSTRUCTION__IN_EDGES, JbcmmPackage.CONTROL_FLOW_EDGE__END);
		}
		return inEdges;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case JbcmmPackage.INSTRUCTION__METHOD:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetMethod((Method)otherEnd, msgs);
			case JbcmmPackage.INSTRUCTION__NEXT_IN_CODE_ORDER:
				if (nextInCodeOrder != null)
					msgs = ((InternalEObject)nextInCodeOrder).eInverseRemove(this, JbcmmPackage.INSTRUCTION__PREVIOUS_IN_CODE_ORDER, Instruction.class, msgs);
				return basicSetNextInCodeOrder((Instruction)otherEnd, msgs);
			case JbcmmPackage.INSTRUCTION__PREVIOUS_IN_CODE_ORDER:
				if (previousInCodeOrder != null)
					msgs = ((InternalEObject)previousInCodeOrder).eInverseRemove(this, JbcmmPackage.INSTRUCTION__NEXT_IN_CODE_ORDER, Instruction.class, msgs);
				return basicSetPreviousInCodeOrder((Instruction)otherEnd, msgs);
			case JbcmmPackage.INSTRUCTION__OUT_EDGES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getOutEdges()).basicAdd(otherEnd, msgs);
			case JbcmmPackage.INSTRUCTION__IN_EDGES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getInEdges()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case JbcmmPackage.INSTRUCTION__METHOD:
				return basicSetMethod(null, msgs);
			case JbcmmPackage.INSTRUCTION__NEXT_IN_CODE_ORDER:
				return basicSetNextInCodeOrder(null, msgs);
			case JbcmmPackage.INSTRUCTION__PREVIOUS_IN_CODE_ORDER:
				return basicSetPreviousInCodeOrder(null, msgs);
			case JbcmmPackage.INSTRUCTION__OUT_EDGES:
				return ((InternalEList<?>)getOutEdges()).basicRemove(otherEnd, msgs);
			case JbcmmPackage.INSTRUCTION__IN_EDGES:
				return ((InternalEList<?>)getInEdges()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case JbcmmPackage.INSTRUCTION__METHOD:
				return eInternalContainer().eInverseRemove(this, JbcmmPackage.METHOD__INSTRUCTIONS, Method.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case JbcmmPackage.INSTRUCTION__METHOD:
				return getMethod();
			case JbcmmPackage.INSTRUCTION__NEXT_IN_CODE_ORDER:
				if (resolve) return getNextInCodeOrder();
				return basicGetNextInCodeOrder();
			case JbcmmPackage.INSTRUCTION__PREVIOUS_IN_CODE_ORDER:
				if (resolve) return getPreviousInCodeOrder();
				return basicGetPreviousInCodeOrder();
			case JbcmmPackage.INSTRUCTION__LINENUMBER:
				return getLinenumber();
			case JbcmmPackage.INSTRUCTION__INDEX:
				return getIndex();
			case JbcmmPackage.INSTRUCTION__OPCODE:
				return getOpcode();
			case JbcmmPackage.INSTRUCTION__HUMAN_READABLE:
				return getHumanReadable();
			case JbcmmPackage.INSTRUCTION__OUT_EDGES:
				return getOutEdges();
			case JbcmmPackage.INSTRUCTION__IN_EDGES:
				return getInEdges();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case JbcmmPackage.INSTRUCTION__METHOD:
				setMethod((Method)newValue);
				return;
			case JbcmmPackage.INSTRUCTION__NEXT_IN_CODE_ORDER:
				setNextInCodeOrder((Instruction)newValue);
				return;
			case JbcmmPackage.INSTRUCTION__PREVIOUS_IN_CODE_ORDER:
				setPreviousInCodeOrder((Instruction)newValue);
				return;
			case JbcmmPackage.INSTRUCTION__LINENUMBER:
				setLinenumber((Integer)newValue);
				return;
			case JbcmmPackage.INSTRUCTION__INDEX:
				setIndex((Integer)newValue);
				return;
			case JbcmmPackage.INSTRUCTION__OPCODE:
				setOpcode((String)newValue);
				return;
			case JbcmmPackage.INSTRUCTION__HUMAN_READABLE:
				setHumanReadable((String)newValue);
				return;
			case JbcmmPackage.INSTRUCTION__OUT_EDGES:
				getOutEdges().clear();
				getOutEdges().addAll((Collection<? extends ControlFlowEdge>)newValue);
				return;
			case JbcmmPackage.INSTRUCTION__IN_EDGES:
				getInEdges().clear();
				getInEdges().addAll((Collection<? extends ControlFlowEdge>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case JbcmmPackage.INSTRUCTION__METHOD:
				setMethod((Method)null);
				return;
			case JbcmmPackage.INSTRUCTION__NEXT_IN_CODE_ORDER:
				setNextInCodeOrder((Instruction)null);
				return;
			case JbcmmPackage.INSTRUCTION__PREVIOUS_IN_CODE_ORDER:
				setPreviousInCodeOrder((Instruction)null);
				return;
			case JbcmmPackage.INSTRUCTION__LINENUMBER:
				setLinenumber(LINENUMBER_EDEFAULT);
				return;
			case JbcmmPackage.INSTRUCTION__INDEX:
				setIndex(INDEX_EDEFAULT);
				return;
			case JbcmmPackage.INSTRUCTION__OPCODE:
				setOpcode(OPCODE_EDEFAULT);
				return;
			case JbcmmPackage.INSTRUCTION__HUMAN_READABLE:
				setHumanReadable(HUMAN_READABLE_EDEFAULT);
				return;
			case JbcmmPackage.INSTRUCTION__OUT_EDGES:
				getOutEdges().clear();
				return;
			case JbcmmPackage.INSTRUCTION__IN_EDGES:
				getInEdges().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case JbcmmPackage.INSTRUCTION__METHOD:
				return getMethod() != null;
			case JbcmmPackage.INSTRUCTION__NEXT_IN_CODE_ORDER:
				return nextInCodeOrder != null;
			case JbcmmPackage.INSTRUCTION__PREVIOUS_IN_CODE_ORDER:
				return previousInCodeOrder != null;
			case JbcmmPackage.INSTRUCTION__LINENUMBER:
				return linenumber != LINENUMBER_EDEFAULT;
			case JbcmmPackage.INSTRUCTION__INDEX:
				return index != INDEX_EDEFAULT;
			case JbcmmPackage.INSTRUCTION__OPCODE:
				return OPCODE_EDEFAULT == null ? opcode != null : !OPCODE_EDEFAULT.equals(opcode);
			case JbcmmPackage.INSTRUCTION__HUMAN_READABLE:
				return HUMAN_READABLE_EDEFAULT == null ? humanReadable != null : !HUMAN_READABLE_EDEFAULT.equals(humanReadable);
			case JbcmmPackage.INSTRUCTION__OUT_EDGES:
				return outEdges != null && !outEdges.isEmpty();
			case JbcmmPackage.INSTRUCTION__IN_EDGES:
				return inEdges != null && !inEdges.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (linenumber: ");
		result.append(linenumber);
		result.append(", index: ");
		result.append(index);
		result.append(", opcode: ");
		result.append(opcode);
		result.append(", humanReadable: ");
		result.append(humanReadable);
		result.append(')');
		return result.toString();
	}

} //InstructionImpl
