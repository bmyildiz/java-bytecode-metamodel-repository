/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.DaloadInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Daload Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DaloadInstructionImpl extends SimpleInstructionImpl implements DaloadInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DaloadInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getDaloadInstruction();
	}

} //DaloadInstructionImpl
