/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.If_acmpneInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>If acmpne Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class If_acmpneInstructionImpl extends JumpInstructionImpl implements If_acmpneInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected If_acmpneInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIf_acmpneInstruction();
	}

} //If_acmpneInstructionImpl
