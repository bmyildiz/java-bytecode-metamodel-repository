/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.ArraylengthInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Arraylength Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ArraylengthInstructionImpl extends SimpleInstructionImpl implements ArraylengthInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ArraylengthInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getArraylengthInstruction();
	}

} //ArraylengthInstructionImpl
