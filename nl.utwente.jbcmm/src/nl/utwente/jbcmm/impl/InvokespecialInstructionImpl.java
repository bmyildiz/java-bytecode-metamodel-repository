/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.InvokespecialInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Invokespecial Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class InvokespecialInstructionImpl extends MethodInstructionImpl implements InvokespecialInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InvokespecialInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getInvokespecialInstruction();
	}

} //InvokespecialInstructionImpl
