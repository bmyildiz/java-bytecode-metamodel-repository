/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.FastoreInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Fastore Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FastoreInstructionImpl extends SimpleInstructionImpl implements FastoreInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FastoreInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getFastoreInstruction();
	}

} //FastoreInstructionImpl
