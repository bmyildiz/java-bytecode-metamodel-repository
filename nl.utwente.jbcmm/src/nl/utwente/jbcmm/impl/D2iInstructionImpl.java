/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.D2iInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>D2i Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class D2iInstructionImpl extends SimpleInstructionImpl implements D2iInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected D2iInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getD2iInstruction();
	}

} //D2iInstructionImpl
