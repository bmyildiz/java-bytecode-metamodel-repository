/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.DmulInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dmul Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DmulInstructionImpl extends SimpleInstructionImpl implements DmulInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DmulInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getDmulInstruction();
	}

} //DmulInstructionImpl
