/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.FieldInstruction;
import nl.utwente.jbcmm.FieldReference;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Field Instruction</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.impl.FieldInstructionImpl#getFieldReference <em>Field Reference</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class FieldInstructionImpl extends InstructionImpl implements FieldInstruction {
	/**
	 * The cached value of the '{@link #getFieldReference() <em>Field Reference</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFieldReference()
	 * @generated
	 * @ordered
	 */
	protected FieldReference fieldReference;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FieldInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getFieldInstruction();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FieldReference getFieldReference() {
		return fieldReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFieldReference(FieldReference newFieldReference, NotificationChain msgs) {
		FieldReference oldFieldReference = fieldReference;
		fieldReference = newFieldReference;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JbcmmPackage.FIELD_INSTRUCTION__FIELD_REFERENCE, oldFieldReference, newFieldReference);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFieldReference(FieldReference newFieldReference) {
		if (newFieldReference != fieldReference) {
			NotificationChain msgs = null;
			if (fieldReference != null)
				msgs = ((InternalEObject)fieldReference).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.FIELD_INSTRUCTION__FIELD_REFERENCE, null, msgs);
			if (newFieldReference != null)
				msgs = ((InternalEObject)newFieldReference).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.FIELD_INSTRUCTION__FIELD_REFERENCE, null, msgs);
			msgs = basicSetFieldReference(newFieldReference, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.FIELD_INSTRUCTION__FIELD_REFERENCE, newFieldReference, newFieldReference));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case JbcmmPackage.FIELD_INSTRUCTION__FIELD_REFERENCE:
				return basicSetFieldReference(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case JbcmmPackage.FIELD_INSTRUCTION__FIELD_REFERENCE:
				return getFieldReference();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case JbcmmPackage.FIELD_INSTRUCTION__FIELD_REFERENCE:
				setFieldReference((FieldReference)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case JbcmmPackage.FIELD_INSTRUCTION__FIELD_REFERENCE:
				setFieldReference((FieldReference)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case JbcmmPackage.FIELD_INSTRUCTION__FIELD_REFERENCE:
				return fieldReference != null;
		}
		return super.eIsSet(featureID);
	}

} //FieldInstructionImpl
