/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.BaloadInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Baload Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BaloadInstructionImpl extends SimpleInstructionImpl implements BaloadInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BaloadInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getBaloadInstruction();
	}

} //BaloadInstructionImpl
