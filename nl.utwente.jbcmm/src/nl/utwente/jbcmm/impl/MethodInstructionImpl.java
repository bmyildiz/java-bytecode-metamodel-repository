/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.JbcmmPackage;
import nl.utwente.jbcmm.MethodInstruction;

import nl.utwente.jbcmm.MethodReference;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Method Instruction</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.impl.MethodInstructionImpl#getMethodReference <em>Method Reference</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class MethodInstructionImpl extends InstructionImpl implements MethodInstruction {
	/**
	 * The cached value of the '{@link #getMethodReference() <em>Method Reference</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMethodReference()
	 * @generated
	 * @ordered
	 */
	protected MethodReference methodReference;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MethodInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getMethodInstruction();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MethodReference getMethodReference() {
		return methodReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMethodReference(MethodReference newMethodReference, NotificationChain msgs) {
		MethodReference oldMethodReference = methodReference;
		methodReference = newMethodReference;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JbcmmPackage.METHOD_INSTRUCTION__METHOD_REFERENCE, oldMethodReference, newMethodReference);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMethodReference(MethodReference newMethodReference) {
		if (newMethodReference != methodReference) {
			NotificationChain msgs = null;
			if (methodReference != null)
				msgs = ((InternalEObject)methodReference).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.METHOD_INSTRUCTION__METHOD_REFERENCE, null, msgs);
			if (newMethodReference != null)
				msgs = ((InternalEObject)newMethodReference).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JbcmmPackage.METHOD_INSTRUCTION__METHOD_REFERENCE, null, msgs);
			msgs = basicSetMethodReference(newMethodReference, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JbcmmPackage.METHOD_INSTRUCTION__METHOD_REFERENCE, newMethodReference, newMethodReference));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case JbcmmPackage.METHOD_INSTRUCTION__METHOD_REFERENCE:
				return basicSetMethodReference(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case JbcmmPackage.METHOD_INSTRUCTION__METHOD_REFERENCE:
				return getMethodReference();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case JbcmmPackage.METHOD_INSTRUCTION__METHOD_REFERENCE:
				setMethodReference((MethodReference)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case JbcmmPackage.METHOD_INSTRUCTION__METHOD_REFERENCE:
				setMethodReference((MethodReference)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case JbcmmPackage.METHOD_INSTRUCTION__METHOD_REFERENCE:
				return methodReference != null;
		}
		return super.eIsSet(featureID);
	}

} //MethodInstructionImpl
