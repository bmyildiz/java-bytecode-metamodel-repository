/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.Aconst_nullInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Aconst null Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class Aconst_nullInstructionImpl extends SimpleInstructionImpl implements Aconst_nullInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Aconst_nullInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getAconst_nullInstruction();
	}

} //Aconst_nullInstructionImpl
