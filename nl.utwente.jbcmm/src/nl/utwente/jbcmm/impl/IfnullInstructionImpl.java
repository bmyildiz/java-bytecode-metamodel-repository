/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.IfnullInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ifnull Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IfnullInstructionImpl extends JumpInstructionImpl implements IfnullInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IfnullInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getIfnullInstruction();
	}

} //IfnullInstructionImpl
