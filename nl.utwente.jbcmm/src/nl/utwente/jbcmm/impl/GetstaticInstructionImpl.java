/**
 */
package nl.utwente.jbcmm.impl;

import nl.utwente.jbcmm.GetstaticInstruction;
import nl.utwente.jbcmm.JbcmmPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Getstatic Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class GetstaticInstructionImpl extends FieldInstructionImpl implements GetstaticInstruction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GetstaticInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JbcmmPackage.eINSTANCE.getGetstaticInstruction();
	}

} //GetstaticInstructionImpl
