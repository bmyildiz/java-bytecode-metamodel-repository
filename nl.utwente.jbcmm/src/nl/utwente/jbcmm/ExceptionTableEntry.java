/**
 */
package nl.utwente.jbcmm;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Exception Table Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.ExceptionTableEntry#getMethod <em>Method</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.ExceptionTableEntry#getStartInstruction <em>Start Instruction</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.ExceptionTableEntry#getEndInstruction <em>End Instruction</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.ExceptionTableEntry#getHandlerInstruction <em>Handler Instruction</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.ExceptionTableEntry#getCatchType <em>Catch Type</em>}</li>
 * </ul>
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getExceptionTableEntry()
 * @model
 * @generated
 */
public interface ExceptionTableEntry extends Identifiable {
	/**
	 * Returns the value of the '<em><b>Method</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link nl.utwente.jbcmm.Method#getExceptionTable <em>Exception Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Method</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Method</em>' container reference.
	 * @see #setMethod(Method)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getExceptionTableEntry_Method()
	 * @see nl.utwente.jbcmm.Method#getExceptionTable
	 * @model opposite="exceptionTable" required="true" transient="false"
	 * @generated
	 */
	Method getMethod();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.ExceptionTableEntry#getMethod <em>Method</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Method</em>' container reference.
	 * @see #getMethod()
	 * @generated
	 */
	void setMethod(Method value);

	/**
	 * Returns the value of the '<em><b>Start Instruction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Instruction</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Instruction</em>' reference.
	 * @see #setStartInstruction(Instruction)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getExceptionTableEntry_StartInstruction()
	 * @model required="true"
	 * @generated
	 */
	Instruction getStartInstruction();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.ExceptionTableEntry#getStartInstruction <em>Start Instruction</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Instruction</em>' reference.
	 * @see #getStartInstruction()
	 * @generated
	 */
	void setStartInstruction(Instruction value);

	/**
	 * Returns the value of the '<em><b>End Instruction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Instruction</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Instruction</em>' reference.
	 * @see #setEndInstruction(Instruction)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getExceptionTableEntry_EndInstruction()
	 * @model required="true"
	 * @generated
	 */
	Instruction getEndInstruction();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.ExceptionTableEntry#getEndInstruction <em>End Instruction</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Instruction</em>' reference.
	 * @see #getEndInstruction()
	 * @generated
	 */
	void setEndInstruction(Instruction value);

	/**
	 * Returns the value of the '<em><b>Handler Instruction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Handler Instruction</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Handler Instruction</em>' reference.
	 * @see #setHandlerInstruction(Instruction)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getExceptionTableEntry_HandlerInstruction()
	 * @model required="true"
	 * @generated
	 */
	Instruction getHandlerInstruction();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.ExceptionTableEntry#getHandlerInstruction <em>Handler Instruction</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Handler Instruction</em>' reference.
	 * @see #getHandlerInstruction()
	 * @generated
	 */
	void setHandlerInstruction(Instruction value);

	/**
	 * Returns the value of the '<em><b>Catch Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Catch Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Catch Type</em>' containment reference.
	 * @see #setCatchType(TypeReference)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getExceptionTableEntry_CatchType()
	 * @model containment="true"
	 * @generated
	 */
	TypeReference getCatchType();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.ExceptionTableEntry#getCatchType <em>Catch Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Catch Type</em>' containment reference.
	 * @see #getCatchType()
	 * @generated
	 */
	void setCatchType(TypeReference value);

} // ExceptionTableEntry
