/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>If icmpge Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIf_icmpgeInstruction()
 * @model
 * @generated
 */
public interface If_icmpgeInstruction extends JumpInstruction {
} // If_icmpgeInstruction
