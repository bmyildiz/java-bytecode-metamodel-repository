/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dup Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getDupInstruction()
 * @model
 * @generated
 */
public interface DupInstruction extends SimpleInstruction {
} // DupInstruction
