/**
 */
package nl.utwente.jbcmm.util;

import nl.utwente.jbcmm.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see nl.utwente.jbcmm.JbcmmPackage
 * @generated
 */
public class JbcmmSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static JbcmmPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JbcmmSwitch() {
		if (modelPackage == null) {
			modelPackage = JbcmmPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case JbcmmPackage.IDENTIFIABLE: {
				Identifiable identifiable = (Identifiable)theEObject;
				T result = caseIdentifiable(identifiable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.PROJECT: {
				Project project = (Project)theEObject;
				T result = caseProject(project);
				if (result == null) result = caseIdentifiable(project);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.CLAZZ: {
				Clazz clazz = (Clazz)theEObject;
				T result = caseClazz(clazz);
				if (result == null) result = caseIdentifiable(clazz);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.SIGNATURE: {
				Signature signature = (Signature)theEObject;
				T result = caseSignature(signature);
				if (result == null) result = caseIdentifiable(signature);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.CLASS_SIGNATURE: {
				ClassSignature classSignature = (ClassSignature)theEObject;
				T result = caseClassSignature(classSignature);
				if (result == null) result = caseSignature(classSignature);
				if (result == null) result = caseIdentifiable(classSignature);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.METHOD_SIGNATURE: {
				MethodSignature methodSignature = (MethodSignature)theEObject;
				T result = caseMethodSignature(methodSignature);
				if (result == null) result = caseSignature(methodSignature);
				if (result == null) result = caseIdentifiable(methodSignature);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.FIELD_TYPE_SIGNATURE: {
				FieldTypeSignature fieldTypeSignature = (FieldTypeSignature)theEObject;
				T result = caseFieldTypeSignature(fieldTypeSignature);
				if (result == null) result = caseSignature(fieldTypeSignature);
				if (result == null) result = caseIdentifiable(fieldTypeSignature);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.TYPE_REFERENCE: {
				TypeReference typeReference = (TypeReference)theEObject;
				T result = caseTypeReference(typeReference);
				if (result == null) result = caseIdentifiable(typeReference);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.METHOD_DESCRIPTOR: {
				MethodDescriptor methodDescriptor = (MethodDescriptor)theEObject;
				T result = caseMethodDescriptor(methodDescriptor);
				if (result == null) result = caseIdentifiable(methodDescriptor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.METHOD_REFERENCE: {
				MethodReference methodReference = (MethodReference)theEObject;
				T result = caseMethodReference(methodReference);
				if (result == null) result = caseIdentifiable(methodReference);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.FIELD_REFERENCE: {
				FieldReference fieldReference = (FieldReference)theEObject;
				T result = caseFieldReference(fieldReference);
				if (result == null) result = caseIdentifiable(fieldReference);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.FIELD: {
				Field field = (Field)theEObject;
				T result = caseField(field);
				if (result == null) result = caseIdentifiable(field);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.ANNOTATION: {
				Annotation annotation = (Annotation)theEObject;
				T result = caseAnnotation(annotation);
				if (result == null) result = caseIdentifiable(annotation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.ELEMENT_VALUE_PAIR: {
				ElementValuePair elementValuePair = (ElementValuePair)theEObject;
				T result = caseElementValuePair(elementValuePair);
				if (result == null) result = caseIdentifiable(elementValuePair);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.ELEMENT_VALUE: {
				ElementValue elementValue = (ElementValue)theEObject;
				T result = caseElementValue(elementValue);
				if (result == null) result = caseIdentifiable(elementValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.ELEMENTARY_VALUE: {
				ElementaryValue elementaryValue = (ElementaryValue)theEObject;
				T result = caseElementaryValue(elementaryValue);
				if (result == null) result = caseIdentifiable(elementaryValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.BOOLEAN_VALUE: {
				BooleanValue booleanValue = (BooleanValue)theEObject;
				T result = caseBooleanValue(booleanValue);
				if (result == null) result = caseElementaryValue(booleanValue);
				if (result == null) result = caseIdentifiable(booleanValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.CHAR_VALUE: {
				CharValue charValue = (CharValue)theEObject;
				T result = caseCharValue(charValue);
				if (result == null) result = caseElementaryValue(charValue);
				if (result == null) result = caseIdentifiable(charValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.BYTE_VALUE: {
				ByteValue byteValue = (ByteValue)theEObject;
				T result = caseByteValue(byteValue);
				if (result == null) result = caseElementaryValue(byteValue);
				if (result == null) result = caseIdentifiable(byteValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.SHORT_VALUE: {
				ShortValue shortValue = (ShortValue)theEObject;
				T result = caseShortValue(shortValue);
				if (result == null) result = caseElementaryValue(shortValue);
				if (result == null) result = caseIdentifiable(shortValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.INT_VALUE: {
				IntValue intValue = (IntValue)theEObject;
				T result = caseIntValue(intValue);
				if (result == null) result = caseElementaryValue(intValue);
				if (result == null) result = caseIdentifiable(intValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.LONG_VALUE: {
				LongValue longValue = (LongValue)theEObject;
				T result = caseLongValue(longValue);
				if (result == null) result = caseElementaryValue(longValue);
				if (result == null) result = caseIdentifiable(longValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.FLOAT_VALUE: {
				FloatValue floatValue = (FloatValue)theEObject;
				T result = caseFloatValue(floatValue);
				if (result == null) result = caseElementaryValue(floatValue);
				if (result == null) result = caseIdentifiable(floatValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.DOUBLE_VALUE: {
				DoubleValue doubleValue = (DoubleValue)theEObject;
				T result = caseDoubleValue(doubleValue);
				if (result == null) result = caseElementaryValue(doubleValue);
				if (result == null) result = caseIdentifiable(doubleValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.STRING_VALUE: {
				StringValue stringValue = (StringValue)theEObject;
				T result = caseStringValue(stringValue);
				if (result == null) result = caseElementaryValue(stringValue);
				if (result == null) result = caseIdentifiable(stringValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.ENUM_VALUE: {
				EnumValue enumValue = (EnumValue)theEObject;
				T result = caseEnumValue(enumValue);
				if (result == null) result = caseIdentifiable(enumValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.METHOD: {
				Method method = (Method)theEObject;
				T result = caseMethod(method);
				if (result == null) result = caseIdentifiable(method);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.LOCAL_VARIABLE_TABLE_ENTRY: {
				LocalVariableTableEntry localVariableTableEntry = (LocalVariableTableEntry)theEObject;
				T result = caseLocalVariableTableEntry(localVariableTableEntry);
				if (result == null) result = caseIdentifiable(localVariableTableEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.EXCEPTION_TABLE_ENTRY: {
				ExceptionTableEntry exceptionTableEntry = (ExceptionTableEntry)theEObject;
				T result = caseExceptionTableEntry(exceptionTableEntry);
				if (result == null) result = caseIdentifiable(exceptionTableEntry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.CONTROL_FLOW_EDGE: {
				ControlFlowEdge controlFlowEdge = (ControlFlowEdge)theEObject;
				T result = caseControlFlowEdge(controlFlowEdge);
				if (result == null) result = caseIdentifiable(controlFlowEdge);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.UNCONDITIONAL_EDGE: {
				UnconditionalEdge unconditionalEdge = (UnconditionalEdge)theEObject;
				T result = caseUnconditionalEdge(unconditionalEdge);
				if (result == null) result = caseControlFlowEdge(unconditionalEdge);
				if (result == null) result = caseIdentifiable(unconditionalEdge);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.CONDITIONAL_EDGE: {
				ConditionalEdge conditionalEdge = (ConditionalEdge)theEObject;
				T result = caseConditionalEdge(conditionalEdge);
				if (result == null) result = caseControlFlowEdge(conditionalEdge);
				if (result == null) result = caseIdentifiable(conditionalEdge);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.SWITCH_CASE_EDGE: {
				SwitchCaseEdge switchCaseEdge = (SwitchCaseEdge)theEObject;
				T result = caseSwitchCaseEdge(switchCaseEdge);
				if (result == null) result = caseControlFlowEdge(switchCaseEdge);
				if (result == null) result = caseIdentifiable(switchCaseEdge);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.SWITCH_DEFAULT_EDGE: {
				SwitchDefaultEdge switchDefaultEdge = (SwitchDefaultEdge)theEObject;
				T result = caseSwitchDefaultEdge(switchDefaultEdge);
				if (result == null) result = caseControlFlowEdge(switchDefaultEdge);
				if (result == null) result = caseIdentifiable(switchDefaultEdge);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.EXCEPTIONAL_EDGE: {
				ExceptionalEdge exceptionalEdge = (ExceptionalEdge)theEObject;
				T result = caseExceptionalEdge(exceptionalEdge);
				if (result == null) result = caseControlFlowEdge(exceptionalEdge);
				if (result == null) result = caseIdentifiable(exceptionalEdge);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.INSTRUCTION: {
				Instruction instruction = (Instruction)theEObject;
				T result = caseInstruction(instruction);
				if (result == null) result = caseIdentifiable(instruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.FIELD_INSTRUCTION: {
				FieldInstruction fieldInstruction = (FieldInstruction)theEObject;
				T result = caseFieldInstruction(fieldInstruction);
				if (result == null) result = caseInstruction(fieldInstruction);
				if (result == null) result = caseIdentifiable(fieldInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.SIMPLE_INSTRUCTION: {
				SimpleInstruction simpleInstruction = (SimpleInstruction)theEObject;
				T result = caseSimpleInstruction(simpleInstruction);
				if (result == null) result = caseInstruction(simpleInstruction);
				if (result == null) result = caseIdentifiable(simpleInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.INT_INSTRUCTION: {
				IntInstruction intInstruction = (IntInstruction)theEObject;
				T result = caseIntInstruction(intInstruction);
				if (result == null) result = caseInstruction(intInstruction);
				if (result == null) result = caseIdentifiable(intInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.JUMP_INSTRUCTION: {
				JumpInstruction jumpInstruction = (JumpInstruction)theEObject;
				T result = caseJumpInstruction(jumpInstruction);
				if (result == null) result = caseInstruction(jumpInstruction);
				if (result == null) result = caseIdentifiable(jumpInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.LDC_INSTRUCTION: {
				LdcInstruction ldcInstruction = (LdcInstruction)theEObject;
				T result = caseLdcInstruction(ldcInstruction);
				if (result == null) result = caseInstruction(ldcInstruction);
				if (result == null) result = caseIdentifiable(ldcInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.METHOD_INSTRUCTION: {
				MethodInstruction methodInstruction = (MethodInstruction)theEObject;
				T result = caseMethodInstruction(methodInstruction);
				if (result == null) result = caseInstruction(methodInstruction);
				if (result == null) result = caseIdentifiable(methodInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.TYPE_INSTRUCTION: {
				TypeInstruction typeInstruction = (TypeInstruction)theEObject;
				T result = caseTypeInstruction(typeInstruction);
				if (result == null) result = caseInstruction(typeInstruction);
				if (result == null) result = caseIdentifiable(typeInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.VAR_INSTRUCTION: {
				VarInstruction varInstruction = (VarInstruction)theEObject;
				T result = caseVarInstruction(varInstruction);
				if (result == null) result = caseInstruction(varInstruction);
				if (result == null) result = caseIdentifiable(varInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.IINC_INSTRUCTION: {
				IincInstruction iincInstruction = (IincInstruction)theEObject;
				T result = caseIincInstruction(iincInstruction);
				if (result == null) result = caseVarInstruction(iincInstruction);
				if (result == null) result = caseInstruction(iincInstruction);
				if (result == null) result = caseIdentifiable(iincInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.MULTIANEWARRAY_INSTRUCTION: {
				MultianewarrayInstruction multianewarrayInstruction = (MultianewarrayInstruction)theEObject;
				T result = caseMultianewarrayInstruction(multianewarrayInstruction);
				if (result == null) result = caseInstruction(multianewarrayInstruction);
				if (result == null) result = caseIdentifiable(multianewarrayInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.SWITCH_INSTRUCTION: {
				SwitchInstruction switchInstruction = (SwitchInstruction)theEObject;
				T result = caseSwitchInstruction(switchInstruction);
				if (result == null) result = caseInstruction(switchInstruction);
				if (result == null) result = caseIdentifiable(switchInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.LDC_INT_INSTRUCTION: {
				LdcIntInstruction ldcIntInstruction = (LdcIntInstruction)theEObject;
				T result = caseLdcIntInstruction(ldcIntInstruction);
				if (result == null) result = caseLdcInstruction(ldcIntInstruction);
				if (result == null) result = caseInstruction(ldcIntInstruction);
				if (result == null) result = caseIdentifiable(ldcIntInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.LDC_LONG_INSTRUCTION: {
				LdcLongInstruction ldcLongInstruction = (LdcLongInstruction)theEObject;
				T result = caseLdcLongInstruction(ldcLongInstruction);
				if (result == null) result = caseLdcInstruction(ldcLongInstruction);
				if (result == null) result = caseInstruction(ldcLongInstruction);
				if (result == null) result = caseIdentifiable(ldcLongInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.LDC_FLOAT_INSTRUCTION: {
				LdcFloatInstruction ldcFloatInstruction = (LdcFloatInstruction)theEObject;
				T result = caseLdcFloatInstruction(ldcFloatInstruction);
				if (result == null) result = caseLdcInstruction(ldcFloatInstruction);
				if (result == null) result = caseInstruction(ldcFloatInstruction);
				if (result == null) result = caseIdentifiable(ldcFloatInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.LDC_DOUBLE_INSTRUCTION: {
				LdcDoubleInstruction ldcDoubleInstruction = (LdcDoubleInstruction)theEObject;
				T result = caseLdcDoubleInstruction(ldcDoubleInstruction);
				if (result == null) result = caseLdcInstruction(ldcDoubleInstruction);
				if (result == null) result = caseInstruction(ldcDoubleInstruction);
				if (result == null) result = caseIdentifiable(ldcDoubleInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.LDC_STRING_INSTRUCTION: {
				LdcStringInstruction ldcStringInstruction = (LdcStringInstruction)theEObject;
				T result = caseLdcStringInstruction(ldcStringInstruction);
				if (result == null) result = caseLdcInstruction(ldcStringInstruction);
				if (result == null) result = caseInstruction(ldcStringInstruction);
				if (result == null) result = caseIdentifiable(ldcStringInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.LDC_TYPE_INSTRUCTION: {
				LdcTypeInstruction ldcTypeInstruction = (LdcTypeInstruction)theEObject;
				T result = caseLdcTypeInstruction(ldcTypeInstruction);
				if (result == null) result = caseLdcInstruction(ldcTypeInstruction);
				if (result == null) result = caseInstruction(ldcTypeInstruction);
				if (result == null) result = caseIdentifiable(ldcTypeInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.GETSTATIC_INSTRUCTION: {
				GetstaticInstruction getstaticInstruction = (GetstaticInstruction)theEObject;
				T result = caseGetstaticInstruction(getstaticInstruction);
				if (result == null) result = caseFieldInstruction(getstaticInstruction);
				if (result == null) result = caseInstruction(getstaticInstruction);
				if (result == null) result = caseIdentifiable(getstaticInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.PUTSTATIC_INSTRUCTION: {
				PutstaticInstruction putstaticInstruction = (PutstaticInstruction)theEObject;
				T result = casePutstaticInstruction(putstaticInstruction);
				if (result == null) result = caseFieldInstruction(putstaticInstruction);
				if (result == null) result = caseInstruction(putstaticInstruction);
				if (result == null) result = caseIdentifiable(putstaticInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.GETFIELD_INSTRUCTION: {
				GetfieldInstruction getfieldInstruction = (GetfieldInstruction)theEObject;
				T result = caseGetfieldInstruction(getfieldInstruction);
				if (result == null) result = caseFieldInstruction(getfieldInstruction);
				if (result == null) result = caseInstruction(getfieldInstruction);
				if (result == null) result = caseIdentifiable(getfieldInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.PUTFIELD_INSTRUCTION: {
				PutfieldInstruction putfieldInstruction = (PutfieldInstruction)theEObject;
				T result = casePutfieldInstruction(putfieldInstruction);
				if (result == null) result = caseFieldInstruction(putfieldInstruction);
				if (result == null) result = caseInstruction(putfieldInstruction);
				if (result == null) result = caseIdentifiable(putfieldInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.NOP_INSTRUCTION: {
				NopInstruction nopInstruction = (NopInstruction)theEObject;
				T result = caseNopInstruction(nopInstruction);
				if (result == null) result = caseSimpleInstruction(nopInstruction);
				if (result == null) result = caseInstruction(nopInstruction);
				if (result == null) result = caseIdentifiable(nopInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.ACONST_NULL_INSTRUCTION: {
				Aconst_nullInstruction aconst_nullInstruction = (Aconst_nullInstruction)theEObject;
				T result = caseAconst_nullInstruction(aconst_nullInstruction);
				if (result == null) result = caseSimpleInstruction(aconst_nullInstruction);
				if (result == null) result = caseInstruction(aconst_nullInstruction);
				if (result == null) result = caseIdentifiable(aconst_nullInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.ICONST_M1_INSTRUCTION: {
				Iconst_m1Instruction iconst_m1Instruction = (Iconst_m1Instruction)theEObject;
				T result = caseIconst_m1Instruction(iconst_m1Instruction);
				if (result == null) result = caseSimpleInstruction(iconst_m1Instruction);
				if (result == null) result = caseInstruction(iconst_m1Instruction);
				if (result == null) result = caseIdentifiable(iconst_m1Instruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.ICONST_0INSTRUCTION: {
				Iconst_0Instruction iconst_0Instruction = (Iconst_0Instruction)theEObject;
				T result = caseIconst_0Instruction(iconst_0Instruction);
				if (result == null) result = caseSimpleInstruction(iconst_0Instruction);
				if (result == null) result = caseInstruction(iconst_0Instruction);
				if (result == null) result = caseIdentifiable(iconst_0Instruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.ICONST_1INSTRUCTION: {
				Iconst_1Instruction iconst_1Instruction = (Iconst_1Instruction)theEObject;
				T result = caseIconst_1Instruction(iconst_1Instruction);
				if (result == null) result = caseSimpleInstruction(iconst_1Instruction);
				if (result == null) result = caseInstruction(iconst_1Instruction);
				if (result == null) result = caseIdentifiable(iconst_1Instruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.ICONST_2INSTRUCTION: {
				Iconst_2Instruction iconst_2Instruction = (Iconst_2Instruction)theEObject;
				T result = caseIconst_2Instruction(iconst_2Instruction);
				if (result == null) result = caseSimpleInstruction(iconst_2Instruction);
				if (result == null) result = caseInstruction(iconst_2Instruction);
				if (result == null) result = caseIdentifiable(iconst_2Instruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.ICONST_3INSTRUCTION: {
				Iconst_3Instruction iconst_3Instruction = (Iconst_3Instruction)theEObject;
				T result = caseIconst_3Instruction(iconst_3Instruction);
				if (result == null) result = caseSimpleInstruction(iconst_3Instruction);
				if (result == null) result = caseInstruction(iconst_3Instruction);
				if (result == null) result = caseIdentifiable(iconst_3Instruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.ICONST_4INSTRUCTION: {
				Iconst_4Instruction iconst_4Instruction = (Iconst_4Instruction)theEObject;
				T result = caseIconst_4Instruction(iconst_4Instruction);
				if (result == null) result = caseSimpleInstruction(iconst_4Instruction);
				if (result == null) result = caseInstruction(iconst_4Instruction);
				if (result == null) result = caseIdentifiable(iconst_4Instruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.ICONST_5INSTRUCTION: {
				Iconst_5Instruction iconst_5Instruction = (Iconst_5Instruction)theEObject;
				T result = caseIconst_5Instruction(iconst_5Instruction);
				if (result == null) result = caseSimpleInstruction(iconst_5Instruction);
				if (result == null) result = caseInstruction(iconst_5Instruction);
				if (result == null) result = caseIdentifiable(iconst_5Instruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.LCONST_0INSTRUCTION: {
				Lconst_0Instruction lconst_0Instruction = (Lconst_0Instruction)theEObject;
				T result = caseLconst_0Instruction(lconst_0Instruction);
				if (result == null) result = caseSimpleInstruction(lconst_0Instruction);
				if (result == null) result = caseInstruction(lconst_0Instruction);
				if (result == null) result = caseIdentifiable(lconst_0Instruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.LCONST_1INSTRUCTION: {
				Lconst_1Instruction lconst_1Instruction = (Lconst_1Instruction)theEObject;
				T result = caseLconst_1Instruction(lconst_1Instruction);
				if (result == null) result = caseSimpleInstruction(lconst_1Instruction);
				if (result == null) result = caseInstruction(lconst_1Instruction);
				if (result == null) result = caseIdentifiable(lconst_1Instruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.FCONST_0INSTRUCTION: {
				Fconst_0Instruction fconst_0Instruction = (Fconst_0Instruction)theEObject;
				T result = caseFconst_0Instruction(fconst_0Instruction);
				if (result == null) result = caseSimpleInstruction(fconst_0Instruction);
				if (result == null) result = caseInstruction(fconst_0Instruction);
				if (result == null) result = caseIdentifiable(fconst_0Instruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.FCONST_1INSTRUCTION: {
				Fconst_1Instruction fconst_1Instruction = (Fconst_1Instruction)theEObject;
				T result = caseFconst_1Instruction(fconst_1Instruction);
				if (result == null) result = caseSimpleInstruction(fconst_1Instruction);
				if (result == null) result = caseInstruction(fconst_1Instruction);
				if (result == null) result = caseIdentifiable(fconst_1Instruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.FCONST_2INSTRUCTION: {
				Fconst_2Instruction fconst_2Instruction = (Fconst_2Instruction)theEObject;
				T result = caseFconst_2Instruction(fconst_2Instruction);
				if (result == null) result = caseSimpleInstruction(fconst_2Instruction);
				if (result == null) result = caseInstruction(fconst_2Instruction);
				if (result == null) result = caseIdentifiable(fconst_2Instruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.DCONST_0INSTRUCTION: {
				Dconst_0Instruction dconst_0Instruction = (Dconst_0Instruction)theEObject;
				T result = caseDconst_0Instruction(dconst_0Instruction);
				if (result == null) result = caseSimpleInstruction(dconst_0Instruction);
				if (result == null) result = caseInstruction(dconst_0Instruction);
				if (result == null) result = caseIdentifiable(dconst_0Instruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.DCONST_1INSTRUCTION: {
				Dconst_1Instruction dconst_1Instruction = (Dconst_1Instruction)theEObject;
				T result = caseDconst_1Instruction(dconst_1Instruction);
				if (result == null) result = caseSimpleInstruction(dconst_1Instruction);
				if (result == null) result = caseInstruction(dconst_1Instruction);
				if (result == null) result = caseIdentifiable(dconst_1Instruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.IALOAD_INSTRUCTION: {
				IaloadInstruction ialoadInstruction = (IaloadInstruction)theEObject;
				T result = caseIaloadInstruction(ialoadInstruction);
				if (result == null) result = caseSimpleInstruction(ialoadInstruction);
				if (result == null) result = caseInstruction(ialoadInstruction);
				if (result == null) result = caseIdentifiable(ialoadInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.LALOAD_INSTRUCTION: {
				LaloadInstruction laloadInstruction = (LaloadInstruction)theEObject;
				T result = caseLaloadInstruction(laloadInstruction);
				if (result == null) result = caseSimpleInstruction(laloadInstruction);
				if (result == null) result = caseInstruction(laloadInstruction);
				if (result == null) result = caseIdentifiable(laloadInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.FALOAD_INSTRUCTION: {
				FaloadInstruction faloadInstruction = (FaloadInstruction)theEObject;
				T result = caseFaloadInstruction(faloadInstruction);
				if (result == null) result = caseSimpleInstruction(faloadInstruction);
				if (result == null) result = caseInstruction(faloadInstruction);
				if (result == null) result = caseIdentifiable(faloadInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.DALOAD_INSTRUCTION: {
				DaloadInstruction daloadInstruction = (DaloadInstruction)theEObject;
				T result = caseDaloadInstruction(daloadInstruction);
				if (result == null) result = caseSimpleInstruction(daloadInstruction);
				if (result == null) result = caseInstruction(daloadInstruction);
				if (result == null) result = caseIdentifiable(daloadInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.AALOAD_INSTRUCTION: {
				AaloadInstruction aaloadInstruction = (AaloadInstruction)theEObject;
				T result = caseAaloadInstruction(aaloadInstruction);
				if (result == null) result = caseSimpleInstruction(aaloadInstruction);
				if (result == null) result = caseInstruction(aaloadInstruction);
				if (result == null) result = caseIdentifiable(aaloadInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.BALOAD_INSTRUCTION: {
				BaloadInstruction baloadInstruction = (BaloadInstruction)theEObject;
				T result = caseBaloadInstruction(baloadInstruction);
				if (result == null) result = caseSimpleInstruction(baloadInstruction);
				if (result == null) result = caseInstruction(baloadInstruction);
				if (result == null) result = caseIdentifiable(baloadInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.CALOAD_INSTRUCTION: {
				CaloadInstruction caloadInstruction = (CaloadInstruction)theEObject;
				T result = caseCaloadInstruction(caloadInstruction);
				if (result == null) result = caseSimpleInstruction(caloadInstruction);
				if (result == null) result = caseInstruction(caloadInstruction);
				if (result == null) result = caseIdentifiable(caloadInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.SALOAD_INSTRUCTION: {
				SaloadInstruction saloadInstruction = (SaloadInstruction)theEObject;
				T result = caseSaloadInstruction(saloadInstruction);
				if (result == null) result = caseSimpleInstruction(saloadInstruction);
				if (result == null) result = caseInstruction(saloadInstruction);
				if (result == null) result = caseIdentifiable(saloadInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.IASTORE_INSTRUCTION: {
				IastoreInstruction iastoreInstruction = (IastoreInstruction)theEObject;
				T result = caseIastoreInstruction(iastoreInstruction);
				if (result == null) result = caseSimpleInstruction(iastoreInstruction);
				if (result == null) result = caseInstruction(iastoreInstruction);
				if (result == null) result = caseIdentifiable(iastoreInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.LASTORE_INSTRUCTION: {
				LastoreInstruction lastoreInstruction = (LastoreInstruction)theEObject;
				T result = caseLastoreInstruction(lastoreInstruction);
				if (result == null) result = caseSimpleInstruction(lastoreInstruction);
				if (result == null) result = caseInstruction(lastoreInstruction);
				if (result == null) result = caseIdentifiable(lastoreInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.FASTORE_INSTRUCTION: {
				FastoreInstruction fastoreInstruction = (FastoreInstruction)theEObject;
				T result = caseFastoreInstruction(fastoreInstruction);
				if (result == null) result = caseSimpleInstruction(fastoreInstruction);
				if (result == null) result = caseInstruction(fastoreInstruction);
				if (result == null) result = caseIdentifiable(fastoreInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.DASTORE_INSTRUCTION: {
				DastoreInstruction dastoreInstruction = (DastoreInstruction)theEObject;
				T result = caseDastoreInstruction(dastoreInstruction);
				if (result == null) result = caseSimpleInstruction(dastoreInstruction);
				if (result == null) result = caseInstruction(dastoreInstruction);
				if (result == null) result = caseIdentifiable(dastoreInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.AASTORE_INSTRUCTION: {
				AastoreInstruction aastoreInstruction = (AastoreInstruction)theEObject;
				T result = caseAastoreInstruction(aastoreInstruction);
				if (result == null) result = caseSimpleInstruction(aastoreInstruction);
				if (result == null) result = caseInstruction(aastoreInstruction);
				if (result == null) result = caseIdentifiable(aastoreInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.BASTORE_INSTRUCTION: {
				BastoreInstruction bastoreInstruction = (BastoreInstruction)theEObject;
				T result = caseBastoreInstruction(bastoreInstruction);
				if (result == null) result = caseSimpleInstruction(bastoreInstruction);
				if (result == null) result = caseInstruction(bastoreInstruction);
				if (result == null) result = caseIdentifiable(bastoreInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.CASTORE_INSTRUCTION: {
				CastoreInstruction castoreInstruction = (CastoreInstruction)theEObject;
				T result = caseCastoreInstruction(castoreInstruction);
				if (result == null) result = caseSimpleInstruction(castoreInstruction);
				if (result == null) result = caseInstruction(castoreInstruction);
				if (result == null) result = caseIdentifiable(castoreInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.SASTORE_INSTRUCTION: {
				SastoreInstruction sastoreInstruction = (SastoreInstruction)theEObject;
				T result = caseSastoreInstruction(sastoreInstruction);
				if (result == null) result = caseSimpleInstruction(sastoreInstruction);
				if (result == null) result = caseInstruction(sastoreInstruction);
				if (result == null) result = caseIdentifiable(sastoreInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.POP_INSTRUCTION: {
				PopInstruction popInstruction = (PopInstruction)theEObject;
				T result = casePopInstruction(popInstruction);
				if (result == null) result = caseSimpleInstruction(popInstruction);
				if (result == null) result = caseInstruction(popInstruction);
				if (result == null) result = caseIdentifiable(popInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.POP2_INSTRUCTION: {
				Pop2Instruction pop2Instruction = (Pop2Instruction)theEObject;
				T result = casePop2Instruction(pop2Instruction);
				if (result == null) result = caseSimpleInstruction(pop2Instruction);
				if (result == null) result = caseInstruction(pop2Instruction);
				if (result == null) result = caseIdentifiable(pop2Instruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.DUP_INSTRUCTION: {
				DupInstruction dupInstruction = (DupInstruction)theEObject;
				T result = caseDupInstruction(dupInstruction);
				if (result == null) result = caseSimpleInstruction(dupInstruction);
				if (result == null) result = caseInstruction(dupInstruction);
				if (result == null) result = caseIdentifiable(dupInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.DUP_X1_INSTRUCTION: {
				Dup_x1Instruction dup_x1Instruction = (Dup_x1Instruction)theEObject;
				T result = caseDup_x1Instruction(dup_x1Instruction);
				if (result == null) result = caseSimpleInstruction(dup_x1Instruction);
				if (result == null) result = caseInstruction(dup_x1Instruction);
				if (result == null) result = caseIdentifiable(dup_x1Instruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.DUP_X2_INSTRUCTION: {
				Dup_x2Instruction dup_x2Instruction = (Dup_x2Instruction)theEObject;
				T result = caseDup_x2Instruction(dup_x2Instruction);
				if (result == null) result = caseSimpleInstruction(dup_x2Instruction);
				if (result == null) result = caseInstruction(dup_x2Instruction);
				if (result == null) result = caseIdentifiable(dup_x2Instruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.DUP2_INSTRUCTION: {
				Dup2Instruction dup2Instruction = (Dup2Instruction)theEObject;
				T result = caseDup2Instruction(dup2Instruction);
				if (result == null) result = caseSimpleInstruction(dup2Instruction);
				if (result == null) result = caseInstruction(dup2Instruction);
				if (result == null) result = caseIdentifiable(dup2Instruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.DUP2_X1_INSTRUCTION: {
				Dup2_x1Instruction dup2_x1Instruction = (Dup2_x1Instruction)theEObject;
				T result = caseDup2_x1Instruction(dup2_x1Instruction);
				if (result == null) result = caseSimpleInstruction(dup2_x1Instruction);
				if (result == null) result = caseInstruction(dup2_x1Instruction);
				if (result == null) result = caseIdentifiable(dup2_x1Instruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.DUP2_X2_INSTRUCTION: {
				Dup2_x2Instruction dup2_x2Instruction = (Dup2_x2Instruction)theEObject;
				T result = caseDup2_x2Instruction(dup2_x2Instruction);
				if (result == null) result = caseSimpleInstruction(dup2_x2Instruction);
				if (result == null) result = caseInstruction(dup2_x2Instruction);
				if (result == null) result = caseIdentifiable(dup2_x2Instruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.SWAP_INSTRUCTION: {
				SwapInstruction swapInstruction = (SwapInstruction)theEObject;
				T result = caseSwapInstruction(swapInstruction);
				if (result == null) result = caseSimpleInstruction(swapInstruction);
				if (result == null) result = caseInstruction(swapInstruction);
				if (result == null) result = caseIdentifiable(swapInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.IADD_INSTRUCTION: {
				IaddInstruction iaddInstruction = (IaddInstruction)theEObject;
				T result = caseIaddInstruction(iaddInstruction);
				if (result == null) result = caseSimpleInstruction(iaddInstruction);
				if (result == null) result = caseInstruction(iaddInstruction);
				if (result == null) result = caseIdentifiable(iaddInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.LADD_INSTRUCTION: {
				LaddInstruction laddInstruction = (LaddInstruction)theEObject;
				T result = caseLaddInstruction(laddInstruction);
				if (result == null) result = caseSimpleInstruction(laddInstruction);
				if (result == null) result = caseInstruction(laddInstruction);
				if (result == null) result = caseIdentifiable(laddInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.FADD_INSTRUCTION: {
				FaddInstruction faddInstruction = (FaddInstruction)theEObject;
				T result = caseFaddInstruction(faddInstruction);
				if (result == null) result = caseSimpleInstruction(faddInstruction);
				if (result == null) result = caseInstruction(faddInstruction);
				if (result == null) result = caseIdentifiable(faddInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.DADD_INSTRUCTION: {
				DaddInstruction daddInstruction = (DaddInstruction)theEObject;
				T result = caseDaddInstruction(daddInstruction);
				if (result == null) result = caseSimpleInstruction(daddInstruction);
				if (result == null) result = caseInstruction(daddInstruction);
				if (result == null) result = caseIdentifiable(daddInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.ISUB_INSTRUCTION: {
				IsubInstruction isubInstruction = (IsubInstruction)theEObject;
				T result = caseIsubInstruction(isubInstruction);
				if (result == null) result = caseSimpleInstruction(isubInstruction);
				if (result == null) result = caseInstruction(isubInstruction);
				if (result == null) result = caseIdentifiable(isubInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.LSUB_INSTRUCTION: {
				LsubInstruction lsubInstruction = (LsubInstruction)theEObject;
				T result = caseLsubInstruction(lsubInstruction);
				if (result == null) result = caseSimpleInstruction(lsubInstruction);
				if (result == null) result = caseInstruction(lsubInstruction);
				if (result == null) result = caseIdentifiable(lsubInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.FSUB_INSTRUCTION: {
				FsubInstruction fsubInstruction = (FsubInstruction)theEObject;
				T result = caseFsubInstruction(fsubInstruction);
				if (result == null) result = caseSimpleInstruction(fsubInstruction);
				if (result == null) result = caseInstruction(fsubInstruction);
				if (result == null) result = caseIdentifiable(fsubInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.DSUB_INSTRUCTION: {
				DsubInstruction dsubInstruction = (DsubInstruction)theEObject;
				T result = caseDsubInstruction(dsubInstruction);
				if (result == null) result = caseSimpleInstruction(dsubInstruction);
				if (result == null) result = caseInstruction(dsubInstruction);
				if (result == null) result = caseIdentifiable(dsubInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.IMUL_INSTRUCTION: {
				ImulInstruction imulInstruction = (ImulInstruction)theEObject;
				T result = caseImulInstruction(imulInstruction);
				if (result == null) result = caseSimpleInstruction(imulInstruction);
				if (result == null) result = caseInstruction(imulInstruction);
				if (result == null) result = caseIdentifiable(imulInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.LMUL_INSTRUCTION: {
				LmulInstruction lmulInstruction = (LmulInstruction)theEObject;
				T result = caseLmulInstruction(lmulInstruction);
				if (result == null) result = caseSimpleInstruction(lmulInstruction);
				if (result == null) result = caseInstruction(lmulInstruction);
				if (result == null) result = caseIdentifiable(lmulInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.FMUL_INSTRUCTION: {
				FmulInstruction fmulInstruction = (FmulInstruction)theEObject;
				T result = caseFmulInstruction(fmulInstruction);
				if (result == null) result = caseSimpleInstruction(fmulInstruction);
				if (result == null) result = caseInstruction(fmulInstruction);
				if (result == null) result = caseIdentifiable(fmulInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.DMUL_INSTRUCTION: {
				DmulInstruction dmulInstruction = (DmulInstruction)theEObject;
				T result = caseDmulInstruction(dmulInstruction);
				if (result == null) result = caseSimpleInstruction(dmulInstruction);
				if (result == null) result = caseInstruction(dmulInstruction);
				if (result == null) result = caseIdentifiable(dmulInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.IDIV_INSTRUCTION: {
				IdivInstruction idivInstruction = (IdivInstruction)theEObject;
				T result = caseIdivInstruction(idivInstruction);
				if (result == null) result = caseSimpleInstruction(idivInstruction);
				if (result == null) result = caseInstruction(idivInstruction);
				if (result == null) result = caseIdentifiable(idivInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.LDIV_INSTRUCTION: {
				LdivInstruction ldivInstruction = (LdivInstruction)theEObject;
				T result = caseLdivInstruction(ldivInstruction);
				if (result == null) result = caseSimpleInstruction(ldivInstruction);
				if (result == null) result = caseInstruction(ldivInstruction);
				if (result == null) result = caseIdentifiable(ldivInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.FDIV_INSTRUCTION: {
				FdivInstruction fdivInstruction = (FdivInstruction)theEObject;
				T result = caseFdivInstruction(fdivInstruction);
				if (result == null) result = caseSimpleInstruction(fdivInstruction);
				if (result == null) result = caseInstruction(fdivInstruction);
				if (result == null) result = caseIdentifiable(fdivInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.DDIV_INSTRUCTION: {
				DdivInstruction ddivInstruction = (DdivInstruction)theEObject;
				T result = caseDdivInstruction(ddivInstruction);
				if (result == null) result = caseSimpleInstruction(ddivInstruction);
				if (result == null) result = caseInstruction(ddivInstruction);
				if (result == null) result = caseIdentifiable(ddivInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.IREM_INSTRUCTION: {
				IremInstruction iremInstruction = (IremInstruction)theEObject;
				T result = caseIremInstruction(iremInstruction);
				if (result == null) result = caseSimpleInstruction(iremInstruction);
				if (result == null) result = caseInstruction(iremInstruction);
				if (result == null) result = caseIdentifiable(iremInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.LREM_INSTRUCTION: {
				LremInstruction lremInstruction = (LremInstruction)theEObject;
				T result = caseLremInstruction(lremInstruction);
				if (result == null) result = caseSimpleInstruction(lremInstruction);
				if (result == null) result = caseInstruction(lremInstruction);
				if (result == null) result = caseIdentifiable(lremInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.FREM_INSTRUCTION: {
				FremInstruction fremInstruction = (FremInstruction)theEObject;
				T result = caseFremInstruction(fremInstruction);
				if (result == null) result = caseSimpleInstruction(fremInstruction);
				if (result == null) result = caseInstruction(fremInstruction);
				if (result == null) result = caseIdentifiable(fremInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.DREM_INSTRUCTION: {
				DremInstruction dremInstruction = (DremInstruction)theEObject;
				T result = caseDremInstruction(dremInstruction);
				if (result == null) result = caseSimpleInstruction(dremInstruction);
				if (result == null) result = caseInstruction(dremInstruction);
				if (result == null) result = caseIdentifiable(dremInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.INEG_INSTRUCTION: {
				InegInstruction inegInstruction = (InegInstruction)theEObject;
				T result = caseInegInstruction(inegInstruction);
				if (result == null) result = caseSimpleInstruction(inegInstruction);
				if (result == null) result = caseInstruction(inegInstruction);
				if (result == null) result = caseIdentifiable(inegInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.LNEG_INSTRUCTION: {
				LnegInstruction lnegInstruction = (LnegInstruction)theEObject;
				T result = caseLnegInstruction(lnegInstruction);
				if (result == null) result = caseSimpleInstruction(lnegInstruction);
				if (result == null) result = caseInstruction(lnegInstruction);
				if (result == null) result = caseIdentifiable(lnegInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.FNEG_INSTRUCTION: {
				FnegInstruction fnegInstruction = (FnegInstruction)theEObject;
				T result = caseFnegInstruction(fnegInstruction);
				if (result == null) result = caseSimpleInstruction(fnegInstruction);
				if (result == null) result = caseInstruction(fnegInstruction);
				if (result == null) result = caseIdentifiable(fnegInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.DNEG_INSTRUCTION: {
				DnegInstruction dnegInstruction = (DnegInstruction)theEObject;
				T result = caseDnegInstruction(dnegInstruction);
				if (result == null) result = caseSimpleInstruction(dnegInstruction);
				if (result == null) result = caseInstruction(dnegInstruction);
				if (result == null) result = caseIdentifiable(dnegInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.ISHL_INSTRUCTION: {
				IshlInstruction ishlInstruction = (IshlInstruction)theEObject;
				T result = caseIshlInstruction(ishlInstruction);
				if (result == null) result = caseSimpleInstruction(ishlInstruction);
				if (result == null) result = caseInstruction(ishlInstruction);
				if (result == null) result = caseIdentifiable(ishlInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.LSHL_INSTRUCTION: {
				LshlInstruction lshlInstruction = (LshlInstruction)theEObject;
				T result = caseLshlInstruction(lshlInstruction);
				if (result == null) result = caseSimpleInstruction(lshlInstruction);
				if (result == null) result = caseInstruction(lshlInstruction);
				if (result == null) result = caseIdentifiable(lshlInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.ISHR_INSTRUCTION: {
				IshrInstruction ishrInstruction = (IshrInstruction)theEObject;
				T result = caseIshrInstruction(ishrInstruction);
				if (result == null) result = caseSimpleInstruction(ishrInstruction);
				if (result == null) result = caseInstruction(ishrInstruction);
				if (result == null) result = caseIdentifiable(ishrInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.LSHR_INSTRUCTION: {
				LshrInstruction lshrInstruction = (LshrInstruction)theEObject;
				T result = caseLshrInstruction(lshrInstruction);
				if (result == null) result = caseSimpleInstruction(lshrInstruction);
				if (result == null) result = caseInstruction(lshrInstruction);
				if (result == null) result = caseIdentifiable(lshrInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.IUSHR_INSTRUCTION: {
				IushrInstruction iushrInstruction = (IushrInstruction)theEObject;
				T result = caseIushrInstruction(iushrInstruction);
				if (result == null) result = caseSimpleInstruction(iushrInstruction);
				if (result == null) result = caseInstruction(iushrInstruction);
				if (result == null) result = caseIdentifiable(iushrInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.LUSHR_INSTRUCTION: {
				LushrInstruction lushrInstruction = (LushrInstruction)theEObject;
				T result = caseLushrInstruction(lushrInstruction);
				if (result == null) result = caseSimpleInstruction(lushrInstruction);
				if (result == null) result = caseInstruction(lushrInstruction);
				if (result == null) result = caseIdentifiable(lushrInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.IAND_INSTRUCTION: {
				IandInstruction iandInstruction = (IandInstruction)theEObject;
				T result = caseIandInstruction(iandInstruction);
				if (result == null) result = caseSimpleInstruction(iandInstruction);
				if (result == null) result = caseInstruction(iandInstruction);
				if (result == null) result = caseIdentifiable(iandInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.LAND_INSTRUCTION: {
				LandInstruction landInstruction = (LandInstruction)theEObject;
				T result = caseLandInstruction(landInstruction);
				if (result == null) result = caseSimpleInstruction(landInstruction);
				if (result == null) result = caseInstruction(landInstruction);
				if (result == null) result = caseIdentifiable(landInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.IOR_INSTRUCTION: {
				IorInstruction iorInstruction = (IorInstruction)theEObject;
				T result = caseIorInstruction(iorInstruction);
				if (result == null) result = caseSimpleInstruction(iorInstruction);
				if (result == null) result = caseInstruction(iorInstruction);
				if (result == null) result = caseIdentifiable(iorInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.LOR_INSTRUCTION: {
				LorInstruction lorInstruction = (LorInstruction)theEObject;
				T result = caseLorInstruction(lorInstruction);
				if (result == null) result = caseSimpleInstruction(lorInstruction);
				if (result == null) result = caseInstruction(lorInstruction);
				if (result == null) result = caseIdentifiable(lorInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.IXOR_INSTRUCTION: {
				IxorInstruction ixorInstruction = (IxorInstruction)theEObject;
				T result = caseIxorInstruction(ixorInstruction);
				if (result == null) result = caseSimpleInstruction(ixorInstruction);
				if (result == null) result = caseInstruction(ixorInstruction);
				if (result == null) result = caseIdentifiable(ixorInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.LXOR_INSTRUCTION: {
				LxorInstruction lxorInstruction = (LxorInstruction)theEObject;
				T result = caseLxorInstruction(lxorInstruction);
				if (result == null) result = caseSimpleInstruction(lxorInstruction);
				if (result == null) result = caseInstruction(lxorInstruction);
				if (result == null) result = caseIdentifiable(lxorInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.I2L_INSTRUCTION: {
				I2lInstruction i2lInstruction = (I2lInstruction)theEObject;
				T result = caseI2lInstruction(i2lInstruction);
				if (result == null) result = caseSimpleInstruction(i2lInstruction);
				if (result == null) result = caseInstruction(i2lInstruction);
				if (result == null) result = caseIdentifiable(i2lInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.I2F_INSTRUCTION: {
				I2fInstruction i2fInstruction = (I2fInstruction)theEObject;
				T result = caseI2fInstruction(i2fInstruction);
				if (result == null) result = caseSimpleInstruction(i2fInstruction);
				if (result == null) result = caseInstruction(i2fInstruction);
				if (result == null) result = caseIdentifiable(i2fInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.I2D_INSTRUCTION: {
				I2dInstruction i2dInstruction = (I2dInstruction)theEObject;
				T result = caseI2dInstruction(i2dInstruction);
				if (result == null) result = caseSimpleInstruction(i2dInstruction);
				if (result == null) result = caseInstruction(i2dInstruction);
				if (result == null) result = caseIdentifiable(i2dInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.L2I_INSTRUCTION: {
				L2iInstruction l2iInstruction = (L2iInstruction)theEObject;
				T result = caseL2iInstruction(l2iInstruction);
				if (result == null) result = caseSimpleInstruction(l2iInstruction);
				if (result == null) result = caseInstruction(l2iInstruction);
				if (result == null) result = caseIdentifiable(l2iInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.L2F_INSTRUCTION: {
				L2fInstruction l2fInstruction = (L2fInstruction)theEObject;
				T result = caseL2fInstruction(l2fInstruction);
				if (result == null) result = caseSimpleInstruction(l2fInstruction);
				if (result == null) result = caseInstruction(l2fInstruction);
				if (result == null) result = caseIdentifiable(l2fInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.L2D_INSTRUCTION: {
				L2dInstruction l2dInstruction = (L2dInstruction)theEObject;
				T result = caseL2dInstruction(l2dInstruction);
				if (result == null) result = caseSimpleInstruction(l2dInstruction);
				if (result == null) result = caseInstruction(l2dInstruction);
				if (result == null) result = caseIdentifiable(l2dInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.F2I_INSTRUCTION: {
				F2iInstruction f2iInstruction = (F2iInstruction)theEObject;
				T result = caseF2iInstruction(f2iInstruction);
				if (result == null) result = caseSimpleInstruction(f2iInstruction);
				if (result == null) result = caseInstruction(f2iInstruction);
				if (result == null) result = caseIdentifiable(f2iInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.F2L_INSTRUCTION: {
				F2lInstruction f2lInstruction = (F2lInstruction)theEObject;
				T result = caseF2lInstruction(f2lInstruction);
				if (result == null) result = caseSimpleInstruction(f2lInstruction);
				if (result == null) result = caseInstruction(f2lInstruction);
				if (result == null) result = caseIdentifiable(f2lInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.F2D_INSTRUCTION: {
				F2dInstruction f2dInstruction = (F2dInstruction)theEObject;
				T result = caseF2dInstruction(f2dInstruction);
				if (result == null) result = caseSimpleInstruction(f2dInstruction);
				if (result == null) result = caseInstruction(f2dInstruction);
				if (result == null) result = caseIdentifiable(f2dInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.D2I_INSTRUCTION: {
				D2iInstruction d2iInstruction = (D2iInstruction)theEObject;
				T result = caseD2iInstruction(d2iInstruction);
				if (result == null) result = caseSimpleInstruction(d2iInstruction);
				if (result == null) result = caseInstruction(d2iInstruction);
				if (result == null) result = caseIdentifiable(d2iInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.D2L_INSTRUCTION: {
				D2lInstruction d2lInstruction = (D2lInstruction)theEObject;
				T result = caseD2lInstruction(d2lInstruction);
				if (result == null) result = caseSimpleInstruction(d2lInstruction);
				if (result == null) result = caseInstruction(d2lInstruction);
				if (result == null) result = caseIdentifiable(d2lInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.D2F_INSTRUCTION: {
				D2fInstruction d2fInstruction = (D2fInstruction)theEObject;
				T result = caseD2fInstruction(d2fInstruction);
				if (result == null) result = caseSimpleInstruction(d2fInstruction);
				if (result == null) result = caseInstruction(d2fInstruction);
				if (result == null) result = caseIdentifiable(d2fInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.I2B_INSTRUCTION: {
				I2bInstruction i2bInstruction = (I2bInstruction)theEObject;
				T result = caseI2bInstruction(i2bInstruction);
				if (result == null) result = caseSimpleInstruction(i2bInstruction);
				if (result == null) result = caseInstruction(i2bInstruction);
				if (result == null) result = caseIdentifiable(i2bInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.I2C_INSTRUCTION: {
				I2cInstruction i2cInstruction = (I2cInstruction)theEObject;
				T result = caseI2cInstruction(i2cInstruction);
				if (result == null) result = caseSimpleInstruction(i2cInstruction);
				if (result == null) result = caseInstruction(i2cInstruction);
				if (result == null) result = caseIdentifiable(i2cInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.I2S_INSTRUCTION: {
				I2sInstruction i2sInstruction = (I2sInstruction)theEObject;
				T result = caseI2sInstruction(i2sInstruction);
				if (result == null) result = caseSimpleInstruction(i2sInstruction);
				if (result == null) result = caseInstruction(i2sInstruction);
				if (result == null) result = caseIdentifiable(i2sInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.LCMP_INSTRUCTION: {
				LcmpInstruction lcmpInstruction = (LcmpInstruction)theEObject;
				T result = caseLcmpInstruction(lcmpInstruction);
				if (result == null) result = caseSimpleInstruction(lcmpInstruction);
				if (result == null) result = caseInstruction(lcmpInstruction);
				if (result == null) result = caseIdentifiable(lcmpInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.FCMPL_INSTRUCTION: {
				FcmplInstruction fcmplInstruction = (FcmplInstruction)theEObject;
				T result = caseFcmplInstruction(fcmplInstruction);
				if (result == null) result = caseSimpleInstruction(fcmplInstruction);
				if (result == null) result = caseInstruction(fcmplInstruction);
				if (result == null) result = caseIdentifiable(fcmplInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.FCMPG_INSTRUCTION: {
				FcmpgInstruction fcmpgInstruction = (FcmpgInstruction)theEObject;
				T result = caseFcmpgInstruction(fcmpgInstruction);
				if (result == null) result = caseSimpleInstruction(fcmpgInstruction);
				if (result == null) result = caseInstruction(fcmpgInstruction);
				if (result == null) result = caseIdentifiable(fcmpgInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.DCMPL_INSTRUCTION: {
				DcmplInstruction dcmplInstruction = (DcmplInstruction)theEObject;
				T result = caseDcmplInstruction(dcmplInstruction);
				if (result == null) result = caseSimpleInstruction(dcmplInstruction);
				if (result == null) result = caseInstruction(dcmplInstruction);
				if (result == null) result = caseIdentifiable(dcmplInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.DCMPG_INSTRUCTION: {
				DcmpgInstruction dcmpgInstruction = (DcmpgInstruction)theEObject;
				T result = caseDcmpgInstruction(dcmpgInstruction);
				if (result == null) result = caseSimpleInstruction(dcmpgInstruction);
				if (result == null) result = caseInstruction(dcmpgInstruction);
				if (result == null) result = caseIdentifiable(dcmpgInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.IRETURN_INSTRUCTION: {
				IreturnInstruction ireturnInstruction = (IreturnInstruction)theEObject;
				T result = caseIreturnInstruction(ireturnInstruction);
				if (result == null) result = caseSimpleInstruction(ireturnInstruction);
				if (result == null) result = caseInstruction(ireturnInstruction);
				if (result == null) result = caseIdentifiable(ireturnInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.LRETURN_INSTRUCTION: {
				LreturnInstruction lreturnInstruction = (LreturnInstruction)theEObject;
				T result = caseLreturnInstruction(lreturnInstruction);
				if (result == null) result = caseSimpleInstruction(lreturnInstruction);
				if (result == null) result = caseInstruction(lreturnInstruction);
				if (result == null) result = caseIdentifiable(lreturnInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.FRETURN_INSTRUCTION: {
				FreturnInstruction freturnInstruction = (FreturnInstruction)theEObject;
				T result = caseFreturnInstruction(freturnInstruction);
				if (result == null) result = caseSimpleInstruction(freturnInstruction);
				if (result == null) result = caseInstruction(freturnInstruction);
				if (result == null) result = caseIdentifiable(freturnInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.DRETURN_INSTRUCTION: {
				DreturnInstruction dreturnInstruction = (DreturnInstruction)theEObject;
				T result = caseDreturnInstruction(dreturnInstruction);
				if (result == null) result = caseSimpleInstruction(dreturnInstruction);
				if (result == null) result = caseInstruction(dreturnInstruction);
				if (result == null) result = caseIdentifiable(dreturnInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.ARETURN_INSTRUCTION: {
				AreturnInstruction areturnInstruction = (AreturnInstruction)theEObject;
				T result = caseAreturnInstruction(areturnInstruction);
				if (result == null) result = caseSimpleInstruction(areturnInstruction);
				if (result == null) result = caseInstruction(areturnInstruction);
				if (result == null) result = caseIdentifiable(areturnInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.RETURN_INSTRUCTION: {
				ReturnInstruction returnInstruction = (ReturnInstruction)theEObject;
				T result = caseReturnInstruction(returnInstruction);
				if (result == null) result = caseSimpleInstruction(returnInstruction);
				if (result == null) result = caseInstruction(returnInstruction);
				if (result == null) result = caseIdentifiable(returnInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.ARRAYLENGTH_INSTRUCTION: {
				ArraylengthInstruction arraylengthInstruction = (ArraylengthInstruction)theEObject;
				T result = caseArraylengthInstruction(arraylengthInstruction);
				if (result == null) result = caseSimpleInstruction(arraylengthInstruction);
				if (result == null) result = caseInstruction(arraylengthInstruction);
				if (result == null) result = caseIdentifiable(arraylengthInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.ATHROW_INSTRUCTION: {
				AthrowInstruction athrowInstruction = (AthrowInstruction)theEObject;
				T result = caseAthrowInstruction(athrowInstruction);
				if (result == null) result = caseSimpleInstruction(athrowInstruction);
				if (result == null) result = caseInstruction(athrowInstruction);
				if (result == null) result = caseIdentifiable(athrowInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.MONITORENTER_INSTRUCTION: {
				MonitorenterInstruction monitorenterInstruction = (MonitorenterInstruction)theEObject;
				T result = caseMonitorenterInstruction(monitorenterInstruction);
				if (result == null) result = caseSimpleInstruction(monitorenterInstruction);
				if (result == null) result = caseInstruction(monitorenterInstruction);
				if (result == null) result = caseIdentifiable(monitorenterInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.MONITOREXIT_INSTRUCTION: {
				MonitorexitInstruction monitorexitInstruction = (MonitorexitInstruction)theEObject;
				T result = caseMonitorexitInstruction(monitorexitInstruction);
				if (result == null) result = caseSimpleInstruction(monitorexitInstruction);
				if (result == null) result = caseInstruction(monitorexitInstruction);
				if (result == null) result = caseIdentifiable(monitorexitInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.BIPUSH_INSTRUCTION: {
				BipushInstruction bipushInstruction = (BipushInstruction)theEObject;
				T result = caseBipushInstruction(bipushInstruction);
				if (result == null) result = caseIntInstruction(bipushInstruction);
				if (result == null) result = caseInstruction(bipushInstruction);
				if (result == null) result = caseIdentifiable(bipushInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.SIPUSH_INSTRUCTION: {
				SipushInstruction sipushInstruction = (SipushInstruction)theEObject;
				T result = caseSipushInstruction(sipushInstruction);
				if (result == null) result = caseIntInstruction(sipushInstruction);
				if (result == null) result = caseInstruction(sipushInstruction);
				if (result == null) result = caseIdentifiable(sipushInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.NEWARRAY_INSTRUCTION: {
				NewarrayInstruction newarrayInstruction = (NewarrayInstruction)theEObject;
				T result = caseNewarrayInstruction(newarrayInstruction);
				if (result == null) result = caseIntInstruction(newarrayInstruction);
				if (result == null) result = caseInstruction(newarrayInstruction);
				if (result == null) result = caseIdentifiable(newarrayInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.IFEQ_INSTRUCTION: {
				IfeqInstruction ifeqInstruction = (IfeqInstruction)theEObject;
				T result = caseIfeqInstruction(ifeqInstruction);
				if (result == null) result = caseJumpInstruction(ifeqInstruction);
				if (result == null) result = caseInstruction(ifeqInstruction);
				if (result == null) result = caseIdentifiable(ifeqInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.IFNE_INSTRUCTION: {
				IfneInstruction ifneInstruction = (IfneInstruction)theEObject;
				T result = caseIfneInstruction(ifneInstruction);
				if (result == null) result = caseJumpInstruction(ifneInstruction);
				if (result == null) result = caseInstruction(ifneInstruction);
				if (result == null) result = caseIdentifiable(ifneInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.IFLT_INSTRUCTION: {
				IfltInstruction ifltInstruction = (IfltInstruction)theEObject;
				T result = caseIfltInstruction(ifltInstruction);
				if (result == null) result = caseJumpInstruction(ifltInstruction);
				if (result == null) result = caseInstruction(ifltInstruction);
				if (result == null) result = caseIdentifiable(ifltInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.IFGE_INSTRUCTION: {
				IfgeInstruction ifgeInstruction = (IfgeInstruction)theEObject;
				T result = caseIfgeInstruction(ifgeInstruction);
				if (result == null) result = caseJumpInstruction(ifgeInstruction);
				if (result == null) result = caseInstruction(ifgeInstruction);
				if (result == null) result = caseIdentifiable(ifgeInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.IFGT_INSTRUCTION: {
				IfgtInstruction ifgtInstruction = (IfgtInstruction)theEObject;
				T result = caseIfgtInstruction(ifgtInstruction);
				if (result == null) result = caseJumpInstruction(ifgtInstruction);
				if (result == null) result = caseInstruction(ifgtInstruction);
				if (result == null) result = caseIdentifiable(ifgtInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.IFLE_INSTRUCTION: {
				IfleInstruction ifleInstruction = (IfleInstruction)theEObject;
				T result = caseIfleInstruction(ifleInstruction);
				if (result == null) result = caseJumpInstruction(ifleInstruction);
				if (result == null) result = caseInstruction(ifleInstruction);
				if (result == null) result = caseIdentifiable(ifleInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.IF_ICMPEQ_INSTRUCTION: {
				If_icmpeqInstruction if_icmpeqInstruction = (If_icmpeqInstruction)theEObject;
				T result = caseIf_icmpeqInstruction(if_icmpeqInstruction);
				if (result == null) result = caseJumpInstruction(if_icmpeqInstruction);
				if (result == null) result = caseInstruction(if_icmpeqInstruction);
				if (result == null) result = caseIdentifiable(if_icmpeqInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.IF_ICMPNE_INSTRUCTION: {
				If_icmpneInstruction if_icmpneInstruction = (If_icmpneInstruction)theEObject;
				T result = caseIf_icmpneInstruction(if_icmpneInstruction);
				if (result == null) result = caseJumpInstruction(if_icmpneInstruction);
				if (result == null) result = caseInstruction(if_icmpneInstruction);
				if (result == null) result = caseIdentifiable(if_icmpneInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.IF_ICMPLT_INSTRUCTION: {
				If_icmpltInstruction if_icmpltInstruction = (If_icmpltInstruction)theEObject;
				T result = caseIf_icmpltInstruction(if_icmpltInstruction);
				if (result == null) result = caseJumpInstruction(if_icmpltInstruction);
				if (result == null) result = caseInstruction(if_icmpltInstruction);
				if (result == null) result = caseIdentifiable(if_icmpltInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.IF_ICMPGE_INSTRUCTION: {
				If_icmpgeInstruction if_icmpgeInstruction = (If_icmpgeInstruction)theEObject;
				T result = caseIf_icmpgeInstruction(if_icmpgeInstruction);
				if (result == null) result = caseJumpInstruction(if_icmpgeInstruction);
				if (result == null) result = caseInstruction(if_icmpgeInstruction);
				if (result == null) result = caseIdentifiable(if_icmpgeInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.IF_ICMPGT_INSTRUCTION: {
				If_icmpgtInstruction if_icmpgtInstruction = (If_icmpgtInstruction)theEObject;
				T result = caseIf_icmpgtInstruction(if_icmpgtInstruction);
				if (result == null) result = caseJumpInstruction(if_icmpgtInstruction);
				if (result == null) result = caseInstruction(if_icmpgtInstruction);
				if (result == null) result = caseIdentifiable(if_icmpgtInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.IF_ICMPLE_INSTRUCTION: {
				If_icmpleInstruction if_icmpleInstruction = (If_icmpleInstruction)theEObject;
				T result = caseIf_icmpleInstruction(if_icmpleInstruction);
				if (result == null) result = caseJumpInstruction(if_icmpleInstruction);
				if (result == null) result = caseInstruction(if_icmpleInstruction);
				if (result == null) result = caseIdentifiable(if_icmpleInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.IF_ACMPEQ_INSTRUCTION: {
				If_acmpeqInstruction if_acmpeqInstruction = (If_acmpeqInstruction)theEObject;
				T result = caseIf_acmpeqInstruction(if_acmpeqInstruction);
				if (result == null) result = caseJumpInstruction(if_acmpeqInstruction);
				if (result == null) result = caseInstruction(if_acmpeqInstruction);
				if (result == null) result = caseIdentifiable(if_acmpeqInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.IF_ACMPNE_INSTRUCTION: {
				If_acmpneInstruction if_acmpneInstruction = (If_acmpneInstruction)theEObject;
				T result = caseIf_acmpneInstruction(if_acmpneInstruction);
				if (result == null) result = caseJumpInstruction(if_acmpneInstruction);
				if (result == null) result = caseInstruction(if_acmpneInstruction);
				if (result == null) result = caseIdentifiable(if_acmpneInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.GOTO_INSTRUCTION: {
				GotoInstruction gotoInstruction = (GotoInstruction)theEObject;
				T result = caseGotoInstruction(gotoInstruction);
				if (result == null) result = caseJumpInstruction(gotoInstruction);
				if (result == null) result = caseInstruction(gotoInstruction);
				if (result == null) result = caseIdentifiable(gotoInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.JSR_INSTRUCTION: {
				JsrInstruction jsrInstruction = (JsrInstruction)theEObject;
				T result = caseJsrInstruction(jsrInstruction);
				if (result == null) result = caseJumpInstruction(jsrInstruction);
				if (result == null) result = caseInstruction(jsrInstruction);
				if (result == null) result = caseIdentifiable(jsrInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.IFNULL_INSTRUCTION: {
				IfnullInstruction ifnullInstruction = (IfnullInstruction)theEObject;
				T result = caseIfnullInstruction(ifnullInstruction);
				if (result == null) result = caseJumpInstruction(ifnullInstruction);
				if (result == null) result = caseInstruction(ifnullInstruction);
				if (result == null) result = caseIdentifiable(ifnullInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.IFNONNULL_INSTRUCTION: {
				IfnonnullInstruction ifnonnullInstruction = (IfnonnullInstruction)theEObject;
				T result = caseIfnonnullInstruction(ifnonnullInstruction);
				if (result == null) result = caseJumpInstruction(ifnonnullInstruction);
				if (result == null) result = caseInstruction(ifnonnullInstruction);
				if (result == null) result = caseIdentifiable(ifnonnullInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.INVOKEVIRTUAL_INSTRUCTION: {
				InvokevirtualInstruction invokevirtualInstruction = (InvokevirtualInstruction)theEObject;
				T result = caseInvokevirtualInstruction(invokevirtualInstruction);
				if (result == null) result = caseMethodInstruction(invokevirtualInstruction);
				if (result == null) result = caseInstruction(invokevirtualInstruction);
				if (result == null) result = caseIdentifiable(invokevirtualInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.INVOKESPECIAL_INSTRUCTION: {
				InvokespecialInstruction invokespecialInstruction = (InvokespecialInstruction)theEObject;
				T result = caseInvokespecialInstruction(invokespecialInstruction);
				if (result == null) result = caseMethodInstruction(invokespecialInstruction);
				if (result == null) result = caseInstruction(invokespecialInstruction);
				if (result == null) result = caseIdentifiable(invokespecialInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.INVOKESTATIC_INSTRUCTION: {
				InvokestaticInstruction invokestaticInstruction = (InvokestaticInstruction)theEObject;
				T result = caseInvokestaticInstruction(invokestaticInstruction);
				if (result == null) result = caseMethodInstruction(invokestaticInstruction);
				if (result == null) result = caseInstruction(invokestaticInstruction);
				if (result == null) result = caseIdentifiable(invokestaticInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.INVOKEINTERFACE_INSTRUCTION: {
				InvokeinterfaceInstruction invokeinterfaceInstruction = (InvokeinterfaceInstruction)theEObject;
				T result = caseInvokeinterfaceInstruction(invokeinterfaceInstruction);
				if (result == null) result = caseMethodInstruction(invokeinterfaceInstruction);
				if (result == null) result = caseInstruction(invokeinterfaceInstruction);
				if (result == null) result = caseIdentifiable(invokeinterfaceInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.NEW_INSTRUCTION: {
				NewInstruction newInstruction = (NewInstruction)theEObject;
				T result = caseNewInstruction(newInstruction);
				if (result == null) result = caseTypeInstruction(newInstruction);
				if (result == null) result = caseInstruction(newInstruction);
				if (result == null) result = caseIdentifiable(newInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.ANEWARRAY_INSTRUCTION: {
				AnewarrayInstruction anewarrayInstruction = (AnewarrayInstruction)theEObject;
				T result = caseAnewarrayInstruction(anewarrayInstruction);
				if (result == null) result = caseTypeInstruction(anewarrayInstruction);
				if (result == null) result = caseInstruction(anewarrayInstruction);
				if (result == null) result = caseIdentifiable(anewarrayInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.CHECKCAST_INSTRUCTION: {
				CheckcastInstruction checkcastInstruction = (CheckcastInstruction)theEObject;
				T result = caseCheckcastInstruction(checkcastInstruction);
				if (result == null) result = caseTypeInstruction(checkcastInstruction);
				if (result == null) result = caseInstruction(checkcastInstruction);
				if (result == null) result = caseIdentifiable(checkcastInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.INSTANCEOF_INSTRUCTION: {
				InstanceofInstruction instanceofInstruction = (InstanceofInstruction)theEObject;
				T result = caseInstanceofInstruction(instanceofInstruction);
				if (result == null) result = caseTypeInstruction(instanceofInstruction);
				if (result == null) result = caseInstruction(instanceofInstruction);
				if (result == null) result = caseIdentifiable(instanceofInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.ILOAD_INSTRUCTION: {
				IloadInstruction iloadInstruction = (IloadInstruction)theEObject;
				T result = caseIloadInstruction(iloadInstruction);
				if (result == null) result = caseVarInstruction(iloadInstruction);
				if (result == null) result = caseInstruction(iloadInstruction);
				if (result == null) result = caseIdentifiable(iloadInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.LLOAD_INSTRUCTION: {
				LloadInstruction lloadInstruction = (LloadInstruction)theEObject;
				T result = caseLloadInstruction(lloadInstruction);
				if (result == null) result = caseVarInstruction(lloadInstruction);
				if (result == null) result = caseInstruction(lloadInstruction);
				if (result == null) result = caseIdentifiable(lloadInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.FLOAD_INSTRUCTION: {
				FloadInstruction floadInstruction = (FloadInstruction)theEObject;
				T result = caseFloadInstruction(floadInstruction);
				if (result == null) result = caseVarInstruction(floadInstruction);
				if (result == null) result = caseInstruction(floadInstruction);
				if (result == null) result = caseIdentifiable(floadInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.DLOAD_INSTRUCTION: {
				DloadInstruction dloadInstruction = (DloadInstruction)theEObject;
				T result = caseDloadInstruction(dloadInstruction);
				if (result == null) result = caseVarInstruction(dloadInstruction);
				if (result == null) result = caseInstruction(dloadInstruction);
				if (result == null) result = caseIdentifiable(dloadInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.ALOAD_INSTRUCTION: {
				AloadInstruction aloadInstruction = (AloadInstruction)theEObject;
				T result = caseAloadInstruction(aloadInstruction);
				if (result == null) result = caseVarInstruction(aloadInstruction);
				if (result == null) result = caseInstruction(aloadInstruction);
				if (result == null) result = caseIdentifiable(aloadInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.ISTORE_INSTRUCTION: {
				IstoreInstruction istoreInstruction = (IstoreInstruction)theEObject;
				T result = caseIstoreInstruction(istoreInstruction);
				if (result == null) result = caseVarInstruction(istoreInstruction);
				if (result == null) result = caseInstruction(istoreInstruction);
				if (result == null) result = caseIdentifiable(istoreInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.LSTORE_INSTRUCTION: {
				LstoreInstruction lstoreInstruction = (LstoreInstruction)theEObject;
				T result = caseLstoreInstruction(lstoreInstruction);
				if (result == null) result = caseVarInstruction(lstoreInstruction);
				if (result == null) result = caseInstruction(lstoreInstruction);
				if (result == null) result = caseIdentifiable(lstoreInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.FSTORE_INSTRUCTION: {
				FstoreInstruction fstoreInstruction = (FstoreInstruction)theEObject;
				T result = caseFstoreInstruction(fstoreInstruction);
				if (result == null) result = caseVarInstruction(fstoreInstruction);
				if (result == null) result = caseInstruction(fstoreInstruction);
				if (result == null) result = caseIdentifiable(fstoreInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.DSTORE_INSTRUCTION: {
				DstoreInstruction dstoreInstruction = (DstoreInstruction)theEObject;
				T result = caseDstoreInstruction(dstoreInstruction);
				if (result == null) result = caseVarInstruction(dstoreInstruction);
				if (result == null) result = caseInstruction(dstoreInstruction);
				if (result == null) result = caseIdentifiable(dstoreInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.ASTORE_INSTRUCTION: {
				AstoreInstruction astoreInstruction = (AstoreInstruction)theEObject;
				T result = caseAstoreInstruction(astoreInstruction);
				if (result == null) result = caseVarInstruction(astoreInstruction);
				if (result == null) result = caseInstruction(astoreInstruction);
				if (result == null) result = caseIdentifiable(astoreInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case JbcmmPackage.RET_INSTRUCTION: {
				RetInstruction retInstruction = (RetInstruction)theEObject;
				T result = caseRetInstruction(retInstruction);
				if (result == null) result = caseVarInstruction(retInstruction);
				if (result == null) result = caseInstruction(retInstruction);
				if (result == null) result = caseIdentifiable(retInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Identifiable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Identifiable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIdentifiable(Identifiable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Project</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Project</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProject(Project object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Clazz</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Clazz</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseClazz(Clazz object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Signature</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Signature</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSignature(Signature object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Class Signature</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Class Signature</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseClassSignature(ClassSignature object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Method Signature</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Method Signature</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMethodSignature(MethodSignature object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Field Type Signature</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Field Type Signature</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFieldTypeSignature(FieldTypeSignature object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Type Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Type Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTypeReference(TypeReference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Method Descriptor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Method Descriptor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMethodDescriptor(MethodDescriptor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Method Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Method Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMethodReference(MethodReference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Field Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Field Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFieldReference(FieldReference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Field</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Field</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseField(Field object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Annotation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnotation(Annotation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Element Value Pair</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Element Value Pair</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElementValuePair(ElementValuePair object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Element Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Element Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElementValue(ElementValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Elementary Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Elementary Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElementaryValue(ElementaryValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Boolean Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Boolean Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBooleanValue(BooleanValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Char Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Char Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCharValue(CharValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Byte Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Byte Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseByteValue(ByteValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Short Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Short Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseShortValue(ShortValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Int Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Int Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIntValue(IntValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Long Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Long Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLongValue(LongValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Float Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Float Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFloatValue(FloatValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Double Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Double Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDoubleValue(DoubleValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>String Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>String Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStringValue(StringValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Enum Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Enum Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEnumValue(EnumValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Method</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Method</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMethod(Method object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Local Variable Table Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Local Variable Table Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLocalVariableTableEntry(LocalVariableTableEntry object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Exception Table Entry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Exception Table Entry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExceptionTableEntry(ExceptionTableEntry object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Control Flow Edge</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Control Flow Edge</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseControlFlowEdge(ControlFlowEdge object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Unconditional Edge</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unconditional Edge</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUnconditionalEdge(UnconditionalEdge object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Conditional Edge</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Conditional Edge</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConditionalEdge(ConditionalEdge object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Switch Case Edge</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Switch Case Edge</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSwitchCaseEdge(SwitchCaseEdge object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Switch Default Edge</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Switch Default Edge</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSwitchDefaultEdge(SwitchDefaultEdge object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Exceptional Edge</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Exceptional Edge</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExceptionalEdge(ExceptionalEdge object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInstruction(Instruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Field Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Field Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFieldInstruction(FieldInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simple Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simple Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimpleInstruction(SimpleInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Int Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Int Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIntInstruction(IntInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Jump Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Jump Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseJumpInstruction(JumpInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ldc Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ldc Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLdcInstruction(LdcInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Method Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Method Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMethodInstruction(MethodInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Type Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Type Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTypeInstruction(TypeInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Var Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Var Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVarInstruction(VarInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Iinc Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Iinc Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIincInstruction(IincInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Multianewarray Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Multianewarray Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMultianewarrayInstruction(MultianewarrayInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Switch Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Switch Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSwitchInstruction(SwitchInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ldc Int Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ldc Int Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLdcIntInstruction(LdcIntInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ldc Long Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ldc Long Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLdcLongInstruction(LdcLongInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ldc Float Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ldc Float Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLdcFloatInstruction(LdcFloatInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ldc Double Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ldc Double Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLdcDoubleInstruction(LdcDoubleInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ldc String Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ldc String Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLdcStringInstruction(LdcStringInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ldc Type Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ldc Type Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLdcTypeInstruction(LdcTypeInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Getstatic Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Getstatic Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGetstaticInstruction(GetstaticInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Putstatic Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Putstatic Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePutstaticInstruction(PutstaticInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Getfield Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Getfield Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGetfieldInstruction(GetfieldInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Putfield Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Putfield Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePutfieldInstruction(PutfieldInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Nop Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Nop Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNopInstruction(NopInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aconst null Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aconst null Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAconst_nullInstruction(Aconst_nullInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Iconst m1 Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Iconst m1 Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIconst_m1Instruction(Iconst_m1Instruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Iconst 0Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Iconst 0Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIconst_0Instruction(Iconst_0Instruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Iconst 1Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Iconst 1Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIconst_1Instruction(Iconst_1Instruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Iconst 2Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Iconst 2Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIconst_2Instruction(Iconst_2Instruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Iconst 3Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Iconst 3Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIconst_3Instruction(Iconst_3Instruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Iconst 4Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Iconst 4Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIconst_4Instruction(Iconst_4Instruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Iconst 5Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Iconst 5Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIconst_5Instruction(Iconst_5Instruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Lconst 0Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Lconst 0Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLconst_0Instruction(Lconst_0Instruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Lconst 1Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Lconst 1Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLconst_1Instruction(Lconst_1Instruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Fconst 0Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Fconst 0Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFconst_0Instruction(Fconst_0Instruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Fconst 1Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Fconst 1Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFconst_1Instruction(Fconst_1Instruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Fconst 2Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Fconst 2Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFconst_2Instruction(Fconst_2Instruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dconst 0Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dconst 0Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDconst_0Instruction(Dconst_0Instruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dconst 1Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dconst 1Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDconst_1Instruction(Dconst_1Instruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Iaload Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Iaload Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIaloadInstruction(IaloadInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Laload Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Laload Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLaloadInstruction(LaloadInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Faload Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Faload Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFaloadInstruction(FaloadInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Daload Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Daload Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDaloadInstruction(DaloadInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aaload Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aaload Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAaloadInstruction(AaloadInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Baload Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Baload Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaloadInstruction(BaloadInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Caload Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Caload Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCaloadInstruction(CaloadInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Saload Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Saload Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSaloadInstruction(SaloadInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Iastore Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Iastore Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIastoreInstruction(IastoreInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Lastore Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Lastore Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLastoreInstruction(LastoreInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Fastore Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Fastore Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFastoreInstruction(FastoreInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dastore Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dastore Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDastoreInstruction(DastoreInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aastore Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aastore Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAastoreInstruction(AastoreInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Bastore Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Bastore Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBastoreInstruction(BastoreInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Castore Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Castore Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCastoreInstruction(CastoreInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sastore Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sastore Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSastoreInstruction(SastoreInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pop Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pop Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePopInstruction(PopInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pop2 Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pop2 Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePop2Instruction(Pop2Instruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dup Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dup Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDupInstruction(DupInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dup x1 Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dup x1 Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDup_x1Instruction(Dup_x1Instruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dup x2 Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dup x2 Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDup_x2Instruction(Dup_x2Instruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dup2 Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dup2 Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDup2Instruction(Dup2Instruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dup2 x1 Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dup2 x1 Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDup2_x1Instruction(Dup2_x1Instruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dup2 x2 Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dup2 x2 Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDup2_x2Instruction(Dup2_x2Instruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Swap Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Swap Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSwapInstruction(SwapInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Iadd Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Iadd Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIaddInstruction(IaddInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ladd Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ladd Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLaddInstruction(LaddInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Fadd Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Fadd Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFaddInstruction(FaddInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dadd Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dadd Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDaddInstruction(DaddInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Isub Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Isub Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsubInstruction(IsubInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Lsub Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Lsub Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLsubInstruction(LsubInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Fsub Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Fsub Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFsubInstruction(FsubInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dsub Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dsub Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDsubInstruction(DsubInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Imul Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Imul Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseImulInstruction(ImulInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Lmul Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Lmul Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLmulInstruction(LmulInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Fmul Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Fmul Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFmulInstruction(FmulInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dmul Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dmul Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDmulInstruction(DmulInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Idiv Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Idiv Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIdivInstruction(IdivInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ldiv Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ldiv Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLdivInstruction(LdivInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Fdiv Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Fdiv Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFdivInstruction(FdivInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ddiv Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ddiv Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDdivInstruction(DdivInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Irem Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Irem Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIremInstruction(IremInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Lrem Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Lrem Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLremInstruction(LremInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Frem Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Frem Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFremInstruction(FremInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Drem Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Drem Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDremInstruction(DremInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ineg Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ineg Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInegInstruction(InegInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Lneg Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Lneg Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLnegInstruction(LnegInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Fneg Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Fneg Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFnegInstruction(FnegInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dneg Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dneg Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDnegInstruction(DnegInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ishl Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ishl Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIshlInstruction(IshlInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Lshl Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Lshl Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLshlInstruction(LshlInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ishr Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ishr Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIshrInstruction(IshrInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Lshr Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Lshr Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLshrInstruction(LshrInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Iushr Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Iushr Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIushrInstruction(IushrInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Lushr Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Lushr Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLushrInstruction(LushrInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Iand Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Iand Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIandInstruction(IandInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Land Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Land Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLandInstruction(LandInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ior Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ior Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIorInstruction(IorInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Lor Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Lor Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLorInstruction(LorInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ixor Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ixor Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIxorInstruction(IxorInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Lxor Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Lxor Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLxorInstruction(LxorInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>I2l Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>I2l Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseI2lInstruction(I2lInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>I2f Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>I2f Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseI2fInstruction(I2fInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>I2d Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>I2d Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseI2dInstruction(I2dInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>L2i Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>L2i Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseL2iInstruction(L2iInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>L2f Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>L2f Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseL2fInstruction(L2fInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>L2d Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>L2d Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseL2dInstruction(L2dInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>F2i Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>F2i Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseF2iInstruction(F2iInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>F2l Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>F2l Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseF2lInstruction(F2lInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>F2d Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>F2d Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseF2dInstruction(F2dInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>D2i Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>D2i Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseD2iInstruction(D2iInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>D2l Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>D2l Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseD2lInstruction(D2lInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>D2f Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>D2f Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseD2fInstruction(D2fInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>I2b Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>I2b Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseI2bInstruction(I2bInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>I2c Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>I2c Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseI2cInstruction(I2cInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>I2s Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>I2s Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseI2sInstruction(I2sInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Lcmp Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Lcmp Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLcmpInstruction(LcmpInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Fcmpl Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Fcmpl Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFcmplInstruction(FcmplInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Fcmpg Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Fcmpg Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFcmpgInstruction(FcmpgInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dcmpl Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dcmpl Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDcmplInstruction(DcmplInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dcmpg Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dcmpg Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDcmpgInstruction(DcmpgInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ireturn Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ireturn Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIreturnInstruction(IreturnInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Lreturn Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Lreturn Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLreturnInstruction(LreturnInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Freturn Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Freturn Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFreturnInstruction(FreturnInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dreturn Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dreturn Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDreturnInstruction(DreturnInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Areturn Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Areturn Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAreturnInstruction(AreturnInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Return Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Return Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReturnInstruction(ReturnInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Arraylength Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Arraylength Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseArraylengthInstruction(ArraylengthInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Athrow Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Athrow Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAthrowInstruction(AthrowInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Monitorenter Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Monitorenter Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMonitorenterInstruction(MonitorenterInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Monitorexit Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Monitorexit Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMonitorexitInstruction(MonitorexitInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Bipush Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Bipush Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBipushInstruction(BipushInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sipush Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sipush Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSipushInstruction(SipushInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Newarray Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Newarray Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNewarrayInstruction(NewarrayInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ifeq Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ifeq Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIfeqInstruction(IfeqInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ifne Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ifne Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIfneInstruction(IfneInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Iflt Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Iflt Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIfltInstruction(IfltInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ifge Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ifge Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIfgeInstruction(IfgeInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ifgt Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ifgt Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIfgtInstruction(IfgtInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ifle Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ifle Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIfleInstruction(IfleInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>If icmpeq Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>If icmpeq Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIf_icmpeqInstruction(If_icmpeqInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>If icmpne Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>If icmpne Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIf_icmpneInstruction(If_icmpneInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>If icmplt Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>If icmplt Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIf_icmpltInstruction(If_icmpltInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>If icmpge Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>If icmpge Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIf_icmpgeInstruction(If_icmpgeInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>If icmpgt Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>If icmpgt Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIf_icmpgtInstruction(If_icmpgtInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>If icmple Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>If icmple Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIf_icmpleInstruction(If_icmpleInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>If acmpeq Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>If acmpeq Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIf_acmpeqInstruction(If_acmpeqInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>If acmpne Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>If acmpne Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIf_acmpneInstruction(If_acmpneInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Goto Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Goto Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGotoInstruction(GotoInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Jsr Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Jsr Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseJsrInstruction(JsrInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ifnull Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ifnull Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIfnullInstruction(IfnullInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ifnonnull Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ifnonnull Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIfnonnullInstruction(IfnonnullInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Invokevirtual Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Invokevirtual Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInvokevirtualInstruction(InvokevirtualInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Invokespecial Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Invokespecial Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInvokespecialInstruction(InvokespecialInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Invokestatic Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Invokestatic Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInvokestaticInstruction(InvokestaticInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Invokeinterface Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Invokeinterface Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInvokeinterfaceInstruction(InvokeinterfaceInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>New Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>New Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNewInstruction(NewInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Anewarray Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Anewarray Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnewarrayInstruction(AnewarrayInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Checkcast Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Checkcast Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCheckcastInstruction(CheckcastInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Instanceof Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Instanceof Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInstanceofInstruction(InstanceofInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Iload Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Iload Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIloadInstruction(IloadInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Lload Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Lload Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLloadInstruction(LloadInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Fload Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Fload Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFloadInstruction(FloadInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dload Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dload Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDloadInstruction(DloadInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aload Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aload Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAloadInstruction(AloadInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Istore Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Istore Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIstoreInstruction(IstoreInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Lstore Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Lstore Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLstoreInstruction(LstoreInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Fstore Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Fstore Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFstoreInstruction(FstoreInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dstore Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dstore Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDstoreInstruction(DstoreInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Astore Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Astore Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAstoreInstruction(AstoreInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ret Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ret Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRetInstruction(RetInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //JbcmmSwitch
