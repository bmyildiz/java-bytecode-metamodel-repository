/**
 */
package nl.utwente.jbcmm.util;

import nl.utwente.jbcmm.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see nl.utwente.jbcmm.JbcmmPackage
 * @generated
 */
public class JbcmmAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static JbcmmPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JbcmmAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = JbcmmPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JbcmmSwitch<Adapter> modelSwitch =
		new JbcmmSwitch<Adapter>() {
			@Override
			public Adapter caseIdentifiable(Identifiable object) {
				return createIdentifiableAdapter();
			}
			@Override
			public Adapter caseProject(Project object) {
				return createProjectAdapter();
			}
			@Override
			public Adapter caseClazz(Clazz object) {
				return createClazzAdapter();
			}
			@Override
			public Adapter caseSignature(Signature object) {
				return createSignatureAdapter();
			}
			@Override
			public Adapter caseClassSignature(ClassSignature object) {
				return createClassSignatureAdapter();
			}
			@Override
			public Adapter caseMethodSignature(MethodSignature object) {
				return createMethodSignatureAdapter();
			}
			@Override
			public Adapter caseFieldTypeSignature(FieldTypeSignature object) {
				return createFieldTypeSignatureAdapter();
			}
			@Override
			public Adapter caseTypeReference(TypeReference object) {
				return createTypeReferenceAdapter();
			}
			@Override
			public Adapter caseMethodDescriptor(MethodDescriptor object) {
				return createMethodDescriptorAdapter();
			}
			@Override
			public Adapter caseMethodReference(MethodReference object) {
				return createMethodReferenceAdapter();
			}
			@Override
			public Adapter caseFieldReference(FieldReference object) {
				return createFieldReferenceAdapter();
			}
			@Override
			public Adapter caseField(Field object) {
				return createFieldAdapter();
			}
			@Override
			public Adapter caseAnnotation(Annotation object) {
				return createAnnotationAdapter();
			}
			@Override
			public Adapter caseElementValuePair(ElementValuePair object) {
				return createElementValuePairAdapter();
			}
			@Override
			public Adapter caseElementValue(ElementValue object) {
				return createElementValueAdapter();
			}
			@Override
			public Adapter caseElementaryValue(ElementaryValue object) {
				return createElementaryValueAdapter();
			}
			@Override
			public Adapter caseBooleanValue(BooleanValue object) {
				return createBooleanValueAdapter();
			}
			@Override
			public Adapter caseCharValue(CharValue object) {
				return createCharValueAdapter();
			}
			@Override
			public Adapter caseByteValue(ByteValue object) {
				return createByteValueAdapter();
			}
			@Override
			public Adapter caseShortValue(ShortValue object) {
				return createShortValueAdapter();
			}
			@Override
			public Adapter caseIntValue(IntValue object) {
				return createIntValueAdapter();
			}
			@Override
			public Adapter caseLongValue(LongValue object) {
				return createLongValueAdapter();
			}
			@Override
			public Adapter caseFloatValue(FloatValue object) {
				return createFloatValueAdapter();
			}
			@Override
			public Adapter caseDoubleValue(DoubleValue object) {
				return createDoubleValueAdapter();
			}
			@Override
			public Adapter caseStringValue(StringValue object) {
				return createStringValueAdapter();
			}
			@Override
			public Adapter caseEnumValue(EnumValue object) {
				return createEnumValueAdapter();
			}
			@Override
			public Adapter caseMethod(Method object) {
				return createMethodAdapter();
			}
			@Override
			public Adapter caseLocalVariableTableEntry(LocalVariableTableEntry object) {
				return createLocalVariableTableEntryAdapter();
			}
			@Override
			public Adapter caseExceptionTableEntry(ExceptionTableEntry object) {
				return createExceptionTableEntryAdapter();
			}
			@Override
			public Adapter caseControlFlowEdge(ControlFlowEdge object) {
				return createControlFlowEdgeAdapter();
			}
			@Override
			public Adapter caseUnconditionalEdge(UnconditionalEdge object) {
				return createUnconditionalEdgeAdapter();
			}
			@Override
			public Adapter caseConditionalEdge(ConditionalEdge object) {
				return createConditionalEdgeAdapter();
			}
			@Override
			public Adapter caseSwitchCaseEdge(SwitchCaseEdge object) {
				return createSwitchCaseEdgeAdapter();
			}
			@Override
			public Adapter caseSwitchDefaultEdge(SwitchDefaultEdge object) {
				return createSwitchDefaultEdgeAdapter();
			}
			@Override
			public Adapter caseExceptionalEdge(ExceptionalEdge object) {
				return createExceptionalEdgeAdapter();
			}
			@Override
			public Adapter caseInstruction(Instruction object) {
				return createInstructionAdapter();
			}
			@Override
			public Adapter caseFieldInstruction(FieldInstruction object) {
				return createFieldInstructionAdapter();
			}
			@Override
			public Adapter caseSimpleInstruction(SimpleInstruction object) {
				return createSimpleInstructionAdapter();
			}
			@Override
			public Adapter caseIntInstruction(IntInstruction object) {
				return createIntInstructionAdapter();
			}
			@Override
			public Adapter caseJumpInstruction(JumpInstruction object) {
				return createJumpInstructionAdapter();
			}
			@Override
			public Adapter caseLdcInstruction(LdcInstruction object) {
				return createLdcInstructionAdapter();
			}
			@Override
			public Adapter caseMethodInstruction(MethodInstruction object) {
				return createMethodInstructionAdapter();
			}
			@Override
			public Adapter caseTypeInstruction(TypeInstruction object) {
				return createTypeInstructionAdapter();
			}
			@Override
			public Adapter caseVarInstruction(VarInstruction object) {
				return createVarInstructionAdapter();
			}
			@Override
			public Adapter caseIincInstruction(IincInstruction object) {
				return createIincInstructionAdapter();
			}
			@Override
			public Adapter caseMultianewarrayInstruction(MultianewarrayInstruction object) {
				return createMultianewarrayInstructionAdapter();
			}
			@Override
			public Adapter caseSwitchInstruction(SwitchInstruction object) {
				return createSwitchInstructionAdapter();
			}
			@Override
			public Adapter caseLdcIntInstruction(LdcIntInstruction object) {
				return createLdcIntInstructionAdapter();
			}
			@Override
			public Adapter caseLdcLongInstruction(LdcLongInstruction object) {
				return createLdcLongInstructionAdapter();
			}
			@Override
			public Adapter caseLdcFloatInstruction(LdcFloatInstruction object) {
				return createLdcFloatInstructionAdapter();
			}
			@Override
			public Adapter caseLdcDoubleInstruction(LdcDoubleInstruction object) {
				return createLdcDoubleInstructionAdapter();
			}
			@Override
			public Adapter caseLdcStringInstruction(LdcStringInstruction object) {
				return createLdcStringInstructionAdapter();
			}
			@Override
			public Adapter caseLdcTypeInstruction(LdcTypeInstruction object) {
				return createLdcTypeInstructionAdapter();
			}
			@Override
			public Adapter caseGetstaticInstruction(GetstaticInstruction object) {
				return createGetstaticInstructionAdapter();
			}
			@Override
			public Adapter casePutstaticInstruction(PutstaticInstruction object) {
				return createPutstaticInstructionAdapter();
			}
			@Override
			public Adapter caseGetfieldInstruction(GetfieldInstruction object) {
				return createGetfieldInstructionAdapter();
			}
			@Override
			public Adapter casePutfieldInstruction(PutfieldInstruction object) {
				return createPutfieldInstructionAdapter();
			}
			@Override
			public Adapter caseNopInstruction(NopInstruction object) {
				return createNopInstructionAdapter();
			}
			@Override
			public Adapter caseAconst_nullInstruction(Aconst_nullInstruction object) {
				return createAconst_nullInstructionAdapter();
			}
			@Override
			public Adapter caseIconst_m1Instruction(Iconst_m1Instruction object) {
				return createIconst_m1InstructionAdapter();
			}
			@Override
			public Adapter caseIconst_0Instruction(Iconst_0Instruction object) {
				return createIconst_0InstructionAdapter();
			}
			@Override
			public Adapter caseIconst_1Instruction(Iconst_1Instruction object) {
				return createIconst_1InstructionAdapter();
			}
			@Override
			public Adapter caseIconst_2Instruction(Iconst_2Instruction object) {
				return createIconst_2InstructionAdapter();
			}
			@Override
			public Adapter caseIconst_3Instruction(Iconst_3Instruction object) {
				return createIconst_3InstructionAdapter();
			}
			@Override
			public Adapter caseIconst_4Instruction(Iconst_4Instruction object) {
				return createIconst_4InstructionAdapter();
			}
			@Override
			public Adapter caseIconst_5Instruction(Iconst_5Instruction object) {
				return createIconst_5InstructionAdapter();
			}
			@Override
			public Adapter caseLconst_0Instruction(Lconst_0Instruction object) {
				return createLconst_0InstructionAdapter();
			}
			@Override
			public Adapter caseLconst_1Instruction(Lconst_1Instruction object) {
				return createLconst_1InstructionAdapter();
			}
			@Override
			public Adapter caseFconst_0Instruction(Fconst_0Instruction object) {
				return createFconst_0InstructionAdapter();
			}
			@Override
			public Adapter caseFconst_1Instruction(Fconst_1Instruction object) {
				return createFconst_1InstructionAdapter();
			}
			@Override
			public Adapter caseFconst_2Instruction(Fconst_2Instruction object) {
				return createFconst_2InstructionAdapter();
			}
			@Override
			public Adapter caseDconst_0Instruction(Dconst_0Instruction object) {
				return createDconst_0InstructionAdapter();
			}
			@Override
			public Adapter caseDconst_1Instruction(Dconst_1Instruction object) {
				return createDconst_1InstructionAdapter();
			}
			@Override
			public Adapter caseIaloadInstruction(IaloadInstruction object) {
				return createIaloadInstructionAdapter();
			}
			@Override
			public Adapter caseLaloadInstruction(LaloadInstruction object) {
				return createLaloadInstructionAdapter();
			}
			@Override
			public Adapter caseFaloadInstruction(FaloadInstruction object) {
				return createFaloadInstructionAdapter();
			}
			@Override
			public Adapter caseDaloadInstruction(DaloadInstruction object) {
				return createDaloadInstructionAdapter();
			}
			@Override
			public Adapter caseAaloadInstruction(AaloadInstruction object) {
				return createAaloadInstructionAdapter();
			}
			@Override
			public Adapter caseBaloadInstruction(BaloadInstruction object) {
				return createBaloadInstructionAdapter();
			}
			@Override
			public Adapter caseCaloadInstruction(CaloadInstruction object) {
				return createCaloadInstructionAdapter();
			}
			@Override
			public Adapter caseSaloadInstruction(SaloadInstruction object) {
				return createSaloadInstructionAdapter();
			}
			@Override
			public Adapter caseIastoreInstruction(IastoreInstruction object) {
				return createIastoreInstructionAdapter();
			}
			@Override
			public Adapter caseLastoreInstruction(LastoreInstruction object) {
				return createLastoreInstructionAdapter();
			}
			@Override
			public Adapter caseFastoreInstruction(FastoreInstruction object) {
				return createFastoreInstructionAdapter();
			}
			@Override
			public Adapter caseDastoreInstruction(DastoreInstruction object) {
				return createDastoreInstructionAdapter();
			}
			@Override
			public Adapter caseAastoreInstruction(AastoreInstruction object) {
				return createAastoreInstructionAdapter();
			}
			@Override
			public Adapter caseBastoreInstruction(BastoreInstruction object) {
				return createBastoreInstructionAdapter();
			}
			@Override
			public Adapter caseCastoreInstruction(CastoreInstruction object) {
				return createCastoreInstructionAdapter();
			}
			@Override
			public Adapter caseSastoreInstruction(SastoreInstruction object) {
				return createSastoreInstructionAdapter();
			}
			@Override
			public Adapter casePopInstruction(PopInstruction object) {
				return createPopInstructionAdapter();
			}
			@Override
			public Adapter casePop2Instruction(Pop2Instruction object) {
				return createPop2InstructionAdapter();
			}
			@Override
			public Adapter caseDupInstruction(DupInstruction object) {
				return createDupInstructionAdapter();
			}
			@Override
			public Adapter caseDup_x1Instruction(Dup_x1Instruction object) {
				return createDup_x1InstructionAdapter();
			}
			@Override
			public Adapter caseDup_x2Instruction(Dup_x2Instruction object) {
				return createDup_x2InstructionAdapter();
			}
			@Override
			public Adapter caseDup2Instruction(Dup2Instruction object) {
				return createDup2InstructionAdapter();
			}
			@Override
			public Adapter caseDup2_x1Instruction(Dup2_x1Instruction object) {
				return createDup2_x1InstructionAdapter();
			}
			@Override
			public Adapter caseDup2_x2Instruction(Dup2_x2Instruction object) {
				return createDup2_x2InstructionAdapter();
			}
			@Override
			public Adapter caseSwapInstruction(SwapInstruction object) {
				return createSwapInstructionAdapter();
			}
			@Override
			public Adapter caseIaddInstruction(IaddInstruction object) {
				return createIaddInstructionAdapter();
			}
			@Override
			public Adapter caseLaddInstruction(LaddInstruction object) {
				return createLaddInstructionAdapter();
			}
			@Override
			public Adapter caseFaddInstruction(FaddInstruction object) {
				return createFaddInstructionAdapter();
			}
			@Override
			public Adapter caseDaddInstruction(DaddInstruction object) {
				return createDaddInstructionAdapter();
			}
			@Override
			public Adapter caseIsubInstruction(IsubInstruction object) {
				return createIsubInstructionAdapter();
			}
			@Override
			public Adapter caseLsubInstruction(LsubInstruction object) {
				return createLsubInstructionAdapter();
			}
			@Override
			public Adapter caseFsubInstruction(FsubInstruction object) {
				return createFsubInstructionAdapter();
			}
			@Override
			public Adapter caseDsubInstruction(DsubInstruction object) {
				return createDsubInstructionAdapter();
			}
			@Override
			public Adapter caseImulInstruction(ImulInstruction object) {
				return createImulInstructionAdapter();
			}
			@Override
			public Adapter caseLmulInstruction(LmulInstruction object) {
				return createLmulInstructionAdapter();
			}
			@Override
			public Adapter caseFmulInstruction(FmulInstruction object) {
				return createFmulInstructionAdapter();
			}
			@Override
			public Adapter caseDmulInstruction(DmulInstruction object) {
				return createDmulInstructionAdapter();
			}
			@Override
			public Adapter caseIdivInstruction(IdivInstruction object) {
				return createIdivInstructionAdapter();
			}
			@Override
			public Adapter caseLdivInstruction(LdivInstruction object) {
				return createLdivInstructionAdapter();
			}
			@Override
			public Adapter caseFdivInstruction(FdivInstruction object) {
				return createFdivInstructionAdapter();
			}
			@Override
			public Adapter caseDdivInstruction(DdivInstruction object) {
				return createDdivInstructionAdapter();
			}
			@Override
			public Adapter caseIremInstruction(IremInstruction object) {
				return createIremInstructionAdapter();
			}
			@Override
			public Adapter caseLremInstruction(LremInstruction object) {
				return createLremInstructionAdapter();
			}
			@Override
			public Adapter caseFremInstruction(FremInstruction object) {
				return createFremInstructionAdapter();
			}
			@Override
			public Adapter caseDremInstruction(DremInstruction object) {
				return createDremInstructionAdapter();
			}
			@Override
			public Adapter caseInegInstruction(InegInstruction object) {
				return createInegInstructionAdapter();
			}
			@Override
			public Adapter caseLnegInstruction(LnegInstruction object) {
				return createLnegInstructionAdapter();
			}
			@Override
			public Adapter caseFnegInstruction(FnegInstruction object) {
				return createFnegInstructionAdapter();
			}
			@Override
			public Adapter caseDnegInstruction(DnegInstruction object) {
				return createDnegInstructionAdapter();
			}
			@Override
			public Adapter caseIshlInstruction(IshlInstruction object) {
				return createIshlInstructionAdapter();
			}
			@Override
			public Adapter caseLshlInstruction(LshlInstruction object) {
				return createLshlInstructionAdapter();
			}
			@Override
			public Adapter caseIshrInstruction(IshrInstruction object) {
				return createIshrInstructionAdapter();
			}
			@Override
			public Adapter caseLshrInstruction(LshrInstruction object) {
				return createLshrInstructionAdapter();
			}
			@Override
			public Adapter caseIushrInstruction(IushrInstruction object) {
				return createIushrInstructionAdapter();
			}
			@Override
			public Adapter caseLushrInstruction(LushrInstruction object) {
				return createLushrInstructionAdapter();
			}
			@Override
			public Adapter caseIandInstruction(IandInstruction object) {
				return createIandInstructionAdapter();
			}
			@Override
			public Adapter caseLandInstruction(LandInstruction object) {
				return createLandInstructionAdapter();
			}
			@Override
			public Adapter caseIorInstruction(IorInstruction object) {
				return createIorInstructionAdapter();
			}
			@Override
			public Adapter caseLorInstruction(LorInstruction object) {
				return createLorInstructionAdapter();
			}
			@Override
			public Adapter caseIxorInstruction(IxorInstruction object) {
				return createIxorInstructionAdapter();
			}
			@Override
			public Adapter caseLxorInstruction(LxorInstruction object) {
				return createLxorInstructionAdapter();
			}
			@Override
			public Adapter caseI2lInstruction(I2lInstruction object) {
				return createI2lInstructionAdapter();
			}
			@Override
			public Adapter caseI2fInstruction(I2fInstruction object) {
				return createI2fInstructionAdapter();
			}
			@Override
			public Adapter caseI2dInstruction(I2dInstruction object) {
				return createI2dInstructionAdapter();
			}
			@Override
			public Adapter caseL2iInstruction(L2iInstruction object) {
				return createL2iInstructionAdapter();
			}
			@Override
			public Adapter caseL2fInstruction(L2fInstruction object) {
				return createL2fInstructionAdapter();
			}
			@Override
			public Adapter caseL2dInstruction(L2dInstruction object) {
				return createL2dInstructionAdapter();
			}
			@Override
			public Adapter caseF2iInstruction(F2iInstruction object) {
				return createF2iInstructionAdapter();
			}
			@Override
			public Adapter caseF2lInstruction(F2lInstruction object) {
				return createF2lInstructionAdapter();
			}
			@Override
			public Adapter caseF2dInstruction(F2dInstruction object) {
				return createF2dInstructionAdapter();
			}
			@Override
			public Adapter caseD2iInstruction(D2iInstruction object) {
				return createD2iInstructionAdapter();
			}
			@Override
			public Adapter caseD2lInstruction(D2lInstruction object) {
				return createD2lInstructionAdapter();
			}
			@Override
			public Adapter caseD2fInstruction(D2fInstruction object) {
				return createD2fInstructionAdapter();
			}
			@Override
			public Adapter caseI2bInstruction(I2bInstruction object) {
				return createI2bInstructionAdapter();
			}
			@Override
			public Adapter caseI2cInstruction(I2cInstruction object) {
				return createI2cInstructionAdapter();
			}
			@Override
			public Adapter caseI2sInstruction(I2sInstruction object) {
				return createI2sInstructionAdapter();
			}
			@Override
			public Adapter caseLcmpInstruction(LcmpInstruction object) {
				return createLcmpInstructionAdapter();
			}
			@Override
			public Adapter caseFcmplInstruction(FcmplInstruction object) {
				return createFcmplInstructionAdapter();
			}
			@Override
			public Adapter caseFcmpgInstruction(FcmpgInstruction object) {
				return createFcmpgInstructionAdapter();
			}
			@Override
			public Adapter caseDcmplInstruction(DcmplInstruction object) {
				return createDcmplInstructionAdapter();
			}
			@Override
			public Adapter caseDcmpgInstruction(DcmpgInstruction object) {
				return createDcmpgInstructionAdapter();
			}
			@Override
			public Adapter caseIreturnInstruction(IreturnInstruction object) {
				return createIreturnInstructionAdapter();
			}
			@Override
			public Adapter caseLreturnInstruction(LreturnInstruction object) {
				return createLreturnInstructionAdapter();
			}
			@Override
			public Adapter caseFreturnInstruction(FreturnInstruction object) {
				return createFreturnInstructionAdapter();
			}
			@Override
			public Adapter caseDreturnInstruction(DreturnInstruction object) {
				return createDreturnInstructionAdapter();
			}
			@Override
			public Adapter caseAreturnInstruction(AreturnInstruction object) {
				return createAreturnInstructionAdapter();
			}
			@Override
			public Adapter caseReturnInstruction(ReturnInstruction object) {
				return createReturnInstructionAdapter();
			}
			@Override
			public Adapter caseArraylengthInstruction(ArraylengthInstruction object) {
				return createArraylengthInstructionAdapter();
			}
			@Override
			public Adapter caseAthrowInstruction(AthrowInstruction object) {
				return createAthrowInstructionAdapter();
			}
			@Override
			public Adapter caseMonitorenterInstruction(MonitorenterInstruction object) {
				return createMonitorenterInstructionAdapter();
			}
			@Override
			public Adapter caseMonitorexitInstruction(MonitorexitInstruction object) {
				return createMonitorexitInstructionAdapter();
			}
			@Override
			public Adapter caseBipushInstruction(BipushInstruction object) {
				return createBipushInstructionAdapter();
			}
			@Override
			public Adapter caseSipushInstruction(SipushInstruction object) {
				return createSipushInstructionAdapter();
			}
			@Override
			public Adapter caseNewarrayInstruction(NewarrayInstruction object) {
				return createNewarrayInstructionAdapter();
			}
			@Override
			public Adapter caseIfeqInstruction(IfeqInstruction object) {
				return createIfeqInstructionAdapter();
			}
			@Override
			public Adapter caseIfneInstruction(IfneInstruction object) {
				return createIfneInstructionAdapter();
			}
			@Override
			public Adapter caseIfltInstruction(IfltInstruction object) {
				return createIfltInstructionAdapter();
			}
			@Override
			public Adapter caseIfgeInstruction(IfgeInstruction object) {
				return createIfgeInstructionAdapter();
			}
			@Override
			public Adapter caseIfgtInstruction(IfgtInstruction object) {
				return createIfgtInstructionAdapter();
			}
			@Override
			public Adapter caseIfleInstruction(IfleInstruction object) {
				return createIfleInstructionAdapter();
			}
			@Override
			public Adapter caseIf_icmpeqInstruction(If_icmpeqInstruction object) {
				return createIf_icmpeqInstructionAdapter();
			}
			@Override
			public Adapter caseIf_icmpneInstruction(If_icmpneInstruction object) {
				return createIf_icmpneInstructionAdapter();
			}
			@Override
			public Adapter caseIf_icmpltInstruction(If_icmpltInstruction object) {
				return createIf_icmpltInstructionAdapter();
			}
			@Override
			public Adapter caseIf_icmpgeInstruction(If_icmpgeInstruction object) {
				return createIf_icmpgeInstructionAdapter();
			}
			@Override
			public Adapter caseIf_icmpgtInstruction(If_icmpgtInstruction object) {
				return createIf_icmpgtInstructionAdapter();
			}
			@Override
			public Adapter caseIf_icmpleInstruction(If_icmpleInstruction object) {
				return createIf_icmpleInstructionAdapter();
			}
			@Override
			public Adapter caseIf_acmpeqInstruction(If_acmpeqInstruction object) {
				return createIf_acmpeqInstructionAdapter();
			}
			@Override
			public Adapter caseIf_acmpneInstruction(If_acmpneInstruction object) {
				return createIf_acmpneInstructionAdapter();
			}
			@Override
			public Adapter caseGotoInstruction(GotoInstruction object) {
				return createGotoInstructionAdapter();
			}
			@Override
			public Adapter caseJsrInstruction(JsrInstruction object) {
				return createJsrInstructionAdapter();
			}
			@Override
			public Adapter caseIfnullInstruction(IfnullInstruction object) {
				return createIfnullInstructionAdapter();
			}
			@Override
			public Adapter caseIfnonnullInstruction(IfnonnullInstruction object) {
				return createIfnonnullInstructionAdapter();
			}
			@Override
			public Adapter caseInvokevirtualInstruction(InvokevirtualInstruction object) {
				return createInvokevirtualInstructionAdapter();
			}
			@Override
			public Adapter caseInvokespecialInstruction(InvokespecialInstruction object) {
				return createInvokespecialInstructionAdapter();
			}
			@Override
			public Adapter caseInvokestaticInstruction(InvokestaticInstruction object) {
				return createInvokestaticInstructionAdapter();
			}
			@Override
			public Adapter caseInvokeinterfaceInstruction(InvokeinterfaceInstruction object) {
				return createInvokeinterfaceInstructionAdapter();
			}
			@Override
			public Adapter caseNewInstruction(NewInstruction object) {
				return createNewInstructionAdapter();
			}
			@Override
			public Adapter caseAnewarrayInstruction(AnewarrayInstruction object) {
				return createAnewarrayInstructionAdapter();
			}
			@Override
			public Adapter caseCheckcastInstruction(CheckcastInstruction object) {
				return createCheckcastInstructionAdapter();
			}
			@Override
			public Adapter caseInstanceofInstruction(InstanceofInstruction object) {
				return createInstanceofInstructionAdapter();
			}
			@Override
			public Adapter caseIloadInstruction(IloadInstruction object) {
				return createIloadInstructionAdapter();
			}
			@Override
			public Adapter caseLloadInstruction(LloadInstruction object) {
				return createLloadInstructionAdapter();
			}
			@Override
			public Adapter caseFloadInstruction(FloadInstruction object) {
				return createFloadInstructionAdapter();
			}
			@Override
			public Adapter caseDloadInstruction(DloadInstruction object) {
				return createDloadInstructionAdapter();
			}
			@Override
			public Adapter caseAloadInstruction(AloadInstruction object) {
				return createAloadInstructionAdapter();
			}
			@Override
			public Adapter caseIstoreInstruction(IstoreInstruction object) {
				return createIstoreInstructionAdapter();
			}
			@Override
			public Adapter caseLstoreInstruction(LstoreInstruction object) {
				return createLstoreInstructionAdapter();
			}
			@Override
			public Adapter caseFstoreInstruction(FstoreInstruction object) {
				return createFstoreInstructionAdapter();
			}
			@Override
			public Adapter caseDstoreInstruction(DstoreInstruction object) {
				return createDstoreInstructionAdapter();
			}
			@Override
			public Adapter caseAstoreInstruction(AstoreInstruction object) {
				return createAstoreInstructionAdapter();
			}
			@Override
			public Adapter caseRetInstruction(RetInstruction object) {
				return createRetInstructionAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.Identifiable <em>Identifiable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.Identifiable
	 * @generated
	 */
	public Adapter createIdentifiableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.Project <em>Project</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.Project
	 * @generated
	 */
	public Adapter createProjectAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.Clazz <em>Clazz</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.Clazz
	 * @generated
	 */
	public Adapter createClazzAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.Signature <em>Signature</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.Signature
	 * @generated
	 */
	public Adapter createSignatureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.ClassSignature <em>Class Signature</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.ClassSignature
	 * @generated
	 */
	public Adapter createClassSignatureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.MethodSignature <em>Method Signature</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.MethodSignature
	 * @generated
	 */
	public Adapter createMethodSignatureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.FieldTypeSignature <em>Field Type Signature</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.FieldTypeSignature
	 * @generated
	 */
	public Adapter createFieldTypeSignatureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.TypeReference <em>Type Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.TypeReference
	 * @generated
	 */
	public Adapter createTypeReferenceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.MethodDescriptor <em>Method Descriptor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.MethodDescriptor
	 * @generated
	 */
	public Adapter createMethodDescriptorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.MethodReference <em>Method Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.MethodReference
	 * @generated
	 */
	public Adapter createMethodReferenceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.FieldReference <em>Field Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.FieldReference
	 * @generated
	 */
	public Adapter createFieldReferenceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.Field <em>Field</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.Field
	 * @generated
	 */
	public Adapter createFieldAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.Annotation <em>Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.Annotation
	 * @generated
	 */
	public Adapter createAnnotationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.ElementValuePair <em>Element Value Pair</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.ElementValuePair
	 * @generated
	 */
	public Adapter createElementValuePairAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.ElementValue <em>Element Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.ElementValue
	 * @generated
	 */
	public Adapter createElementValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.ElementaryValue <em>Elementary Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.ElementaryValue
	 * @generated
	 */
	public Adapter createElementaryValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.BooleanValue <em>Boolean Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.BooleanValue
	 * @generated
	 */
	public Adapter createBooleanValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.CharValue <em>Char Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.CharValue
	 * @generated
	 */
	public Adapter createCharValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.ByteValue <em>Byte Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.ByteValue
	 * @generated
	 */
	public Adapter createByteValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.ShortValue <em>Short Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.ShortValue
	 * @generated
	 */
	public Adapter createShortValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.IntValue <em>Int Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.IntValue
	 * @generated
	 */
	public Adapter createIntValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.LongValue <em>Long Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.LongValue
	 * @generated
	 */
	public Adapter createLongValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.FloatValue <em>Float Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.FloatValue
	 * @generated
	 */
	public Adapter createFloatValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.DoubleValue <em>Double Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.DoubleValue
	 * @generated
	 */
	public Adapter createDoubleValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.StringValue <em>String Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.StringValue
	 * @generated
	 */
	public Adapter createStringValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.EnumValue <em>Enum Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.EnumValue
	 * @generated
	 */
	public Adapter createEnumValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.Method <em>Method</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.Method
	 * @generated
	 */
	public Adapter createMethodAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.LocalVariableTableEntry <em>Local Variable Table Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.LocalVariableTableEntry
	 * @generated
	 */
	public Adapter createLocalVariableTableEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.ExceptionTableEntry <em>Exception Table Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.ExceptionTableEntry
	 * @generated
	 */
	public Adapter createExceptionTableEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.ControlFlowEdge <em>Control Flow Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.ControlFlowEdge
	 * @generated
	 */
	public Adapter createControlFlowEdgeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.UnconditionalEdge <em>Unconditional Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.UnconditionalEdge
	 * @generated
	 */
	public Adapter createUnconditionalEdgeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.ConditionalEdge <em>Conditional Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.ConditionalEdge
	 * @generated
	 */
	public Adapter createConditionalEdgeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.SwitchCaseEdge <em>Switch Case Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.SwitchCaseEdge
	 * @generated
	 */
	public Adapter createSwitchCaseEdgeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.SwitchDefaultEdge <em>Switch Default Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.SwitchDefaultEdge
	 * @generated
	 */
	public Adapter createSwitchDefaultEdgeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.ExceptionalEdge <em>Exceptional Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.ExceptionalEdge
	 * @generated
	 */
	public Adapter createExceptionalEdgeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.Instruction <em>Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.Instruction
	 * @generated
	 */
	public Adapter createInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.FieldInstruction <em>Field Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.FieldInstruction
	 * @generated
	 */
	public Adapter createFieldInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.SimpleInstruction <em>Simple Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.SimpleInstruction
	 * @generated
	 */
	public Adapter createSimpleInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.IntInstruction <em>Int Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.IntInstruction
	 * @generated
	 */
	public Adapter createIntInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.JumpInstruction <em>Jump Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.JumpInstruction
	 * @generated
	 */
	public Adapter createJumpInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.LdcInstruction <em>Ldc Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.LdcInstruction
	 * @generated
	 */
	public Adapter createLdcInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.MethodInstruction <em>Method Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.MethodInstruction
	 * @generated
	 */
	public Adapter createMethodInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.TypeInstruction <em>Type Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.TypeInstruction
	 * @generated
	 */
	public Adapter createTypeInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.VarInstruction <em>Var Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.VarInstruction
	 * @generated
	 */
	public Adapter createVarInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.IincInstruction <em>Iinc Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.IincInstruction
	 * @generated
	 */
	public Adapter createIincInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.MultianewarrayInstruction <em>Multianewarray Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.MultianewarrayInstruction
	 * @generated
	 */
	public Adapter createMultianewarrayInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.SwitchInstruction <em>Switch Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.SwitchInstruction
	 * @generated
	 */
	public Adapter createSwitchInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.LdcIntInstruction <em>Ldc Int Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.LdcIntInstruction
	 * @generated
	 */
	public Adapter createLdcIntInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.LdcLongInstruction <em>Ldc Long Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.LdcLongInstruction
	 * @generated
	 */
	public Adapter createLdcLongInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.LdcFloatInstruction <em>Ldc Float Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.LdcFloatInstruction
	 * @generated
	 */
	public Adapter createLdcFloatInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.LdcDoubleInstruction <em>Ldc Double Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.LdcDoubleInstruction
	 * @generated
	 */
	public Adapter createLdcDoubleInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.LdcStringInstruction <em>Ldc String Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.LdcStringInstruction
	 * @generated
	 */
	public Adapter createLdcStringInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.LdcTypeInstruction <em>Ldc Type Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.LdcTypeInstruction
	 * @generated
	 */
	public Adapter createLdcTypeInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.GetstaticInstruction <em>Getstatic Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.GetstaticInstruction
	 * @generated
	 */
	public Adapter createGetstaticInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.PutstaticInstruction <em>Putstatic Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.PutstaticInstruction
	 * @generated
	 */
	public Adapter createPutstaticInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.GetfieldInstruction <em>Getfield Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.GetfieldInstruction
	 * @generated
	 */
	public Adapter createGetfieldInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.PutfieldInstruction <em>Putfield Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.PutfieldInstruction
	 * @generated
	 */
	public Adapter createPutfieldInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.NopInstruction <em>Nop Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.NopInstruction
	 * @generated
	 */
	public Adapter createNopInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.Aconst_nullInstruction <em>Aconst null Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.Aconst_nullInstruction
	 * @generated
	 */
	public Adapter createAconst_nullInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.Iconst_m1Instruction <em>Iconst m1 Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.Iconst_m1Instruction
	 * @generated
	 */
	public Adapter createIconst_m1InstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.Iconst_0Instruction <em>Iconst 0Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.Iconst_0Instruction
	 * @generated
	 */
	public Adapter createIconst_0InstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.Iconst_1Instruction <em>Iconst 1Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.Iconst_1Instruction
	 * @generated
	 */
	public Adapter createIconst_1InstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.Iconst_2Instruction <em>Iconst 2Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.Iconst_2Instruction
	 * @generated
	 */
	public Adapter createIconst_2InstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.Iconst_3Instruction <em>Iconst 3Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.Iconst_3Instruction
	 * @generated
	 */
	public Adapter createIconst_3InstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.Iconst_4Instruction <em>Iconst 4Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.Iconst_4Instruction
	 * @generated
	 */
	public Adapter createIconst_4InstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.Iconst_5Instruction <em>Iconst 5Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.Iconst_5Instruction
	 * @generated
	 */
	public Adapter createIconst_5InstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.Lconst_0Instruction <em>Lconst 0Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.Lconst_0Instruction
	 * @generated
	 */
	public Adapter createLconst_0InstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.Lconst_1Instruction <em>Lconst 1Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.Lconst_1Instruction
	 * @generated
	 */
	public Adapter createLconst_1InstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.Fconst_0Instruction <em>Fconst 0Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.Fconst_0Instruction
	 * @generated
	 */
	public Adapter createFconst_0InstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.Fconst_1Instruction <em>Fconst 1Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.Fconst_1Instruction
	 * @generated
	 */
	public Adapter createFconst_1InstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.Fconst_2Instruction <em>Fconst 2Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.Fconst_2Instruction
	 * @generated
	 */
	public Adapter createFconst_2InstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.Dconst_0Instruction <em>Dconst 0Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.Dconst_0Instruction
	 * @generated
	 */
	public Adapter createDconst_0InstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.Dconst_1Instruction <em>Dconst 1Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.Dconst_1Instruction
	 * @generated
	 */
	public Adapter createDconst_1InstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.IaloadInstruction <em>Iaload Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.IaloadInstruction
	 * @generated
	 */
	public Adapter createIaloadInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.LaloadInstruction <em>Laload Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.LaloadInstruction
	 * @generated
	 */
	public Adapter createLaloadInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.FaloadInstruction <em>Faload Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.FaloadInstruction
	 * @generated
	 */
	public Adapter createFaloadInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.DaloadInstruction <em>Daload Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.DaloadInstruction
	 * @generated
	 */
	public Adapter createDaloadInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.AaloadInstruction <em>Aaload Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.AaloadInstruction
	 * @generated
	 */
	public Adapter createAaloadInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.BaloadInstruction <em>Baload Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.BaloadInstruction
	 * @generated
	 */
	public Adapter createBaloadInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.CaloadInstruction <em>Caload Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.CaloadInstruction
	 * @generated
	 */
	public Adapter createCaloadInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.SaloadInstruction <em>Saload Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.SaloadInstruction
	 * @generated
	 */
	public Adapter createSaloadInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.IastoreInstruction <em>Iastore Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.IastoreInstruction
	 * @generated
	 */
	public Adapter createIastoreInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.LastoreInstruction <em>Lastore Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.LastoreInstruction
	 * @generated
	 */
	public Adapter createLastoreInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.FastoreInstruction <em>Fastore Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.FastoreInstruction
	 * @generated
	 */
	public Adapter createFastoreInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.DastoreInstruction <em>Dastore Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.DastoreInstruction
	 * @generated
	 */
	public Adapter createDastoreInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.AastoreInstruction <em>Aastore Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.AastoreInstruction
	 * @generated
	 */
	public Adapter createAastoreInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.BastoreInstruction <em>Bastore Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.BastoreInstruction
	 * @generated
	 */
	public Adapter createBastoreInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.CastoreInstruction <em>Castore Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.CastoreInstruction
	 * @generated
	 */
	public Adapter createCastoreInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.SastoreInstruction <em>Sastore Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.SastoreInstruction
	 * @generated
	 */
	public Adapter createSastoreInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.PopInstruction <em>Pop Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.PopInstruction
	 * @generated
	 */
	public Adapter createPopInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.Pop2Instruction <em>Pop2 Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.Pop2Instruction
	 * @generated
	 */
	public Adapter createPop2InstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.DupInstruction <em>Dup Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.DupInstruction
	 * @generated
	 */
	public Adapter createDupInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.Dup_x1Instruction <em>Dup x1 Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.Dup_x1Instruction
	 * @generated
	 */
	public Adapter createDup_x1InstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.Dup_x2Instruction <em>Dup x2 Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.Dup_x2Instruction
	 * @generated
	 */
	public Adapter createDup_x2InstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.Dup2Instruction <em>Dup2 Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.Dup2Instruction
	 * @generated
	 */
	public Adapter createDup2InstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.Dup2_x1Instruction <em>Dup2 x1 Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.Dup2_x1Instruction
	 * @generated
	 */
	public Adapter createDup2_x1InstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.Dup2_x2Instruction <em>Dup2 x2 Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.Dup2_x2Instruction
	 * @generated
	 */
	public Adapter createDup2_x2InstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.SwapInstruction <em>Swap Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.SwapInstruction
	 * @generated
	 */
	public Adapter createSwapInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.IaddInstruction <em>Iadd Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.IaddInstruction
	 * @generated
	 */
	public Adapter createIaddInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.LaddInstruction <em>Ladd Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.LaddInstruction
	 * @generated
	 */
	public Adapter createLaddInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.FaddInstruction <em>Fadd Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.FaddInstruction
	 * @generated
	 */
	public Adapter createFaddInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.DaddInstruction <em>Dadd Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.DaddInstruction
	 * @generated
	 */
	public Adapter createDaddInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.IsubInstruction <em>Isub Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.IsubInstruction
	 * @generated
	 */
	public Adapter createIsubInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.LsubInstruction <em>Lsub Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.LsubInstruction
	 * @generated
	 */
	public Adapter createLsubInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.FsubInstruction <em>Fsub Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.FsubInstruction
	 * @generated
	 */
	public Adapter createFsubInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.DsubInstruction <em>Dsub Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.DsubInstruction
	 * @generated
	 */
	public Adapter createDsubInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.ImulInstruction <em>Imul Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.ImulInstruction
	 * @generated
	 */
	public Adapter createImulInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.LmulInstruction <em>Lmul Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.LmulInstruction
	 * @generated
	 */
	public Adapter createLmulInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.FmulInstruction <em>Fmul Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.FmulInstruction
	 * @generated
	 */
	public Adapter createFmulInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.DmulInstruction <em>Dmul Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.DmulInstruction
	 * @generated
	 */
	public Adapter createDmulInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.IdivInstruction <em>Idiv Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.IdivInstruction
	 * @generated
	 */
	public Adapter createIdivInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.LdivInstruction <em>Ldiv Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.LdivInstruction
	 * @generated
	 */
	public Adapter createLdivInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.FdivInstruction <em>Fdiv Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.FdivInstruction
	 * @generated
	 */
	public Adapter createFdivInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.DdivInstruction <em>Ddiv Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.DdivInstruction
	 * @generated
	 */
	public Adapter createDdivInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.IremInstruction <em>Irem Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.IremInstruction
	 * @generated
	 */
	public Adapter createIremInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.LremInstruction <em>Lrem Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.LremInstruction
	 * @generated
	 */
	public Adapter createLremInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.FremInstruction <em>Frem Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.FremInstruction
	 * @generated
	 */
	public Adapter createFremInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.DremInstruction <em>Drem Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.DremInstruction
	 * @generated
	 */
	public Adapter createDremInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.InegInstruction <em>Ineg Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.InegInstruction
	 * @generated
	 */
	public Adapter createInegInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.LnegInstruction <em>Lneg Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.LnegInstruction
	 * @generated
	 */
	public Adapter createLnegInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.FnegInstruction <em>Fneg Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.FnegInstruction
	 * @generated
	 */
	public Adapter createFnegInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.DnegInstruction <em>Dneg Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.DnegInstruction
	 * @generated
	 */
	public Adapter createDnegInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.IshlInstruction <em>Ishl Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.IshlInstruction
	 * @generated
	 */
	public Adapter createIshlInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.LshlInstruction <em>Lshl Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.LshlInstruction
	 * @generated
	 */
	public Adapter createLshlInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.IshrInstruction <em>Ishr Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.IshrInstruction
	 * @generated
	 */
	public Adapter createIshrInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.LshrInstruction <em>Lshr Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.LshrInstruction
	 * @generated
	 */
	public Adapter createLshrInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.IushrInstruction <em>Iushr Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.IushrInstruction
	 * @generated
	 */
	public Adapter createIushrInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.LushrInstruction <em>Lushr Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.LushrInstruction
	 * @generated
	 */
	public Adapter createLushrInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.IandInstruction <em>Iand Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.IandInstruction
	 * @generated
	 */
	public Adapter createIandInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.LandInstruction <em>Land Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.LandInstruction
	 * @generated
	 */
	public Adapter createLandInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.IorInstruction <em>Ior Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.IorInstruction
	 * @generated
	 */
	public Adapter createIorInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.LorInstruction <em>Lor Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.LorInstruction
	 * @generated
	 */
	public Adapter createLorInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.IxorInstruction <em>Ixor Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.IxorInstruction
	 * @generated
	 */
	public Adapter createIxorInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.LxorInstruction <em>Lxor Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.LxorInstruction
	 * @generated
	 */
	public Adapter createLxorInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.I2lInstruction <em>I2l Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.I2lInstruction
	 * @generated
	 */
	public Adapter createI2lInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.I2fInstruction <em>I2f Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.I2fInstruction
	 * @generated
	 */
	public Adapter createI2fInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.I2dInstruction <em>I2d Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.I2dInstruction
	 * @generated
	 */
	public Adapter createI2dInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.L2iInstruction <em>L2i Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.L2iInstruction
	 * @generated
	 */
	public Adapter createL2iInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.L2fInstruction <em>L2f Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.L2fInstruction
	 * @generated
	 */
	public Adapter createL2fInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.L2dInstruction <em>L2d Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.L2dInstruction
	 * @generated
	 */
	public Adapter createL2dInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.F2iInstruction <em>F2i Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.F2iInstruction
	 * @generated
	 */
	public Adapter createF2iInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.F2lInstruction <em>F2l Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.F2lInstruction
	 * @generated
	 */
	public Adapter createF2lInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.F2dInstruction <em>F2d Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.F2dInstruction
	 * @generated
	 */
	public Adapter createF2dInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.D2iInstruction <em>D2i Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.D2iInstruction
	 * @generated
	 */
	public Adapter createD2iInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.D2lInstruction <em>D2l Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.D2lInstruction
	 * @generated
	 */
	public Adapter createD2lInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.D2fInstruction <em>D2f Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.D2fInstruction
	 * @generated
	 */
	public Adapter createD2fInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.I2bInstruction <em>I2b Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.I2bInstruction
	 * @generated
	 */
	public Adapter createI2bInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.I2cInstruction <em>I2c Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.I2cInstruction
	 * @generated
	 */
	public Adapter createI2cInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.I2sInstruction <em>I2s Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.I2sInstruction
	 * @generated
	 */
	public Adapter createI2sInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.LcmpInstruction <em>Lcmp Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.LcmpInstruction
	 * @generated
	 */
	public Adapter createLcmpInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.FcmplInstruction <em>Fcmpl Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.FcmplInstruction
	 * @generated
	 */
	public Adapter createFcmplInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.FcmpgInstruction <em>Fcmpg Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.FcmpgInstruction
	 * @generated
	 */
	public Adapter createFcmpgInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.DcmplInstruction <em>Dcmpl Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.DcmplInstruction
	 * @generated
	 */
	public Adapter createDcmplInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.DcmpgInstruction <em>Dcmpg Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.DcmpgInstruction
	 * @generated
	 */
	public Adapter createDcmpgInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.IreturnInstruction <em>Ireturn Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.IreturnInstruction
	 * @generated
	 */
	public Adapter createIreturnInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.LreturnInstruction <em>Lreturn Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.LreturnInstruction
	 * @generated
	 */
	public Adapter createLreturnInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.FreturnInstruction <em>Freturn Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.FreturnInstruction
	 * @generated
	 */
	public Adapter createFreturnInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.DreturnInstruction <em>Dreturn Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.DreturnInstruction
	 * @generated
	 */
	public Adapter createDreturnInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.AreturnInstruction <em>Areturn Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.AreturnInstruction
	 * @generated
	 */
	public Adapter createAreturnInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.ReturnInstruction <em>Return Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.ReturnInstruction
	 * @generated
	 */
	public Adapter createReturnInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.ArraylengthInstruction <em>Arraylength Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.ArraylengthInstruction
	 * @generated
	 */
	public Adapter createArraylengthInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.AthrowInstruction <em>Athrow Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.AthrowInstruction
	 * @generated
	 */
	public Adapter createAthrowInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.MonitorenterInstruction <em>Monitorenter Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.MonitorenterInstruction
	 * @generated
	 */
	public Adapter createMonitorenterInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.MonitorexitInstruction <em>Monitorexit Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.MonitorexitInstruction
	 * @generated
	 */
	public Adapter createMonitorexitInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.BipushInstruction <em>Bipush Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.BipushInstruction
	 * @generated
	 */
	public Adapter createBipushInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.SipushInstruction <em>Sipush Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.SipushInstruction
	 * @generated
	 */
	public Adapter createSipushInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.NewarrayInstruction <em>Newarray Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.NewarrayInstruction
	 * @generated
	 */
	public Adapter createNewarrayInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.IfeqInstruction <em>Ifeq Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.IfeqInstruction
	 * @generated
	 */
	public Adapter createIfeqInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.IfneInstruction <em>Ifne Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.IfneInstruction
	 * @generated
	 */
	public Adapter createIfneInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.IfltInstruction <em>Iflt Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.IfltInstruction
	 * @generated
	 */
	public Adapter createIfltInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.IfgeInstruction <em>Ifge Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.IfgeInstruction
	 * @generated
	 */
	public Adapter createIfgeInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.IfgtInstruction <em>Ifgt Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.IfgtInstruction
	 * @generated
	 */
	public Adapter createIfgtInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.IfleInstruction <em>Ifle Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.IfleInstruction
	 * @generated
	 */
	public Adapter createIfleInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.If_icmpeqInstruction <em>If icmpeq Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.If_icmpeqInstruction
	 * @generated
	 */
	public Adapter createIf_icmpeqInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.If_icmpneInstruction <em>If icmpne Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.If_icmpneInstruction
	 * @generated
	 */
	public Adapter createIf_icmpneInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.If_icmpltInstruction <em>If icmplt Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.If_icmpltInstruction
	 * @generated
	 */
	public Adapter createIf_icmpltInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.If_icmpgeInstruction <em>If icmpge Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.If_icmpgeInstruction
	 * @generated
	 */
	public Adapter createIf_icmpgeInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.If_icmpgtInstruction <em>If icmpgt Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.If_icmpgtInstruction
	 * @generated
	 */
	public Adapter createIf_icmpgtInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.If_icmpleInstruction <em>If icmple Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.If_icmpleInstruction
	 * @generated
	 */
	public Adapter createIf_icmpleInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.If_acmpeqInstruction <em>If acmpeq Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.If_acmpeqInstruction
	 * @generated
	 */
	public Adapter createIf_acmpeqInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.If_acmpneInstruction <em>If acmpne Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.If_acmpneInstruction
	 * @generated
	 */
	public Adapter createIf_acmpneInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.GotoInstruction <em>Goto Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.GotoInstruction
	 * @generated
	 */
	public Adapter createGotoInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.JsrInstruction <em>Jsr Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.JsrInstruction
	 * @generated
	 */
	public Adapter createJsrInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.IfnullInstruction <em>Ifnull Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.IfnullInstruction
	 * @generated
	 */
	public Adapter createIfnullInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.IfnonnullInstruction <em>Ifnonnull Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.IfnonnullInstruction
	 * @generated
	 */
	public Adapter createIfnonnullInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.InvokevirtualInstruction <em>Invokevirtual Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.InvokevirtualInstruction
	 * @generated
	 */
	public Adapter createInvokevirtualInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.InvokespecialInstruction <em>Invokespecial Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.InvokespecialInstruction
	 * @generated
	 */
	public Adapter createInvokespecialInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.InvokestaticInstruction <em>Invokestatic Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.InvokestaticInstruction
	 * @generated
	 */
	public Adapter createInvokestaticInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.InvokeinterfaceInstruction <em>Invokeinterface Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.InvokeinterfaceInstruction
	 * @generated
	 */
	public Adapter createInvokeinterfaceInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.NewInstruction <em>New Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.NewInstruction
	 * @generated
	 */
	public Adapter createNewInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.AnewarrayInstruction <em>Anewarray Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.AnewarrayInstruction
	 * @generated
	 */
	public Adapter createAnewarrayInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.CheckcastInstruction <em>Checkcast Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.CheckcastInstruction
	 * @generated
	 */
	public Adapter createCheckcastInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.InstanceofInstruction <em>Instanceof Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.InstanceofInstruction
	 * @generated
	 */
	public Adapter createInstanceofInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.IloadInstruction <em>Iload Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.IloadInstruction
	 * @generated
	 */
	public Adapter createIloadInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.LloadInstruction <em>Lload Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.LloadInstruction
	 * @generated
	 */
	public Adapter createLloadInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.FloadInstruction <em>Fload Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.FloadInstruction
	 * @generated
	 */
	public Adapter createFloadInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.DloadInstruction <em>Dload Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.DloadInstruction
	 * @generated
	 */
	public Adapter createDloadInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.AloadInstruction <em>Aload Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.AloadInstruction
	 * @generated
	 */
	public Adapter createAloadInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.IstoreInstruction <em>Istore Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.IstoreInstruction
	 * @generated
	 */
	public Adapter createIstoreInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.LstoreInstruction <em>Lstore Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.LstoreInstruction
	 * @generated
	 */
	public Adapter createLstoreInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.FstoreInstruction <em>Fstore Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.FstoreInstruction
	 * @generated
	 */
	public Adapter createFstoreInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.DstoreInstruction <em>Dstore Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.DstoreInstruction
	 * @generated
	 */
	public Adapter createDstoreInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.AstoreInstruction <em>Astore Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.AstoreInstruction
	 * @generated
	 */
	public Adapter createAstoreInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link nl.utwente.jbcmm.RetInstruction <em>Ret Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see nl.utwente.jbcmm.RetInstruction
	 * @generated
	 */
	public Adapter createRetInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //JbcmmAdapterFactory
