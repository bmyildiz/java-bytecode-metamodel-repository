/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Iconst m1 Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIconst_m1Instruction()
 * @model
 * @generated
 */
public interface Iconst_m1Instruction extends SimpleInstruction {
} // Iconst_m1Instruction
