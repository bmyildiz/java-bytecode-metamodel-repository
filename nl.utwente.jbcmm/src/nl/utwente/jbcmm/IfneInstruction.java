/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ifne Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIfneInstruction()
 * @model
 * @generated
 */
public interface IfneInstruction extends JumpInstruction {
} // IfneInstruction
