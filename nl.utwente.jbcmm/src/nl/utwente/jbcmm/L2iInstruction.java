/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>L2i Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getL2iInstruction()
 * @model
 * @generated
 */
public interface L2iInstruction extends SimpleInstruction {
} // L2iInstruction
