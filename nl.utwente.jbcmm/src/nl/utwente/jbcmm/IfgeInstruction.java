/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ifge Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIfgeInstruction()
 * @model
 * @generated
 */
public interface IfgeInstruction extends JumpInstruction {
} // IfgeInstruction
