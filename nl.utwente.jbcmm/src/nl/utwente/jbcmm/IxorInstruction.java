/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ixor Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIxorInstruction()
 * @model
 * @generated
 */
public interface IxorInstruction extends SimpleInstruction {
} // IxorInstruction
