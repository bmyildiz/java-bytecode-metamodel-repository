/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ifle Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIfleInstruction()
 * @model
 * @generated
 */
public interface IfleInstruction extends JumpInstruction {
} // IfleInstruction
