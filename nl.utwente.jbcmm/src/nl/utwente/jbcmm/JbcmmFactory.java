/**
 */
package nl.utwente.jbcmm;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see nl.utwente.jbcmm.JbcmmPackage
 * @generated
 */
public interface JbcmmFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	JbcmmFactory eINSTANCE = nl.utwente.jbcmm.impl.JbcmmFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Identifiable</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Identifiable</em>'.
	 * @generated
	 */
	Identifiable createIdentifiable();

	/**
	 * Returns a new object of class '<em>Project</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Project</em>'.
	 * @generated
	 */
	Project createProject();

	/**
	 * Returns a new object of class '<em>Clazz</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Clazz</em>'.
	 * @generated
	 */
	Clazz createClazz();

	/**
	 * Returns a new object of class '<em>Class Signature</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Class Signature</em>'.
	 * @generated
	 */
	ClassSignature createClassSignature();

	/**
	 * Returns a new object of class '<em>Method Signature</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Method Signature</em>'.
	 * @generated
	 */
	MethodSignature createMethodSignature();

	/**
	 * Returns a new object of class '<em>Field Type Signature</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Field Type Signature</em>'.
	 * @generated
	 */
	FieldTypeSignature createFieldTypeSignature();

	/**
	 * Returns a new object of class '<em>Type Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Type Reference</em>'.
	 * @generated
	 */
	TypeReference createTypeReference();

	/**
	 * Returns a new object of class '<em>Method Descriptor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Method Descriptor</em>'.
	 * @generated
	 */
	MethodDescriptor createMethodDescriptor();

	/**
	 * Returns a new object of class '<em>Method Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Method Reference</em>'.
	 * @generated
	 */
	MethodReference createMethodReference();

	/**
	 * Returns a new object of class '<em>Field Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Field Reference</em>'.
	 * @generated
	 */
	FieldReference createFieldReference();

	/**
	 * Returns a new object of class '<em>Field</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Field</em>'.
	 * @generated
	 */
	Field createField();

	/**
	 * Returns a new object of class '<em>Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Annotation</em>'.
	 * @generated
	 */
	Annotation createAnnotation();

	/**
	 * Returns a new object of class '<em>Element Value Pair</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Element Value Pair</em>'.
	 * @generated
	 */
	ElementValuePair createElementValuePair();

	/**
	 * Returns a new object of class '<em>Element Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Element Value</em>'.
	 * @generated
	 */
	ElementValue createElementValue();

	/**
	 * Returns a new object of class '<em>Elementary Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Elementary Value</em>'.
	 * @generated
	 */
	ElementaryValue createElementaryValue();

	/**
	 * Returns a new object of class '<em>Boolean Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Boolean Value</em>'.
	 * @generated
	 */
	BooleanValue createBooleanValue();

	/**
	 * Returns a new object of class '<em>Char Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Char Value</em>'.
	 * @generated
	 */
	CharValue createCharValue();

	/**
	 * Returns a new object of class '<em>Byte Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Byte Value</em>'.
	 * @generated
	 */
	ByteValue createByteValue();

	/**
	 * Returns a new object of class '<em>Short Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Short Value</em>'.
	 * @generated
	 */
	ShortValue createShortValue();

	/**
	 * Returns a new object of class '<em>Int Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Int Value</em>'.
	 * @generated
	 */
	IntValue createIntValue();

	/**
	 * Returns a new object of class '<em>Long Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Long Value</em>'.
	 * @generated
	 */
	LongValue createLongValue();

	/**
	 * Returns a new object of class '<em>Float Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Float Value</em>'.
	 * @generated
	 */
	FloatValue createFloatValue();

	/**
	 * Returns a new object of class '<em>Double Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Double Value</em>'.
	 * @generated
	 */
	DoubleValue createDoubleValue();

	/**
	 * Returns a new object of class '<em>String Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>String Value</em>'.
	 * @generated
	 */
	StringValue createStringValue();

	/**
	 * Returns a new object of class '<em>Enum Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Enum Value</em>'.
	 * @generated
	 */
	EnumValue createEnumValue();

	/**
	 * Returns a new object of class '<em>Method</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Method</em>'.
	 * @generated
	 */
	Method createMethod();

	/**
	 * Returns a new object of class '<em>Local Variable Table Entry</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Local Variable Table Entry</em>'.
	 * @generated
	 */
	LocalVariableTableEntry createLocalVariableTableEntry();

	/**
	 * Returns a new object of class '<em>Exception Table Entry</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Exception Table Entry</em>'.
	 * @generated
	 */
	ExceptionTableEntry createExceptionTableEntry();

	/**
	 * Returns a new object of class '<em>Unconditional Edge</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Unconditional Edge</em>'.
	 * @generated
	 */
	UnconditionalEdge createUnconditionalEdge();

	/**
	 * Returns a new object of class '<em>Conditional Edge</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Conditional Edge</em>'.
	 * @generated
	 */
	ConditionalEdge createConditionalEdge();

	/**
	 * Returns a new object of class '<em>Switch Case Edge</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Switch Case Edge</em>'.
	 * @generated
	 */
	SwitchCaseEdge createSwitchCaseEdge();

	/**
	 * Returns a new object of class '<em>Switch Default Edge</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Switch Default Edge</em>'.
	 * @generated
	 */
	SwitchDefaultEdge createSwitchDefaultEdge();

	/**
	 * Returns a new object of class '<em>Exceptional Edge</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Exceptional Edge</em>'.
	 * @generated
	 */
	ExceptionalEdge createExceptionalEdge();

	/**
	 * Returns a new object of class '<em>Iinc Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Iinc Instruction</em>'.
	 * @generated
	 */
	IincInstruction createIincInstruction();

	/**
	 * Returns a new object of class '<em>Multianewarray Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Multianewarray Instruction</em>'.
	 * @generated
	 */
	MultianewarrayInstruction createMultianewarrayInstruction();

	/**
	 * Returns a new object of class '<em>Switch Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Switch Instruction</em>'.
	 * @generated
	 */
	SwitchInstruction createSwitchInstruction();

	/**
	 * Returns a new object of class '<em>Ldc Int Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ldc Int Instruction</em>'.
	 * @generated
	 */
	LdcIntInstruction createLdcIntInstruction();

	/**
	 * Returns a new object of class '<em>Ldc Long Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ldc Long Instruction</em>'.
	 * @generated
	 */
	LdcLongInstruction createLdcLongInstruction();

	/**
	 * Returns a new object of class '<em>Ldc Float Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ldc Float Instruction</em>'.
	 * @generated
	 */
	LdcFloatInstruction createLdcFloatInstruction();

	/**
	 * Returns a new object of class '<em>Ldc Double Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ldc Double Instruction</em>'.
	 * @generated
	 */
	LdcDoubleInstruction createLdcDoubleInstruction();

	/**
	 * Returns a new object of class '<em>Ldc String Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ldc String Instruction</em>'.
	 * @generated
	 */
	LdcStringInstruction createLdcStringInstruction();

	/**
	 * Returns a new object of class '<em>Ldc Type Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ldc Type Instruction</em>'.
	 * @generated
	 */
	LdcTypeInstruction createLdcTypeInstruction();

	/**
	 * Returns a new object of class '<em>Getstatic Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Getstatic Instruction</em>'.
	 * @generated
	 */
	GetstaticInstruction createGetstaticInstruction();

	/**
	 * Returns a new object of class '<em>Putstatic Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Putstatic Instruction</em>'.
	 * @generated
	 */
	PutstaticInstruction createPutstaticInstruction();

	/**
	 * Returns a new object of class '<em>Getfield Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Getfield Instruction</em>'.
	 * @generated
	 */
	GetfieldInstruction createGetfieldInstruction();

	/**
	 * Returns a new object of class '<em>Putfield Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Putfield Instruction</em>'.
	 * @generated
	 */
	PutfieldInstruction createPutfieldInstruction();

	/**
	 * Returns a new object of class '<em>Nop Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Nop Instruction</em>'.
	 * @generated
	 */
	NopInstruction createNopInstruction();

	/**
	 * Returns a new object of class '<em>Aconst null Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Aconst null Instruction</em>'.
	 * @generated
	 */
	Aconst_nullInstruction createAconst_nullInstruction();

	/**
	 * Returns a new object of class '<em>Iconst m1 Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Iconst m1 Instruction</em>'.
	 * @generated
	 */
	Iconst_m1Instruction createIconst_m1Instruction();

	/**
	 * Returns a new object of class '<em>Iconst 0Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Iconst 0Instruction</em>'.
	 * @generated
	 */
	Iconst_0Instruction createIconst_0Instruction();

	/**
	 * Returns a new object of class '<em>Iconst 1Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Iconst 1Instruction</em>'.
	 * @generated
	 */
	Iconst_1Instruction createIconst_1Instruction();

	/**
	 * Returns a new object of class '<em>Iconst 2Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Iconst 2Instruction</em>'.
	 * @generated
	 */
	Iconst_2Instruction createIconst_2Instruction();

	/**
	 * Returns a new object of class '<em>Iconst 3Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Iconst 3Instruction</em>'.
	 * @generated
	 */
	Iconst_3Instruction createIconst_3Instruction();

	/**
	 * Returns a new object of class '<em>Iconst 4Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Iconst 4Instruction</em>'.
	 * @generated
	 */
	Iconst_4Instruction createIconst_4Instruction();

	/**
	 * Returns a new object of class '<em>Iconst 5Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Iconst 5Instruction</em>'.
	 * @generated
	 */
	Iconst_5Instruction createIconst_5Instruction();

	/**
	 * Returns a new object of class '<em>Lconst 0Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Lconst 0Instruction</em>'.
	 * @generated
	 */
	Lconst_0Instruction createLconst_0Instruction();

	/**
	 * Returns a new object of class '<em>Lconst 1Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Lconst 1Instruction</em>'.
	 * @generated
	 */
	Lconst_1Instruction createLconst_1Instruction();

	/**
	 * Returns a new object of class '<em>Fconst 0Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Fconst 0Instruction</em>'.
	 * @generated
	 */
	Fconst_0Instruction createFconst_0Instruction();

	/**
	 * Returns a new object of class '<em>Fconst 1Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Fconst 1Instruction</em>'.
	 * @generated
	 */
	Fconst_1Instruction createFconst_1Instruction();

	/**
	 * Returns a new object of class '<em>Fconst 2Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Fconst 2Instruction</em>'.
	 * @generated
	 */
	Fconst_2Instruction createFconst_2Instruction();

	/**
	 * Returns a new object of class '<em>Dconst 0Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dconst 0Instruction</em>'.
	 * @generated
	 */
	Dconst_0Instruction createDconst_0Instruction();

	/**
	 * Returns a new object of class '<em>Dconst 1Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dconst 1Instruction</em>'.
	 * @generated
	 */
	Dconst_1Instruction createDconst_1Instruction();

	/**
	 * Returns a new object of class '<em>Iaload Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Iaload Instruction</em>'.
	 * @generated
	 */
	IaloadInstruction createIaloadInstruction();

	/**
	 * Returns a new object of class '<em>Laload Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Laload Instruction</em>'.
	 * @generated
	 */
	LaloadInstruction createLaloadInstruction();

	/**
	 * Returns a new object of class '<em>Faload Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Faload Instruction</em>'.
	 * @generated
	 */
	FaloadInstruction createFaloadInstruction();

	/**
	 * Returns a new object of class '<em>Daload Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Daload Instruction</em>'.
	 * @generated
	 */
	DaloadInstruction createDaloadInstruction();

	/**
	 * Returns a new object of class '<em>Aaload Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Aaload Instruction</em>'.
	 * @generated
	 */
	AaloadInstruction createAaloadInstruction();

	/**
	 * Returns a new object of class '<em>Baload Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Baload Instruction</em>'.
	 * @generated
	 */
	BaloadInstruction createBaloadInstruction();

	/**
	 * Returns a new object of class '<em>Caload Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Caload Instruction</em>'.
	 * @generated
	 */
	CaloadInstruction createCaloadInstruction();

	/**
	 * Returns a new object of class '<em>Saload Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Saload Instruction</em>'.
	 * @generated
	 */
	SaloadInstruction createSaloadInstruction();

	/**
	 * Returns a new object of class '<em>Iastore Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Iastore Instruction</em>'.
	 * @generated
	 */
	IastoreInstruction createIastoreInstruction();

	/**
	 * Returns a new object of class '<em>Lastore Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Lastore Instruction</em>'.
	 * @generated
	 */
	LastoreInstruction createLastoreInstruction();

	/**
	 * Returns a new object of class '<em>Fastore Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Fastore Instruction</em>'.
	 * @generated
	 */
	FastoreInstruction createFastoreInstruction();

	/**
	 * Returns a new object of class '<em>Dastore Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dastore Instruction</em>'.
	 * @generated
	 */
	DastoreInstruction createDastoreInstruction();

	/**
	 * Returns a new object of class '<em>Aastore Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Aastore Instruction</em>'.
	 * @generated
	 */
	AastoreInstruction createAastoreInstruction();

	/**
	 * Returns a new object of class '<em>Bastore Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Bastore Instruction</em>'.
	 * @generated
	 */
	BastoreInstruction createBastoreInstruction();

	/**
	 * Returns a new object of class '<em>Castore Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Castore Instruction</em>'.
	 * @generated
	 */
	CastoreInstruction createCastoreInstruction();

	/**
	 * Returns a new object of class '<em>Sastore Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sastore Instruction</em>'.
	 * @generated
	 */
	SastoreInstruction createSastoreInstruction();

	/**
	 * Returns a new object of class '<em>Pop Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Pop Instruction</em>'.
	 * @generated
	 */
	PopInstruction createPopInstruction();

	/**
	 * Returns a new object of class '<em>Pop2 Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Pop2 Instruction</em>'.
	 * @generated
	 */
	Pop2Instruction createPop2Instruction();

	/**
	 * Returns a new object of class '<em>Dup Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dup Instruction</em>'.
	 * @generated
	 */
	DupInstruction createDupInstruction();

	/**
	 * Returns a new object of class '<em>Dup x1 Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dup x1 Instruction</em>'.
	 * @generated
	 */
	Dup_x1Instruction createDup_x1Instruction();

	/**
	 * Returns a new object of class '<em>Dup x2 Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dup x2 Instruction</em>'.
	 * @generated
	 */
	Dup_x2Instruction createDup_x2Instruction();

	/**
	 * Returns a new object of class '<em>Dup2 Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dup2 Instruction</em>'.
	 * @generated
	 */
	Dup2Instruction createDup2Instruction();

	/**
	 * Returns a new object of class '<em>Dup2 x1 Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dup2 x1 Instruction</em>'.
	 * @generated
	 */
	Dup2_x1Instruction createDup2_x1Instruction();

	/**
	 * Returns a new object of class '<em>Dup2 x2 Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dup2 x2 Instruction</em>'.
	 * @generated
	 */
	Dup2_x2Instruction createDup2_x2Instruction();

	/**
	 * Returns a new object of class '<em>Swap Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Swap Instruction</em>'.
	 * @generated
	 */
	SwapInstruction createSwapInstruction();

	/**
	 * Returns a new object of class '<em>Iadd Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Iadd Instruction</em>'.
	 * @generated
	 */
	IaddInstruction createIaddInstruction();

	/**
	 * Returns a new object of class '<em>Ladd Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ladd Instruction</em>'.
	 * @generated
	 */
	LaddInstruction createLaddInstruction();

	/**
	 * Returns a new object of class '<em>Fadd Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Fadd Instruction</em>'.
	 * @generated
	 */
	FaddInstruction createFaddInstruction();

	/**
	 * Returns a new object of class '<em>Dadd Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dadd Instruction</em>'.
	 * @generated
	 */
	DaddInstruction createDaddInstruction();

	/**
	 * Returns a new object of class '<em>Isub Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Isub Instruction</em>'.
	 * @generated
	 */
	IsubInstruction createIsubInstruction();

	/**
	 * Returns a new object of class '<em>Lsub Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Lsub Instruction</em>'.
	 * @generated
	 */
	LsubInstruction createLsubInstruction();

	/**
	 * Returns a new object of class '<em>Fsub Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Fsub Instruction</em>'.
	 * @generated
	 */
	FsubInstruction createFsubInstruction();

	/**
	 * Returns a new object of class '<em>Dsub Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dsub Instruction</em>'.
	 * @generated
	 */
	DsubInstruction createDsubInstruction();

	/**
	 * Returns a new object of class '<em>Imul Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Imul Instruction</em>'.
	 * @generated
	 */
	ImulInstruction createImulInstruction();

	/**
	 * Returns a new object of class '<em>Lmul Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Lmul Instruction</em>'.
	 * @generated
	 */
	LmulInstruction createLmulInstruction();

	/**
	 * Returns a new object of class '<em>Fmul Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Fmul Instruction</em>'.
	 * @generated
	 */
	FmulInstruction createFmulInstruction();

	/**
	 * Returns a new object of class '<em>Dmul Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dmul Instruction</em>'.
	 * @generated
	 */
	DmulInstruction createDmulInstruction();

	/**
	 * Returns a new object of class '<em>Idiv Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Idiv Instruction</em>'.
	 * @generated
	 */
	IdivInstruction createIdivInstruction();

	/**
	 * Returns a new object of class '<em>Ldiv Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ldiv Instruction</em>'.
	 * @generated
	 */
	LdivInstruction createLdivInstruction();

	/**
	 * Returns a new object of class '<em>Fdiv Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Fdiv Instruction</em>'.
	 * @generated
	 */
	FdivInstruction createFdivInstruction();

	/**
	 * Returns a new object of class '<em>Ddiv Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ddiv Instruction</em>'.
	 * @generated
	 */
	DdivInstruction createDdivInstruction();

	/**
	 * Returns a new object of class '<em>Irem Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Irem Instruction</em>'.
	 * @generated
	 */
	IremInstruction createIremInstruction();

	/**
	 * Returns a new object of class '<em>Lrem Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Lrem Instruction</em>'.
	 * @generated
	 */
	LremInstruction createLremInstruction();

	/**
	 * Returns a new object of class '<em>Frem Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Frem Instruction</em>'.
	 * @generated
	 */
	FremInstruction createFremInstruction();

	/**
	 * Returns a new object of class '<em>Drem Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Drem Instruction</em>'.
	 * @generated
	 */
	DremInstruction createDremInstruction();

	/**
	 * Returns a new object of class '<em>Ineg Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ineg Instruction</em>'.
	 * @generated
	 */
	InegInstruction createInegInstruction();

	/**
	 * Returns a new object of class '<em>Lneg Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Lneg Instruction</em>'.
	 * @generated
	 */
	LnegInstruction createLnegInstruction();

	/**
	 * Returns a new object of class '<em>Fneg Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Fneg Instruction</em>'.
	 * @generated
	 */
	FnegInstruction createFnegInstruction();

	/**
	 * Returns a new object of class '<em>Dneg Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dneg Instruction</em>'.
	 * @generated
	 */
	DnegInstruction createDnegInstruction();

	/**
	 * Returns a new object of class '<em>Ishl Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ishl Instruction</em>'.
	 * @generated
	 */
	IshlInstruction createIshlInstruction();

	/**
	 * Returns a new object of class '<em>Lshl Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Lshl Instruction</em>'.
	 * @generated
	 */
	LshlInstruction createLshlInstruction();

	/**
	 * Returns a new object of class '<em>Ishr Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ishr Instruction</em>'.
	 * @generated
	 */
	IshrInstruction createIshrInstruction();

	/**
	 * Returns a new object of class '<em>Lshr Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Lshr Instruction</em>'.
	 * @generated
	 */
	LshrInstruction createLshrInstruction();

	/**
	 * Returns a new object of class '<em>Iushr Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Iushr Instruction</em>'.
	 * @generated
	 */
	IushrInstruction createIushrInstruction();

	/**
	 * Returns a new object of class '<em>Lushr Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Lushr Instruction</em>'.
	 * @generated
	 */
	LushrInstruction createLushrInstruction();

	/**
	 * Returns a new object of class '<em>Iand Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Iand Instruction</em>'.
	 * @generated
	 */
	IandInstruction createIandInstruction();

	/**
	 * Returns a new object of class '<em>Land Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Land Instruction</em>'.
	 * @generated
	 */
	LandInstruction createLandInstruction();

	/**
	 * Returns a new object of class '<em>Ior Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ior Instruction</em>'.
	 * @generated
	 */
	IorInstruction createIorInstruction();

	/**
	 * Returns a new object of class '<em>Lor Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Lor Instruction</em>'.
	 * @generated
	 */
	LorInstruction createLorInstruction();

	/**
	 * Returns a new object of class '<em>Ixor Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ixor Instruction</em>'.
	 * @generated
	 */
	IxorInstruction createIxorInstruction();

	/**
	 * Returns a new object of class '<em>Lxor Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Lxor Instruction</em>'.
	 * @generated
	 */
	LxorInstruction createLxorInstruction();

	/**
	 * Returns a new object of class '<em>I2l Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>I2l Instruction</em>'.
	 * @generated
	 */
	I2lInstruction createI2lInstruction();

	/**
	 * Returns a new object of class '<em>I2f Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>I2f Instruction</em>'.
	 * @generated
	 */
	I2fInstruction createI2fInstruction();

	/**
	 * Returns a new object of class '<em>I2d Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>I2d Instruction</em>'.
	 * @generated
	 */
	I2dInstruction createI2dInstruction();

	/**
	 * Returns a new object of class '<em>L2i Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>L2i Instruction</em>'.
	 * @generated
	 */
	L2iInstruction createL2iInstruction();

	/**
	 * Returns a new object of class '<em>L2f Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>L2f Instruction</em>'.
	 * @generated
	 */
	L2fInstruction createL2fInstruction();

	/**
	 * Returns a new object of class '<em>L2d Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>L2d Instruction</em>'.
	 * @generated
	 */
	L2dInstruction createL2dInstruction();

	/**
	 * Returns a new object of class '<em>F2i Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>F2i Instruction</em>'.
	 * @generated
	 */
	F2iInstruction createF2iInstruction();

	/**
	 * Returns a new object of class '<em>F2l Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>F2l Instruction</em>'.
	 * @generated
	 */
	F2lInstruction createF2lInstruction();

	/**
	 * Returns a new object of class '<em>F2d Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>F2d Instruction</em>'.
	 * @generated
	 */
	F2dInstruction createF2dInstruction();

	/**
	 * Returns a new object of class '<em>D2i Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>D2i Instruction</em>'.
	 * @generated
	 */
	D2iInstruction createD2iInstruction();

	/**
	 * Returns a new object of class '<em>D2l Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>D2l Instruction</em>'.
	 * @generated
	 */
	D2lInstruction createD2lInstruction();

	/**
	 * Returns a new object of class '<em>D2f Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>D2f Instruction</em>'.
	 * @generated
	 */
	D2fInstruction createD2fInstruction();

	/**
	 * Returns a new object of class '<em>I2b Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>I2b Instruction</em>'.
	 * @generated
	 */
	I2bInstruction createI2bInstruction();

	/**
	 * Returns a new object of class '<em>I2c Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>I2c Instruction</em>'.
	 * @generated
	 */
	I2cInstruction createI2cInstruction();

	/**
	 * Returns a new object of class '<em>I2s Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>I2s Instruction</em>'.
	 * @generated
	 */
	I2sInstruction createI2sInstruction();

	/**
	 * Returns a new object of class '<em>Lcmp Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Lcmp Instruction</em>'.
	 * @generated
	 */
	LcmpInstruction createLcmpInstruction();

	/**
	 * Returns a new object of class '<em>Fcmpl Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Fcmpl Instruction</em>'.
	 * @generated
	 */
	FcmplInstruction createFcmplInstruction();

	/**
	 * Returns a new object of class '<em>Fcmpg Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Fcmpg Instruction</em>'.
	 * @generated
	 */
	FcmpgInstruction createFcmpgInstruction();

	/**
	 * Returns a new object of class '<em>Dcmpl Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dcmpl Instruction</em>'.
	 * @generated
	 */
	DcmplInstruction createDcmplInstruction();

	/**
	 * Returns a new object of class '<em>Dcmpg Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dcmpg Instruction</em>'.
	 * @generated
	 */
	DcmpgInstruction createDcmpgInstruction();

	/**
	 * Returns a new object of class '<em>Ireturn Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ireturn Instruction</em>'.
	 * @generated
	 */
	IreturnInstruction createIreturnInstruction();

	/**
	 * Returns a new object of class '<em>Lreturn Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Lreturn Instruction</em>'.
	 * @generated
	 */
	LreturnInstruction createLreturnInstruction();

	/**
	 * Returns a new object of class '<em>Freturn Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Freturn Instruction</em>'.
	 * @generated
	 */
	FreturnInstruction createFreturnInstruction();

	/**
	 * Returns a new object of class '<em>Dreturn Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dreturn Instruction</em>'.
	 * @generated
	 */
	DreturnInstruction createDreturnInstruction();

	/**
	 * Returns a new object of class '<em>Areturn Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Areturn Instruction</em>'.
	 * @generated
	 */
	AreturnInstruction createAreturnInstruction();

	/**
	 * Returns a new object of class '<em>Return Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Return Instruction</em>'.
	 * @generated
	 */
	ReturnInstruction createReturnInstruction();

	/**
	 * Returns a new object of class '<em>Arraylength Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Arraylength Instruction</em>'.
	 * @generated
	 */
	ArraylengthInstruction createArraylengthInstruction();

	/**
	 * Returns a new object of class '<em>Athrow Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Athrow Instruction</em>'.
	 * @generated
	 */
	AthrowInstruction createAthrowInstruction();

	/**
	 * Returns a new object of class '<em>Monitorenter Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Monitorenter Instruction</em>'.
	 * @generated
	 */
	MonitorenterInstruction createMonitorenterInstruction();

	/**
	 * Returns a new object of class '<em>Monitorexit Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Monitorexit Instruction</em>'.
	 * @generated
	 */
	MonitorexitInstruction createMonitorexitInstruction();

	/**
	 * Returns a new object of class '<em>Bipush Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Bipush Instruction</em>'.
	 * @generated
	 */
	BipushInstruction createBipushInstruction();

	/**
	 * Returns a new object of class '<em>Sipush Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sipush Instruction</em>'.
	 * @generated
	 */
	SipushInstruction createSipushInstruction();

	/**
	 * Returns a new object of class '<em>Newarray Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Newarray Instruction</em>'.
	 * @generated
	 */
	NewarrayInstruction createNewarrayInstruction();

	/**
	 * Returns a new object of class '<em>Ifeq Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ifeq Instruction</em>'.
	 * @generated
	 */
	IfeqInstruction createIfeqInstruction();

	/**
	 * Returns a new object of class '<em>Ifne Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ifne Instruction</em>'.
	 * @generated
	 */
	IfneInstruction createIfneInstruction();

	/**
	 * Returns a new object of class '<em>Iflt Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Iflt Instruction</em>'.
	 * @generated
	 */
	IfltInstruction createIfltInstruction();

	/**
	 * Returns a new object of class '<em>Ifge Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ifge Instruction</em>'.
	 * @generated
	 */
	IfgeInstruction createIfgeInstruction();

	/**
	 * Returns a new object of class '<em>Ifgt Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ifgt Instruction</em>'.
	 * @generated
	 */
	IfgtInstruction createIfgtInstruction();

	/**
	 * Returns a new object of class '<em>Ifle Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ifle Instruction</em>'.
	 * @generated
	 */
	IfleInstruction createIfleInstruction();

	/**
	 * Returns a new object of class '<em>If icmpeq Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>If icmpeq Instruction</em>'.
	 * @generated
	 */
	If_icmpeqInstruction createIf_icmpeqInstruction();

	/**
	 * Returns a new object of class '<em>If icmpne Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>If icmpne Instruction</em>'.
	 * @generated
	 */
	If_icmpneInstruction createIf_icmpneInstruction();

	/**
	 * Returns a new object of class '<em>If icmplt Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>If icmplt Instruction</em>'.
	 * @generated
	 */
	If_icmpltInstruction createIf_icmpltInstruction();

	/**
	 * Returns a new object of class '<em>If icmpge Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>If icmpge Instruction</em>'.
	 * @generated
	 */
	If_icmpgeInstruction createIf_icmpgeInstruction();

	/**
	 * Returns a new object of class '<em>If icmpgt Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>If icmpgt Instruction</em>'.
	 * @generated
	 */
	If_icmpgtInstruction createIf_icmpgtInstruction();

	/**
	 * Returns a new object of class '<em>If icmple Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>If icmple Instruction</em>'.
	 * @generated
	 */
	If_icmpleInstruction createIf_icmpleInstruction();

	/**
	 * Returns a new object of class '<em>If acmpeq Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>If acmpeq Instruction</em>'.
	 * @generated
	 */
	If_acmpeqInstruction createIf_acmpeqInstruction();

	/**
	 * Returns a new object of class '<em>If acmpne Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>If acmpne Instruction</em>'.
	 * @generated
	 */
	If_acmpneInstruction createIf_acmpneInstruction();

	/**
	 * Returns a new object of class '<em>Goto Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Goto Instruction</em>'.
	 * @generated
	 */
	GotoInstruction createGotoInstruction();

	/**
	 * Returns a new object of class '<em>Jsr Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Jsr Instruction</em>'.
	 * @generated
	 */
	JsrInstruction createJsrInstruction();

	/**
	 * Returns a new object of class '<em>Ifnull Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ifnull Instruction</em>'.
	 * @generated
	 */
	IfnullInstruction createIfnullInstruction();

	/**
	 * Returns a new object of class '<em>Ifnonnull Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ifnonnull Instruction</em>'.
	 * @generated
	 */
	IfnonnullInstruction createIfnonnullInstruction();

	/**
	 * Returns a new object of class '<em>Invokevirtual Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Invokevirtual Instruction</em>'.
	 * @generated
	 */
	InvokevirtualInstruction createInvokevirtualInstruction();

	/**
	 * Returns a new object of class '<em>Invokespecial Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Invokespecial Instruction</em>'.
	 * @generated
	 */
	InvokespecialInstruction createInvokespecialInstruction();

	/**
	 * Returns a new object of class '<em>Invokestatic Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Invokestatic Instruction</em>'.
	 * @generated
	 */
	InvokestaticInstruction createInvokestaticInstruction();

	/**
	 * Returns a new object of class '<em>Invokeinterface Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Invokeinterface Instruction</em>'.
	 * @generated
	 */
	InvokeinterfaceInstruction createInvokeinterfaceInstruction();

	/**
	 * Returns a new object of class '<em>New Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>New Instruction</em>'.
	 * @generated
	 */
	NewInstruction createNewInstruction();

	/**
	 * Returns a new object of class '<em>Anewarray Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Anewarray Instruction</em>'.
	 * @generated
	 */
	AnewarrayInstruction createAnewarrayInstruction();

	/**
	 * Returns a new object of class '<em>Checkcast Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Checkcast Instruction</em>'.
	 * @generated
	 */
	CheckcastInstruction createCheckcastInstruction();

	/**
	 * Returns a new object of class '<em>Instanceof Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Instanceof Instruction</em>'.
	 * @generated
	 */
	InstanceofInstruction createInstanceofInstruction();

	/**
	 * Returns a new object of class '<em>Iload Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Iload Instruction</em>'.
	 * @generated
	 */
	IloadInstruction createIloadInstruction();

	/**
	 * Returns a new object of class '<em>Lload Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Lload Instruction</em>'.
	 * @generated
	 */
	LloadInstruction createLloadInstruction();

	/**
	 * Returns a new object of class '<em>Fload Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Fload Instruction</em>'.
	 * @generated
	 */
	FloadInstruction createFloadInstruction();

	/**
	 * Returns a new object of class '<em>Dload Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dload Instruction</em>'.
	 * @generated
	 */
	DloadInstruction createDloadInstruction();

	/**
	 * Returns a new object of class '<em>Aload Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Aload Instruction</em>'.
	 * @generated
	 */
	AloadInstruction createAloadInstruction();

	/**
	 * Returns a new object of class '<em>Istore Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Istore Instruction</em>'.
	 * @generated
	 */
	IstoreInstruction createIstoreInstruction();

	/**
	 * Returns a new object of class '<em>Lstore Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Lstore Instruction</em>'.
	 * @generated
	 */
	LstoreInstruction createLstoreInstruction();

	/**
	 * Returns a new object of class '<em>Fstore Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Fstore Instruction</em>'.
	 * @generated
	 */
	FstoreInstruction createFstoreInstruction();

	/**
	 * Returns a new object of class '<em>Dstore Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dstore Instruction</em>'.
	 * @generated
	 */
	DstoreInstruction createDstoreInstruction();

	/**
	 * Returns a new object of class '<em>Astore Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Astore Instruction</em>'.
	 * @generated
	 */
	AstoreInstruction createAstoreInstruction();

	/**
	 * Returns a new object of class '<em>Ret Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ret Instruction</em>'.
	 * @generated
	 */
	RetInstruction createRetInstruction();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	JbcmmPackage getJbcmmPackage();

} //JbcmmFactory
