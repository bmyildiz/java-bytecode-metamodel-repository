/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>I2d Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getI2dInstruction()
 * @model
 * @generated
 */
public interface I2dInstruction extends SimpleInstruction {
} // I2dInstruction
