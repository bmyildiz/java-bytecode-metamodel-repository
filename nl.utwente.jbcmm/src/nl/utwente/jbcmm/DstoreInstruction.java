/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dstore Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getDstoreInstruction()
 * @model
 * @generated
 */
public interface DstoreInstruction extends VarInstruction {
} // DstoreInstruction
