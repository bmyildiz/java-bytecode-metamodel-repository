/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ladd Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getLaddInstruction()
 * @model
 * @generated
 */
public interface LaddInstruction extends SimpleInstruction {
} // LaddInstruction
