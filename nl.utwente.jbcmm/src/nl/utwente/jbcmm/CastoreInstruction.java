/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Castore Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getCastoreInstruction()
 * @model
 * @generated
 */
public interface CastoreInstruction extends SimpleInstruction {
} // CastoreInstruction
