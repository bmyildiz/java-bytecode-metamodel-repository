/**
 */
package nl.utwente.jbcmm;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Element Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.ElementValue#getConstantValue <em>Constant Value</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.ElementValue#getEnumValue <em>Enum Value</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.ElementValue#getAnnotationValue <em>Annotation Value</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.ElementValue#getArrayValue <em>Array Value</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.ElementValue#getClassValue <em>Class Value</em>}</li>
 * </ul>
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getElementValue()
 * @model
 * @generated
 */
public interface ElementValue extends Identifiable {
	/**
	 * Returns the value of the '<em><b>Constant Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constant Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constant Value</em>' containment reference.
	 * @see #setConstantValue(ElementaryValue)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getElementValue_ConstantValue()
	 * @model containment="true"
	 * @generated
	 */
	ElementaryValue getConstantValue();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.ElementValue#getConstantValue <em>Constant Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constant Value</em>' containment reference.
	 * @see #getConstantValue()
	 * @generated
	 */
	void setConstantValue(ElementaryValue value);

	/**
	 * Returns the value of the '<em><b>Enum Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enum Value</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enum Value</em>' containment reference.
	 * @see #setEnumValue(EnumValue)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getElementValue_EnumValue()
	 * @model containment="true"
	 * @generated
	 */
	EnumValue getEnumValue();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.ElementValue#getEnumValue <em>Enum Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Enum Value</em>' containment reference.
	 * @see #getEnumValue()
	 * @generated
	 */
	void setEnumValue(EnumValue value);

	/**
	 * Returns the value of the '<em><b>Annotation Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotation Value</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotation Value</em>' containment reference.
	 * @see #setAnnotationValue(Annotation)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getElementValue_AnnotationValue()
	 * @model containment="true"
	 * @generated
	 */
	Annotation getAnnotationValue();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.ElementValue#getAnnotationValue <em>Annotation Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Annotation Value</em>' containment reference.
	 * @see #getAnnotationValue()
	 * @generated
	 */
	void setAnnotationValue(Annotation value);

	/**
	 * Returns the value of the '<em><b>Array Value</b></em>' containment reference list.
	 * The list contents are of type {@link nl.utwente.jbcmm.ElementValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Array Value</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Array Value</em>' containment reference list.
	 * @see nl.utwente.jbcmm.JbcmmPackage#getElementValue_ArrayValue()
	 * @model containment="true"
	 * @generated
	 */
	EList<ElementValue> getArrayValue();

	/**
	 * Returns the value of the '<em><b>Class Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class Value</em>' containment reference.
	 * @see #setClassValue(TypeReference)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getElementValue_ClassValue()
	 * @model containment="true"
	 * @generated
	 */
	TypeReference getClassValue();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.ElementValue#getClassValue <em>Class Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Class Value</em>' containment reference.
	 * @see #getClassValue()
	 * @generated
	 */
	void setClassValue(TypeReference value);

} // ElementValue
