/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fconst 1Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getFconst_1Instruction()
 * @model
 * @generated
 */
public interface Fconst_1Instruction extends SimpleInstruction {
} // Fconst_1Instruction
