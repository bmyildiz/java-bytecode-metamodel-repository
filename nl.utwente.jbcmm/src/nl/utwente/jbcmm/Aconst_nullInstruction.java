/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aconst null Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getAconst_nullInstruction()
 * @model
 * @generated
 */
public interface Aconst_nullInstruction extends SimpleInstruction {
} // Aconst_nullInstruction
