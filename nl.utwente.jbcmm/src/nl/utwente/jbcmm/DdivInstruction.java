/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ddiv Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getDdivInstruction()
 * @model
 * @generated
 */
public interface DdivInstruction extends SimpleInstruction {
} // DdivInstruction
