/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Imul Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getImulInstruction()
 * @model
 * @generated
 */
public interface ImulInstruction extends SimpleInstruction {
} // ImulInstruction
