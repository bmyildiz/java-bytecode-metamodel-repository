/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sipush Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getSipushInstruction()
 * @model
 * @generated
 */
public interface SipushInstruction extends IntInstruction {
} // SipushInstruction
