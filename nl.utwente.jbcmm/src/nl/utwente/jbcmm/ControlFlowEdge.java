/**
 */
package nl.utwente.jbcmm;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Control Flow Edge</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.ControlFlowEdge#getStart <em>Start</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.ControlFlowEdge#getEnd <em>End</em>}</li>
 * </ul>
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getControlFlowEdge()
 * @model abstract="true"
 * @generated
 */
public interface ControlFlowEdge extends Identifiable {
	/**
	 * Returns the value of the '<em><b>Start</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link nl.utwente.jbcmm.Instruction#getOutEdges <em>Out Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start</em>' container reference.
	 * @see #setStart(Instruction)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getControlFlowEdge_Start()
	 * @see nl.utwente.jbcmm.Instruction#getOutEdges
	 * @model opposite="outEdges" required="true" transient="false"
	 * @generated
	 */
	Instruction getStart();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.ControlFlowEdge#getStart <em>Start</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start</em>' container reference.
	 * @see #getStart()
	 * @generated
	 */
	void setStart(Instruction value);

	/**
	 * Returns the value of the '<em><b>End</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link nl.utwente.jbcmm.Instruction#getInEdges <em>In Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End</em>' reference.
	 * @see #setEnd(Instruction)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getControlFlowEdge_End()
	 * @see nl.utwente.jbcmm.Instruction#getInEdges
	 * @model opposite="inEdges" required="true"
	 * @generated
	 */
	Instruction getEnd();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.ControlFlowEdge#getEnd <em>End</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End</em>' reference.
	 * @see #getEnd()
	 * @generated
	 */
	void setEnd(Instruction value);

} // ControlFlowEdge
