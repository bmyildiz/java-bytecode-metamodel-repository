/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Var Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.VarInstruction#getLocalVariable <em>Local Variable</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.VarInstruction#getVarIndex <em>Var Index</em>}</li>
 * </ul>
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getVarInstruction()
 * @model abstract="true"
 * @generated
 */
public interface VarInstruction extends Instruction {
	/**
	 * Returns the value of the '<em><b>Local Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Variable</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Variable</em>' reference.
	 * @see #setLocalVariable(LocalVariableTableEntry)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getVarInstruction_LocalVariable()
	 * @model
	 * @generated
	 */
	LocalVariableTableEntry getLocalVariable();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.VarInstruction#getLocalVariable <em>Local Variable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Local Variable</em>' reference.
	 * @see #getLocalVariable()
	 * @generated
	 */
	void setLocalVariable(LocalVariableTableEntry value);

	/**
	 * Returns the value of the '<em><b>Var Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Var Index</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Var Index</em>' attribute.
	 * @see #setVarIndex(int)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getVarInstruction_VarIndex()
	 * @model required="true"
	 * @generated
	 */
	int getVarIndex();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.VarInstruction#getVarIndex <em>Var Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Var Index</em>' attribute.
	 * @see #getVarIndex()
	 * @generated
	 */
	void setVarIndex(int value);

} // VarInstruction
