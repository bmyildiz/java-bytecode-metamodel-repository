/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Jsr Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getJsrInstruction()
 * @model
 * @generated
 */
public interface JsrInstruction extends JumpInstruction {
} // JsrInstruction
