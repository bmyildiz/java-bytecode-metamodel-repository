/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Invokevirtual Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getInvokevirtualInstruction()
 * @model
 * @generated
 */
public interface InvokevirtualInstruction extends MethodInstruction {
} // InvokevirtualInstruction
