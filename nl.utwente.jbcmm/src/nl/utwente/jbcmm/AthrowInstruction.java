/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Athrow Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getAthrowInstruction()
 * @model
 * @generated
 */
public interface AthrowInstruction extends SimpleInstruction {
} // AthrowInstruction
