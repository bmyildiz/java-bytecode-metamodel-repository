/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>I2b Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getI2bInstruction()
 * @model
 * @generated
 */
public interface I2bInstruction extends SimpleInstruction {
} // I2bInstruction
