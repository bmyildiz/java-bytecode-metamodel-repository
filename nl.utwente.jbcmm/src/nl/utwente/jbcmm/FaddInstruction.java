/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fadd Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getFaddInstruction()
 * @model
 * @generated
 */
public interface FaddInstruction extends SimpleInstruction {
} // FaddInstruction
