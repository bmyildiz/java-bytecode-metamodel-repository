/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simple Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getSimpleInstruction()
 * @model abstract="true"
 * @generated
 */
public interface SimpleInstruction extends Instruction {
} // SimpleInstruction
