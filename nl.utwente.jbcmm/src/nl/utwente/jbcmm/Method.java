/**
 */
package nl.utwente.jbcmm;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Method</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.Method#getClass_ <em>Class</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Method#getName <em>Name</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Method#getInstructions <em>Instructions</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Method#getFirstInstruction <em>First Instruction</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Method#isPublic <em>Public</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Method#isPrivate <em>Private</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Method#isProtected <em>Protected</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Method#isStatic <em>Static</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Method#isFinal <em>Final</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Method#isSynthetic <em>Synthetic</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Method#isSynchronized <em>Synchronized</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Method#isBridge <em>Bridge</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Method#isVarArgs <em>Var Args</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Method#isNative <em>Native</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Method#isAbstract <em>Abstract</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Method#isStrict <em>Strict</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Method#isDeprecated <em>Deprecated</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Method#getRuntimeInvisibleAnnotations <em>Runtime Invisible Annotations</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Method#getRuntimeVisibleAnnotations <em>Runtime Visible Annotations</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Method#getAnnotationDefaultValue <em>Annotation Default Value</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Method#getDescriptor <em>Descriptor</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Method#getSignature <em>Signature</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Method#getExceptionsCanBeThrown <em>Exceptions Can Be Thrown</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Method#getExceptionTable <em>Exception Table</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Method#getLocalVariableTable <em>Local Variable Table</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Method#getRuntimeInvisibleParameterAnnotations <em>Runtime Invisible Parameter Annotations</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Method#getRuntimeVisibleParameterAnnotations <em>Runtime Visible Parameter Annotations</em>}</li>
 * </ul>
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getMethod()
 * @model
 * @generated
 */
public interface Method extends Identifiable {
	/**
	 * Returns the value of the '<em><b>Class</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link nl.utwente.jbcmm.Clazz#getMethods <em>Methods</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class</em>' container reference.
	 * @see #setClass(Clazz)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getMethod_Class()
	 * @see nl.utwente.jbcmm.Clazz#getMethods
	 * @model opposite="methods" transient="false"
	 * @generated
	 */
	Clazz getClass_();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Method#getClass_ <em>Class</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Class</em>' container reference.
	 * @see #getClass_()
	 * @generated
	 */
	void setClass(Clazz value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getMethod_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Method#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Instructions</b></em>' containment reference list.
	 * The list contents are of type {@link nl.utwente.jbcmm.Instruction}.
	 * It is bidirectional and its opposite is '{@link nl.utwente.jbcmm.Instruction#getMethod <em>Method</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Instructions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Instructions</em>' containment reference list.
	 * @see nl.utwente.jbcmm.JbcmmPackage#getMethod_Instructions()
	 * @see nl.utwente.jbcmm.Instruction#getMethod
	 * @model opposite="method" containment="true" ordered="false"
	 * @generated
	 */
	EList<Instruction> getInstructions();

	/**
	 * Returns the value of the '<em><b>First Instruction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First Instruction</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First Instruction</em>' reference.
	 * @see #setFirstInstruction(Instruction)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getMethod_FirstInstruction()
	 * @model
	 * @generated
	 */
	Instruction getFirstInstruction();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Method#getFirstInstruction <em>First Instruction</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First Instruction</em>' reference.
	 * @see #getFirstInstruction()
	 * @generated
	 */
	void setFirstInstruction(Instruction value);

	/**
	 * Returns the value of the '<em><b>Public</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Public</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Public</em>' attribute.
	 * @see #setPublic(boolean)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getMethod_Public()
	 * @model required="true"
	 * @generated
	 */
	boolean isPublic();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Method#isPublic <em>Public</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Public</em>' attribute.
	 * @see #isPublic()
	 * @generated
	 */
	void setPublic(boolean value);

	/**
	 * Returns the value of the '<em><b>Private</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Private</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Private</em>' attribute.
	 * @see #setPrivate(boolean)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getMethod_Private()
	 * @model required="true"
	 * @generated
	 */
	boolean isPrivate();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Method#isPrivate <em>Private</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Private</em>' attribute.
	 * @see #isPrivate()
	 * @generated
	 */
	void setPrivate(boolean value);

	/**
	 * Returns the value of the '<em><b>Protected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Protected</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Protected</em>' attribute.
	 * @see #setProtected(boolean)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getMethod_Protected()
	 * @model required="true"
	 * @generated
	 */
	boolean isProtected();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Method#isProtected <em>Protected</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Protected</em>' attribute.
	 * @see #isProtected()
	 * @generated
	 */
	void setProtected(boolean value);

	/**
	 * Returns the value of the '<em><b>Static</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Static</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Static</em>' attribute.
	 * @see #setStatic(boolean)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getMethod_Static()
	 * @model required="true"
	 * @generated
	 */
	boolean isStatic();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Method#isStatic <em>Static</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Static</em>' attribute.
	 * @see #isStatic()
	 * @generated
	 */
	void setStatic(boolean value);

	/**
	 * Returns the value of the '<em><b>Final</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Final</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Final</em>' attribute.
	 * @see #setFinal(boolean)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getMethod_Final()
	 * @model required="true"
	 * @generated
	 */
	boolean isFinal();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Method#isFinal <em>Final</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Final</em>' attribute.
	 * @see #isFinal()
	 * @generated
	 */
	void setFinal(boolean value);

	/**
	 * Returns the value of the '<em><b>Synthetic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Synthetic</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Synthetic</em>' attribute.
	 * @see #setSynthetic(boolean)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getMethod_Synthetic()
	 * @model required="true"
	 * @generated
	 */
	boolean isSynthetic();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Method#isSynthetic <em>Synthetic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Synthetic</em>' attribute.
	 * @see #isSynthetic()
	 * @generated
	 */
	void setSynthetic(boolean value);

	/**
	 * Returns the value of the '<em><b>Synchronized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Synchronized</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Synchronized</em>' attribute.
	 * @see #setSynchronized(boolean)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getMethod_Synchronized()
	 * @model required="true"
	 * @generated
	 */
	boolean isSynchronized();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Method#isSynchronized <em>Synchronized</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Synchronized</em>' attribute.
	 * @see #isSynchronized()
	 * @generated
	 */
	void setSynchronized(boolean value);

	/**
	 * Returns the value of the '<em><b>Bridge</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bridge</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bridge</em>' attribute.
	 * @see #setBridge(boolean)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getMethod_Bridge()
	 * @model required="true"
	 * @generated
	 */
	boolean isBridge();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Method#isBridge <em>Bridge</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bridge</em>' attribute.
	 * @see #isBridge()
	 * @generated
	 */
	void setBridge(boolean value);

	/**
	 * Returns the value of the '<em><b>Var Args</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Var Args</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Var Args</em>' attribute.
	 * @see #setVarArgs(boolean)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getMethod_VarArgs()
	 * @model required="true"
	 * @generated
	 */
	boolean isVarArgs();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Method#isVarArgs <em>Var Args</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Var Args</em>' attribute.
	 * @see #isVarArgs()
	 * @generated
	 */
	void setVarArgs(boolean value);

	/**
	 * Returns the value of the '<em><b>Native</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Native</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Native</em>' attribute.
	 * @see #setNative(boolean)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getMethod_Native()
	 * @model required="true"
	 * @generated
	 */
	boolean isNative();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Method#isNative <em>Native</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Native</em>' attribute.
	 * @see #isNative()
	 * @generated
	 */
	void setNative(boolean value);

	/**
	 * Returns the value of the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abstract</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abstract</em>' attribute.
	 * @see #setAbstract(boolean)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getMethod_Abstract()
	 * @model required="true"
	 * @generated
	 */
	boolean isAbstract();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Method#isAbstract <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abstract</em>' attribute.
	 * @see #isAbstract()
	 * @generated
	 */
	void setAbstract(boolean value);

	/**
	 * Returns the value of the '<em><b>Strict</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Strict</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Strict</em>' attribute.
	 * @see #setStrict(boolean)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getMethod_Strict()
	 * @model required="true"
	 * @generated
	 */
	boolean isStrict();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Method#isStrict <em>Strict</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Strict</em>' attribute.
	 * @see #isStrict()
	 * @generated
	 */
	void setStrict(boolean value);

	/**
	 * Returns the value of the '<em><b>Deprecated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deprecated</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deprecated</em>' attribute.
	 * @see #setDeprecated(boolean)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getMethod_Deprecated()
	 * @model required="true"
	 * @generated
	 */
	boolean isDeprecated();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Method#isDeprecated <em>Deprecated</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Deprecated</em>' attribute.
	 * @see #isDeprecated()
	 * @generated
	 */
	void setDeprecated(boolean value);

	/**
	 * Returns the value of the '<em><b>Runtime Invisible Annotations</b></em>' containment reference list.
	 * The list contents are of type {@link nl.utwente.jbcmm.Annotation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Runtime Invisible Annotations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Runtime Invisible Annotations</em>' containment reference list.
	 * @see nl.utwente.jbcmm.JbcmmPackage#getMethod_RuntimeInvisibleAnnotations()
	 * @model containment="true"
	 * @generated
	 */
	EList<Annotation> getRuntimeInvisibleAnnotations();

	/**
	 * Returns the value of the '<em><b>Runtime Visible Annotations</b></em>' containment reference list.
	 * The list contents are of type {@link nl.utwente.jbcmm.Annotation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Runtime Visible Annotations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Runtime Visible Annotations</em>' containment reference list.
	 * @see nl.utwente.jbcmm.JbcmmPackage#getMethod_RuntimeVisibleAnnotations()
	 * @model containment="true"
	 * @generated
	 */
	EList<Annotation> getRuntimeVisibleAnnotations();

	/**
	 * Returns the value of the '<em><b>Annotation Default Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotation Default Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotation Default Value</em>' containment reference.
	 * @see #setAnnotationDefaultValue(ElementValue)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getMethod_AnnotationDefaultValue()
	 * @model containment="true"
	 * @generated
	 */
	ElementValue getAnnotationDefaultValue();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Method#getAnnotationDefaultValue <em>Annotation Default Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Annotation Default Value</em>' containment reference.
	 * @see #getAnnotationDefaultValue()
	 * @generated
	 */
	void setAnnotationDefaultValue(ElementValue value);

	/**
	 * Returns the value of the '<em><b>Descriptor</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Descriptor</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Descriptor</em>' containment reference.
	 * @see #setDescriptor(MethodDescriptor)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getMethod_Descriptor()
	 * @model containment="true" required="true"
	 * @generated
	 */
	MethodDescriptor getDescriptor();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Method#getDescriptor <em>Descriptor</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Descriptor</em>' containment reference.
	 * @see #getDescriptor()
	 * @generated
	 */
	void setDescriptor(MethodDescriptor value);

	/**
	 * Returns the value of the '<em><b>Signature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Signature</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Signature</em>' containment reference.
	 * @see #setSignature(MethodSignature)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getMethod_Signature()
	 * @model containment="true"
	 * @generated
	 */
	MethodSignature getSignature();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Method#getSignature <em>Signature</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Signature</em>' containment reference.
	 * @see #getSignature()
	 * @generated
	 */
	void setSignature(MethodSignature value);

	/**
	 * Returns the value of the '<em><b>Exceptions Can Be Thrown</b></em>' containment reference list.
	 * The list contents are of type {@link nl.utwente.jbcmm.TypeReference}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exceptions Can Be Thrown</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exceptions Can Be Thrown</em>' containment reference list.
	 * @see nl.utwente.jbcmm.JbcmmPackage#getMethod_ExceptionsCanBeThrown()
	 * @model containment="true"
	 * @generated
	 */
	EList<TypeReference> getExceptionsCanBeThrown();

	/**
	 * Returns the value of the '<em><b>Exception Table</b></em>' containment reference list.
	 * The list contents are of type {@link nl.utwente.jbcmm.ExceptionTableEntry}.
	 * It is bidirectional and its opposite is '{@link nl.utwente.jbcmm.ExceptionTableEntry#getMethod <em>Method</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exception Table</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exception Table</em>' containment reference list.
	 * @see nl.utwente.jbcmm.JbcmmPackage#getMethod_ExceptionTable()
	 * @see nl.utwente.jbcmm.ExceptionTableEntry#getMethod
	 * @model opposite="method" containment="true"
	 * @generated
	 */
	EList<ExceptionTableEntry> getExceptionTable();

	/**
	 * Returns the value of the '<em><b>Local Variable Table</b></em>' containment reference list.
	 * The list contents are of type {@link nl.utwente.jbcmm.LocalVariableTableEntry}.
	 * It is bidirectional and its opposite is '{@link nl.utwente.jbcmm.LocalVariableTableEntry#getMethod <em>Method</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Variable Table</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Variable Table</em>' containment reference list.
	 * @see nl.utwente.jbcmm.JbcmmPackage#getMethod_LocalVariableTable()
	 * @see nl.utwente.jbcmm.LocalVariableTableEntry#getMethod
	 * @model opposite="method" containment="true"
	 * @generated
	 */
	EList<LocalVariableTableEntry> getLocalVariableTable();

	/**
	 * Returns the value of the '<em><b>Runtime Invisible Parameter Annotations</b></em>' containment reference list.
	 * The list contents are of type {@link nl.utwente.jbcmm.Annotation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Runtime Invisible Parameter Annotations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Runtime Invisible Parameter Annotations</em>' containment reference list.
	 * @see nl.utwente.jbcmm.JbcmmPackage#getMethod_RuntimeInvisibleParameterAnnotations()
	 * @model containment="true"
	 * @generated
	 */
	EList<Annotation> getRuntimeInvisibleParameterAnnotations();

	/**
	 * Returns the value of the '<em><b>Runtime Visible Parameter Annotations</b></em>' containment reference list.
	 * The list contents are of type {@link nl.utwente.jbcmm.Annotation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Runtime Visible Parameter Annotations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Runtime Visible Parameter Annotations</em>' containment reference list.
	 * @see nl.utwente.jbcmm.JbcmmPackage#getMethod_RuntimeVisibleParameterAnnotations()
	 * @model containment="true"
	 * @generated
	 */
	EList<Annotation> getRuntimeVisibleParameterAnnotations();

} // Method
