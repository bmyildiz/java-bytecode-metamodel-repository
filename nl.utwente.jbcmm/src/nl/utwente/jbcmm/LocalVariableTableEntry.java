/**
 */
package nl.utwente.jbcmm;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Local Variable Table Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.LocalVariableTableEntry#getMethod <em>Method</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.LocalVariableTableEntry#getStartInstruction <em>Start Instruction</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.LocalVariableTableEntry#getEndInstruction <em>End Instruction</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.LocalVariableTableEntry#getName <em>Name</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.LocalVariableTableEntry#getIndex <em>Index</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.LocalVariableTableEntry#getSignature <em>Signature</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.LocalVariableTableEntry#getDescriptor <em>Descriptor</em>}</li>
 * </ul>
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getLocalVariableTableEntry()
 * @model
 * @generated
 */
public interface LocalVariableTableEntry extends Identifiable {
	/**
	 * Returns the value of the '<em><b>Method</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link nl.utwente.jbcmm.Method#getLocalVariableTable <em>Local Variable Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Method</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Method</em>' container reference.
	 * @see #setMethod(Method)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getLocalVariableTableEntry_Method()
	 * @see nl.utwente.jbcmm.Method#getLocalVariableTable
	 * @model opposite="localVariableTable" required="true" transient="false"
	 * @generated
	 */
	Method getMethod();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.LocalVariableTableEntry#getMethod <em>Method</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Method</em>' container reference.
	 * @see #getMethod()
	 * @generated
	 */
	void setMethod(Method value);

	/**
	 * Returns the value of the '<em><b>Start Instruction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Instruction</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Instruction</em>' reference.
	 * @see #setStartInstruction(Instruction)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getLocalVariableTableEntry_StartInstruction()
	 * @model required="true"
	 * @generated
	 */
	Instruction getStartInstruction();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.LocalVariableTableEntry#getStartInstruction <em>Start Instruction</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Instruction</em>' reference.
	 * @see #getStartInstruction()
	 * @generated
	 */
	void setStartInstruction(Instruction value);

	/**
	 * Returns the value of the '<em><b>End Instruction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Instruction</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Instruction</em>' reference.
	 * @see #setEndInstruction(Instruction)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getLocalVariableTableEntry_EndInstruction()
	 * @model required="true"
	 * @generated
	 */
	Instruction getEndInstruction();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.LocalVariableTableEntry#getEndInstruction <em>End Instruction</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Instruction</em>' reference.
	 * @see #getEndInstruction()
	 * @generated
	 */
	void setEndInstruction(Instruction value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getLocalVariableTableEntry_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.LocalVariableTableEntry#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Index</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Index</em>' attribute.
	 * @see #setIndex(int)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getLocalVariableTableEntry_Index()
	 * @model required="true"
	 * @generated
	 */
	int getIndex();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.LocalVariableTableEntry#getIndex <em>Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Index</em>' attribute.
	 * @see #getIndex()
	 * @generated
	 */
	void setIndex(int value);

	/**
	 * Returns the value of the '<em><b>Signature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Signature</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Signature</em>' reference.
	 * @see #setSignature(FieldTypeSignature)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getLocalVariableTableEntry_Signature()
	 * @model
	 * @generated
	 */
	FieldTypeSignature getSignature();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.LocalVariableTableEntry#getSignature <em>Signature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Signature</em>' reference.
	 * @see #getSignature()
	 * @generated
	 */
	void setSignature(FieldTypeSignature value);

	/**
	 * Returns the value of the '<em><b>Descriptor</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Descriptor</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Descriptor</em>' containment reference.
	 * @see #setDescriptor(TypeReference)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getLocalVariableTableEntry_Descriptor()
	 * @model containment="true" required="true"
	 * @generated
	 */
	TypeReference getDescriptor();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.LocalVariableTableEntry#getDescriptor <em>Descriptor</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Descriptor</em>' containment reference.
	 * @see #getDescriptor()
	 * @generated
	 */
	void setDescriptor(TypeReference value);

} // LocalVariableTableEntry
