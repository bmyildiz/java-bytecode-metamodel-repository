/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Iastore Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIastoreInstruction()
 * @model
 * @generated
 */
public interface IastoreInstruction extends SimpleInstruction {
} // IastoreInstruction
