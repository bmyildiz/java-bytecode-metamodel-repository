/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>If icmpne Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIf_icmpneInstruction()
 * @model
 * @generated
 */
public interface If_icmpneInstruction extends JumpInstruction {
} // If_icmpneInstruction
