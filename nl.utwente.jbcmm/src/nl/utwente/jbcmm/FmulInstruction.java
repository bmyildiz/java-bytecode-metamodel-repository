/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fmul Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getFmulInstruction()
 * @model
 * @generated
 */
public interface FmulInstruction extends SimpleInstruction {
} // FmulInstruction
