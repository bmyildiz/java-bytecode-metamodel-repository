/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>If icmpeq Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIf_icmpeqInstruction()
 * @model
 * @generated
 */
public interface If_icmpeqInstruction extends JumpInstruction {
} // If_icmpeqInstruction
