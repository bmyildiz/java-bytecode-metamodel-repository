/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dup2 x1 Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getDup2_x1Instruction()
 * @model
 * @generated
 */
public interface Dup2_x1Instruction extends SimpleInstruction {
} // Dup2_x1Instruction
