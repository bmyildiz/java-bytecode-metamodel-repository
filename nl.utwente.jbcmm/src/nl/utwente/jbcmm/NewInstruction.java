/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>New Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getNewInstruction()
 * @model
 * @generated
 */
public interface NewInstruction extends TypeInstruction {
} // NewInstruction
