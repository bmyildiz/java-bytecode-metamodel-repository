/**
 */
package nl.utwente.jbcmm;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.Instruction#getMethod <em>Method</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Instruction#getNextInCodeOrder <em>Next In Code Order</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Instruction#getPreviousInCodeOrder <em>Previous In Code Order</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Instruction#getLinenumber <em>Linenumber</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Instruction#getIndex <em>Index</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Instruction#getOpcode <em>Opcode</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Instruction#getHumanReadable <em>Human Readable</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Instruction#getOutEdges <em>Out Edges</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Instruction#getInEdges <em>In Edges</em>}</li>
 * </ul>
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getInstruction()
 * @model abstract="true"
 * @generated
 */
public interface Instruction extends Identifiable {
	/**
	 * Returns the value of the '<em><b>Method</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link nl.utwente.jbcmm.Method#getInstructions <em>Instructions</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Method</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Method</em>' container reference.
	 * @see #setMethod(Method)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getInstruction_Method()
	 * @see nl.utwente.jbcmm.Method#getInstructions
	 * @model opposite="instructions" required="true" transient="false"
	 * @generated
	 */
	Method getMethod();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Instruction#getMethod <em>Method</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Method</em>' container reference.
	 * @see #getMethod()
	 * @generated
	 */
	void setMethod(Method value);

	/**
	 * Returns the value of the '<em><b>Next In Code Order</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link nl.utwente.jbcmm.Instruction#getPreviousInCodeOrder <em>Previous In Code Order</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Next In Code Order</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Next In Code Order</em>' reference.
	 * @see #setNextInCodeOrder(Instruction)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getInstruction_NextInCodeOrder()
	 * @see nl.utwente.jbcmm.Instruction#getPreviousInCodeOrder
	 * @model opposite="previousInCodeOrder"
	 * @generated
	 */
	Instruction getNextInCodeOrder();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Instruction#getNextInCodeOrder <em>Next In Code Order</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Next In Code Order</em>' reference.
	 * @see #getNextInCodeOrder()
	 * @generated
	 */
	void setNextInCodeOrder(Instruction value);

	/**
	 * Returns the value of the '<em><b>Previous In Code Order</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link nl.utwente.jbcmm.Instruction#getNextInCodeOrder <em>Next In Code Order</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Previous In Code Order</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Previous In Code Order</em>' reference.
	 * @see #setPreviousInCodeOrder(Instruction)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getInstruction_PreviousInCodeOrder()
	 * @see nl.utwente.jbcmm.Instruction#getNextInCodeOrder
	 * @model opposite="nextInCodeOrder"
	 * @generated
	 */
	Instruction getPreviousInCodeOrder();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Instruction#getPreviousInCodeOrder <em>Previous In Code Order</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Previous In Code Order</em>' reference.
	 * @see #getPreviousInCodeOrder()
	 * @generated
	 */
	void setPreviousInCodeOrder(Instruction value);

	/**
	 * Returns the value of the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Linenumber</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Linenumber</em>' attribute.
	 * @see #setLinenumber(int)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getInstruction_Linenumber()
	 * @model
	 * @generated
	 */
	int getLinenumber();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Instruction#getLinenumber <em>Linenumber</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Linenumber</em>' attribute.
	 * @see #getLinenumber()
	 * @generated
	 */
	void setLinenumber(int value);

	/**
	 * Returns the value of the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Index</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Index</em>' attribute.
	 * @see #setIndex(int)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getInstruction_Index()
	 * @model required="true"
	 * @generated
	 */
	int getIndex();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Instruction#getIndex <em>Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Index</em>' attribute.
	 * @see #getIndex()
	 * @generated
	 */
	void setIndex(int value);

	/**
	 * Returns the value of the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Opcode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Opcode</em>' attribute.
	 * @see #setOpcode(String)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getInstruction_Opcode()
	 * @model required="true"
	 * @generated
	 */
	String getOpcode();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Instruction#getOpcode <em>Opcode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Opcode</em>' attribute.
	 * @see #getOpcode()
	 * @generated
	 */
	void setOpcode(String value);

	/**
	 * Returns the value of the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Human Readable</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Human Readable</em>' attribute.
	 * @see #setHumanReadable(String)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getInstruction_HumanReadable()
	 * @model
	 * @generated
	 */
	String getHumanReadable();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Instruction#getHumanReadable <em>Human Readable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Human Readable</em>' attribute.
	 * @see #getHumanReadable()
	 * @generated
	 */
	void setHumanReadable(String value);

	/**
	 * Returns the value of the '<em><b>Out Edges</b></em>' containment reference list.
	 * The list contents are of type {@link nl.utwente.jbcmm.ControlFlowEdge}.
	 * It is bidirectional and its opposite is '{@link nl.utwente.jbcmm.ControlFlowEdge#getStart <em>Start</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Out Edges</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Out Edges</em>' containment reference list.
	 * @see nl.utwente.jbcmm.JbcmmPackage#getInstruction_OutEdges()
	 * @see nl.utwente.jbcmm.ControlFlowEdge#getStart
	 * @model opposite="start" containment="true"
	 * @generated
	 */
	EList<ControlFlowEdge> getOutEdges();

	/**
	 * Returns the value of the '<em><b>In Edges</b></em>' reference list.
	 * The list contents are of type {@link nl.utwente.jbcmm.ControlFlowEdge}.
	 * It is bidirectional and its opposite is '{@link nl.utwente.jbcmm.ControlFlowEdge#getEnd <em>End</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>In Edges</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>In Edges</em>' reference list.
	 * @see nl.utwente.jbcmm.JbcmmPackage#getInstruction_InEdges()
	 * @see nl.utwente.jbcmm.ControlFlowEdge#getEnd
	 * @model opposite="end"
	 * @generated
	 */
	EList<ControlFlowEdge> getInEdges();

} // Instruction
