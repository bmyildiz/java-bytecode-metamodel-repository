/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dneg Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getDnegInstruction()
 * @model
 * @generated
 */
public interface DnegInstruction extends SimpleInstruction {
} // DnegInstruction
