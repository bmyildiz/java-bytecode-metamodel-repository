/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ret Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getRetInstruction()
 * @model
 * @generated
 */
public interface RetInstruction extends VarInstruction {
} // RetInstruction
