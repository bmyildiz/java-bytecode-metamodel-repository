/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>I2s Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getI2sInstruction()
 * @model
 * @generated
 */
public interface I2sInstruction extends SimpleInstruction {
} // I2sInstruction
