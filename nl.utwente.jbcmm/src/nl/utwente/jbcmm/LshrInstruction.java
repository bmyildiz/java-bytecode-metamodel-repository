/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lshr Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getLshrInstruction()
 * @model
 * @generated
 */
public interface LshrInstruction extends SimpleInstruction {
} // LshrInstruction
