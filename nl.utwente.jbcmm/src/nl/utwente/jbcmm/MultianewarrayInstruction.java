/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Multianewarray Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.MultianewarrayInstruction#getTypeReference <em>Type Reference</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.MultianewarrayInstruction#getDims <em>Dims</em>}</li>
 * </ul>
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getMultianewarrayInstruction()
 * @model
 * @generated
 */
public interface MultianewarrayInstruction extends Instruction {
	/**
	 * Returns the value of the '<em><b>Type Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Reference</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Reference</em>' containment reference.
	 * @see #setTypeReference(TypeReference)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getMultianewarrayInstruction_TypeReference()
	 * @model containment="true" required="true"
	 * @generated
	 */
	TypeReference getTypeReference();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.MultianewarrayInstruction#getTypeReference <em>Type Reference</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type Reference</em>' containment reference.
	 * @see #getTypeReference()
	 * @generated
	 */
	void setTypeReference(TypeReference value);

	/**
	 * Returns the value of the '<em><b>Dims</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dims</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dims</em>' attribute.
	 * @see #setDims(int)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getMultianewarrayInstruction_Dims()
	 * @model required="true"
	 * @generated
	 */
	int getDims();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.MultianewarrayInstruction#getDims <em>Dims</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dims</em>' attribute.
	 * @see #getDims()
	 * @generated
	 */
	void setDims(int value);

} // MultianewarrayInstruction
