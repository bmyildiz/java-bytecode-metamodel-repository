/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ifgt Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIfgtInstruction()
 * @model
 * @generated
 */
public interface IfgtInstruction extends JumpInstruction {
} // IfgtInstruction
