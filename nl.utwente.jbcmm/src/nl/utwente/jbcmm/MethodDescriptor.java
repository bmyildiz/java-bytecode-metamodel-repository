/**
 */
package nl.utwente.jbcmm;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Method Descriptor</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.MethodDescriptor#getParameterTypes <em>Parameter Types</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.MethodDescriptor#getResultType <em>Result Type</em>}</li>
 * </ul>
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getMethodDescriptor()
 * @model
 * @generated
 */
public interface MethodDescriptor extends Identifiable {
	/**
	 * Returns the value of the '<em><b>Parameter Types</b></em>' containment reference list.
	 * The list contents are of type {@link nl.utwente.jbcmm.TypeReference}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Types</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Types</em>' containment reference list.
	 * @see nl.utwente.jbcmm.JbcmmPackage#getMethodDescriptor_ParameterTypes()
	 * @model containment="true"
	 * @generated
	 */
	EList<TypeReference> getParameterTypes();

	/**
	 * Returns the value of the '<em><b>Result Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Result Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Result Type</em>' containment reference.
	 * @see #setResultType(TypeReference)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getMethodDescriptor_ResultType()
	 * @model containment="true" required="true"
	 * @generated
	 */
	TypeReference getResultType();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.MethodDescriptor#getResultType <em>Result Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Result Type</em>' containment reference.
	 * @see #getResultType()
	 * @generated
	 */
	void setResultType(TypeReference value);

} // MethodDescriptor
