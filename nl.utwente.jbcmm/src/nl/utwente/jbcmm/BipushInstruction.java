/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bipush Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getBipushInstruction()
 * @model
 * @generated
 */
public interface BipushInstruction extends IntInstruction {
} // BipushInstruction
