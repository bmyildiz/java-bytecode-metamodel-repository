/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lsub Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getLsubInstruction()
 * @model
 * @generated
 */
public interface LsubInstruction extends SimpleInstruction {
} // LsubInstruction
