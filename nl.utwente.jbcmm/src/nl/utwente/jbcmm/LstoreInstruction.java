/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lstore Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getLstoreInstruction()
 * @model
 * @generated
 */
public interface LstoreInstruction extends VarInstruction {
} // LstoreInstruction
