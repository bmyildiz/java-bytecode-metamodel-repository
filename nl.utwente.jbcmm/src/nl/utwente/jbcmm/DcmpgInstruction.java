/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dcmpg Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getDcmpgInstruction()
 * @model
 * @generated
 */
public interface DcmpgInstruction extends SimpleInstruction {
} // DcmpgInstruction
