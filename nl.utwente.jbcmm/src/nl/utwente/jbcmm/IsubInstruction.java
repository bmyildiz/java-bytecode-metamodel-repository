/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Isub Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIsubInstruction()
 * @model
 * @generated
 */
public interface IsubInstruction extends SimpleInstruction {
} // IsubInstruction
