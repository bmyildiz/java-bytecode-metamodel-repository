/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>L2d Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getL2dInstruction()
 * @model
 * @generated
 */
public interface L2dInstruction extends SimpleInstruction {
} // L2dInstruction
