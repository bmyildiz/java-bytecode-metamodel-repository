/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Monitorenter Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getMonitorenterInstruction()
 * @model
 * @generated
 */
public interface MonitorenterInstruction extends SimpleInstruction {
} // MonitorenterInstruction
