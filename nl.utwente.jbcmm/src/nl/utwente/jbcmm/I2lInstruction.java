/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>I2l Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getI2lInstruction()
 * @model
 * @generated
 */
public interface I2lInstruction extends SimpleInstruction {
} // I2lInstruction
