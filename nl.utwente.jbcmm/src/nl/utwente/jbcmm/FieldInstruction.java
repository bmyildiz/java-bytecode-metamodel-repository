/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Field Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.FieldInstruction#getFieldReference <em>Field Reference</em>}</li>
 * </ul>
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getFieldInstruction()
 * @model abstract="true"
 * @generated
 */
public interface FieldInstruction extends Instruction {
	/**
	 * Returns the value of the '<em><b>Field Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Field Reference</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Field Reference</em>' containment reference.
	 * @see #setFieldReference(FieldReference)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getFieldInstruction_FieldReference()
	 * @model containment="true" required="true"
	 * @generated
	 */
	FieldReference getFieldReference();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.FieldInstruction#getFieldReference <em>Field Reference</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Field Reference</em>' containment reference.
	 * @see #getFieldReference()
	 * @generated
	 */
	void setFieldReference(FieldReference value);

} // FieldInstruction
