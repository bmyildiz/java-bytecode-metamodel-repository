/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>If acmpne Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIf_acmpneInstruction()
 * @model
 * @generated
 */
public interface If_acmpneInstruction extends JumpInstruction {
} // If_acmpneInstruction
