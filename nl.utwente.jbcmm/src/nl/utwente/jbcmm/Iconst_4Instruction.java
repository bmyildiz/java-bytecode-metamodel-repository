/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Iconst 4Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIconst_4Instruction()
 * @model
 * @generated
 */
public interface Iconst_4Instruction extends SimpleInstruction {
} // Iconst_4Instruction
