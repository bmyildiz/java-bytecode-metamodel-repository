/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Iushr Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIushrInstruction()
 * @model
 * @generated
 */
public interface IushrInstruction extends SimpleInstruction {
} // IushrInstruction
