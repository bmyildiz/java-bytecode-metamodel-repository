/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dcmpl Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getDcmplInstruction()
 * @model
 * @generated
 */
public interface DcmplInstruction extends SimpleInstruction {
} // DcmplInstruction
