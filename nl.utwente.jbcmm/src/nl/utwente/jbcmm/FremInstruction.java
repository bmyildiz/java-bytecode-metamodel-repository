/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Frem Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getFremInstruction()
 * @model
 * @generated
 */
public interface FremInstruction extends SimpleInstruction {
} // FremInstruction
