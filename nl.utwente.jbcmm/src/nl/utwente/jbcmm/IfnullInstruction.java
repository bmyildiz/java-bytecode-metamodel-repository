/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ifnull Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIfnullInstruction()
 * @model
 * @generated
 */
public interface IfnullInstruction extends JumpInstruction {
} // IfnullInstruction
