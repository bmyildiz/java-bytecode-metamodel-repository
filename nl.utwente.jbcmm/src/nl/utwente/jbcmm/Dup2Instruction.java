/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dup2 Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getDup2Instruction()
 * @model
 * @generated
 */
public interface Dup2Instruction extends SimpleInstruction {
} // Dup2Instruction
