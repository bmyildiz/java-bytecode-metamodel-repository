/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aload Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getAloadInstruction()
 * @model
 * @generated
 */
public interface AloadInstruction extends VarInstruction {
} // AloadInstruction
