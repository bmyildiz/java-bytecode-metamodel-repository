/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Method Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.MethodInstruction#getMethodReference <em>Method Reference</em>}</li>
 * </ul>
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getMethodInstruction()
 * @model abstract="true"
 * @generated
 */
public interface MethodInstruction extends Instruction {
	/**
	 * Returns the value of the '<em><b>Method Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Method Reference</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Method Reference</em>' containment reference.
	 * @see #setMethodReference(MethodReference)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getMethodInstruction_MethodReference()
	 * @model containment="true" required="true"
	 * @generated
	 */
	MethodReference getMethodReference();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.MethodInstruction#getMethodReference <em>Method Reference</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Method Reference</em>' containment reference.
	 * @see #getMethodReference()
	 * @generated
	 */
	void setMethodReference(MethodReference value);

} // MethodInstruction
