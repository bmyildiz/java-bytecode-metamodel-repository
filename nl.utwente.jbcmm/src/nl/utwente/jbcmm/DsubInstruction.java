/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dsub Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getDsubInstruction()
 * @model
 * @generated
 */
public interface DsubInstruction extends SimpleInstruction {
} // DsubInstruction
