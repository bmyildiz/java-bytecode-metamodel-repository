/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lastore Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getLastoreInstruction()
 * @model
 * @generated
 */
public interface LastoreInstruction extends SimpleInstruction {
} // LastoreInstruction
