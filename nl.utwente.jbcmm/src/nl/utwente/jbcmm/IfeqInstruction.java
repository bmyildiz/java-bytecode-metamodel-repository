/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ifeq Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIfeqInstruction()
 * @model
 * @generated
 */
public interface IfeqInstruction extends JumpInstruction {
} // IfeqInstruction
