/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lxor Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getLxorInstruction()
 * @model
 * @generated
 */
public interface LxorInstruction extends SimpleInstruction {
} // LxorInstruction
