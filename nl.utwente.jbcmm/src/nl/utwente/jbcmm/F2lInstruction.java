/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>F2l Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getF2lInstruction()
 * @model
 * @generated
 */
public interface F2lInstruction extends SimpleInstruction {
} // F2lInstruction
