/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>I2c Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getI2cInstruction()
 * @model
 * @generated
 */
public interface I2cInstruction extends SimpleInstruction {
} // I2cInstruction
