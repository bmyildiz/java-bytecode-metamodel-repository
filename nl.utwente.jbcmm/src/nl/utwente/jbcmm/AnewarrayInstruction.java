/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Anewarray Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getAnewarrayInstruction()
 * @model
 * @generated
 */
public interface AnewarrayInstruction extends TypeInstruction {
} // AnewarrayInstruction
