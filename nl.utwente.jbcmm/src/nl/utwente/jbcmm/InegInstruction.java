/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ineg Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getInegInstruction()
 * @model
 * @generated
 */
public interface InegInstruction extends SimpleInstruction {
} // InegInstruction
