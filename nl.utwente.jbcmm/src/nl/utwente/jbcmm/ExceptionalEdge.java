/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Exceptional Edge</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.ExceptionalEdge#getExceptionTableEntry <em>Exception Table Entry</em>}</li>
 * </ul>
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getExceptionalEdge()
 * @model
 * @generated
 */
public interface ExceptionalEdge extends ControlFlowEdge {
	/**
	 * Returns the value of the '<em><b>Exception Table Entry</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exception Table Entry</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exception Table Entry</em>' reference.
	 * @see #setExceptionTableEntry(ExceptionTableEntry)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getExceptionalEdge_ExceptionTableEntry()
	 * @model required="true"
	 * @generated
	 */
	ExceptionTableEntry getExceptionTableEntry();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.ExceptionalEdge#getExceptionTableEntry <em>Exception Table Entry</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exception Table Entry</em>' reference.
	 * @see #getExceptionTableEntry()
	 * @generated
	 */
	void setExceptionTableEntry(ExceptionTableEntry value);

} // ExceptionalEdge
