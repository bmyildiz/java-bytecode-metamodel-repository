/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fstore Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getFstoreInstruction()
 * @model
 * @generated
 */
public interface FstoreInstruction extends VarInstruction {
} // FstoreInstruction
