/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Return Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getReturnInstruction()
 * @model
 * @generated
 */
public interface ReturnInstruction extends SimpleInstruction {
} // ReturnInstruction
