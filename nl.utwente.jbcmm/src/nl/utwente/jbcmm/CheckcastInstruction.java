/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Checkcast Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getCheckcastInstruction()
 * @model
 * @generated
 */
public interface CheckcastInstruction extends TypeInstruction {
} // CheckcastInstruction
