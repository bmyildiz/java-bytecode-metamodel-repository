/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lload Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getLloadInstruction()
 * @model
 * @generated
 */
public interface LloadInstruction extends VarInstruction {
} // LloadInstruction
