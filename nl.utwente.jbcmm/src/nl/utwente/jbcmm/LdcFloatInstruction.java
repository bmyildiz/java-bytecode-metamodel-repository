/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ldc Float Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.LdcFloatInstruction#getConstant <em>Constant</em>}</li>
 * </ul>
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getLdcFloatInstruction()
 * @model
 * @generated
 */
public interface LdcFloatInstruction extends LdcInstruction {
	/**
	 * Returns the value of the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constant</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constant</em>' attribute.
	 * @see #setConstant(float)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getLdcFloatInstruction_Constant()
	 * @model required="true"
	 * @generated
	 */
	float getConstant();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.LdcFloatInstruction#getConstant <em>Constant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constant</em>' attribute.
	 * @see #getConstant()
	 * @generated
	 */
	void setConstant(float value);

} // LdcFloatInstruction
