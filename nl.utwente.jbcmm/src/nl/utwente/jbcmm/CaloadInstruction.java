/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Caload Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getCaloadInstruction()
 * @model
 * @generated
 */
public interface CaloadInstruction extends SimpleInstruction {
} // CaloadInstruction
