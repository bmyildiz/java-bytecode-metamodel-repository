/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fdiv Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getFdivInstruction()
 * @model
 * @generated
 */
public interface FdivInstruction extends SimpleInstruction {
} // FdivInstruction
