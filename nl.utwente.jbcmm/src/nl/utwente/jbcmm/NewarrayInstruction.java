/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Newarray Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getNewarrayInstruction()
 * @model
 * @generated
 */
public interface NewarrayInstruction extends IntInstruction {
} // NewarrayInstruction
