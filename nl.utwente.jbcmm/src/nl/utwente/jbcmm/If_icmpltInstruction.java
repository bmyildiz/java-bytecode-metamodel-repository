/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>If icmplt Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIf_icmpltInstruction()
 * @model
 * @generated
 */
public interface If_icmpltInstruction extends JumpInstruction {
} // If_icmpltInstruction
