/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Putfield Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getPutfieldInstruction()
 * @model
 * @generated
 */
public interface PutfieldInstruction extends FieldInstruction {
} // PutfieldInstruction
