/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Iconst 3Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIconst_3Instruction()
 * @model
 * @generated
 */
public interface Iconst_3Instruction extends SimpleInstruction {
} // Iconst_3Instruction
