/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Invokeinterface Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getInvokeinterfaceInstruction()
 * @model
 * @generated
 */
public interface InvokeinterfaceInstruction extends MethodInstruction {
} // InvokeinterfaceInstruction
