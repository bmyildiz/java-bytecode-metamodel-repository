/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Iconst 2Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIconst_2Instruction()
 * @model
 * @generated
 */
public interface Iconst_2Instruction extends SimpleInstruction {
} // Iconst_2Instruction
