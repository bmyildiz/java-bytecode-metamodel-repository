/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Laload Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getLaloadInstruction()
 * @model
 * @generated
 */
public interface LaloadInstruction extends SimpleInstruction {
} // LaloadInstruction
