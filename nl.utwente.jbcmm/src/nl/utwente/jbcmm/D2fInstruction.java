/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>D2f Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getD2fInstruction()
 * @model
 * @generated
 */
public interface D2fInstruction extends SimpleInstruction {
} // D2fInstruction
