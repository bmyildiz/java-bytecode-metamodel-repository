/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lconst 0Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getLconst_0Instruction()
 * @model
 * @generated
 */
public interface Lconst_0Instruction extends SimpleInstruction {
} // Lconst_0Instruction
