/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dreturn Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getDreturnInstruction()
 * @model
 * @generated
 */
public interface DreturnInstruction extends SimpleInstruction {
} // DreturnInstruction
