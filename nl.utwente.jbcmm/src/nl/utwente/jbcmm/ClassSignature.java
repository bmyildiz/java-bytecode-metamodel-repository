/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class Signature</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getClassSignature()
 * @model
 * @generated
 */
public interface ClassSignature extends Signature {
} // ClassSignature
