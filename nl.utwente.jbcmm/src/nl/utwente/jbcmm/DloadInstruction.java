/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dload Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getDloadInstruction()
 * @model
 * @generated
 */
public interface DloadInstruction extends VarInstruction {
} // DloadInstruction
