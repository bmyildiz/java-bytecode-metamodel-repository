/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lconst 1Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getLconst_1Instruction()
 * @model
 * @generated
 */
public interface Lconst_1Instruction extends SimpleInstruction {
} // Lconst_1Instruction
