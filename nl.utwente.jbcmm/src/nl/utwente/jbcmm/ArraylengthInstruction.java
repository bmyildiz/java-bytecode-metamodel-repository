/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Arraylength Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getArraylengthInstruction()
 * @model
 * @generated
 */
public interface ArraylengthInstruction extends SimpleInstruction {
} // ArraylengthInstruction
