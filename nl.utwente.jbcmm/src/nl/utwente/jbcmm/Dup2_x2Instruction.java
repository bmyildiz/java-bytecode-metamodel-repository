/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dup2 x2 Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getDup2_x2Instruction()
 * @model
 * @generated
 */
public interface Dup2_x2Instruction extends SimpleInstruction {
} // Dup2_x2Instruction
