/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Field Type Signature</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getFieldTypeSignature()
 * @model
 * @generated
 */
public interface FieldTypeSignature extends Signature {
} // FieldTypeSignature
