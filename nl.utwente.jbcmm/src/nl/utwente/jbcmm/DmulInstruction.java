/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dmul Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getDmulInstruction()
 * @model
 * @generated
 */
public interface DmulInstruction extends SimpleInstruction {
} // DmulInstruction
