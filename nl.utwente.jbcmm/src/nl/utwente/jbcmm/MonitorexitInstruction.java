/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Monitorexit Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getMonitorexitInstruction()
 * @model
 * @generated
 */
public interface MonitorexitInstruction extends SimpleInstruction {
} // MonitorexitInstruction
