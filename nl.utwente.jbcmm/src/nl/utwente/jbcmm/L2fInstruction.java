/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>L2f Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getL2fInstruction()
 * @model
 * @generated
 */
public interface L2fInstruction extends SimpleInstruction {
} // L2fInstruction
