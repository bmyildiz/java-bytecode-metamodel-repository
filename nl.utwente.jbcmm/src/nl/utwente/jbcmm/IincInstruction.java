/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Iinc Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.IincInstruction#getIncr <em>Incr</em>}</li>
 * </ul>
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIincInstruction()
 * @model
 * @generated
 */
public interface IincInstruction extends VarInstruction {
	/**
	 * Returns the value of the '<em><b>Incr</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Incr</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Incr</em>' attribute.
	 * @see #setIncr(int)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getIincInstruction_Incr()
	 * @model required="true"
	 * @generated
	 */
	int getIncr();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.IincInstruction#getIncr <em>Incr</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Incr</em>' attribute.
	 * @see #getIncr()
	 * @generated
	 */
	void setIncr(int value);

} // IincInstruction
