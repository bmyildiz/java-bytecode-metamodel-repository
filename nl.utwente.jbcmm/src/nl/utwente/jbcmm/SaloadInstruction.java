/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Saload Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getSaloadInstruction()
 * @model
 * @generated
 */
public interface SaloadInstruction extends SimpleInstruction {
} // SaloadInstruction
