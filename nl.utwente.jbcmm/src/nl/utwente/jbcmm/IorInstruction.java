/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ior Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIorInstruction()
 * @model
 * @generated
 */
public interface IorInstruction extends SimpleInstruction {
} // IorInstruction
