/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Iand Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIandInstruction()
 * @model
 * @generated
 */
public interface IandInstruction extends SimpleInstruction {
} // IandInstruction
