/**
 */
package nl.utwente.jbcmm;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Enum Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.EnumValue#getConstName <em>Const Name</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.EnumValue#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getEnumValue()
 * @model
 * @generated
 */
public interface EnumValue extends Identifiable {
	/**
	 * Returns the value of the '<em><b>Const Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Const Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Const Name</em>' attribute.
	 * @see #setConstName(String)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getEnumValue_ConstName()
	 * @model required="true"
	 * @generated
	 */
	String getConstName();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.EnumValue#getConstName <em>Const Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Const Name</em>' attribute.
	 * @see #getConstName()
	 * @generated
	 */
	void setConstName(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' containment reference.
	 * @see #setType(TypeReference)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getEnumValue_Type()
	 * @model containment="true" required="true"
	 * @generated
	 */
	TypeReference getType();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.EnumValue#getType <em>Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' containment reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(TypeReference value);

} // EnumValue
