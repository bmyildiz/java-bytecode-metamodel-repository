/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Getstatic Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getGetstaticInstruction()
 * @model
 * @generated
 */
public interface GetstaticInstruction extends FieldInstruction {
} // GetstaticInstruction
