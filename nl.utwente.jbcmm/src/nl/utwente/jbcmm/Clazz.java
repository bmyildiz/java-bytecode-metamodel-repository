/**
 */
package nl.utwente.jbcmm;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Clazz</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.Clazz#getProject <em>Project</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Clazz#getMethods <em>Methods</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Clazz#getSubclasses <em>Subclasses</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Clazz#getName <em>Name</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Clazz#getMinorVersion <em>Minor Version</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Clazz#getMajorVersion <em>Major Version</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Clazz#isPublic <em>Public</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Clazz#isFinal <em>Final</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Clazz#isSuper <em>Super</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Clazz#isInterface <em>Interface</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Clazz#isAbstract <em>Abstract</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Clazz#isSynthetic <em>Synthetic</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Clazz#isAnnotation <em>Annotation</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Clazz#isEnum <em>Enum</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Clazz#getSourceFileName <em>Source File Name</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Clazz#isDeprecated <em>Deprecated</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Clazz#getSourceDebugExtension <em>Source Debug Extension</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Clazz#getRuntimeInvisibleAnnotations <em>Runtime Invisible Annotations</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Clazz#getRuntimeVisibleAnnotations <em>Runtime Visible Annotations</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Clazz#getFields <em>Fields</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Clazz#getSuperClass <em>Super Class</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Clazz#getInterfaces <em>Interfaces</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Clazz#getSignature <em>Signature</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Clazz#isPrivate <em>Private</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Clazz#isProtected <em>Protected</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Clazz#getOuterClass <em>Outer Class</em>}</li>
 *   <li>{@link nl.utwente.jbcmm.Clazz#getEnclosingMethod <em>Enclosing Method</em>}</li>
 * </ul>
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getClazz()
 * @model
 * @generated
 */
public interface Clazz extends Identifiable {
	/**
	 * Returns the value of the '<em><b>Project</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link nl.utwente.jbcmm.Project#getClasses <em>Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Project</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Project</em>' container reference.
	 * @see #setProject(Project)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getClazz_Project()
	 * @see nl.utwente.jbcmm.Project#getClasses
	 * @model opposite="classes" transient="false"
	 * @generated
	 */
	Project getProject();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Clazz#getProject <em>Project</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Project</em>' container reference.
	 * @see #getProject()
	 * @generated
	 */
	void setProject(Project value);

	/**
	 * Returns the value of the '<em><b>Methods</b></em>' containment reference list.
	 * The list contents are of type {@link nl.utwente.jbcmm.Method}.
	 * It is bidirectional and its opposite is '{@link nl.utwente.jbcmm.Method#getClass_ <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Methods</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Methods</em>' containment reference list.
	 * @see nl.utwente.jbcmm.JbcmmPackage#getClazz_Methods()
	 * @see nl.utwente.jbcmm.Method#getClass_
	 * @model opposite="class" containment="true"
	 * @generated
	 */
	EList<Method> getMethods();

	/**
	 * Returns the value of the '<em><b>Subclasses</b></em>' reference list.
	 * The list contents are of type {@link nl.utwente.jbcmm.Clazz}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subclasses</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subclasses</em>' reference list.
	 * @see nl.utwente.jbcmm.JbcmmPackage#getClazz_Subclasses()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Clazz> getSubclasses();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getClazz_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Clazz#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Minor Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Minor Version</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Minor Version</em>' attribute.
	 * @see #setMinorVersion(int)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getClazz_MinorVersion()
	 * @model required="true"
	 * @generated
	 */
	int getMinorVersion();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Clazz#getMinorVersion <em>Minor Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Minor Version</em>' attribute.
	 * @see #getMinorVersion()
	 * @generated
	 */
	void setMinorVersion(int value);

	/**
	 * Returns the value of the '<em><b>Major Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Major Version</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Major Version</em>' attribute.
	 * @see #setMajorVersion(int)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getClazz_MajorVersion()
	 * @model required="true"
	 * @generated
	 */
	int getMajorVersion();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Clazz#getMajorVersion <em>Major Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Major Version</em>' attribute.
	 * @see #getMajorVersion()
	 * @generated
	 */
	void setMajorVersion(int value);

	/**
	 * Returns the value of the '<em><b>Public</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Public</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Public</em>' attribute.
	 * @see #setPublic(boolean)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getClazz_Public()
	 * @model required="true"
	 * @generated
	 */
	boolean isPublic();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Clazz#isPublic <em>Public</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Public</em>' attribute.
	 * @see #isPublic()
	 * @generated
	 */
	void setPublic(boolean value);

	/**
	 * Returns the value of the '<em><b>Final</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Final</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Final</em>' attribute.
	 * @see #setFinal(boolean)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getClazz_Final()
	 * @model required="true"
	 * @generated
	 */
	boolean isFinal();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Clazz#isFinal <em>Final</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Final</em>' attribute.
	 * @see #isFinal()
	 * @generated
	 */
	void setFinal(boolean value);

	/**
	 * Returns the value of the '<em><b>Super</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Super</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Super</em>' attribute.
	 * @see #setSuper(boolean)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getClazz_Super()
	 * @model required="true"
	 * @generated
	 */
	boolean isSuper();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Clazz#isSuper <em>Super</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Super</em>' attribute.
	 * @see #isSuper()
	 * @generated
	 */
	void setSuper(boolean value);

	/**
	 * Returns the value of the '<em><b>Interface</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interface</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interface</em>' attribute.
	 * @see #setInterface(boolean)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getClazz_Interface()
	 * @model required="true"
	 * @generated
	 */
	boolean isInterface();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Clazz#isInterface <em>Interface</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interface</em>' attribute.
	 * @see #isInterface()
	 * @generated
	 */
	void setInterface(boolean value);

	/**
	 * Returns the value of the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abstract</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abstract</em>' attribute.
	 * @see #setAbstract(boolean)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getClazz_Abstract()
	 * @model required="true"
	 * @generated
	 */
	boolean isAbstract();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Clazz#isAbstract <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abstract</em>' attribute.
	 * @see #isAbstract()
	 * @generated
	 */
	void setAbstract(boolean value);

	/**
	 * Returns the value of the '<em><b>Synthetic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Synthetic</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Synthetic</em>' attribute.
	 * @see #setSynthetic(boolean)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getClazz_Synthetic()
	 * @model required="true"
	 * @generated
	 */
	boolean isSynthetic();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Clazz#isSynthetic <em>Synthetic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Synthetic</em>' attribute.
	 * @see #isSynthetic()
	 * @generated
	 */
	void setSynthetic(boolean value);

	/**
	 * Returns the value of the '<em><b>Annotation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotation</em>' attribute.
	 * @see #setAnnotation(boolean)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getClazz_Annotation()
	 * @model required="true"
	 * @generated
	 */
	boolean isAnnotation();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Clazz#isAnnotation <em>Annotation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Annotation</em>' attribute.
	 * @see #isAnnotation()
	 * @generated
	 */
	void setAnnotation(boolean value);

	/**
	 * Returns the value of the '<em><b>Enum</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enum</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enum</em>' attribute.
	 * @see #setEnum(boolean)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getClazz_Enum()
	 * @model required="true"
	 * @generated
	 */
	boolean isEnum();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Clazz#isEnum <em>Enum</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Enum</em>' attribute.
	 * @see #isEnum()
	 * @generated
	 */
	void setEnum(boolean value);

	/**
	 * Returns the value of the '<em><b>Source File Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source File Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source File Name</em>' attribute.
	 * @see #setSourceFileName(String)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getClazz_SourceFileName()
	 * @model
	 * @generated
	 */
	String getSourceFileName();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Clazz#getSourceFileName <em>Source File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source File Name</em>' attribute.
	 * @see #getSourceFileName()
	 * @generated
	 */
	void setSourceFileName(String value);

	/**
	 * Returns the value of the '<em><b>Deprecated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deprecated</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deprecated</em>' attribute.
	 * @see #setDeprecated(boolean)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getClazz_Deprecated()
	 * @model
	 * @generated
	 */
	boolean isDeprecated();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Clazz#isDeprecated <em>Deprecated</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Deprecated</em>' attribute.
	 * @see #isDeprecated()
	 * @generated
	 */
	void setDeprecated(boolean value);

	/**
	 * Returns the value of the '<em><b>Source Debug Extension</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Debug Extension</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Debug Extension</em>' attribute.
	 * @see #setSourceDebugExtension(String)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getClazz_SourceDebugExtension()
	 * @model
	 * @generated
	 */
	String getSourceDebugExtension();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Clazz#getSourceDebugExtension <em>Source Debug Extension</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Debug Extension</em>' attribute.
	 * @see #getSourceDebugExtension()
	 * @generated
	 */
	void setSourceDebugExtension(String value);

	/**
	 * Returns the value of the '<em><b>Runtime Invisible Annotations</b></em>' containment reference list.
	 * The list contents are of type {@link nl.utwente.jbcmm.Annotation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Runtime Invisible Annotations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Runtime Invisible Annotations</em>' containment reference list.
	 * @see nl.utwente.jbcmm.JbcmmPackage#getClazz_RuntimeInvisibleAnnotations()
	 * @model containment="true"
	 * @generated
	 */
	EList<Annotation> getRuntimeInvisibleAnnotations();

	/**
	 * Returns the value of the '<em><b>Runtime Visible Annotations</b></em>' containment reference list.
	 * The list contents are of type {@link nl.utwente.jbcmm.Annotation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Runtime Visible Annotations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Runtime Visible Annotations</em>' containment reference list.
	 * @see nl.utwente.jbcmm.JbcmmPackage#getClazz_RuntimeVisibleAnnotations()
	 * @model containment="true"
	 * @generated
	 */
	EList<Annotation> getRuntimeVisibleAnnotations();

	/**
	 * Returns the value of the '<em><b>Fields</b></em>' containment reference list.
	 * The list contents are of type {@link nl.utwente.jbcmm.Field}.
	 * It is bidirectional and its opposite is '{@link nl.utwente.jbcmm.Field#getClass_ <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fields</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fields</em>' containment reference list.
	 * @see nl.utwente.jbcmm.JbcmmPackage#getClazz_Fields()
	 * @see nl.utwente.jbcmm.Field#getClass_
	 * @model opposite="class" containment="true"
	 * @generated
	 */
	EList<Field> getFields();

	/**
	 * Returns the value of the '<em><b>Super Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Super Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Super Class</em>' containment reference.
	 * @see #setSuperClass(TypeReference)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getClazz_SuperClass()
	 * @model containment="true" required="true"
	 * @generated
	 */
	TypeReference getSuperClass();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Clazz#getSuperClass <em>Super Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Super Class</em>' containment reference.
	 * @see #getSuperClass()
	 * @generated
	 */
	void setSuperClass(TypeReference value);

	/**
	 * Returns the value of the '<em><b>Interfaces</b></em>' containment reference list.
	 * The list contents are of type {@link nl.utwente.jbcmm.TypeReference}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interfaces</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interfaces</em>' containment reference list.
	 * @see nl.utwente.jbcmm.JbcmmPackage#getClazz_Interfaces()
	 * @model containment="true"
	 * @generated
	 */
	EList<TypeReference> getInterfaces();

	/**
	 * Returns the value of the '<em><b>Signature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Signature</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Signature</em>' containment reference.
	 * @see #setSignature(ClassSignature)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getClazz_Signature()
	 * @model containment="true"
	 * @generated
	 */
	ClassSignature getSignature();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Clazz#getSignature <em>Signature</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Signature</em>' containment reference.
	 * @see #getSignature()
	 * @generated
	 */
	void setSignature(ClassSignature value);

	/**
	 * Returns the value of the '<em><b>Private</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Private</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Private</em>' attribute.
	 * @see #setPrivate(boolean)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getClazz_Private()
	 * @model required="true"
	 * @generated
	 */
	boolean isPrivate();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Clazz#isPrivate <em>Private</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Private</em>' attribute.
	 * @see #isPrivate()
	 * @generated
	 */
	void setPrivate(boolean value);

	/**
	 * Returns the value of the '<em><b>Protected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Protected</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Protected</em>' attribute.
	 * @see #setProtected(boolean)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getClazz_Protected()
	 * @model required="true"
	 * @generated
	 */
	boolean isProtected();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Clazz#isProtected <em>Protected</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Protected</em>' attribute.
	 * @see #isProtected()
	 * @generated
	 */
	void setProtected(boolean value);

	/**
	 * Returns the value of the '<em><b>Outer Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outer Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outer Class</em>' containment reference.
	 * @see #setOuterClass(TypeReference)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getClazz_OuterClass()
	 * @model containment="true"
	 * @generated
	 */
	TypeReference getOuterClass();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Clazz#getOuterClass <em>Outer Class</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Outer Class</em>' containment reference.
	 * @see #getOuterClass()
	 * @generated
	 */
	void setOuterClass(TypeReference value);

	/**
	 * Returns the value of the '<em><b>Enclosing Method</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enclosing Method</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enclosing Method</em>' containment reference.
	 * @see #setEnclosingMethod(MethodReference)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getClazz_EnclosingMethod()
	 * @model containment="true"
	 * @generated
	 */
	MethodReference getEnclosingMethod();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.Clazz#getEnclosingMethod <em>Enclosing Method</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Enclosing Method</em>' containment reference.
	 * @see #getEnclosingMethod()
	 * @generated
	 */
	void setEnclosingMethod(MethodReference value);

} // Clazz
