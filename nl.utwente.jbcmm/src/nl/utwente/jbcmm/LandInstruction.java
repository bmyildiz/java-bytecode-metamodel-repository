/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Land Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getLandInstruction()
 * @model
 * @generated
 */
public interface LandInstruction extends SimpleInstruction {
} // LandInstruction
