/**
 */
package nl.utwente.jbcmm;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see nl.utwente.jbcmm.JbcmmFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/OCL/Import ecore='http://www.eclipse.org/emf/2002/Ecore'"
 * @generated
 */
public interface JbcmmPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "jbcmm";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://utwente.nl/jbcmm";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "nl.utwente.jbcmm";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	JbcmmPackage eINSTANCE = nl.utwente.jbcmm.impl.JbcmmPackageImpl.init();

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.IdentifiableImpl <em>Identifiable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.IdentifiableImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIdentifiable()
	 * @generated
	 */
	int IDENTIFIABLE = 0;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIABLE__UUID = 0;

	/**
	 * The number of structural features of the '<em>Identifiable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIABLE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Identifiable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDENTIFIABLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.ProjectImpl <em>Project</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.ProjectImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getProject()
	 * @generated
	 */
	int PROJECT = 1;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT__UUID = IDENTIFIABLE__UUID;

	/**
	 * The feature id for the '<em><b>Classes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT__CLASSES = IDENTIFIABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Main Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT__MAIN_CLASS = IDENTIFIABLE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Project</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT_FEATURE_COUNT = IDENTIFIABLE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Project</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT_OPERATION_COUNT = IDENTIFIABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.ClazzImpl <em>Clazz</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.ClazzImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getClazz()
	 * @generated
	 */
	int CLAZZ = 2;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAZZ__UUID = IDENTIFIABLE__UUID;

	/**
	 * The feature id for the '<em><b>Project</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAZZ__PROJECT = IDENTIFIABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Methods</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAZZ__METHODS = IDENTIFIABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Subclasses</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAZZ__SUBCLASSES = IDENTIFIABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAZZ__NAME = IDENTIFIABLE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Minor Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAZZ__MINOR_VERSION = IDENTIFIABLE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Major Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAZZ__MAJOR_VERSION = IDENTIFIABLE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Public</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAZZ__PUBLIC = IDENTIFIABLE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Final</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAZZ__FINAL = IDENTIFIABLE_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Super</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAZZ__SUPER = IDENTIFIABLE_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Interface</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAZZ__INTERFACE = IDENTIFIABLE_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAZZ__ABSTRACT = IDENTIFIABLE_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Synthetic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAZZ__SYNTHETIC = IDENTIFIABLE_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAZZ__ANNOTATION = IDENTIFIABLE_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Enum</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAZZ__ENUM = IDENTIFIABLE_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Source File Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAZZ__SOURCE_FILE_NAME = IDENTIFIABLE_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Deprecated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAZZ__DEPRECATED = IDENTIFIABLE_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Source Debug Extension</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAZZ__SOURCE_DEBUG_EXTENSION = IDENTIFIABLE_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Runtime Invisible Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAZZ__RUNTIME_INVISIBLE_ANNOTATIONS = IDENTIFIABLE_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>Runtime Visible Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAZZ__RUNTIME_VISIBLE_ANNOTATIONS = IDENTIFIABLE_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>Fields</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAZZ__FIELDS = IDENTIFIABLE_FEATURE_COUNT + 19;

	/**
	 * The feature id for the '<em><b>Super Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAZZ__SUPER_CLASS = IDENTIFIABLE_FEATURE_COUNT + 20;

	/**
	 * The feature id for the '<em><b>Interfaces</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAZZ__INTERFACES = IDENTIFIABLE_FEATURE_COUNT + 21;

	/**
	 * The feature id for the '<em><b>Signature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAZZ__SIGNATURE = IDENTIFIABLE_FEATURE_COUNT + 22;

	/**
	 * The feature id for the '<em><b>Private</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAZZ__PRIVATE = IDENTIFIABLE_FEATURE_COUNT + 23;

	/**
	 * The feature id for the '<em><b>Protected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAZZ__PROTECTED = IDENTIFIABLE_FEATURE_COUNT + 24;

	/**
	 * The feature id for the '<em><b>Outer Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAZZ__OUTER_CLASS = IDENTIFIABLE_FEATURE_COUNT + 25;

	/**
	 * The feature id for the '<em><b>Enclosing Method</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAZZ__ENCLOSING_METHOD = IDENTIFIABLE_FEATURE_COUNT + 26;

	/**
	 * The number of structural features of the '<em>Clazz</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAZZ_FEATURE_COUNT = IDENTIFIABLE_FEATURE_COUNT + 27;

	/**
	 * The number of operations of the '<em>Clazz</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLAZZ_OPERATION_COUNT = IDENTIFIABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.SignatureImpl <em>Signature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.SignatureImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getSignature()
	 * @generated
	 */
	int SIGNATURE = 3;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNATURE__UUID = IDENTIFIABLE__UUID;

	/**
	 * The feature id for the '<em><b>Signature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNATURE__SIGNATURE = IDENTIFIABLE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Signature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNATURE_FEATURE_COUNT = IDENTIFIABLE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Signature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNATURE_OPERATION_COUNT = IDENTIFIABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.ClassSignatureImpl <em>Class Signature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.ClassSignatureImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getClassSignature()
	 * @generated
	 */
	int CLASS_SIGNATURE = 4;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_SIGNATURE__UUID = SIGNATURE__UUID;

	/**
	 * The feature id for the '<em><b>Signature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_SIGNATURE__SIGNATURE = SIGNATURE__SIGNATURE;

	/**
	 * The number of structural features of the '<em>Class Signature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_SIGNATURE_FEATURE_COUNT = SIGNATURE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Class Signature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_SIGNATURE_OPERATION_COUNT = SIGNATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.MethodSignatureImpl <em>Method Signature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.MethodSignatureImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getMethodSignature()
	 * @generated
	 */
	int METHOD_SIGNATURE = 5;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_SIGNATURE__UUID = SIGNATURE__UUID;

	/**
	 * The feature id for the '<em><b>Signature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_SIGNATURE__SIGNATURE = SIGNATURE__SIGNATURE;

	/**
	 * The number of structural features of the '<em>Method Signature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_SIGNATURE_FEATURE_COUNT = SIGNATURE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Method Signature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_SIGNATURE_OPERATION_COUNT = SIGNATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.FieldTypeSignatureImpl <em>Field Type Signature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.FieldTypeSignatureImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getFieldTypeSignature()
	 * @generated
	 */
	int FIELD_TYPE_SIGNATURE = 6;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_TYPE_SIGNATURE__UUID = SIGNATURE__UUID;

	/**
	 * The feature id for the '<em><b>Signature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_TYPE_SIGNATURE__SIGNATURE = SIGNATURE__SIGNATURE;

	/**
	 * The number of structural features of the '<em>Field Type Signature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_TYPE_SIGNATURE_FEATURE_COUNT = SIGNATURE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Field Type Signature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_TYPE_SIGNATURE_OPERATION_COUNT = SIGNATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.TypeReferenceImpl <em>Type Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.TypeReferenceImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getTypeReference()
	 * @generated
	 */
	int TYPE_REFERENCE = 7;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_REFERENCE__UUID = IDENTIFIABLE__UUID;

	/**
	 * The feature id for the '<em><b>Referenced Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_REFERENCE__REFERENCED_CLASS = IDENTIFIABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type Descriptor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_REFERENCE__TYPE_DESCRIPTOR = IDENTIFIABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Runtime Invisible Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_REFERENCE__RUNTIME_INVISIBLE_ANNOTATIONS = IDENTIFIABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Runtime Visible Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_REFERENCE__RUNTIME_VISIBLE_ANNOTATIONS = IDENTIFIABLE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Type Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_REFERENCE_FEATURE_COUNT = IDENTIFIABLE_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Type Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_REFERENCE_OPERATION_COUNT = IDENTIFIABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.MethodDescriptorImpl <em>Method Descriptor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.MethodDescriptorImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getMethodDescriptor()
	 * @generated
	 */
	int METHOD_DESCRIPTOR = 8;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_DESCRIPTOR__UUID = IDENTIFIABLE__UUID;

	/**
	 * The feature id for the '<em><b>Parameter Types</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_DESCRIPTOR__PARAMETER_TYPES = IDENTIFIABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Result Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_DESCRIPTOR__RESULT_TYPE = IDENTIFIABLE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Method Descriptor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_DESCRIPTOR_FEATURE_COUNT = IDENTIFIABLE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Method Descriptor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_DESCRIPTOR_OPERATION_COUNT = IDENTIFIABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.MethodReferenceImpl <em>Method Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.MethodReferenceImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getMethodReference()
	 * @generated
	 */
	int METHOD_REFERENCE = 9;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_REFERENCE__UUID = IDENTIFIABLE__UUID;

	/**
	 * The feature id for the '<em><b>Declaring Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_REFERENCE__DECLARING_CLASS = IDENTIFIABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_REFERENCE__NAME = IDENTIFIABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Descriptor</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_REFERENCE__DESCRIPTOR = IDENTIFIABLE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Method Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_REFERENCE_FEATURE_COUNT = IDENTIFIABLE_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Method Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_REFERENCE_OPERATION_COUNT = IDENTIFIABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.FieldReferenceImpl <em>Field Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.FieldReferenceImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getFieldReference()
	 * @generated
	 */
	int FIELD_REFERENCE = 10;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_REFERENCE__UUID = IDENTIFIABLE__UUID;

	/**
	 * The feature id for the '<em><b>Declaring Class</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_REFERENCE__DECLARING_CLASS = IDENTIFIABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_REFERENCE__NAME = IDENTIFIABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Descriptor</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_REFERENCE__DESCRIPTOR = IDENTIFIABLE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Field Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_REFERENCE_FEATURE_COUNT = IDENTIFIABLE_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Field Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_REFERENCE_OPERATION_COUNT = IDENTIFIABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.FieldImpl <em>Field</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.FieldImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getField()
	 * @generated
	 */
	int FIELD = 11;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD__UUID = IDENTIFIABLE__UUID;

	/**
	 * The feature id for the '<em><b>Public</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD__PUBLIC = IDENTIFIABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Private</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD__PRIVATE = IDENTIFIABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Protected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD__PROTECTED = IDENTIFIABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Static</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD__STATIC = IDENTIFIABLE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Final</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD__FINAL = IDENTIFIABLE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Volatile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD__VOLATILE = IDENTIFIABLE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Transient</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD__TRANSIENT = IDENTIFIABLE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Synthetic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD__SYNTHETIC = IDENTIFIABLE_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Enum</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD__ENUM = IDENTIFIABLE_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD__NAME = IDENTIFIABLE_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Constant Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD__CONSTANT_VALUE = IDENTIFIABLE_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Deprecated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD__DEPRECATED = IDENTIFIABLE_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Runtime Invisible Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD__RUNTIME_INVISIBLE_ANNOTATIONS = IDENTIFIABLE_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Runtime Visible Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD__RUNTIME_VISIBLE_ANNOTATIONS = IDENTIFIABLE_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Descriptor</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD__DESCRIPTOR = IDENTIFIABLE_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Signature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD__SIGNATURE = IDENTIFIABLE_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Class</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD__CLASS = IDENTIFIABLE_FEATURE_COUNT + 16;

	/**
	 * The number of structural features of the '<em>Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_FEATURE_COUNT = IDENTIFIABLE_FEATURE_COUNT + 17;

	/**
	 * The number of operations of the '<em>Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OPERATION_COUNT = IDENTIFIABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.AnnotationImpl <em>Annotation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.AnnotationImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getAnnotation()
	 * @generated
	 */
	int ANNOTATION = 12;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION__UUID = IDENTIFIABLE__UUID;

	/**
	 * The feature id for the '<em><b>Element Value Pairs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION__ELEMENT_VALUE_PAIRS = IDENTIFIABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION__TYPE = IDENTIFIABLE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_FEATURE_COUNT = IDENTIFIABLE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_OPERATION_COUNT = IDENTIFIABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.ElementValuePairImpl <em>Element Value Pair</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.ElementValuePairImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getElementValuePair()
	 * @generated
	 */
	int ELEMENT_VALUE_PAIR = 13;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_VALUE_PAIR__UUID = IDENTIFIABLE__UUID;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_VALUE_PAIR__VALUE = IDENTIFIABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_VALUE_PAIR__NAME = IDENTIFIABLE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Element Value Pair</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_VALUE_PAIR_FEATURE_COUNT = IDENTIFIABLE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Element Value Pair</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_VALUE_PAIR_OPERATION_COUNT = IDENTIFIABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.ElementValueImpl <em>Element Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.ElementValueImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getElementValue()
	 * @generated
	 */
	int ELEMENT_VALUE = 14;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_VALUE__UUID = IDENTIFIABLE__UUID;

	/**
	 * The feature id for the '<em><b>Constant Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_VALUE__CONSTANT_VALUE = IDENTIFIABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Enum Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_VALUE__ENUM_VALUE = IDENTIFIABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Annotation Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_VALUE__ANNOTATION_VALUE = IDENTIFIABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Array Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_VALUE__ARRAY_VALUE = IDENTIFIABLE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Class Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_VALUE__CLASS_VALUE = IDENTIFIABLE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Element Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_VALUE_FEATURE_COUNT = IDENTIFIABLE_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Element Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_VALUE_OPERATION_COUNT = IDENTIFIABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.ElementaryValueImpl <em>Elementary Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.ElementaryValueImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getElementaryValue()
	 * @generated
	 */
	int ELEMENTARY_VALUE = 15;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENTARY_VALUE__UUID = IDENTIFIABLE__UUID;

	/**
	 * The number of structural features of the '<em>Elementary Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENTARY_VALUE_FEATURE_COUNT = IDENTIFIABLE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Elementary Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENTARY_VALUE_OPERATION_COUNT = IDENTIFIABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.BooleanValueImpl <em>Boolean Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.BooleanValueImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getBooleanValue()
	 * @generated
	 */
	int BOOLEAN_VALUE = 16;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_VALUE__UUID = ELEMENTARY_VALUE__UUID;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_VALUE__VALUE = ELEMENTARY_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Boolean Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_VALUE_FEATURE_COUNT = ELEMENTARY_VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Boolean Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_VALUE_OPERATION_COUNT = ELEMENTARY_VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.CharValueImpl <em>Char Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.CharValueImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getCharValue()
	 * @generated
	 */
	int CHAR_VALUE = 17;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHAR_VALUE__UUID = ELEMENTARY_VALUE__UUID;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHAR_VALUE__VALUE = ELEMENTARY_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Char Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHAR_VALUE_FEATURE_COUNT = ELEMENTARY_VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Char Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHAR_VALUE_OPERATION_COUNT = ELEMENTARY_VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.ByteValueImpl <em>Byte Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.ByteValueImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getByteValue()
	 * @generated
	 */
	int BYTE_VALUE = 18;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BYTE_VALUE__UUID = ELEMENTARY_VALUE__UUID;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BYTE_VALUE__VALUE = ELEMENTARY_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Byte Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BYTE_VALUE_FEATURE_COUNT = ELEMENTARY_VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Byte Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BYTE_VALUE_OPERATION_COUNT = ELEMENTARY_VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.ShortValueImpl <em>Short Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.ShortValueImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getShortValue()
	 * @generated
	 */
	int SHORT_VALUE = 19;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHORT_VALUE__UUID = ELEMENTARY_VALUE__UUID;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHORT_VALUE__VALUE = ELEMENTARY_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Short Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHORT_VALUE_FEATURE_COUNT = ELEMENTARY_VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Short Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHORT_VALUE_OPERATION_COUNT = ELEMENTARY_VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.IntValueImpl <em>Int Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.IntValueImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIntValue()
	 * @generated
	 */
	int INT_VALUE = 20;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_VALUE__UUID = ELEMENTARY_VALUE__UUID;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_VALUE__VALUE = ELEMENTARY_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Int Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_VALUE_FEATURE_COUNT = ELEMENTARY_VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Int Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_VALUE_OPERATION_COUNT = ELEMENTARY_VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.LongValueImpl <em>Long Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.LongValueImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getLongValue()
	 * @generated
	 */
	int LONG_VALUE = 21;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LONG_VALUE__UUID = ELEMENTARY_VALUE__UUID;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LONG_VALUE__VALUE = ELEMENTARY_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Long Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LONG_VALUE_FEATURE_COUNT = ELEMENTARY_VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Long Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LONG_VALUE_OPERATION_COUNT = ELEMENTARY_VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.FloatValueImpl <em>Float Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.FloatValueImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getFloatValue()
	 * @generated
	 */
	int FLOAT_VALUE = 22;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_VALUE__UUID = ELEMENTARY_VALUE__UUID;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_VALUE__VALUE = ELEMENTARY_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Float Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_VALUE_FEATURE_COUNT = ELEMENTARY_VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Float Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_VALUE_OPERATION_COUNT = ELEMENTARY_VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.DoubleValueImpl <em>Double Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.DoubleValueImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getDoubleValue()
	 * @generated
	 */
	int DOUBLE_VALUE = 23;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_VALUE__UUID = ELEMENTARY_VALUE__UUID;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_VALUE__VALUE = ELEMENTARY_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Double Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_VALUE_FEATURE_COUNT = ELEMENTARY_VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Double Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_VALUE_OPERATION_COUNT = ELEMENTARY_VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.StringValueImpl <em>String Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.StringValueImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getStringValue()
	 * @generated
	 */
	int STRING_VALUE = 24;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_VALUE__UUID = ELEMENTARY_VALUE__UUID;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_VALUE__VALUE = ELEMENTARY_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>String Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_VALUE_FEATURE_COUNT = ELEMENTARY_VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>String Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_VALUE_OPERATION_COUNT = ELEMENTARY_VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.EnumValueImpl <em>Enum Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.EnumValueImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getEnumValue()
	 * @generated
	 */
	int ENUM_VALUE = 25;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_VALUE__UUID = IDENTIFIABLE__UUID;

	/**
	 * The feature id for the '<em><b>Const Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_VALUE__CONST_NAME = IDENTIFIABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_VALUE__TYPE = IDENTIFIABLE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Enum Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_VALUE_FEATURE_COUNT = IDENTIFIABLE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Enum Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUM_VALUE_OPERATION_COUNT = IDENTIFIABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.MethodImpl <em>Method</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.MethodImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getMethod()
	 * @generated
	 */
	int METHOD = 26;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__UUID = IDENTIFIABLE__UUID;

	/**
	 * The feature id for the '<em><b>Class</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__CLASS = IDENTIFIABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__NAME = IDENTIFIABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Instructions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__INSTRUCTIONS = IDENTIFIABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>First Instruction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__FIRST_INSTRUCTION = IDENTIFIABLE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Public</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__PUBLIC = IDENTIFIABLE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Private</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__PRIVATE = IDENTIFIABLE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Protected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__PROTECTED = IDENTIFIABLE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Static</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__STATIC = IDENTIFIABLE_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Final</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__FINAL = IDENTIFIABLE_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Synthetic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__SYNTHETIC = IDENTIFIABLE_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Synchronized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__SYNCHRONIZED = IDENTIFIABLE_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Bridge</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__BRIDGE = IDENTIFIABLE_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Var Args</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__VAR_ARGS = IDENTIFIABLE_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Native</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__NATIVE = IDENTIFIABLE_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__ABSTRACT = IDENTIFIABLE_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Strict</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__STRICT = IDENTIFIABLE_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Deprecated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__DEPRECATED = IDENTIFIABLE_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Runtime Invisible Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__RUNTIME_INVISIBLE_ANNOTATIONS = IDENTIFIABLE_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>Runtime Visible Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__RUNTIME_VISIBLE_ANNOTATIONS = IDENTIFIABLE_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>Annotation Default Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__ANNOTATION_DEFAULT_VALUE = IDENTIFIABLE_FEATURE_COUNT + 19;

	/**
	 * The feature id for the '<em><b>Descriptor</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__DESCRIPTOR = IDENTIFIABLE_FEATURE_COUNT + 20;

	/**
	 * The feature id for the '<em><b>Signature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__SIGNATURE = IDENTIFIABLE_FEATURE_COUNT + 21;

	/**
	 * The feature id for the '<em><b>Exceptions Can Be Thrown</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__EXCEPTIONS_CAN_BE_THROWN = IDENTIFIABLE_FEATURE_COUNT + 22;

	/**
	 * The feature id for the '<em><b>Exception Table</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__EXCEPTION_TABLE = IDENTIFIABLE_FEATURE_COUNT + 23;

	/**
	 * The feature id for the '<em><b>Local Variable Table</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__LOCAL_VARIABLE_TABLE = IDENTIFIABLE_FEATURE_COUNT + 24;

	/**
	 * The feature id for the '<em><b>Runtime Invisible Parameter Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__RUNTIME_INVISIBLE_PARAMETER_ANNOTATIONS = IDENTIFIABLE_FEATURE_COUNT + 25;

	/**
	 * The feature id for the '<em><b>Runtime Visible Parameter Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__RUNTIME_VISIBLE_PARAMETER_ANNOTATIONS = IDENTIFIABLE_FEATURE_COUNT + 26;

	/**
	 * The number of structural features of the '<em>Method</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_FEATURE_COUNT = IDENTIFIABLE_FEATURE_COUNT + 27;

	/**
	 * The number of operations of the '<em>Method</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_OPERATION_COUNT = IDENTIFIABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.LocalVariableTableEntryImpl <em>Local Variable Table Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.LocalVariableTableEntryImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getLocalVariableTableEntry()
	 * @generated
	 */
	int LOCAL_VARIABLE_TABLE_ENTRY = 27;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_VARIABLE_TABLE_ENTRY__UUID = IDENTIFIABLE__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_VARIABLE_TABLE_ENTRY__METHOD = IDENTIFIABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Start Instruction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_VARIABLE_TABLE_ENTRY__START_INSTRUCTION = IDENTIFIABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>End Instruction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_VARIABLE_TABLE_ENTRY__END_INSTRUCTION = IDENTIFIABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_VARIABLE_TABLE_ENTRY__NAME = IDENTIFIABLE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_VARIABLE_TABLE_ENTRY__INDEX = IDENTIFIABLE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Signature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_VARIABLE_TABLE_ENTRY__SIGNATURE = IDENTIFIABLE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Descriptor</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_VARIABLE_TABLE_ENTRY__DESCRIPTOR = IDENTIFIABLE_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Local Variable Table Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_VARIABLE_TABLE_ENTRY_FEATURE_COUNT = IDENTIFIABLE_FEATURE_COUNT + 7;

	/**
	 * The number of operations of the '<em>Local Variable Table Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_VARIABLE_TABLE_ENTRY_OPERATION_COUNT = IDENTIFIABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.ExceptionTableEntryImpl <em>Exception Table Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.ExceptionTableEntryImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getExceptionTableEntry()
	 * @generated
	 */
	int EXCEPTION_TABLE_ENTRY = 28;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_TABLE_ENTRY__UUID = IDENTIFIABLE__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_TABLE_ENTRY__METHOD = IDENTIFIABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Start Instruction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_TABLE_ENTRY__START_INSTRUCTION = IDENTIFIABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>End Instruction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_TABLE_ENTRY__END_INSTRUCTION = IDENTIFIABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Handler Instruction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_TABLE_ENTRY__HANDLER_INSTRUCTION = IDENTIFIABLE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Catch Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_TABLE_ENTRY__CATCH_TYPE = IDENTIFIABLE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Exception Table Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_TABLE_ENTRY_FEATURE_COUNT = IDENTIFIABLE_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Exception Table Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_TABLE_ENTRY_OPERATION_COUNT = IDENTIFIABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.ControlFlowEdgeImpl <em>Control Flow Edge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.ControlFlowEdgeImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getControlFlowEdge()
	 * @generated
	 */
	int CONTROL_FLOW_EDGE = 29;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_EDGE__UUID = IDENTIFIABLE__UUID;

	/**
	 * The feature id for the '<em><b>Start</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_EDGE__START = IDENTIFIABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>End</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_EDGE__END = IDENTIFIABLE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Control Flow Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_EDGE_FEATURE_COUNT = IDENTIFIABLE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Control Flow Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_EDGE_OPERATION_COUNT = IDENTIFIABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.UnconditionalEdgeImpl <em>Unconditional Edge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.UnconditionalEdgeImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getUnconditionalEdge()
	 * @generated
	 */
	int UNCONDITIONAL_EDGE = 30;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNCONDITIONAL_EDGE__UUID = CONTROL_FLOW_EDGE__UUID;

	/**
	 * The feature id for the '<em><b>Start</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNCONDITIONAL_EDGE__START = CONTROL_FLOW_EDGE__START;

	/**
	 * The feature id for the '<em><b>End</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNCONDITIONAL_EDGE__END = CONTROL_FLOW_EDGE__END;

	/**
	 * The number of structural features of the '<em>Unconditional Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNCONDITIONAL_EDGE_FEATURE_COUNT = CONTROL_FLOW_EDGE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Unconditional Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNCONDITIONAL_EDGE_OPERATION_COUNT = CONTROL_FLOW_EDGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.ConditionalEdgeImpl <em>Conditional Edge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.ConditionalEdgeImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getConditionalEdge()
	 * @generated
	 */
	int CONDITIONAL_EDGE = 31;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_EDGE__UUID = CONTROL_FLOW_EDGE__UUID;

	/**
	 * The feature id for the '<em><b>Start</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_EDGE__START = CONTROL_FLOW_EDGE__START;

	/**
	 * The feature id for the '<em><b>End</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_EDGE__END = CONTROL_FLOW_EDGE__END;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_EDGE__CONDITION = CONTROL_FLOW_EDGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Conditional Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_EDGE_FEATURE_COUNT = CONTROL_FLOW_EDGE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Conditional Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_EDGE_OPERATION_COUNT = CONTROL_FLOW_EDGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.SwitchCaseEdgeImpl <em>Switch Case Edge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.SwitchCaseEdgeImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getSwitchCaseEdge()
	 * @generated
	 */
	int SWITCH_CASE_EDGE = 32;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_CASE_EDGE__UUID = CONTROL_FLOW_EDGE__UUID;

	/**
	 * The feature id for the '<em><b>Start</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_CASE_EDGE__START = CONTROL_FLOW_EDGE__START;

	/**
	 * The feature id for the '<em><b>End</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_CASE_EDGE__END = CONTROL_FLOW_EDGE__END;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_CASE_EDGE__CONDITION = CONTROL_FLOW_EDGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Switch Case Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_CASE_EDGE_FEATURE_COUNT = CONTROL_FLOW_EDGE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Switch Case Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_CASE_EDGE_OPERATION_COUNT = CONTROL_FLOW_EDGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.SwitchDefaultEdgeImpl <em>Switch Default Edge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.SwitchDefaultEdgeImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getSwitchDefaultEdge()
	 * @generated
	 */
	int SWITCH_DEFAULT_EDGE = 33;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_DEFAULT_EDGE__UUID = CONTROL_FLOW_EDGE__UUID;

	/**
	 * The feature id for the '<em><b>Start</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_DEFAULT_EDGE__START = CONTROL_FLOW_EDGE__START;

	/**
	 * The feature id for the '<em><b>End</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_DEFAULT_EDGE__END = CONTROL_FLOW_EDGE__END;

	/**
	 * The number of structural features of the '<em>Switch Default Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_DEFAULT_EDGE_FEATURE_COUNT = CONTROL_FLOW_EDGE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Switch Default Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_DEFAULT_EDGE_OPERATION_COUNT = CONTROL_FLOW_EDGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.ExceptionalEdgeImpl <em>Exceptional Edge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.ExceptionalEdgeImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getExceptionalEdge()
	 * @generated
	 */
	int EXCEPTIONAL_EDGE = 34;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTIONAL_EDGE__UUID = CONTROL_FLOW_EDGE__UUID;

	/**
	 * The feature id for the '<em><b>Start</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTIONAL_EDGE__START = CONTROL_FLOW_EDGE__START;

	/**
	 * The feature id for the '<em><b>End</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTIONAL_EDGE__END = CONTROL_FLOW_EDGE__END;

	/**
	 * The feature id for the '<em><b>Exception Table Entry</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTIONAL_EDGE__EXCEPTION_TABLE_ENTRY = CONTROL_FLOW_EDGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Exceptional Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTIONAL_EDGE_FEATURE_COUNT = CONTROL_FLOW_EDGE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Exceptional Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTIONAL_EDGE_OPERATION_COUNT = CONTROL_FLOW_EDGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.InstructionImpl <em>Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.InstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getInstruction()
	 * @generated
	 */
	int INSTRUCTION = 35;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTRUCTION__UUID = IDENTIFIABLE__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTRUCTION__METHOD = IDENTIFIABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTRUCTION__NEXT_IN_CODE_ORDER = IDENTIFIABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTRUCTION__PREVIOUS_IN_CODE_ORDER = IDENTIFIABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTRUCTION__LINENUMBER = IDENTIFIABLE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTRUCTION__INDEX = IDENTIFIABLE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTRUCTION__OPCODE = IDENTIFIABLE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTRUCTION__HUMAN_READABLE = IDENTIFIABLE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTRUCTION__OUT_EDGES = IDENTIFIABLE_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTRUCTION__IN_EDGES = IDENTIFIABLE_FEATURE_COUNT + 8;

	/**
	 * The number of structural features of the '<em>Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTRUCTION_FEATURE_COUNT = IDENTIFIABLE_FEATURE_COUNT + 9;

	/**
	 * The number of operations of the '<em>Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTRUCTION_OPERATION_COUNT = IDENTIFIABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.FieldInstructionImpl <em>Field Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.FieldInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getFieldInstruction()
	 * @generated
	 */
	int FIELD_INSTRUCTION = 36;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_INSTRUCTION__UUID = INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_INSTRUCTION__METHOD = INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_INSTRUCTION__NEXT_IN_CODE_ORDER = INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_INSTRUCTION__LINENUMBER = INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_INSTRUCTION__INDEX = INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_INSTRUCTION__OPCODE = INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_INSTRUCTION__HUMAN_READABLE = INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_INSTRUCTION__OUT_EDGES = INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_INSTRUCTION__IN_EDGES = INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Field Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_INSTRUCTION__FIELD_REFERENCE = INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Field Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_INSTRUCTION_FEATURE_COUNT = INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Field Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_INSTRUCTION_OPERATION_COUNT = INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.SimpleInstructionImpl <em>Simple Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.SimpleInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getSimpleInstruction()
	 * @generated
	 */
	int SIMPLE_INSTRUCTION = 37;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_INSTRUCTION__UUID = INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_INSTRUCTION__METHOD = INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER = INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_INSTRUCTION__LINENUMBER = INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_INSTRUCTION__INDEX = INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_INSTRUCTION__OPCODE = INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_INSTRUCTION__HUMAN_READABLE = INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_INSTRUCTION__OUT_EDGES = INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_INSTRUCTION__IN_EDGES = INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Simple Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_INSTRUCTION_FEATURE_COUNT = INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Simple Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_INSTRUCTION_OPERATION_COUNT = INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.IntInstructionImpl <em>Int Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.IntInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIntInstruction()
	 * @generated
	 */
	int INT_INSTRUCTION = 38;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_INSTRUCTION__UUID = INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_INSTRUCTION__METHOD = INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_INSTRUCTION__NEXT_IN_CODE_ORDER = INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_INSTRUCTION__LINENUMBER = INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_INSTRUCTION__INDEX = INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_INSTRUCTION__OPCODE = INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_INSTRUCTION__HUMAN_READABLE = INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_INSTRUCTION__OUT_EDGES = INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_INSTRUCTION__IN_EDGES = INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Operand</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_INSTRUCTION__OPERAND = INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Int Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_INSTRUCTION_FEATURE_COUNT = INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Int Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_INSTRUCTION_OPERATION_COUNT = INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.JumpInstructionImpl <em>Jump Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.JumpInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getJumpInstruction()
	 * @generated
	 */
	int JUMP_INSTRUCTION = 39;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUMP_INSTRUCTION__UUID = INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUMP_INSTRUCTION__METHOD = INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUMP_INSTRUCTION__NEXT_IN_CODE_ORDER = INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUMP_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUMP_INSTRUCTION__LINENUMBER = INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUMP_INSTRUCTION__INDEX = INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUMP_INSTRUCTION__OPCODE = INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUMP_INSTRUCTION__HUMAN_READABLE = INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUMP_INSTRUCTION__OUT_EDGES = INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUMP_INSTRUCTION__IN_EDGES = INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Jump Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUMP_INSTRUCTION_FEATURE_COUNT = INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Jump Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUMP_INSTRUCTION_OPERATION_COUNT = INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.LdcInstructionImpl <em>Ldc Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.LdcInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getLdcInstruction()
	 * @generated
	 */
	int LDC_INSTRUCTION = 40;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_INSTRUCTION__UUID = INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_INSTRUCTION__METHOD = INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_INSTRUCTION__NEXT_IN_CODE_ORDER = INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_INSTRUCTION__LINENUMBER = INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_INSTRUCTION__INDEX = INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_INSTRUCTION__OPCODE = INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_INSTRUCTION__HUMAN_READABLE = INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_INSTRUCTION__OUT_EDGES = INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_INSTRUCTION__IN_EDGES = INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Ldc Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_INSTRUCTION_FEATURE_COUNT = INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Ldc Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_INSTRUCTION_OPERATION_COUNT = INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.MethodInstructionImpl <em>Method Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.MethodInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getMethodInstruction()
	 * @generated
	 */
	int METHOD_INSTRUCTION = 41;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_INSTRUCTION__UUID = INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_INSTRUCTION__METHOD = INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_INSTRUCTION__NEXT_IN_CODE_ORDER = INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_INSTRUCTION__LINENUMBER = INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_INSTRUCTION__INDEX = INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_INSTRUCTION__OPCODE = INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_INSTRUCTION__HUMAN_READABLE = INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_INSTRUCTION__OUT_EDGES = INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_INSTRUCTION__IN_EDGES = INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Method Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_INSTRUCTION__METHOD_REFERENCE = INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Method Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_INSTRUCTION_FEATURE_COUNT = INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Method Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_INSTRUCTION_OPERATION_COUNT = INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.TypeInstructionImpl <em>Type Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.TypeInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getTypeInstruction()
	 * @generated
	 */
	int TYPE_INSTRUCTION = 42;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_INSTRUCTION__UUID = INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_INSTRUCTION__METHOD = INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_INSTRUCTION__NEXT_IN_CODE_ORDER = INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_INSTRUCTION__LINENUMBER = INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_INSTRUCTION__INDEX = INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_INSTRUCTION__OPCODE = INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_INSTRUCTION__HUMAN_READABLE = INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_INSTRUCTION__OUT_EDGES = INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_INSTRUCTION__IN_EDGES = INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Type Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_INSTRUCTION__TYPE_REFERENCE = INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Type Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_INSTRUCTION_FEATURE_COUNT = INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Type Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_INSTRUCTION_OPERATION_COUNT = INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.VarInstructionImpl <em>Var Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.VarInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getVarInstruction()
	 * @generated
	 */
	int VAR_INSTRUCTION = 43;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VAR_INSTRUCTION__UUID = INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VAR_INSTRUCTION__METHOD = INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VAR_INSTRUCTION__NEXT_IN_CODE_ORDER = INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VAR_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VAR_INSTRUCTION__LINENUMBER = INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VAR_INSTRUCTION__INDEX = INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VAR_INSTRUCTION__OPCODE = INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VAR_INSTRUCTION__HUMAN_READABLE = INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VAR_INSTRUCTION__OUT_EDGES = INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VAR_INSTRUCTION__IN_EDGES = INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Local Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VAR_INSTRUCTION__LOCAL_VARIABLE = INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Var Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VAR_INSTRUCTION__VAR_INDEX = INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Var Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VAR_INSTRUCTION_FEATURE_COUNT = INSTRUCTION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Var Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VAR_INSTRUCTION_OPERATION_COUNT = INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.IincInstructionImpl <em>Iinc Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.IincInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIincInstruction()
	 * @generated
	 */
	int IINC_INSTRUCTION = 44;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IINC_INSTRUCTION__UUID = VAR_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IINC_INSTRUCTION__METHOD = VAR_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IINC_INSTRUCTION__NEXT_IN_CODE_ORDER = VAR_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IINC_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = VAR_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IINC_INSTRUCTION__LINENUMBER = VAR_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IINC_INSTRUCTION__INDEX = VAR_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IINC_INSTRUCTION__OPCODE = VAR_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IINC_INSTRUCTION__HUMAN_READABLE = VAR_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IINC_INSTRUCTION__OUT_EDGES = VAR_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IINC_INSTRUCTION__IN_EDGES = VAR_INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Local Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IINC_INSTRUCTION__LOCAL_VARIABLE = VAR_INSTRUCTION__LOCAL_VARIABLE;

	/**
	 * The feature id for the '<em><b>Var Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IINC_INSTRUCTION__VAR_INDEX = VAR_INSTRUCTION__VAR_INDEX;

	/**
	 * The feature id for the '<em><b>Incr</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IINC_INSTRUCTION__INCR = VAR_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Iinc Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IINC_INSTRUCTION_FEATURE_COUNT = VAR_INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Iinc Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IINC_INSTRUCTION_OPERATION_COUNT = VAR_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.MultianewarrayInstructionImpl <em>Multianewarray Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.MultianewarrayInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getMultianewarrayInstruction()
	 * @generated
	 */
	int MULTIANEWARRAY_INSTRUCTION = 45;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIANEWARRAY_INSTRUCTION__UUID = INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIANEWARRAY_INSTRUCTION__METHOD = INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIANEWARRAY_INSTRUCTION__NEXT_IN_CODE_ORDER = INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIANEWARRAY_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIANEWARRAY_INSTRUCTION__LINENUMBER = INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIANEWARRAY_INSTRUCTION__INDEX = INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIANEWARRAY_INSTRUCTION__OPCODE = INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIANEWARRAY_INSTRUCTION__HUMAN_READABLE = INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIANEWARRAY_INSTRUCTION__OUT_EDGES = INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIANEWARRAY_INSTRUCTION__IN_EDGES = INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Type Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIANEWARRAY_INSTRUCTION__TYPE_REFERENCE = INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Dims</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIANEWARRAY_INSTRUCTION__DIMS = INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Multianewarray Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIANEWARRAY_INSTRUCTION_FEATURE_COUNT = INSTRUCTION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Multianewarray Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIANEWARRAY_INSTRUCTION_OPERATION_COUNT = INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.SwitchInstructionImpl <em>Switch Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.SwitchInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getSwitchInstruction()
	 * @generated
	 */
	int SWITCH_INSTRUCTION = 46;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_INSTRUCTION__UUID = INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_INSTRUCTION__METHOD = INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_INSTRUCTION__NEXT_IN_CODE_ORDER = INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_INSTRUCTION__LINENUMBER = INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_INSTRUCTION__INDEX = INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_INSTRUCTION__OPCODE = INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_INSTRUCTION__HUMAN_READABLE = INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_INSTRUCTION__OUT_EDGES = INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_INSTRUCTION__IN_EDGES = INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Switch Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_INSTRUCTION_FEATURE_COUNT = INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Switch Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWITCH_INSTRUCTION_OPERATION_COUNT = INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.LdcIntInstructionImpl <em>Ldc Int Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.LdcIntInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getLdcIntInstruction()
	 * @generated
	 */
	int LDC_INT_INSTRUCTION = 47;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_INT_INSTRUCTION__UUID = LDC_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_INT_INSTRUCTION__METHOD = LDC_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_INT_INSTRUCTION__NEXT_IN_CODE_ORDER = LDC_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_INT_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = LDC_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_INT_INSTRUCTION__LINENUMBER = LDC_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_INT_INSTRUCTION__INDEX = LDC_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_INT_INSTRUCTION__OPCODE = LDC_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_INT_INSTRUCTION__HUMAN_READABLE = LDC_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_INT_INSTRUCTION__OUT_EDGES = LDC_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_INT_INSTRUCTION__IN_EDGES = LDC_INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_INT_INSTRUCTION__CONSTANT = LDC_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Ldc Int Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_INT_INSTRUCTION_FEATURE_COUNT = LDC_INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Ldc Int Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_INT_INSTRUCTION_OPERATION_COUNT = LDC_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.LdcLongInstructionImpl <em>Ldc Long Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.LdcLongInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getLdcLongInstruction()
	 * @generated
	 */
	int LDC_LONG_INSTRUCTION = 48;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_LONG_INSTRUCTION__UUID = LDC_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_LONG_INSTRUCTION__METHOD = LDC_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_LONG_INSTRUCTION__NEXT_IN_CODE_ORDER = LDC_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_LONG_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = LDC_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_LONG_INSTRUCTION__LINENUMBER = LDC_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_LONG_INSTRUCTION__INDEX = LDC_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_LONG_INSTRUCTION__OPCODE = LDC_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_LONG_INSTRUCTION__HUMAN_READABLE = LDC_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_LONG_INSTRUCTION__OUT_EDGES = LDC_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_LONG_INSTRUCTION__IN_EDGES = LDC_INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_LONG_INSTRUCTION__CONSTANT = LDC_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Ldc Long Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_LONG_INSTRUCTION_FEATURE_COUNT = LDC_INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Ldc Long Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_LONG_INSTRUCTION_OPERATION_COUNT = LDC_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.LdcFloatInstructionImpl <em>Ldc Float Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.LdcFloatInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getLdcFloatInstruction()
	 * @generated
	 */
	int LDC_FLOAT_INSTRUCTION = 49;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_FLOAT_INSTRUCTION__UUID = LDC_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_FLOAT_INSTRUCTION__METHOD = LDC_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_FLOAT_INSTRUCTION__NEXT_IN_CODE_ORDER = LDC_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_FLOAT_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = LDC_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_FLOAT_INSTRUCTION__LINENUMBER = LDC_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_FLOAT_INSTRUCTION__INDEX = LDC_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_FLOAT_INSTRUCTION__OPCODE = LDC_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_FLOAT_INSTRUCTION__HUMAN_READABLE = LDC_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_FLOAT_INSTRUCTION__OUT_EDGES = LDC_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_FLOAT_INSTRUCTION__IN_EDGES = LDC_INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_FLOAT_INSTRUCTION__CONSTANT = LDC_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Ldc Float Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_FLOAT_INSTRUCTION_FEATURE_COUNT = LDC_INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Ldc Float Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_FLOAT_INSTRUCTION_OPERATION_COUNT = LDC_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.LdcDoubleInstructionImpl <em>Ldc Double Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.LdcDoubleInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getLdcDoubleInstruction()
	 * @generated
	 */
	int LDC_DOUBLE_INSTRUCTION = 50;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_DOUBLE_INSTRUCTION__UUID = LDC_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_DOUBLE_INSTRUCTION__METHOD = LDC_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_DOUBLE_INSTRUCTION__NEXT_IN_CODE_ORDER = LDC_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_DOUBLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = LDC_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_DOUBLE_INSTRUCTION__LINENUMBER = LDC_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_DOUBLE_INSTRUCTION__INDEX = LDC_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_DOUBLE_INSTRUCTION__OPCODE = LDC_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_DOUBLE_INSTRUCTION__HUMAN_READABLE = LDC_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_DOUBLE_INSTRUCTION__OUT_EDGES = LDC_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_DOUBLE_INSTRUCTION__IN_EDGES = LDC_INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_DOUBLE_INSTRUCTION__CONSTANT = LDC_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Ldc Double Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_DOUBLE_INSTRUCTION_FEATURE_COUNT = LDC_INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Ldc Double Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_DOUBLE_INSTRUCTION_OPERATION_COUNT = LDC_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.LdcStringInstructionImpl <em>Ldc String Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.LdcStringInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getLdcStringInstruction()
	 * @generated
	 */
	int LDC_STRING_INSTRUCTION = 51;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_STRING_INSTRUCTION__UUID = LDC_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_STRING_INSTRUCTION__METHOD = LDC_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_STRING_INSTRUCTION__NEXT_IN_CODE_ORDER = LDC_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_STRING_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = LDC_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_STRING_INSTRUCTION__LINENUMBER = LDC_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_STRING_INSTRUCTION__INDEX = LDC_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_STRING_INSTRUCTION__OPCODE = LDC_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_STRING_INSTRUCTION__HUMAN_READABLE = LDC_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_STRING_INSTRUCTION__OUT_EDGES = LDC_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_STRING_INSTRUCTION__IN_EDGES = LDC_INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_STRING_INSTRUCTION__CONSTANT = LDC_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Ldc String Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_STRING_INSTRUCTION_FEATURE_COUNT = LDC_INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Ldc String Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_STRING_INSTRUCTION_OPERATION_COUNT = LDC_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.LdcTypeInstructionImpl <em>Ldc Type Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.LdcTypeInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getLdcTypeInstruction()
	 * @generated
	 */
	int LDC_TYPE_INSTRUCTION = 52;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_TYPE_INSTRUCTION__UUID = LDC_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_TYPE_INSTRUCTION__METHOD = LDC_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_TYPE_INSTRUCTION__NEXT_IN_CODE_ORDER = LDC_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_TYPE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = LDC_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_TYPE_INSTRUCTION__LINENUMBER = LDC_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_TYPE_INSTRUCTION__INDEX = LDC_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_TYPE_INSTRUCTION__OPCODE = LDC_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_TYPE_INSTRUCTION__HUMAN_READABLE = LDC_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_TYPE_INSTRUCTION__OUT_EDGES = LDC_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_TYPE_INSTRUCTION__IN_EDGES = LDC_INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Constant</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_TYPE_INSTRUCTION__CONSTANT = LDC_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Ldc Type Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_TYPE_INSTRUCTION_FEATURE_COUNT = LDC_INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Ldc Type Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDC_TYPE_INSTRUCTION_OPERATION_COUNT = LDC_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.GetstaticInstructionImpl <em>Getstatic Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.GetstaticInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getGetstaticInstruction()
	 * @generated
	 */
	int GETSTATIC_INSTRUCTION = 53;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GETSTATIC_INSTRUCTION__UUID = FIELD_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GETSTATIC_INSTRUCTION__METHOD = FIELD_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GETSTATIC_INSTRUCTION__NEXT_IN_CODE_ORDER = FIELD_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GETSTATIC_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = FIELD_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GETSTATIC_INSTRUCTION__LINENUMBER = FIELD_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GETSTATIC_INSTRUCTION__INDEX = FIELD_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GETSTATIC_INSTRUCTION__OPCODE = FIELD_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GETSTATIC_INSTRUCTION__HUMAN_READABLE = FIELD_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GETSTATIC_INSTRUCTION__OUT_EDGES = FIELD_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GETSTATIC_INSTRUCTION__IN_EDGES = FIELD_INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Field Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GETSTATIC_INSTRUCTION__FIELD_REFERENCE = FIELD_INSTRUCTION__FIELD_REFERENCE;

	/**
	 * The number of structural features of the '<em>Getstatic Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GETSTATIC_INSTRUCTION_FEATURE_COUNT = FIELD_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Getstatic Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GETSTATIC_INSTRUCTION_OPERATION_COUNT = FIELD_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.PutstaticInstructionImpl <em>Putstatic Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.PutstaticInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getPutstaticInstruction()
	 * @generated
	 */
	int PUTSTATIC_INSTRUCTION = 54;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUTSTATIC_INSTRUCTION__UUID = FIELD_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUTSTATIC_INSTRUCTION__METHOD = FIELD_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUTSTATIC_INSTRUCTION__NEXT_IN_CODE_ORDER = FIELD_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUTSTATIC_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = FIELD_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUTSTATIC_INSTRUCTION__LINENUMBER = FIELD_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUTSTATIC_INSTRUCTION__INDEX = FIELD_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUTSTATIC_INSTRUCTION__OPCODE = FIELD_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUTSTATIC_INSTRUCTION__HUMAN_READABLE = FIELD_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUTSTATIC_INSTRUCTION__OUT_EDGES = FIELD_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUTSTATIC_INSTRUCTION__IN_EDGES = FIELD_INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Field Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUTSTATIC_INSTRUCTION__FIELD_REFERENCE = FIELD_INSTRUCTION__FIELD_REFERENCE;

	/**
	 * The number of structural features of the '<em>Putstatic Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUTSTATIC_INSTRUCTION_FEATURE_COUNT = FIELD_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Putstatic Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUTSTATIC_INSTRUCTION_OPERATION_COUNT = FIELD_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.GetfieldInstructionImpl <em>Getfield Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.GetfieldInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getGetfieldInstruction()
	 * @generated
	 */
	int GETFIELD_INSTRUCTION = 55;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GETFIELD_INSTRUCTION__UUID = FIELD_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GETFIELD_INSTRUCTION__METHOD = FIELD_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GETFIELD_INSTRUCTION__NEXT_IN_CODE_ORDER = FIELD_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GETFIELD_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = FIELD_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GETFIELD_INSTRUCTION__LINENUMBER = FIELD_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GETFIELD_INSTRUCTION__INDEX = FIELD_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GETFIELD_INSTRUCTION__OPCODE = FIELD_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GETFIELD_INSTRUCTION__HUMAN_READABLE = FIELD_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GETFIELD_INSTRUCTION__OUT_EDGES = FIELD_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GETFIELD_INSTRUCTION__IN_EDGES = FIELD_INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Field Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GETFIELD_INSTRUCTION__FIELD_REFERENCE = FIELD_INSTRUCTION__FIELD_REFERENCE;

	/**
	 * The number of structural features of the '<em>Getfield Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GETFIELD_INSTRUCTION_FEATURE_COUNT = FIELD_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Getfield Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GETFIELD_INSTRUCTION_OPERATION_COUNT = FIELD_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.PutfieldInstructionImpl <em>Putfield Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.PutfieldInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getPutfieldInstruction()
	 * @generated
	 */
	int PUTFIELD_INSTRUCTION = 56;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUTFIELD_INSTRUCTION__UUID = FIELD_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUTFIELD_INSTRUCTION__METHOD = FIELD_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUTFIELD_INSTRUCTION__NEXT_IN_CODE_ORDER = FIELD_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUTFIELD_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = FIELD_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUTFIELD_INSTRUCTION__LINENUMBER = FIELD_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUTFIELD_INSTRUCTION__INDEX = FIELD_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUTFIELD_INSTRUCTION__OPCODE = FIELD_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUTFIELD_INSTRUCTION__HUMAN_READABLE = FIELD_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUTFIELD_INSTRUCTION__OUT_EDGES = FIELD_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUTFIELD_INSTRUCTION__IN_EDGES = FIELD_INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Field Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUTFIELD_INSTRUCTION__FIELD_REFERENCE = FIELD_INSTRUCTION__FIELD_REFERENCE;

	/**
	 * The number of structural features of the '<em>Putfield Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUTFIELD_INSTRUCTION_FEATURE_COUNT = FIELD_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Putfield Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUTFIELD_INSTRUCTION_OPERATION_COUNT = FIELD_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.NopInstructionImpl <em>Nop Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.NopInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getNopInstruction()
	 * @generated
	 */
	int NOP_INSTRUCTION = 57;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOP_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOP_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOP_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOP_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOP_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOP_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOP_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOP_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOP_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOP_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Nop Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOP_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Nop Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOP_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.Aconst_nullInstructionImpl <em>Aconst null Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.Aconst_nullInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getAconst_nullInstruction()
	 * @generated
	 */
	int ACONST_NULL_INSTRUCTION = 58;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACONST_NULL_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACONST_NULL_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACONST_NULL_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACONST_NULL_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACONST_NULL_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACONST_NULL_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACONST_NULL_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACONST_NULL_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACONST_NULL_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACONST_NULL_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Aconst null Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACONST_NULL_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Aconst null Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACONST_NULL_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.Iconst_m1InstructionImpl <em>Iconst m1 Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.Iconst_m1InstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIconst_m1Instruction()
	 * @generated
	 */
	int ICONST_M1_INSTRUCTION = 59;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_M1_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_M1_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_M1_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_M1_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_M1_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_M1_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_M1_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_M1_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_M1_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_M1_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Iconst m1 Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_M1_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Iconst m1 Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_M1_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.Iconst_0InstructionImpl <em>Iconst 0Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.Iconst_0InstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIconst_0Instruction()
	 * @generated
	 */
	int ICONST_0INSTRUCTION = 60;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_0INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_0INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_0INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_0INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_0INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_0INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_0INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_0INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_0INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_0INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Iconst 0Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_0INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Iconst 0Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_0INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.Iconst_1InstructionImpl <em>Iconst 1Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.Iconst_1InstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIconst_1Instruction()
	 * @generated
	 */
	int ICONST_1INSTRUCTION = 61;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_1INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_1INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_1INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_1INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_1INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_1INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_1INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_1INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_1INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_1INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Iconst 1Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_1INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Iconst 1Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_1INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.Iconst_2InstructionImpl <em>Iconst 2Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.Iconst_2InstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIconst_2Instruction()
	 * @generated
	 */
	int ICONST_2INSTRUCTION = 62;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_2INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_2INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_2INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_2INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_2INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_2INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_2INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_2INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_2INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_2INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Iconst 2Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_2INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Iconst 2Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_2INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.Iconst_3InstructionImpl <em>Iconst 3Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.Iconst_3InstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIconst_3Instruction()
	 * @generated
	 */
	int ICONST_3INSTRUCTION = 63;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_3INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_3INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_3INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_3INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_3INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_3INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_3INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_3INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_3INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_3INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Iconst 3Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_3INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Iconst 3Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_3INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.Iconst_4InstructionImpl <em>Iconst 4Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.Iconst_4InstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIconst_4Instruction()
	 * @generated
	 */
	int ICONST_4INSTRUCTION = 64;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_4INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_4INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_4INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_4INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_4INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_4INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_4INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_4INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_4INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_4INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Iconst 4Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_4INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Iconst 4Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_4INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.Iconst_5InstructionImpl <em>Iconst 5Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.Iconst_5InstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIconst_5Instruction()
	 * @generated
	 */
	int ICONST_5INSTRUCTION = 65;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_5INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_5INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_5INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_5INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_5INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_5INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_5INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_5INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_5INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_5INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Iconst 5Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_5INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Iconst 5Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICONST_5INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.Lconst_0InstructionImpl <em>Lconst 0Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.Lconst_0InstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getLconst_0Instruction()
	 * @generated
	 */
	int LCONST_0INSTRUCTION = 66;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCONST_0INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCONST_0INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCONST_0INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCONST_0INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCONST_0INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCONST_0INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCONST_0INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCONST_0INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCONST_0INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCONST_0INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Lconst 0Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCONST_0INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Lconst 0Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCONST_0INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.Lconst_1InstructionImpl <em>Lconst 1Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.Lconst_1InstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getLconst_1Instruction()
	 * @generated
	 */
	int LCONST_1INSTRUCTION = 67;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCONST_1INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCONST_1INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCONST_1INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCONST_1INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCONST_1INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCONST_1INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCONST_1INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCONST_1INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCONST_1INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCONST_1INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Lconst 1Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCONST_1INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Lconst 1Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCONST_1INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.Fconst_0InstructionImpl <em>Fconst 0Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.Fconst_0InstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getFconst_0Instruction()
	 * @generated
	 */
	int FCONST_0INSTRUCTION = 68;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_0INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_0INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_0INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_0INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_0INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_0INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_0INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_0INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_0INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_0INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Fconst 0Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_0INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Fconst 0Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_0INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.Fconst_1InstructionImpl <em>Fconst 1Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.Fconst_1InstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getFconst_1Instruction()
	 * @generated
	 */
	int FCONST_1INSTRUCTION = 69;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_1INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_1INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_1INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_1INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_1INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_1INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_1INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_1INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_1INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_1INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Fconst 1Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_1INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Fconst 1Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_1INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.Fconst_2InstructionImpl <em>Fconst 2Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.Fconst_2InstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getFconst_2Instruction()
	 * @generated
	 */
	int FCONST_2INSTRUCTION = 70;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_2INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_2INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_2INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_2INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_2INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_2INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_2INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_2INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_2INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_2INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Fconst 2Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_2INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Fconst 2Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCONST_2INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.Dconst_0InstructionImpl <em>Dconst 0Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.Dconst_0InstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getDconst_0Instruction()
	 * @generated
	 */
	int DCONST_0INSTRUCTION = 71;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCONST_0INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCONST_0INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCONST_0INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCONST_0INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCONST_0INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCONST_0INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCONST_0INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCONST_0INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCONST_0INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCONST_0INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Dconst 0Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCONST_0INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Dconst 0Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCONST_0INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.Dconst_1InstructionImpl <em>Dconst 1Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.Dconst_1InstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getDconst_1Instruction()
	 * @generated
	 */
	int DCONST_1INSTRUCTION = 72;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCONST_1INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCONST_1INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCONST_1INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCONST_1INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCONST_1INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCONST_1INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCONST_1INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCONST_1INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCONST_1INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCONST_1INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Dconst 1Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCONST_1INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Dconst 1Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCONST_1INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.IaloadInstructionImpl <em>Iaload Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.IaloadInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIaloadInstruction()
	 * @generated
	 */
	int IALOAD_INSTRUCTION = 73;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IALOAD_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IALOAD_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IALOAD_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IALOAD_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IALOAD_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IALOAD_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IALOAD_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IALOAD_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IALOAD_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IALOAD_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Iaload Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IALOAD_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Iaload Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IALOAD_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.LaloadInstructionImpl <em>Laload Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.LaloadInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getLaloadInstruction()
	 * @generated
	 */
	int LALOAD_INSTRUCTION = 74;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LALOAD_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LALOAD_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LALOAD_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LALOAD_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LALOAD_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LALOAD_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LALOAD_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LALOAD_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LALOAD_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LALOAD_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Laload Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LALOAD_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Laload Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LALOAD_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.FaloadInstructionImpl <em>Faload Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.FaloadInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getFaloadInstruction()
	 * @generated
	 */
	int FALOAD_INSTRUCTION = 75;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALOAD_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALOAD_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALOAD_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALOAD_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALOAD_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALOAD_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALOAD_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALOAD_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALOAD_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALOAD_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Faload Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALOAD_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Faload Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FALOAD_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.DaloadInstructionImpl <em>Daload Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.DaloadInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getDaloadInstruction()
	 * @generated
	 */
	int DALOAD_INSTRUCTION = 76;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DALOAD_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DALOAD_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DALOAD_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DALOAD_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DALOAD_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DALOAD_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DALOAD_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DALOAD_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DALOAD_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DALOAD_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Daload Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DALOAD_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Daload Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DALOAD_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.AaloadInstructionImpl <em>Aaload Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.AaloadInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getAaloadInstruction()
	 * @generated
	 */
	int AALOAD_INSTRUCTION = 77;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AALOAD_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AALOAD_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AALOAD_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AALOAD_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AALOAD_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AALOAD_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AALOAD_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AALOAD_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AALOAD_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AALOAD_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Aaload Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AALOAD_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Aaload Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AALOAD_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.BaloadInstructionImpl <em>Baload Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.BaloadInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getBaloadInstruction()
	 * @generated
	 */
	int BALOAD_INSTRUCTION = 78;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BALOAD_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BALOAD_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BALOAD_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BALOAD_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BALOAD_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BALOAD_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BALOAD_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BALOAD_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BALOAD_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BALOAD_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Baload Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BALOAD_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Baload Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BALOAD_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.CaloadInstructionImpl <em>Caload Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.CaloadInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getCaloadInstruction()
	 * @generated
	 */
	int CALOAD_INSTRUCTION = 79;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALOAD_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALOAD_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALOAD_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALOAD_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALOAD_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALOAD_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALOAD_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALOAD_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALOAD_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALOAD_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Caload Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALOAD_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Caload Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALOAD_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.SaloadInstructionImpl <em>Saload Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.SaloadInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getSaloadInstruction()
	 * @generated
	 */
	int SALOAD_INSTRUCTION = 80;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SALOAD_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SALOAD_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SALOAD_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SALOAD_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SALOAD_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SALOAD_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SALOAD_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SALOAD_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SALOAD_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SALOAD_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Saload Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SALOAD_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Saload Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SALOAD_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.IastoreInstructionImpl <em>Iastore Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.IastoreInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIastoreInstruction()
	 * @generated
	 */
	int IASTORE_INSTRUCTION = 81;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IASTORE_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IASTORE_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IASTORE_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IASTORE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IASTORE_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IASTORE_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IASTORE_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IASTORE_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IASTORE_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IASTORE_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Iastore Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IASTORE_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Iastore Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IASTORE_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.LastoreInstructionImpl <em>Lastore Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.LastoreInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getLastoreInstruction()
	 * @generated
	 */
	int LASTORE_INSTRUCTION = 82;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LASTORE_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LASTORE_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LASTORE_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LASTORE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LASTORE_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LASTORE_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LASTORE_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LASTORE_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LASTORE_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LASTORE_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Lastore Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LASTORE_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Lastore Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LASTORE_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.FastoreInstructionImpl <em>Fastore Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.FastoreInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getFastoreInstruction()
	 * @generated
	 */
	int FASTORE_INSTRUCTION = 83;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FASTORE_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FASTORE_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FASTORE_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FASTORE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FASTORE_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FASTORE_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FASTORE_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FASTORE_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FASTORE_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FASTORE_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Fastore Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FASTORE_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Fastore Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FASTORE_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.DastoreInstructionImpl <em>Dastore Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.DastoreInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getDastoreInstruction()
	 * @generated
	 */
	int DASTORE_INSTRUCTION = 84;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DASTORE_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DASTORE_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DASTORE_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DASTORE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DASTORE_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DASTORE_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DASTORE_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DASTORE_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DASTORE_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DASTORE_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Dastore Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DASTORE_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Dastore Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DASTORE_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.AastoreInstructionImpl <em>Aastore Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.AastoreInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getAastoreInstruction()
	 * @generated
	 */
	int AASTORE_INSTRUCTION = 85;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AASTORE_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AASTORE_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AASTORE_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AASTORE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AASTORE_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AASTORE_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AASTORE_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AASTORE_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AASTORE_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AASTORE_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Aastore Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AASTORE_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Aastore Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AASTORE_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.BastoreInstructionImpl <em>Bastore Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.BastoreInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getBastoreInstruction()
	 * @generated
	 */
	int BASTORE_INSTRUCTION = 86;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASTORE_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASTORE_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASTORE_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASTORE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASTORE_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASTORE_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASTORE_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASTORE_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASTORE_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASTORE_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Bastore Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASTORE_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Bastore Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASTORE_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.CastoreInstructionImpl <em>Castore Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.CastoreInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getCastoreInstruction()
	 * @generated
	 */
	int CASTORE_INSTRUCTION = 87;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASTORE_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASTORE_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASTORE_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASTORE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASTORE_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASTORE_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASTORE_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASTORE_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASTORE_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASTORE_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Castore Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASTORE_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Castore Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CASTORE_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.SastoreInstructionImpl <em>Sastore Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.SastoreInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getSastoreInstruction()
	 * @generated
	 */
	int SASTORE_INSTRUCTION = 88;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SASTORE_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SASTORE_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SASTORE_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SASTORE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SASTORE_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SASTORE_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SASTORE_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SASTORE_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SASTORE_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SASTORE_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Sastore Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SASTORE_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Sastore Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SASTORE_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.PopInstructionImpl <em>Pop Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.PopInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getPopInstruction()
	 * @generated
	 */
	int POP_INSTRUCTION = 89;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POP_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POP_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POP_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POP_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POP_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POP_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POP_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POP_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POP_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POP_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Pop Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POP_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Pop Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POP_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.Pop2InstructionImpl <em>Pop2 Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.Pop2InstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getPop2Instruction()
	 * @generated
	 */
	int POP2_INSTRUCTION = 90;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POP2_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POP2_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POP2_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POP2_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POP2_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POP2_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POP2_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POP2_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POP2_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POP2_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Pop2 Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POP2_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Pop2 Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POP2_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.DupInstructionImpl <em>Dup Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.DupInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getDupInstruction()
	 * @generated
	 */
	int DUP_INSTRUCTION = 91;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Dup Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Dup Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.Dup_x1InstructionImpl <em>Dup x1 Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.Dup_x1InstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getDup_x1Instruction()
	 * @generated
	 */
	int DUP_X1_INSTRUCTION = 92;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_X1_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_X1_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_X1_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_X1_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_X1_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_X1_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_X1_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_X1_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_X1_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_X1_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Dup x1 Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_X1_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Dup x1 Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_X1_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.Dup_x2InstructionImpl <em>Dup x2 Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.Dup_x2InstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getDup_x2Instruction()
	 * @generated
	 */
	int DUP_X2_INSTRUCTION = 93;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_X2_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_X2_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_X2_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_X2_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_X2_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_X2_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_X2_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_X2_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_X2_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_X2_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Dup x2 Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_X2_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Dup x2 Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP_X2_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.Dup2InstructionImpl <em>Dup2 Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.Dup2InstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getDup2Instruction()
	 * @generated
	 */
	int DUP2_INSTRUCTION = 94;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Dup2 Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Dup2 Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.Dup2_x1InstructionImpl <em>Dup2 x1 Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.Dup2_x1InstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getDup2_x1Instruction()
	 * @generated
	 */
	int DUP2_X1_INSTRUCTION = 95;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_X1_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_X1_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_X1_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_X1_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_X1_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_X1_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_X1_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_X1_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_X1_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_X1_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Dup2 x1 Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_X1_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Dup2 x1 Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_X1_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.Dup2_x2InstructionImpl <em>Dup2 x2 Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.Dup2_x2InstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getDup2_x2Instruction()
	 * @generated
	 */
	int DUP2_X2_INSTRUCTION = 96;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_X2_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_X2_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_X2_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_X2_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_X2_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_X2_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_X2_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_X2_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_X2_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_X2_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Dup2 x2 Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_X2_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Dup2 x2 Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUP2_X2_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.SwapInstructionImpl <em>Swap Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.SwapInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getSwapInstruction()
	 * @generated
	 */
	int SWAP_INSTRUCTION = 97;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWAP_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWAP_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWAP_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWAP_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWAP_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWAP_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWAP_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWAP_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWAP_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWAP_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Swap Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWAP_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Swap Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWAP_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.IaddInstructionImpl <em>Iadd Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.IaddInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIaddInstruction()
	 * @generated
	 */
	int IADD_INSTRUCTION = 98;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADD_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADD_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADD_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADD_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADD_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADD_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADD_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADD_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADD_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADD_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Iadd Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADD_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Iadd Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADD_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.LaddInstructionImpl <em>Ladd Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.LaddInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getLaddInstruction()
	 * @generated
	 */
	int LADD_INSTRUCTION = 99;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LADD_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LADD_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LADD_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LADD_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LADD_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LADD_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LADD_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LADD_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LADD_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LADD_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Ladd Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LADD_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Ladd Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LADD_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.FaddInstructionImpl <em>Fadd Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.FaddInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getFaddInstruction()
	 * @generated
	 */
	int FADD_INSTRUCTION = 100;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FADD_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FADD_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FADD_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FADD_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FADD_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FADD_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FADD_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FADD_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FADD_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FADD_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Fadd Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FADD_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Fadd Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FADD_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.DaddInstructionImpl <em>Dadd Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.DaddInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getDaddInstruction()
	 * @generated
	 */
	int DADD_INSTRUCTION = 101;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DADD_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DADD_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DADD_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DADD_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DADD_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DADD_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DADD_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DADD_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DADD_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DADD_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Dadd Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DADD_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Dadd Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DADD_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.IsubInstructionImpl <em>Isub Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.IsubInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIsubInstruction()
	 * @generated
	 */
	int ISUB_INSTRUCTION = 102;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Isub Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Isub Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.LsubInstructionImpl <em>Lsub Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.LsubInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getLsubInstruction()
	 * @generated
	 */
	int LSUB_INSTRUCTION = 103;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSUB_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSUB_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSUB_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSUB_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSUB_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSUB_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSUB_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSUB_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSUB_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSUB_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Lsub Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSUB_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Lsub Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSUB_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.FsubInstructionImpl <em>Fsub Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.FsubInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getFsubInstruction()
	 * @generated
	 */
	int FSUB_INSTRUCTION = 104;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSUB_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSUB_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSUB_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSUB_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSUB_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSUB_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSUB_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSUB_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSUB_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSUB_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Fsub Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSUB_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Fsub Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSUB_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.DsubInstructionImpl <em>Dsub Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.DsubInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getDsubInstruction()
	 * @generated
	 */
	int DSUB_INSTRUCTION = 105;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DSUB_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DSUB_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DSUB_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DSUB_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DSUB_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DSUB_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DSUB_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DSUB_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DSUB_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DSUB_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Dsub Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DSUB_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Dsub Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DSUB_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.ImulInstructionImpl <em>Imul Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.ImulInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getImulInstruction()
	 * @generated
	 */
	int IMUL_INSTRUCTION = 106;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMUL_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMUL_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMUL_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMUL_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMUL_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMUL_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMUL_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMUL_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMUL_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMUL_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Imul Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMUL_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Imul Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMUL_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.LmulInstructionImpl <em>Lmul Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.LmulInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getLmulInstruction()
	 * @generated
	 */
	int LMUL_INSTRUCTION = 107;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LMUL_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LMUL_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LMUL_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LMUL_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LMUL_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LMUL_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LMUL_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LMUL_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LMUL_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LMUL_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Lmul Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LMUL_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Lmul Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LMUL_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.FmulInstructionImpl <em>Fmul Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.FmulInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getFmulInstruction()
	 * @generated
	 */
	int FMUL_INSTRUCTION = 108;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMUL_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMUL_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMUL_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMUL_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMUL_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMUL_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMUL_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMUL_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMUL_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMUL_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Fmul Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMUL_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Fmul Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FMUL_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.DmulInstructionImpl <em>Dmul Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.DmulInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getDmulInstruction()
	 * @generated
	 */
	int DMUL_INSTRUCTION = 109;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DMUL_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DMUL_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DMUL_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DMUL_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DMUL_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DMUL_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DMUL_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DMUL_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DMUL_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DMUL_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Dmul Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DMUL_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Dmul Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DMUL_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.IdivInstructionImpl <em>Idiv Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.IdivInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIdivInstruction()
	 * @generated
	 */
	int IDIV_INSTRUCTION = 110;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDIV_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDIV_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDIV_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDIV_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDIV_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDIV_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDIV_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDIV_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDIV_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDIV_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Idiv Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDIV_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Idiv Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IDIV_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.LdivInstructionImpl <em>Ldiv Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.LdivInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getLdivInstruction()
	 * @generated
	 */
	int LDIV_INSTRUCTION = 111;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDIV_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDIV_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDIV_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDIV_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDIV_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDIV_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDIV_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDIV_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDIV_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDIV_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Ldiv Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDIV_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Ldiv Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LDIV_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.FdivInstructionImpl <em>Fdiv Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.FdivInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getFdivInstruction()
	 * @generated
	 */
	int FDIV_INSTRUCTION = 112;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FDIV_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FDIV_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FDIV_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FDIV_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FDIV_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FDIV_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FDIV_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FDIV_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FDIV_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FDIV_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Fdiv Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FDIV_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Fdiv Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FDIV_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.DdivInstructionImpl <em>Ddiv Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.DdivInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getDdivInstruction()
	 * @generated
	 */
	int DDIV_INSTRUCTION = 113;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DDIV_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DDIV_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DDIV_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DDIV_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DDIV_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DDIV_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DDIV_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DDIV_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DDIV_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DDIV_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Ddiv Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DDIV_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Ddiv Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DDIV_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.IremInstructionImpl <em>Irem Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.IremInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIremInstruction()
	 * @generated
	 */
	int IREM_INSTRUCTION = 114;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IREM_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IREM_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IREM_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IREM_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IREM_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IREM_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IREM_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IREM_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IREM_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IREM_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Irem Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IREM_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Irem Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IREM_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.LremInstructionImpl <em>Lrem Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.LremInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getLremInstruction()
	 * @generated
	 */
	int LREM_INSTRUCTION = 115;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LREM_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LREM_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LREM_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LREM_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LREM_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LREM_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LREM_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LREM_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LREM_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LREM_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Lrem Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LREM_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Lrem Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LREM_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.FremInstructionImpl <em>Frem Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.FremInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getFremInstruction()
	 * @generated
	 */
	int FREM_INSTRUCTION = 116;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREM_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREM_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREM_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREM_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREM_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREM_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREM_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREM_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREM_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREM_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Frem Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREM_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Frem Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREM_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.DremInstructionImpl <em>Drem Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.DremInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getDremInstruction()
	 * @generated
	 */
	int DREM_INSTRUCTION = 117;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DREM_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DREM_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DREM_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DREM_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DREM_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DREM_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DREM_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DREM_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DREM_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DREM_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Drem Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DREM_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Drem Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DREM_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.InegInstructionImpl <em>Ineg Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.InegInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getInegInstruction()
	 * @generated
	 */
	int INEG_INSTRUCTION = 118;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INEG_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INEG_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INEG_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INEG_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INEG_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INEG_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INEG_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INEG_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INEG_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INEG_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Ineg Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INEG_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Ineg Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INEG_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.LnegInstructionImpl <em>Lneg Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.LnegInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getLnegInstruction()
	 * @generated
	 */
	int LNEG_INSTRUCTION = 119;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LNEG_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LNEG_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LNEG_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LNEG_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LNEG_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LNEG_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LNEG_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LNEG_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LNEG_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LNEG_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Lneg Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LNEG_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Lneg Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LNEG_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.FnegInstructionImpl <em>Fneg Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.FnegInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getFnegInstruction()
	 * @generated
	 */
	int FNEG_INSTRUCTION = 120;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FNEG_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FNEG_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FNEG_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FNEG_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FNEG_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FNEG_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FNEG_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FNEG_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FNEG_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FNEG_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Fneg Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FNEG_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Fneg Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FNEG_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.DnegInstructionImpl <em>Dneg Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.DnegInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getDnegInstruction()
	 * @generated
	 */
	int DNEG_INSTRUCTION = 121;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DNEG_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DNEG_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DNEG_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DNEG_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DNEG_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DNEG_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DNEG_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DNEG_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DNEG_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DNEG_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Dneg Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DNEG_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Dneg Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DNEG_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.IshlInstructionImpl <em>Ishl Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.IshlInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIshlInstruction()
	 * @generated
	 */
	int ISHL_INSTRUCTION = 122;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISHL_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISHL_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISHL_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISHL_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISHL_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISHL_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISHL_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISHL_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISHL_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISHL_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Ishl Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISHL_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Ishl Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISHL_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.LshlInstructionImpl <em>Lshl Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.LshlInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getLshlInstruction()
	 * @generated
	 */
	int LSHL_INSTRUCTION = 123;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSHL_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSHL_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSHL_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSHL_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSHL_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSHL_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSHL_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSHL_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSHL_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSHL_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Lshl Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSHL_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Lshl Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSHL_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.IshrInstructionImpl <em>Ishr Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.IshrInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIshrInstruction()
	 * @generated
	 */
	int ISHR_INSTRUCTION = 124;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISHR_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISHR_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISHR_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISHR_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISHR_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISHR_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISHR_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISHR_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISHR_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISHR_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Ishr Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISHR_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Ishr Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISHR_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.LshrInstructionImpl <em>Lshr Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.LshrInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getLshrInstruction()
	 * @generated
	 */
	int LSHR_INSTRUCTION = 125;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSHR_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSHR_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSHR_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSHR_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSHR_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSHR_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSHR_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSHR_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSHR_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSHR_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Lshr Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSHR_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Lshr Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSHR_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.IushrInstructionImpl <em>Iushr Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.IushrInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIushrInstruction()
	 * @generated
	 */
	int IUSHR_INSTRUCTION = 126;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IUSHR_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IUSHR_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IUSHR_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IUSHR_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IUSHR_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IUSHR_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IUSHR_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IUSHR_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IUSHR_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IUSHR_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Iushr Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IUSHR_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Iushr Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IUSHR_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.LushrInstructionImpl <em>Lushr Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.LushrInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getLushrInstruction()
	 * @generated
	 */
	int LUSHR_INSTRUCTION = 127;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LUSHR_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LUSHR_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LUSHR_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LUSHR_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LUSHR_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LUSHR_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LUSHR_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LUSHR_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LUSHR_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LUSHR_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Lushr Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LUSHR_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Lushr Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LUSHR_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.IandInstructionImpl <em>Iand Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.IandInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIandInstruction()
	 * @generated
	 */
	int IAND_INSTRUCTION = 128;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IAND_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IAND_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IAND_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IAND_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IAND_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IAND_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IAND_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IAND_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IAND_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IAND_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Iand Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IAND_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Iand Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IAND_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.LandInstructionImpl <em>Land Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.LandInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getLandInstruction()
	 * @generated
	 */
	int LAND_INSTRUCTION = 129;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAND_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAND_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAND_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAND_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAND_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAND_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAND_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAND_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAND_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAND_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Land Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAND_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Land Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAND_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.IorInstructionImpl <em>Ior Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.IorInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIorInstruction()
	 * @generated
	 */
	int IOR_INSTRUCTION = 130;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IOR_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IOR_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IOR_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IOR_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IOR_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IOR_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IOR_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IOR_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IOR_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IOR_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Ior Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IOR_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Ior Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IOR_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.LorInstructionImpl <em>Lor Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.LorInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getLorInstruction()
	 * @generated
	 */
	int LOR_INSTRUCTION = 131;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOR_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOR_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOR_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOR_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOR_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOR_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOR_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOR_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOR_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOR_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Lor Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOR_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Lor Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOR_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.IxorInstructionImpl <em>Ixor Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.IxorInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIxorInstruction()
	 * @generated
	 */
	int IXOR_INSTRUCTION = 132;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IXOR_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IXOR_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IXOR_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IXOR_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IXOR_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IXOR_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IXOR_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IXOR_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IXOR_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IXOR_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Ixor Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IXOR_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Ixor Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IXOR_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.LxorInstructionImpl <em>Lxor Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.LxorInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getLxorInstruction()
	 * @generated
	 */
	int LXOR_INSTRUCTION = 133;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LXOR_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LXOR_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LXOR_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LXOR_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LXOR_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LXOR_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LXOR_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LXOR_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LXOR_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LXOR_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Lxor Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LXOR_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Lxor Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LXOR_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.I2lInstructionImpl <em>I2l Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.I2lInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getI2lInstruction()
	 * @generated
	 */
	int I2L_INSTRUCTION = 134;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2L_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2L_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2L_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2L_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2L_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2L_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2L_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2L_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2L_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2L_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>I2l Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2L_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>I2l Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2L_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.I2fInstructionImpl <em>I2f Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.I2fInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getI2fInstruction()
	 * @generated
	 */
	int I2F_INSTRUCTION = 135;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2F_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2F_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2F_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2F_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2F_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2F_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2F_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2F_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2F_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2F_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>I2f Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2F_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>I2f Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2F_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.I2dInstructionImpl <em>I2d Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.I2dInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getI2dInstruction()
	 * @generated
	 */
	int I2D_INSTRUCTION = 136;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2D_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2D_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2D_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2D_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2D_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2D_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2D_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2D_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2D_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2D_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>I2d Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2D_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>I2d Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2D_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.L2iInstructionImpl <em>L2i Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.L2iInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getL2iInstruction()
	 * @generated
	 */
	int L2I_INSTRUCTION = 137;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2I_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2I_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2I_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2I_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2I_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2I_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2I_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2I_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2I_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2I_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>L2i Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2I_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>L2i Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2I_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.L2fInstructionImpl <em>L2f Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.L2fInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getL2fInstruction()
	 * @generated
	 */
	int L2F_INSTRUCTION = 138;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2F_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2F_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2F_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2F_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2F_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2F_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2F_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2F_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2F_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2F_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>L2f Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2F_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>L2f Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2F_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.L2dInstructionImpl <em>L2d Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.L2dInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getL2dInstruction()
	 * @generated
	 */
	int L2D_INSTRUCTION = 139;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2D_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2D_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2D_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2D_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2D_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2D_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2D_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2D_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2D_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2D_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>L2d Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2D_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>L2d Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int L2D_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.F2iInstructionImpl <em>F2i Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.F2iInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getF2iInstruction()
	 * @generated
	 */
	int F2I_INSTRUCTION = 140;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2I_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2I_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2I_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2I_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2I_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2I_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2I_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2I_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2I_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2I_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>F2i Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2I_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>F2i Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2I_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.F2lInstructionImpl <em>F2l Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.F2lInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getF2lInstruction()
	 * @generated
	 */
	int F2L_INSTRUCTION = 141;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2L_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2L_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2L_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2L_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2L_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2L_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2L_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2L_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2L_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2L_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>F2l Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2L_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>F2l Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2L_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.F2dInstructionImpl <em>F2d Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.F2dInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getF2dInstruction()
	 * @generated
	 */
	int F2D_INSTRUCTION = 142;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2D_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2D_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2D_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2D_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2D_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2D_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2D_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2D_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2D_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2D_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>F2d Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2D_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>F2d Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F2D_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.D2iInstructionImpl <em>D2i Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.D2iInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getD2iInstruction()
	 * @generated
	 */
	int D2I_INSTRUCTION = 143;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2I_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2I_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2I_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2I_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2I_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2I_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2I_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2I_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2I_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2I_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>D2i Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2I_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>D2i Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2I_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.D2lInstructionImpl <em>D2l Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.D2lInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getD2lInstruction()
	 * @generated
	 */
	int D2L_INSTRUCTION = 144;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2L_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2L_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2L_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2L_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2L_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2L_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2L_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2L_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2L_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2L_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>D2l Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2L_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>D2l Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2L_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.D2fInstructionImpl <em>D2f Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.D2fInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getD2fInstruction()
	 * @generated
	 */
	int D2F_INSTRUCTION = 145;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2F_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2F_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2F_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2F_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2F_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2F_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2F_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2F_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2F_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2F_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>D2f Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2F_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>D2f Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int D2F_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.I2bInstructionImpl <em>I2b Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.I2bInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getI2bInstruction()
	 * @generated
	 */
	int I2B_INSTRUCTION = 146;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2B_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2B_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2B_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2B_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2B_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2B_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2B_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2B_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2B_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2B_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>I2b Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2B_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>I2b Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2B_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.I2cInstructionImpl <em>I2c Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.I2cInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getI2cInstruction()
	 * @generated
	 */
	int I2C_INSTRUCTION = 147;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2C_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2C_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2C_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2C_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2C_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2C_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2C_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2C_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2C_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2C_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>I2c Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2C_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>I2c Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2C_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.I2sInstructionImpl <em>I2s Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.I2sInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getI2sInstruction()
	 * @generated
	 */
	int I2S_INSTRUCTION = 148;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2S_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2S_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2S_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2S_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2S_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2S_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2S_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2S_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2S_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2S_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>I2s Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2S_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>I2s Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int I2S_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.LcmpInstructionImpl <em>Lcmp Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.LcmpInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getLcmpInstruction()
	 * @generated
	 */
	int LCMP_INSTRUCTION = 149;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCMP_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCMP_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCMP_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCMP_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCMP_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCMP_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCMP_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCMP_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCMP_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCMP_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Lcmp Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCMP_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Lcmp Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LCMP_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.FcmplInstructionImpl <em>Fcmpl Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.FcmplInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getFcmplInstruction()
	 * @generated
	 */
	int FCMPL_INSTRUCTION = 150;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCMPL_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCMPL_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCMPL_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCMPL_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCMPL_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCMPL_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCMPL_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCMPL_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCMPL_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCMPL_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Fcmpl Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCMPL_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Fcmpl Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCMPL_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.FcmpgInstructionImpl <em>Fcmpg Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.FcmpgInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getFcmpgInstruction()
	 * @generated
	 */
	int FCMPG_INSTRUCTION = 151;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCMPG_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCMPG_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCMPG_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCMPG_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCMPG_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCMPG_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCMPG_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCMPG_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCMPG_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCMPG_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Fcmpg Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCMPG_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Fcmpg Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FCMPG_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.DcmplInstructionImpl <em>Dcmpl Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.DcmplInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getDcmplInstruction()
	 * @generated
	 */
	int DCMPL_INSTRUCTION = 152;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCMPL_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCMPL_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCMPL_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCMPL_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCMPL_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCMPL_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCMPL_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCMPL_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCMPL_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCMPL_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Dcmpl Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCMPL_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Dcmpl Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCMPL_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.DcmpgInstructionImpl <em>Dcmpg Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.DcmpgInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getDcmpgInstruction()
	 * @generated
	 */
	int DCMPG_INSTRUCTION = 153;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCMPG_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCMPG_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCMPG_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCMPG_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCMPG_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCMPG_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCMPG_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCMPG_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCMPG_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCMPG_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Dcmpg Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCMPG_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Dcmpg Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DCMPG_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.IreturnInstructionImpl <em>Ireturn Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.IreturnInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIreturnInstruction()
	 * @generated
	 */
	int IRETURN_INSTRUCTION = 154;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRETURN_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRETURN_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRETURN_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRETURN_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRETURN_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRETURN_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRETURN_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRETURN_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRETURN_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRETURN_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Ireturn Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRETURN_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Ireturn Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRETURN_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.LreturnInstructionImpl <em>Lreturn Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.LreturnInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getLreturnInstruction()
	 * @generated
	 */
	int LRETURN_INSTRUCTION = 155;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LRETURN_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LRETURN_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LRETURN_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LRETURN_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LRETURN_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LRETURN_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LRETURN_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LRETURN_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LRETURN_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LRETURN_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Lreturn Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LRETURN_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Lreturn Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LRETURN_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.FreturnInstructionImpl <em>Freturn Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.FreturnInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getFreturnInstruction()
	 * @generated
	 */
	int FRETURN_INSTRUCTION = 156;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRETURN_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRETURN_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRETURN_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRETURN_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRETURN_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRETURN_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRETURN_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRETURN_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRETURN_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRETURN_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Freturn Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRETURN_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Freturn Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRETURN_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.DreturnInstructionImpl <em>Dreturn Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.DreturnInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getDreturnInstruction()
	 * @generated
	 */
	int DRETURN_INSTRUCTION = 157;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRETURN_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRETURN_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRETURN_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRETURN_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRETURN_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRETURN_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRETURN_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRETURN_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRETURN_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRETURN_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Dreturn Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRETURN_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Dreturn Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DRETURN_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.AreturnInstructionImpl <em>Areturn Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.AreturnInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getAreturnInstruction()
	 * @generated
	 */
	int ARETURN_INSTRUCTION = 158;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARETURN_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARETURN_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARETURN_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARETURN_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARETURN_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARETURN_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARETURN_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARETURN_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARETURN_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARETURN_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Areturn Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARETURN_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Areturn Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARETURN_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.ReturnInstructionImpl <em>Return Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.ReturnInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getReturnInstruction()
	 * @generated
	 */
	int RETURN_INSTRUCTION = 159;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Return Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Return Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.ArraylengthInstructionImpl <em>Arraylength Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.ArraylengthInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getArraylengthInstruction()
	 * @generated
	 */
	int ARRAYLENGTH_INSTRUCTION = 160;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAYLENGTH_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAYLENGTH_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAYLENGTH_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAYLENGTH_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAYLENGTH_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAYLENGTH_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAYLENGTH_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAYLENGTH_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAYLENGTH_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAYLENGTH_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Arraylength Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAYLENGTH_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Arraylength Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAYLENGTH_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.AthrowInstructionImpl <em>Athrow Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.AthrowInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getAthrowInstruction()
	 * @generated
	 */
	int ATHROW_INSTRUCTION = 161;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATHROW_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATHROW_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATHROW_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATHROW_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATHROW_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATHROW_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATHROW_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATHROW_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATHROW_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATHROW_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Athrow Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATHROW_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Athrow Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATHROW_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.MonitorenterInstructionImpl <em>Monitorenter Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.MonitorenterInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getMonitorenterInstruction()
	 * @generated
	 */
	int MONITORENTER_INSTRUCTION = 162;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORENTER_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORENTER_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORENTER_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORENTER_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORENTER_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORENTER_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORENTER_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORENTER_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORENTER_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORENTER_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Monitorenter Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORENTER_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Monitorenter Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORENTER_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.MonitorexitInstructionImpl <em>Monitorexit Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.MonitorexitInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getMonitorexitInstruction()
	 * @generated
	 */
	int MONITOREXIT_INSTRUCTION = 163;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITOREXIT_INSTRUCTION__UUID = SIMPLE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITOREXIT_INSTRUCTION__METHOD = SIMPLE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITOREXIT_INSTRUCTION__NEXT_IN_CODE_ORDER = SIMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITOREXIT_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = SIMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITOREXIT_INSTRUCTION__LINENUMBER = SIMPLE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITOREXIT_INSTRUCTION__INDEX = SIMPLE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITOREXIT_INSTRUCTION__OPCODE = SIMPLE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITOREXIT_INSTRUCTION__HUMAN_READABLE = SIMPLE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITOREXIT_INSTRUCTION__OUT_EDGES = SIMPLE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITOREXIT_INSTRUCTION__IN_EDGES = SIMPLE_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Monitorexit Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITOREXIT_INSTRUCTION_FEATURE_COUNT = SIMPLE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Monitorexit Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITOREXIT_INSTRUCTION_OPERATION_COUNT = SIMPLE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.BipushInstructionImpl <em>Bipush Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.BipushInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getBipushInstruction()
	 * @generated
	 */
	int BIPUSH_INSTRUCTION = 164;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIPUSH_INSTRUCTION__UUID = INT_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIPUSH_INSTRUCTION__METHOD = INT_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIPUSH_INSTRUCTION__NEXT_IN_CODE_ORDER = INT_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIPUSH_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = INT_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIPUSH_INSTRUCTION__LINENUMBER = INT_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIPUSH_INSTRUCTION__INDEX = INT_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIPUSH_INSTRUCTION__OPCODE = INT_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIPUSH_INSTRUCTION__HUMAN_READABLE = INT_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIPUSH_INSTRUCTION__OUT_EDGES = INT_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIPUSH_INSTRUCTION__IN_EDGES = INT_INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Operand</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIPUSH_INSTRUCTION__OPERAND = INT_INSTRUCTION__OPERAND;

	/**
	 * The number of structural features of the '<em>Bipush Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIPUSH_INSTRUCTION_FEATURE_COUNT = INT_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Bipush Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIPUSH_INSTRUCTION_OPERATION_COUNT = INT_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.SipushInstructionImpl <em>Sipush Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.SipushInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getSipushInstruction()
	 * @generated
	 */
	int SIPUSH_INSTRUCTION = 165;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIPUSH_INSTRUCTION__UUID = INT_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIPUSH_INSTRUCTION__METHOD = INT_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIPUSH_INSTRUCTION__NEXT_IN_CODE_ORDER = INT_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIPUSH_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = INT_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIPUSH_INSTRUCTION__LINENUMBER = INT_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIPUSH_INSTRUCTION__INDEX = INT_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIPUSH_INSTRUCTION__OPCODE = INT_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIPUSH_INSTRUCTION__HUMAN_READABLE = INT_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIPUSH_INSTRUCTION__OUT_EDGES = INT_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIPUSH_INSTRUCTION__IN_EDGES = INT_INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Operand</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIPUSH_INSTRUCTION__OPERAND = INT_INSTRUCTION__OPERAND;

	/**
	 * The number of structural features of the '<em>Sipush Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIPUSH_INSTRUCTION_FEATURE_COUNT = INT_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Sipush Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIPUSH_INSTRUCTION_OPERATION_COUNT = INT_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.NewarrayInstructionImpl <em>Newarray Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.NewarrayInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getNewarrayInstruction()
	 * @generated
	 */
	int NEWARRAY_INSTRUCTION = 166;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEWARRAY_INSTRUCTION__UUID = INT_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEWARRAY_INSTRUCTION__METHOD = INT_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEWARRAY_INSTRUCTION__NEXT_IN_CODE_ORDER = INT_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEWARRAY_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = INT_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEWARRAY_INSTRUCTION__LINENUMBER = INT_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEWARRAY_INSTRUCTION__INDEX = INT_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEWARRAY_INSTRUCTION__OPCODE = INT_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEWARRAY_INSTRUCTION__HUMAN_READABLE = INT_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEWARRAY_INSTRUCTION__OUT_EDGES = INT_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEWARRAY_INSTRUCTION__IN_EDGES = INT_INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Operand</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEWARRAY_INSTRUCTION__OPERAND = INT_INSTRUCTION__OPERAND;

	/**
	 * The number of structural features of the '<em>Newarray Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEWARRAY_INSTRUCTION_FEATURE_COUNT = INT_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Newarray Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEWARRAY_INSTRUCTION_OPERATION_COUNT = INT_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.IfeqInstructionImpl <em>Ifeq Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.IfeqInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIfeqInstruction()
	 * @generated
	 */
	int IFEQ_INSTRUCTION = 167;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFEQ_INSTRUCTION__UUID = JUMP_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFEQ_INSTRUCTION__METHOD = JUMP_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFEQ_INSTRUCTION__NEXT_IN_CODE_ORDER = JUMP_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFEQ_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = JUMP_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFEQ_INSTRUCTION__LINENUMBER = JUMP_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFEQ_INSTRUCTION__INDEX = JUMP_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFEQ_INSTRUCTION__OPCODE = JUMP_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFEQ_INSTRUCTION__HUMAN_READABLE = JUMP_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFEQ_INSTRUCTION__OUT_EDGES = JUMP_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFEQ_INSTRUCTION__IN_EDGES = JUMP_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Ifeq Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFEQ_INSTRUCTION_FEATURE_COUNT = JUMP_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Ifeq Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFEQ_INSTRUCTION_OPERATION_COUNT = JUMP_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.IfneInstructionImpl <em>Ifne Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.IfneInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIfneInstruction()
	 * @generated
	 */
	int IFNE_INSTRUCTION = 168;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNE_INSTRUCTION__UUID = JUMP_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNE_INSTRUCTION__METHOD = JUMP_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNE_INSTRUCTION__NEXT_IN_CODE_ORDER = JUMP_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = JUMP_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNE_INSTRUCTION__LINENUMBER = JUMP_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNE_INSTRUCTION__INDEX = JUMP_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNE_INSTRUCTION__OPCODE = JUMP_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNE_INSTRUCTION__HUMAN_READABLE = JUMP_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNE_INSTRUCTION__OUT_EDGES = JUMP_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNE_INSTRUCTION__IN_EDGES = JUMP_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Ifne Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNE_INSTRUCTION_FEATURE_COUNT = JUMP_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Ifne Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNE_INSTRUCTION_OPERATION_COUNT = JUMP_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.IfltInstructionImpl <em>Iflt Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.IfltInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIfltInstruction()
	 * @generated
	 */
	int IFLT_INSTRUCTION = 169;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFLT_INSTRUCTION__UUID = JUMP_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFLT_INSTRUCTION__METHOD = JUMP_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFLT_INSTRUCTION__NEXT_IN_CODE_ORDER = JUMP_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFLT_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = JUMP_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFLT_INSTRUCTION__LINENUMBER = JUMP_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFLT_INSTRUCTION__INDEX = JUMP_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFLT_INSTRUCTION__OPCODE = JUMP_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFLT_INSTRUCTION__HUMAN_READABLE = JUMP_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFLT_INSTRUCTION__OUT_EDGES = JUMP_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFLT_INSTRUCTION__IN_EDGES = JUMP_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Iflt Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFLT_INSTRUCTION_FEATURE_COUNT = JUMP_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Iflt Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFLT_INSTRUCTION_OPERATION_COUNT = JUMP_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.IfgeInstructionImpl <em>Ifge Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.IfgeInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIfgeInstruction()
	 * @generated
	 */
	int IFGE_INSTRUCTION = 170;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFGE_INSTRUCTION__UUID = JUMP_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFGE_INSTRUCTION__METHOD = JUMP_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFGE_INSTRUCTION__NEXT_IN_CODE_ORDER = JUMP_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFGE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = JUMP_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFGE_INSTRUCTION__LINENUMBER = JUMP_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFGE_INSTRUCTION__INDEX = JUMP_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFGE_INSTRUCTION__OPCODE = JUMP_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFGE_INSTRUCTION__HUMAN_READABLE = JUMP_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFGE_INSTRUCTION__OUT_EDGES = JUMP_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFGE_INSTRUCTION__IN_EDGES = JUMP_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Ifge Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFGE_INSTRUCTION_FEATURE_COUNT = JUMP_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Ifge Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFGE_INSTRUCTION_OPERATION_COUNT = JUMP_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.IfgtInstructionImpl <em>Ifgt Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.IfgtInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIfgtInstruction()
	 * @generated
	 */
	int IFGT_INSTRUCTION = 171;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFGT_INSTRUCTION__UUID = JUMP_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFGT_INSTRUCTION__METHOD = JUMP_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFGT_INSTRUCTION__NEXT_IN_CODE_ORDER = JUMP_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFGT_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = JUMP_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFGT_INSTRUCTION__LINENUMBER = JUMP_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFGT_INSTRUCTION__INDEX = JUMP_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFGT_INSTRUCTION__OPCODE = JUMP_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFGT_INSTRUCTION__HUMAN_READABLE = JUMP_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFGT_INSTRUCTION__OUT_EDGES = JUMP_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFGT_INSTRUCTION__IN_EDGES = JUMP_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Ifgt Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFGT_INSTRUCTION_FEATURE_COUNT = JUMP_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Ifgt Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFGT_INSTRUCTION_OPERATION_COUNT = JUMP_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.IfleInstructionImpl <em>Ifle Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.IfleInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIfleInstruction()
	 * @generated
	 */
	int IFLE_INSTRUCTION = 172;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFLE_INSTRUCTION__UUID = JUMP_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFLE_INSTRUCTION__METHOD = JUMP_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFLE_INSTRUCTION__NEXT_IN_CODE_ORDER = JUMP_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = JUMP_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFLE_INSTRUCTION__LINENUMBER = JUMP_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFLE_INSTRUCTION__INDEX = JUMP_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFLE_INSTRUCTION__OPCODE = JUMP_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFLE_INSTRUCTION__HUMAN_READABLE = JUMP_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFLE_INSTRUCTION__OUT_EDGES = JUMP_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFLE_INSTRUCTION__IN_EDGES = JUMP_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Ifle Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFLE_INSTRUCTION_FEATURE_COUNT = JUMP_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Ifle Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFLE_INSTRUCTION_OPERATION_COUNT = JUMP_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.If_icmpeqInstructionImpl <em>If icmpeq Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.If_icmpeqInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIf_icmpeqInstruction()
	 * @generated
	 */
	int IF_ICMPEQ_INSTRUCTION = 173;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPEQ_INSTRUCTION__UUID = JUMP_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPEQ_INSTRUCTION__METHOD = JUMP_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPEQ_INSTRUCTION__NEXT_IN_CODE_ORDER = JUMP_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPEQ_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = JUMP_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPEQ_INSTRUCTION__LINENUMBER = JUMP_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPEQ_INSTRUCTION__INDEX = JUMP_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPEQ_INSTRUCTION__OPCODE = JUMP_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPEQ_INSTRUCTION__HUMAN_READABLE = JUMP_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPEQ_INSTRUCTION__OUT_EDGES = JUMP_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPEQ_INSTRUCTION__IN_EDGES = JUMP_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>If icmpeq Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPEQ_INSTRUCTION_FEATURE_COUNT = JUMP_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>If icmpeq Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPEQ_INSTRUCTION_OPERATION_COUNT = JUMP_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.If_icmpneInstructionImpl <em>If icmpne Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.If_icmpneInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIf_icmpneInstruction()
	 * @generated
	 */
	int IF_ICMPNE_INSTRUCTION = 174;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPNE_INSTRUCTION__UUID = JUMP_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPNE_INSTRUCTION__METHOD = JUMP_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPNE_INSTRUCTION__NEXT_IN_CODE_ORDER = JUMP_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPNE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = JUMP_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPNE_INSTRUCTION__LINENUMBER = JUMP_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPNE_INSTRUCTION__INDEX = JUMP_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPNE_INSTRUCTION__OPCODE = JUMP_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPNE_INSTRUCTION__HUMAN_READABLE = JUMP_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPNE_INSTRUCTION__OUT_EDGES = JUMP_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPNE_INSTRUCTION__IN_EDGES = JUMP_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>If icmpne Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPNE_INSTRUCTION_FEATURE_COUNT = JUMP_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>If icmpne Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPNE_INSTRUCTION_OPERATION_COUNT = JUMP_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.If_icmpltInstructionImpl <em>If icmplt Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.If_icmpltInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIf_icmpltInstruction()
	 * @generated
	 */
	int IF_ICMPLT_INSTRUCTION = 175;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPLT_INSTRUCTION__UUID = JUMP_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPLT_INSTRUCTION__METHOD = JUMP_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPLT_INSTRUCTION__NEXT_IN_CODE_ORDER = JUMP_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPLT_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = JUMP_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPLT_INSTRUCTION__LINENUMBER = JUMP_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPLT_INSTRUCTION__INDEX = JUMP_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPLT_INSTRUCTION__OPCODE = JUMP_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPLT_INSTRUCTION__HUMAN_READABLE = JUMP_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPLT_INSTRUCTION__OUT_EDGES = JUMP_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPLT_INSTRUCTION__IN_EDGES = JUMP_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>If icmplt Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPLT_INSTRUCTION_FEATURE_COUNT = JUMP_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>If icmplt Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPLT_INSTRUCTION_OPERATION_COUNT = JUMP_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.If_icmpgeInstructionImpl <em>If icmpge Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.If_icmpgeInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIf_icmpgeInstruction()
	 * @generated
	 */
	int IF_ICMPGE_INSTRUCTION = 176;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPGE_INSTRUCTION__UUID = JUMP_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPGE_INSTRUCTION__METHOD = JUMP_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPGE_INSTRUCTION__NEXT_IN_CODE_ORDER = JUMP_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPGE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = JUMP_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPGE_INSTRUCTION__LINENUMBER = JUMP_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPGE_INSTRUCTION__INDEX = JUMP_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPGE_INSTRUCTION__OPCODE = JUMP_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPGE_INSTRUCTION__HUMAN_READABLE = JUMP_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPGE_INSTRUCTION__OUT_EDGES = JUMP_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPGE_INSTRUCTION__IN_EDGES = JUMP_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>If icmpge Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPGE_INSTRUCTION_FEATURE_COUNT = JUMP_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>If icmpge Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPGE_INSTRUCTION_OPERATION_COUNT = JUMP_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.If_icmpgtInstructionImpl <em>If icmpgt Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.If_icmpgtInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIf_icmpgtInstruction()
	 * @generated
	 */
	int IF_ICMPGT_INSTRUCTION = 177;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPGT_INSTRUCTION__UUID = JUMP_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPGT_INSTRUCTION__METHOD = JUMP_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPGT_INSTRUCTION__NEXT_IN_CODE_ORDER = JUMP_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPGT_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = JUMP_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPGT_INSTRUCTION__LINENUMBER = JUMP_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPGT_INSTRUCTION__INDEX = JUMP_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPGT_INSTRUCTION__OPCODE = JUMP_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPGT_INSTRUCTION__HUMAN_READABLE = JUMP_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPGT_INSTRUCTION__OUT_EDGES = JUMP_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPGT_INSTRUCTION__IN_EDGES = JUMP_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>If icmpgt Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPGT_INSTRUCTION_FEATURE_COUNT = JUMP_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>If icmpgt Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPGT_INSTRUCTION_OPERATION_COUNT = JUMP_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.If_icmpleInstructionImpl <em>If icmple Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.If_icmpleInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIf_icmpleInstruction()
	 * @generated
	 */
	int IF_ICMPLE_INSTRUCTION = 178;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPLE_INSTRUCTION__UUID = JUMP_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPLE_INSTRUCTION__METHOD = JUMP_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPLE_INSTRUCTION__NEXT_IN_CODE_ORDER = JUMP_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPLE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = JUMP_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPLE_INSTRUCTION__LINENUMBER = JUMP_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPLE_INSTRUCTION__INDEX = JUMP_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPLE_INSTRUCTION__OPCODE = JUMP_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPLE_INSTRUCTION__HUMAN_READABLE = JUMP_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPLE_INSTRUCTION__OUT_EDGES = JUMP_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPLE_INSTRUCTION__IN_EDGES = JUMP_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>If icmple Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPLE_INSTRUCTION_FEATURE_COUNT = JUMP_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>If icmple Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ICMPLE_INSTRUCTION_OPERATION_COUNT = JUMP_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.If_acmpeqInstructionImpl <em>If acmpeq Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.If_acmpeqInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIf_acmpeqInstruction()
	 * @generated
	 */
	int IF_ACMPEQ_INSTRUCTION = 179;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ACMPEQ_INSTRUCTION__UUID = JUMP_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ACMPEQ_INSTRUCTION__METHOD = JUMP_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ACMPEQ_INSTRUCTION__NEXT_IN_CODE_ORDER = JUMP_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ACMPEQ_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = JUMP_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ACMPEQ_INSTRUCTION__LINENUMBER = JUMP_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ACMPEQ_INSTRUCTION__INDEX = JUMP_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ACMPEQ_INSTRUCTION__OPCODE = JUMP_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ACMPEQ_INSTRUCTION__HUMAN_READABLE = JUMP_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ACMPEQ_INSTRUCTION__OUT_EDGES = JUMP_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ACMPEQ_INSTRUCTION__IN_EDGES = JUMP_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>If acmpeq Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ACMPEQ_INSTRUCTION_FEATURE_COUNT = JUMP_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>If acmpeq Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ACMPEQ_INSTRUCTION_OPERATION_COUNT = JUMP_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.If_acmpneInstructionImpl <em>If acmpne Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.If_acmpneInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIf_acmpneInstruction()
	 * @generated
	 */
	int IF_ACMPNE_INSTRUCTION = 180;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ACMPNE_INSTRUCTION__UUID = JUMP_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ACMPNE_INSTRUCTION__METHOD = JUMP_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ACMPNE_INSTRUCTION__NEXT_IN_CODE_ORDER = JUMP_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ACMPNE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = JUMP_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ACMPNE_INSTRUCTION__LINENUMBER = JUMP_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ACMPNE_INSTRUCTION__INDEX = JUMP_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ACMPNE_INSTRUCTION__OPCODE = JUMP_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ACMPNE_INSTRUCTION__HUMAN_READABLE = JUMP_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ACMPNE_INSTRUCTION__OUT_EDGES = JUMP_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ACMPNE_INSTRUCTION__IN_EDGES = JUMP_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>If acmpne Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ACMPNE_INSTRUCTION_FEATURE_COUNT = JUMP_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>If acmpne Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ACMPNE_INSTRUCTION_OPERATION_COUNT = JUMP_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.GotoInstructionImpl <em>Goto Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.GotoInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getGotoInstruction()
	 * @generated
	 */
	int GOTO_INSTRUCTION = 181;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOTO_INSTRUCTION__UUID = JUMP_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOTO_INSTRUCTION__METHOD = JUMP_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOTO_INSTRUCTION__NEXT_IN_CODE_ORDER = JUMP_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOTO_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = JUMP_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOTO_INSTRUCTION__LINENUMBER = JUMP_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOTO_INSTRUCTION__INDEX = JUMP_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOTO_INSTRUCTION__OPCODE = JUMP_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOTO_INSTRUCTION__HUMAN_READABLE = JUMP_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOTO_INSTRUCTION__OUT_EDGES = JUMP_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOTO_INSTRUCTION__IN_EDGES = JUMP_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Goto Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOTO_INSTRUCTION_FEATURE_COUNT = JUMP_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Goto Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOTO_INSTRUCTION_OPERATION_COUNT = JUMP_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.JsrInstructionImpl <em>Jsr Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.JsrInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getJsrInstruction()
	 * @generated
	 */
	int JSR_INSTRUCTION = 182;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSR_INSTRUCTION__UUID = JUMP_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSR_INSTRUCTION__METHOD = JUMP_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSR_INSTRUCTION__NEXT_IN_CODE_ORDER = JUMP_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSR_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = JUMP_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSR_INSTRUCTION__LINENUMBER = JUMP_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSR_INSTRUCTION__INDEX = JUMP_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSR_INSTRUCTION__OPCODE = JUMP_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSR_INSTRUCTION__HUMAN_READABLE = JUMP_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSR_INSTRUCTION__OUT_EDGES = JUMP_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSR_INSTRUCTION__IN_EDGES = JUMP_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Jsr Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSR_INSTRUCTION_FEATURE_COUNT = JUMP_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Jsr Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSR_INSTRUCTION_OPERATION_COUNT = JUMP_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.IfnullInstructionImpl <em>Ifnull Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.IfnullInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIfnullInstruction()
	 * @generated
	 */
	int IFNULL_INSTRUCTION = 183;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNULL_INSTRUCTION__UUID = JUMP_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNULL_INSTRUCTION__METHOD = JUMP_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNULL_INSTRUCTION__NEXT_IN_CODE_ORDER = JUMP_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNULL_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = JUMP_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNULL_INSTRUCTION__LINENUMBER = JUMP_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNULL_INSTRUCTION__INDEX = JUMP_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNULL_INSTRUCTION__OPCODE = JUMP_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNULL_INSTRUCTION__HUMAN_READABLE = JUMP_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNULL_INSTRUCTION__OUT_EDGES = JUMP_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNULL_INSTRUCTION__IN_EDGES = JUMP_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Ifnull Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNULL_INSTRUCTION_FEATURE_COUNT = JUMP_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Ifnull Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNULL_INSTRUCTION_OPERATION_COUNT = JUMP_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.IfnonnullInstructionImpl <em>Ifnonnull Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.IfnonnullInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIfnonnullInstruction()
	 * @generated
	 */
	int IFNONNULL_INSTRUCTION = 184;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNONNULL_INSTRUCTION__UUID = JUMP_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNONNULL_INSTRUCTION__METHOD = JUMP_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNONNULL_INSTRUCTION__NEXT_IN_CODE_ORDER = JUMP_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNONNULL_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = JUMP_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNONNULL_INSTRUCTION__LINENUMBER = JUMP_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNONNULL_INSTRUCTION__INDEX = JUMP_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNONNULL_INSTRUCTION__OPCODE = JUMP_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNONNULL_INSTRUCTION__HUMAN_READABLE = JUMP_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNONNULL_INSTRUCTION__OUT_EDGES = JUMP_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNONNULL_INSTRUCTION__IN_EDGES = JUMP_INSTRUCTION__IN_EDGES;

	/**
	 * The number of structural features of the '<em>Ifnonnull Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNONNULL_INSTRUCTION_FEATURE_COUNT = JUMP_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Ifnonnull Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IFNONNULL_INSTRUCTION_OPERATION_COUNT = JUMP_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.InvokevirtualInstructionImpl <em>Invokevirtual Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.InvokevirtualInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getInvokevirtualInstruction()
	 * @generated
	 */
	int INVOKEVIRTUAL_INSTRUCTION = 185;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKEVIRTUAL_INSTRUCTION__UUID = METHOD_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKEVIRTUAL_INSTRUCTION__METHOD = METHOD_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKEVIRTUAL_INSTRUCTION__NEXT_IN_CODE_ORDER = METHOD_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKEVIRTUAL_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = METHOD_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKEVIRTUAL_INSTRUCTION__LINENUMBER = METHOD_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKEVIRTUAL_INSTRUCTION__INDEX = METHOD_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKEVIRTUAL_INSTRUCTION__OPCODE = METHOD_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKEVIRTUAL_INSTRUCTION__HUMAN_READABLE = METHOD_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKEVIRTUAL_INSTRUCTION__OUT_EDGES = METHOD_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKEVIRTUAL_INSTRUCTION__IN_EDGES = METHOD_INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Method Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKEVIRTUAL_INSTRUCTION__METHOD_REFERENCE = METHOD_INSTRUCTION__METHOD_REFERENCE;

	/**
	 * The number of structural features of the '<em>Invokevirtual Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKEVIRTUAL_INSTRUCTION_FEATURE_COUNT = METHOD_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Invokevirtual Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKEVIRTUAL_INSTRUCTION_OPERATION_COUNT = METHOD_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.InvokespecialInstructionImpl <em>Invokespecial Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.InvokespecialInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getInvokespecialInstruction()
	 * @generated
	 */
	int INVOKESPECIAL_INSTRUCTION = 186;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKESPECIAL_INSTRUCTION__UUID = METHOD_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKESPECIAL_INSTRUCTION__METHOD = METHOD_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKESPECIAL_INSTRUCTION__NEXT_IN_CODE_ORDER = METHOD_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKESPECIAL_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = METHOD_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKESPECIAL_INSTRUCTION__LINENUMBER = METHOD_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKESPECIAL_INSTRUCTION__INDEX = METHOD_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKESPECIAL_INSTRUCTION__OPCODE = METHOD_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKESPECIAL_INSTRUCTION__HUMAN_READABLE = METHOD_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKESPECIAL_INSTRUCTION__OUT_EDGES = METHOD_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKESPECIAL_INSTRUCTION__IN_EDGES = METHOD_INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Method Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKESPECIAL_INSTRUCTION__METHOD_REFERENCE = METHOD_INSTRUCTION__METHOD_REFERENCE;

	/**
	 * The number of structural features of the '<em>Invokespecial Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKESPECIAL_INSTRUCTION_FEATURE_COUNT = METHOD_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Invokespecial Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKESPECIAL_INSTRUCTION_OPERATION_COUNT = METHOD_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.InvokestaticInstructionImpl <em>Invokestatic Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.InvokestaticInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getInvokestaticInstruction()
	 * @generated
	 */
	int INVOKESTATIC_INSTRUCTION = 187;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKESTATIC_INSTRUCTION__UUID = METHOD_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKESTATIC_INSTRUCTION__METHOD = METHOD_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKESTATIC_INSTRUCTION__NEXT_IN_CODE_ORDER = METHOD_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKESTATIC_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = METHOD_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKESTATIC_INSTRUCTION__LINENUMBER = METHOD_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKESTATIC_INSTRUCTION__INDEX = METHOD_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKESTATIC_INSTRUCTION__OPCODE = METHOD_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKESTATIC_INSTRUCTION__HUMAN_READABLE = METHOD_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKESTATIC_INSTRUCTION__OUT_EDGES = METHOD_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKESTATIC_INSTRUCTION__IN_EDGES = METHOD_INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Method Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKESTATIC_INSTRUCTION__METHOD_REFERENCE = METHOD_INSTRUCTION__METHOD_REFERENCE;

	/**
	 * The number of structural features of the '<em>Invokestatic Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKESTATIC_INSTRUCTION_FEATURE_COUNT = METHOD_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Invokestatic Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKESTATIC_INSTRUCTION_OPERATION_COUNT = METHOD_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.InvokeinterfaceInstructionImpl <em>Invokeinterface Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.InvokeinterfaceInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getInvokeinterfaceInstruction()
	 * @generated
	 */
	int INVOKEINTERFACE_INSTRUCTION = 188;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKEINTERFACE_INSTRUCTION__UUID = METHOD_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKEINTERFACE_INSTRUCTION__METHOD = METHOD_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKEINTERFACE_INSTRUCTION__NEXT_IN_CODE_ORDER = METHOD_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKEINTERFACE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = METHOD_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKEINTERFACE_INSTRUCTION__LINENUMBER = METHOD_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKEINTERFACE_INSTRUCTION__INDEX = METHOD_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKEINTERFACE_INSTRUCTION__OPCODE = METHOD_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKEINTERFACE_INSTRUCTION__HUMAN_READABLE = METHOD_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKEINTERFACE_INSTRUCTION__OUT_EDGES = METHOD_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKEINTERFACE_INSTRUCTION__IN_EDGES = METHOD_INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Method Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKEINTERFACE_INSTRUCTION__METHOD_REFERENCE = METHOD_INSTRUCTION__METHOD_REFERENCE;

	/**
	 * The number of structural features of the '<em>Invokeinterface Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKEINTERFACE_INSTRUCTION_FEATURE_COUNT = METHOD_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Invokeinterface Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INVOKEINTERFACE_INSTRUCTION_OPERATION_COUNT = METHOD_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.NewInstructionImpl <em>New Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.NewInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getNewInstruction()
	 * @generated
	 */
	int NEW_INSTRUCTION = 189;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_INSTRUCTION__UUID = TYPE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_INSTRUCTION__METHOD = TYPE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_INSTRUCTION__NEXT_IN_CODE_ORDER = TYPE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = TYPE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_INSTRUCTION__LINENUMBER = TYPE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_INSTRUCTION__INDEX = TYPE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_INSTRUCTION__OPCODE = TYPE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_INSTRUCTION__HUMAN_READABLE = TYPE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_INSTRUCTION__OUT_EDGES = TYPE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_INSTRUCTION__IN_EDGES = TYPE_INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Type Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_INSTRUCTION__TYPE_REFERENCE = TYPE_INSTRUCTION__TYPE_REFERENCE;

	/**
	 * The number of structural features of the '<em>New Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_INSTRUCTION_FEATURE_COUNT = TYPE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>New Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEW_INSTRUCTION_OPERATION_COUNT = TYPE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.AnewarrayInstructionImpl <em>Anewarray Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.AnewarrayInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getAnewarrayInstruction()
	 * @generated
	 */
	int ANEWARRAY_INSTRUCTION = 190;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANEWARRAY_INSTRUCTION__UUID = TYPE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANEWARRAY_INSTRUCTION__METHOD = TYPE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANEWARRAY_INSTRUCTION__NEXT_IN_CODE_ORDER = TYPE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANEWARRAY_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = TYPE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANEWARRAY_INSTRUCTION__LINENUMBER = TYPE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANEWARRAY_INSTRUCTION__INDEX = TYPE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANEWARRAY_INSTRUCTION__OPCODE = TYPE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANEWARRAY_INSTRUCTION__HUMAN_READABLE = TYPE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANEWARRAY_INSTRUCTION__OUT_EDGES = TYPE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANEWARRAY_INSTRUCTION__IN_EDGES = TYPE_INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Type Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANEWARRAY_INSTRUCTION__TYPE_REFERENCE = TYPE_INSTRUCTION__TYPE_REFERENCE;

	/**
	 * The number of structural features of the '<em>Anewarray Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANEWARRAY_INSTRUCTION_FEATURE_COUNT = TYPE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Anewarray Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANEWARRAY_INSTRUCTION_OPERATION_COUNT = TYPE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.CheckcastInstructionImpl <em>Checkcast Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.CheckcastInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getCheckcastInstruction()
	 * @generated
	 */
	int CHECKCAST_INSTRUCTION = 191;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECKCAST_INSTRUCTION__UUID = TYPE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECKCAST_INSTRUCTION__METHOD = TYPE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECKCAST_INSTRUCTION__NEXT_IN_CODE_ORDER = TYPE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECKCAST_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = TYPE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECKCAST_INSTRUCTION__LINENUMBER = TYPE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECKCAST_INSTRUCTION__INDEX = TYPE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECKCAST_INSTRUCTION__OPCODE = TYPE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECKCAST_INSTRUCTION__HUMAN_READABLE = TYPE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECKCAST_INSTRUCTION__OUT_EDGES = TYPE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECKCAST_INSTRUCTION__IN_EDGES = TYPE_INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Type Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECKCAST_INSTRUCTION__TYPE_REFERENCE = TYPE_INSTRUCTION__TYPE_REFERENCE;

	/**
	 * The number of structural features of the '<em>Checkcast Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECKCAST_INSTRUCTION_FEATURE_COUNT = TYPE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Checkcast Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECKCAST_INSTRUCTION_OPERATION_COUNT = TYPE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.InstanceofInstructionImpl <em>Instanceof Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.InstanceofInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getInstanceofInstruction()
	 * @generated
	 */
	int INSTANCEOF_INSTRUCTION = 192;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCEOF_INSTRUCTION__UUID = TYPE_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCEOF_INSTRUCTION__METHOD = TYPE_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCEOF_INSTRUCTION__NEXT_IN_CODE_ORDER = TYPE_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCEOF_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = TYPE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCEOF_INSTRUCTION__LINENUMBER = TYPE_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCEOF_INSTRUCTION__INDEX = TYPE_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCEOF_INSTRUCTION__OPCODE = TYPE_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCEOF_INSTRUCTION__HUMAN_READABLE = TYPE_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCEOF_INSTRUCTION__OUT_EDGES = TYPE_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCEOF_INSTRUCTION__IN_EDGES = TYPE_INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Type Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCEOF_INSTRUCTION__TYPE_REFERENCE = TYPE_INSTRUCTION__TYPE_REFERENCE;

	/**
	 * The number of structural features of the '<em>Instanceof Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCEOF_INSTRUCTION_FEATURE_COUNT = TYPE_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Instanceof Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANCEOF_INSTRUCTION_OPERATION_COUNT = TYPE_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.IloadInstructionImpl <em>Iload Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.IloadInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIloadInstruction()
	 * @generated
	 */
	int ILOAD_INSTRUCTION = 193;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ILOAD_INSTRUCTION__UUID = VAR_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ILOAD_INSTRUCTION__METHOD = VAR_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ILOAD_INSTRUCTION__NEXT_IN_CODE_ORDER = VAR_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ILOAD_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = VAR_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ILOAD_INSTRUCTION__LINENUMBER = VAR_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ILOAD_INSTRUCTION__INDEX = VAR_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ILOAD_INSTRUCTION__OPCODE = VAR_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ILOAD_INSTRUCTION__HUMAN_READABLE = VAR_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ILOAD_INSTRUCTION__OUT_EDGES = VAR_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ILOAD_INSTRUCTION__IN_EDGES = VAR_INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Local Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ILOAD_INSTRUCTION__LOCAL_VARIABLE = VAR_INSTRUCTION__LOCAL_VARIABLE;

	/**
	 * The feature id for the '<em><b>Var Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ILOAD_INSTRUCTION__VAR_INDEX = VAR_INSTRUCTION__VAR_INDEX;

	/**
	 * The number of structural features of the '<em>Iload Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ILOAD_INSTRUCTION_FEATURE_COUNT = VAR_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Iload Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ILOAD_INSTRUCTION_OPERATION_COUNT = VAR_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.LloadInstructionImpl <em>Lload Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.LloadInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getLloadInstruction()
	 * @generated
	 */
	int LLOAD_INSTRUCTION = 194;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LLOAD_INSTRUCTION__UUID = VAR_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LLOAD_INSTRUCTION__METHOD = VAR_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LLOAD_INSTRUCTION__NEXT_IN_CODE_ORDER = VAR_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LLOAD_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = VAR_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LLOAD_INSTRUCTION__LINENUMBER = VAR_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LLOAD_INSTRUCTION__INDEX = VAR_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LLOAD_INSTRUCTION__OPCODE = VAR_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LLOAD_INSTRUCTION__HUMAN_READABLE = VAR_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LLOAD_INSTRUCTION__OUT_EDGES = VAR_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LLOAD_INSTRUCTION__IN_EDGES = VAR_INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Local Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LLOAD_INSTRUCTION__LOCAL_VARIABLE = VAR_INSTRUCTION__LOCAL_VARIABLE;

	/**
	 * The feature id for the '<em><b>Var Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LLOAD_INSTRUCTION__VAR_INDEX = VAR_INSTRUCTION__VAR_INDEX;

	/**
	 * The number of structural features of the '<em>Lload Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LLOAD_INSTRUCTION_FEATURE_COUNT = VAR_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Lload Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LLOAD_INSTRUCTION_OPERATION_COUNT = VAR_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.FloadInstructionImpl <em>Fload Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.FloadInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getFloadInstruction()
	 * @generated
	 */
	int FLOAD_INSTRUCTION = 195;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAD_INSTRUCTION__UUID = VAR_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAD_INSTRUCTION__METHOD = VAR_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAD_INSTRUCTION__NEXT_IN_CODE_ORDER = VAR_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAD_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = VAR_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAD_INSTRUCTION__LINENUMBER = VAR_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAD_INSTRUCTION__INDEX = VAR_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAD_INSTRUCTION__OPCODE = VAR_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAD_INSTRUCTION__HUMAN_READABLE = VAR_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAD_INSTRUCTION__OUT_EDGES = VAR_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAD_INSTRUCTION__IN_EDGES = VAR_INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Local Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAD_INSTRUCTION__LOCAL_VARIABLE = VAR_INSTRUCTION__LOCAL_VARIABLE;

	/**
	 * The feature id for the '<em><b>Var Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAD_INSTRUCTION__VAR_INDEX = VAR_INSTRUCTION__VAR_INDEX;

	/**
	 * The number of structural features of the '<em>Fload Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAD_INSTRUCTION_FEATURE_COUNT = VAR_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Fload Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAD_INSTRUCTION_OPERATION_COUNT = VAR_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.DloadInstructionImpl <em>Dload Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.DloadInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getDloadInstruction()
	 * @generated
	 */
	int DLOAD_INSTRUCTION = 196;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLOAD_INSTRUCTION__UUID = VAR_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLOAD_INSTRUCTION__METHOD = VAR_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLOAD_INSTRUCTION__NEXT_IN_CODE_ORDER = VAR_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLOAD_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = VAR_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLOAD_INSTRUCTION__LINENUMBER = VAR_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLOAD_INSTRUCTION__INDEX = VAR_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLOAD_INSTRUCTION__OPCODE = VAR_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLOAD_INSTRUCTION__HUMAN_READABLE = VAR_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLOAD_INSTRUCTION__OUT_EDGES = VAR_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLOAD_INSTRUCTION__IN_EDGES = VAR_INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Local Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLOAD_INSTRUCTION__LOCAL_VARIABLE = VAR_INSTRUCTION__LOCAL_VARIABLE;

	/**
	 * The feature id for the '<em><b>Var Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLOAD_INSTRUCTION__VAR_INDEX = VAR_INSTRUCTION__VAR_INDEX;

	/**
	 * The number of structural features of the '<em>Dload Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLOAD_INSTRUCTION_FEATURE_COUNT = VAR_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Dload Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLOAD_INSTRUCTION_OPERATION_COUNT = VAR_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.AloadInstructionImpl <em>Aload Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.AloadInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getAloadInstruction()
	 * @generated
	 */
	int ALOAD_INSTRUCTION = 197;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALOAD_INSTRUCTION__UUID = VAR_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALOAD_INSTRUCTION__METHOD = VAR_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALOAD_INSTRUCTION__NEXT_IN_CODE_ORDER = VAR_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALOAD_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = VAR_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALOAD_INSTRUCTION__LINENUMBER = VAR_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALOAD_INSTRUCTION__INDEX = VAR_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALOAD_INSTRUCTION__OPCODE = VAR_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALOAD_INSTRUCTION__HUMAN_READABLE = VAR_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALOAD_INSTRUCTION__OUT_EDGES = VAR_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALOAD_INSTRUCTION__IN_EDGES = VAR_INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Local Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALOAD_INSTRUCTION__LOCAL_VARIABLE = VAR_INSTRUCTION__LOCAL_VARIABLE;

	/**
	 * The feature id for the '<em><b>Var Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALOAD_INSTRUCTION__VAR_INDEX = VAR_INSTRUCTION__VAR_INDEX;

	/**
	 * The number of structural features of the '<em>Aload Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALOAD_INSTRUCTION_FEATURE_COUNT = VAR_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Aload Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALOAD_INSTRUCTION_OPERATION_COUNT = VAR_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.IstoreInstructionImpl <em>Istore Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.IstoreInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getIstoreInstruction()
	 * @generated
	 */
	int ISTORE_INSTRUCTION = 198;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISTORE_INSTRUCTION__UUID = VAR_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISTORE_INSTRUCTION__METHOD = VAR_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISTORE_INSTRUCTION__NEXT_IN_CODE_ORDER = VAR_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISTORE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = VAR_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISTORE_INSTRUCTION__LINENUMBER = VAR_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISTORE_INSTRUCTION__INDEX = VAR_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISTORE_INSTRUCTION__OPCODE = VAR_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISTORE_INSTRUCTION__HUMAN_READABLE = VAR_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISTORE_INSTRUCTION__OUT_EDGES = VAR_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISTORE_INSTRUCTION__IN_EDGES = VAR_INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Local Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISTORE_INSTRUCTION__LOCAL_VARIABLE = VAR_INSTRUCTION__LOCAL_VARIABLE;

	/**
	 * The feature id for the '<em><b>Var Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISTORE_INSTRUCTION__VAR_INDEX = VAR_INSTRUCTION__VAR_INDEX;

	/**
	 * The number of structural features of the '<em>Istore Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISTORE_INSTRUCTION_FEATURE_COUNT = VAR_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Istore Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISTORE_INSTRUCTION_OPERATION_COUNT = VAR_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.LstoreInstructionImpl <em>Lstore Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.LstoreInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getLstoreInstruction()
	 * @generated
	 */
	int LSTORE_INSTRUCTION = 199;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTORE_INSTRUCTION__UUID = VAR_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTORE_INSTRUCTION__METHOD = VAR_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTORE_INSTRUCTION__NEXT_IN_CODE_ORDER = VAR_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTORE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = VAR_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTORE_INSTRUCTION__LINENUMBER = VAR_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTORE_INSTRUCTION__INDEX = VAR_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTORE_INSTRUCTION__OPCODE = VAR_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTORE_INSTRUCTION__HUMAN_READABLE = VAR_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTORE_INSTRUCTION__OUT_EDGES = VAR_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTORE_INSTRUCTION__IN_EDGES = VAR_INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Local Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTORE_INSTRUCTION__LOCAL_VARIABLE = VAR_INSTRUCTION__LOCAL_VARIABLE;

	/**
	 * The feature id for the '<em><b>Var Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTORE_INSTRUCTION__VAR_INDEX = VAR_INSTRUCTION__VAR_INDEX;

	/**
	 * The number of structural features of the '<em>Lstore Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTORE_INSTRUCTION_FEATURE_COUNT = VAR_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Lstore Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LSTORE_INSTRUCTION_OPERATION_COUNT = VAR_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.FstoreInstructionImpl <em>Fstore Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.FstoreInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getFstoreInstruction()
	 * @generated
	 */
	int FSTORE_INSTRUCTION = 200;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSTORE_INSTRUCTION__UUID = VAR_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSTORE_INSTRUCTION__METHOD = VAR_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSTORE_INSTRUCTION__NEXT_IN_CODE_ORDER = VAR_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSTORE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = VAR_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSTORE_INSTRUCTION__LINENUMBER = VAR_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSTORE_INSTRUCTION__INDEX = VAR_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSTORE_INSTRUCTION__OPCODE = VAR_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSTORE_INSTRUCTION__HUMAN_READABLE = VAR_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSTORE_INSTRUCTION__OUT_EDGES = VAR_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSTORE_INSTRUCTION__IN_EDGES = VAR_INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Local Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSTORE_INSTRUCTION__LOCAL_VARIABLE = VAR_INSTRUCTION__LOCAL_VARIABLE;

	/**
	 * The feature id for the '<em><b>Var Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSTORE_INSTRUCTION__VAR_INDEX = VAR_INSTRUCTION__VAR_INDEX;

	/**
	 * The number of structural features of the '<em>Fstore Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSTORE_INSTRUCTION_FEATURE_COUNT = VAR_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Fstore Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSTORE_INSTRUCTION_OPERATION_COUNT = VAR_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.DstoreInstructionImpl <em>Dstore Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.DstoreInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getDstoreInstruction()
	 * @generated
	 */
	int DSTORE_INSTRUCTION = 201;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DSTORE_INSTRUCTION__UUID = VAR_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DSTORE_INSTRUCTION__METHOD = VAR_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DSTORE_INSTRUCTION__NEXT_IN_CODE_ORDER = VAR_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DSTORE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = VAR_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DSTORE_INSTRUCTION__LINENUMBER = VAR_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DSTORE_INSTRUCTION__INDEX = VAR_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DSTORE_INSTRUCTION__OPCODE = VAR_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DSTORE_INSTRUCTION__HUMAN_READABLE = VAR_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DSTORE_INSTRUCTION__OUT_EDGES = VAR_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DSTORE_INSTRUCTION__IN_EDGES = VAR_INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Local Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DSTORE_INSTRUCTION__LOCAL_VARIABLE = VAR_INSTRUCTION__LOCAL_VARIABLE;

	/**
	 * The feature id for the '<em><b>Var Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DSTORE_INSTRUCTION__VAR_INDEX = VAR_INSTRUCTION__VAR_INDEX;

	/**
	 * The number of structural features of the '<em>Dstore Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DSTORE_INSTRUCTION_FEATURE_COUNT = VAR_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Dstore Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DSTORE_INSTRUCTION_OPERATION_COUNT = VAR_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.AstoreInstructionImpl <em>Astore Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.AstoreInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getAstoreInstruction()
	 * @generated
	 */
	int ASTORE_INSTRUCTION = 202;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTORE_INSTRUCTION__UUID = VAR_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTORE_INSTRUCTION__METHOD = VAR_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTORE_INSTRUCTION__NEXT_IN_CODE_ORDER = VAR_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTORE_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = VAR_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTORE_INSTRUCTION__LINENUMBER = VAR_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTORE_INSTRUCTION__INDEX = VAR_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTORE_INSTRUCTION__OPCODE = VAR_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTORE_INSTRUCTION__HUMAN_READABLE = VAR_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTORE_INSTRUCTION__OUT_EDGES = VAR_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTORE_INSTRUCTION__IN_EDGES = VAR_INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Local Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTORE_INSTRUCTION__LOCAL_VARIABLE = VAR_INSTRUCTION__LOCAL_VARIABLE;

	/**
	 * The feature id for the '<em><b>Var Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTORE_INSTRUCTION__VAR_INDEX = VAR_INSTRUCTION__VAR_INDEX;

	/**
	 * The number of structural features of the '<em>Astore Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTORE_INSTRUCTION_FEATURE_COUNT = VAR_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Astore Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASTORE_INSTRUCTION_OPERATION_COUNT = VAR_INSTRUCTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link nl.utwente.jbcmm.impl.RetInstructionImpl <em>Ret Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nl.utwente.jbcmm.impl.RetInstructionImpl
	 * @see nl.utwente.jbcmm.impl.JbcmmPackageImpl#getRetInstruction()
	 * @generated
	 */
	int RET_INSTRUCTION = 203;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RET_INSTRUCTION__UUID = VAR_INSTRUCTION__UUID;

	/**
	 * The feature id for the '<em><b>Method</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RET_INSTRUCTION__METHOD = VAR_INSTRUCTION__METHOD;

	/**
	 * The feature id for the '<em><b>Next In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RET_INSTRUCTION__NEXT_IN_CODE_ORDER = VAR_INSTRUCTION__NEXT_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Previous In Code Order</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RET_INSTRUCTION__PREVIOUS_IN_CODE_ORDER = VAR_INSTRUCTION__PREVIOUS_IN_CODE_ORDER;

	/**
	 * The feature id for the '<em><b>Linenumber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RET_INSTRUCTION__LINENUMBER = VAR_INSTRUCTION__LINENUMBER;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RET_INSTRUCTION__INDEX = VAR_INSTRUCTION__INDEX;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RET_INSTRUCTION__OPCODE = VAR_INSTRUCTION__OPCODE;

	/**
	 * The feature id for the '<em><b>Human Readable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RET_INSTRUCTION__HUMAN_READABLE = VAR_INSTRUCTION__HUMAN_READABLE;

	/**
	 * The feature id for the '<em><b>Out Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RET_INSTRUCTION__OUT_EDGES = VAR_INSTRUCTION__OUT_EDGES;

	/**
	 * The feature id for the '<em><b>In Edges</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RET_INSTRUCTION__IN_EDGES = VAR_INSTRUCTION__IN_EDGES;

	/**
	 * The feature id for the '<em><b>Local Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RET_INSTRUCTION__LOCAL_VARIABLE = VAR_INSTRUCTION__LOCAL_VARIABLE;

	/**
	 * The feature id for the '<em><b>Var Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RET_INSTRUCTION__VAR_INDEX = VAR_INSTRUCTION__VAR_INDEX;

	/**
	 * The number of structural features of the '<em>Ret Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RET_INSTRUCTION_FEATURE_COUNT = VAR_INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Ret Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RET_INSTRUCTION_OPERATION_COUNT = VAR_INSTRUCTION_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.Identifiable <em>Identifiable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Identifiable</em>'.
	 * @see nl.utwente.jbcmm.Identifiable
	 * @generated
	 */
	EClass getIdentifiable();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Identifiable#getUuid <em>Uuid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Uuid</em>'.
	 * @see nl.utwente.jbcmm.Identifiable#getUuid()
	 * @see #getIdentifiable()
	 * @generated
	 */
	EAttribute getIdentifiable_Uuid();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.Project <em>Project</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Project</em>'.
	 * @see nl.utwente.jbcmm.Project
	 * @generated
	 */
	EClass getProject();

	/**
	 * Returns the meta object for the containment reference list '{@link nl.utwente.jbcmm.Project#getClasses <em>Classes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Classes</em>'.
	 * @see nl.utwente.jbcmm.Project#getClasses()
	 * @see #getProject()
	 * @generated
	 */
	EReference getProject_Classes();

	/**
	 * Returns the meta object for the reference '{@link nl.utwente.jbcmm.Project#getMainClass <em>Main Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Main Class</em>'.
	 * @see nl.utwente.jbcmm.Project#getMainClass()
	 * @see #getProject()
	 * @generated
	 */
	EReference getProject_MainClass();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.Clazz <em>Clazz</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Clazz</em>'.
	 * @see nl.utwente.jbcmm.Clazz
	 * @generated
	 */
	EClass getClazz();

	/**
	 * Returns the meta object for the container reference '{@link nl.utwente.jbcmm.Clazz#getProject <em>Project</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Project</em>'.
	 * @see nl.utwente.jbcmm.Clazz#getProject()
	 * @see #getClazz()
	 * @generated
	 */
	EReference getClazz_Project();

	/**
	 * Returns the meta object for the containment reference list '{@link nl.utwente.jbcmm.Clazz#getMethods <em>Methods</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Methods</em>'.
	 * @see nl.utwente.jbcmm.Clazz#getMethods()
	 * @see #getClazz()
	 * @generated
	 */
	EReference getClazz_Methods();

	/**
	 * Returns the meta object for the reference list '{@link nl.utwente.jbcmm.Clazz#getSubclasses <em>Subclasses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Subclasses</em>'.
	 * @see nl.utwente.jbcmm.Clazz#getSubclasses()
	 * @see #getClazz()
	 * @generated
	 */
	EReference getClazz_Subclasses();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Clazz#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see nl.utwente.jbcmm.Clazz#getName()
	 * @see #getClazz()
	 * @generated
	 */
	EAttribute getClazz_Name();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Clazz#getMinorVersion <em>Minor Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Minor Version</em>'.
	 * @see nl.utwente.jbcmm.Clazz#getMinorVersion()
	 * @see #getClazz()
	 * @generated
	 */
	EAttribute getClazz_MinorVersion();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Clazz#getMajorVersion <em>Major Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Major Version</em>'.
	 * @see nl.utwente.jbcmm.Clazz#getMajorVersion()
	 * @see #getClazz()
	 * @generated
	 */
	EAttribute getClazz_MajorVersion();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Clazz#isPublic <em>Public</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Public</em>'.
	 * @see nl.utwente.jbcmm.Clazz#isPublic()
	 * @see #getClazz()
	 * @generated
	 */
	EAttribute getClazz_Public();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Clazz#isFinal <em>Final</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Final</em>'.
	 * @see nl.utwente.jbcmm.Clazz#isFinal()
	 * @see #getClazz()
	 * @generated
	 */
	EAttribute getClazz_Final();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Clazz#isSuper <em>Super</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Super</em>'.
	 * @see nl.utwente.jbcmm.Clazz#isSuper()
	 * @see #getClazz()
	 * @generated
	 */
	EAttribute getClazz_Super();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Clazz#isInterface <em>Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Interface</em>'.
	 * @see nl.utwente.jbcmm.Clazz#isInterface()
	 * @see #getClazz()
	 * @generated
	 */
	EAttribute getClazz_Interface();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Clazz#isAbstract <em>Abstract</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Abstract</em>'.
	 * @see nl.utwente.jbcmm.Clazz#isAbstract()
	 * @see #getClazz()
	 * @generated
	 */
	EAttribute getClazz_Abstract();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Clazz#isSynthetic <em>Synthetic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Synthetic</em>'.
	 * @see nl.utwente.jbcmm.Clazz#isSynthetic()
	 * @see #getClazz()
	 * @generated
	 */
	EAttribute getClazz_Synthetic();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Clazz#isAnnotation <em>Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Annotation</em>'.
	 * @see nl.utwente.jbcmm.Clazz#isAnnotation()
	 * @see #getClazz()
	 * @generated
	 */
	EAttribute getClazz_Annotation();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Clazz#isEnum <em>Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Enum</em>'.
	 * @see nl.utwente.jbcmm.Clazz#isEnum()
	 * @see #getClazz()
	 * @generated
	 */
	EAttribute getClazz_Enum();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Clazz#getSourceFileName <em>Source File Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source File Name</em>'.
	 * @see nl.utwente.jbcmm.Clazz#getSourceFileName()
	 * @see #getClazz()
	 * @generated
	 */
	EAttribute getClazz_SourceFileName();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Clazz#isDeprecated <em>Deprecated</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Deprecated</em>'.
	 * @see nl.utwente.jbcmm.Clazz#isDeprecated()
	 * @see #getClazz()
	 * @generated
	 */
	EAttribute getClazz_Deprecated();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Clazz#getSourceDebugExtension <em>Source Debug Extension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source Debug Extension</em>'.
	 * @see nl.utwente.jbcmm.Clazz#getSourceDebugExtension()
	 * @see #getClazz()
	 * @generated
	 */
	EAttribute getClazz_SourceDebugExtension();

	/**
	 * Returns the meta object for the containment reference list '{@link nl.utwente.jbcmm.Clazz#getRuntimeInvisibleAnnotations <em>Runtime Invisible Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Runtime Invisible Annotations</em>'.
	 * @see nl.utwente.jbcmm.Clazz#getRuntimeInvisibleAnnotations()
	 * @see #getClazz()
	 * @generated
	 */
	EReference getClazz_RuntimeInvisibleAnnotations();

	/**
	 * Returns the meta object for the containment reference list '{@link nl.utwente.jbcmm.Clazz#getRuntimeVisibleAnnotations <em>Runtime Visible Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Runtime Visible Annotations</em>'.
	 * @see nl.utwente.jbcmm.Clazz#getRuntimeVisibleAnnotations()
	 * @see #getClazz()
	 * @generated
	 */
	EReference getClazz_RuntimeVisibleAnnotations();

	/**
	 * Returns the meta object for the containment reference list '{@link nl.utwente.jbcmm.Clazz#getFields <em>Fields</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Fields</em>'.
	 * @see nl.utwente.jbcmm.Clazz#getFields()
	 * @see #getClazz()
	 * @generated
	 */
	EReference getClazz_Fields();

	/**
	 * Returns the meta object for the containment reference '{@link nl.utwente.jbcmm.Clazz#getSuperClass <em>Super Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Super Class</em>'.
	 * @see nl.utwente.jbcmm.Clazz#getSuperClass()
	 * @see #getClazz()
	 * @generated
	 */
	EReference getClazz_SuperClass();

	/**
	 * Returns the meta object for the containment reference list '{@link nl.utwente.jbcmm.Clazz#getInterfaces <em>Interfaces</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Interfaces</em>'.
	 * @see nl.utwente.jbcmm.Clazz#getInterfaces()
	 * @see #getClazz()
	 * @generated
	 */
	EReference getClazz_Interfaces();

	/**
	 * Returns the meta object for the containment reference '{@link nl.utwente.jbcmm.Clazz#getSignature <em>Signature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Signature</em>'.
	 * @see nl.utwente.jbcmm.Clazz#getSignature()
	 * @see #getClazz()
	 * @generated
	 */
	EReference getClazz_Signature();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Clazz#isPrivate <em>Private</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Private</em>'.
	 * @see nl.utwente.jbcmm.Clazz#isPrivate()
	 * @see #getClazz()
	 * @generated
	 */
	EAttribute getClazz_Private();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Clazz#isProtected <em>Protected</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Protected</em>'.
	 * @see nl.utwente.jbcmm.Clazz#isProtected()
	 * @see #getClazz()
	 * @generated
	 */
	EAttribute getClazz_Protected();

	/**
	 * Returns the meta object for the containment reference '{@link nl.utwente.jbcmm.Clazz#getOuterClass <em>Outer Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Outer Class</em>'.
	 * @see nl.utwente.jbcmm.Clazz#getOuterClass()
	 * @see #getClazz()
	 * @generated
	 */
	EReference getClazz_OuterClass();

	/**
	 * Returns the meta object for the containment reference '{@link nl.utwente.jbcmm.Clazz#getEnclosingMethod <em>Enclosing Method</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Enclosing Method</em>'.
	 * @see nl.utwente.jbcmm.Clazz#getEnclosingMethod()
	 * @see #getClazz()
	 * @generated
	 */
	EReference getClazz_EnclosingMethod();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.Signature <em>Signature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Signature</em>'.
	 * @see nl.utwente.jbcmm.Signature
	 * @generated
	 */
	EClass getSignature();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Signature#getSignature <em>Signature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Signature</em>'.
	 * @see nl.utwente.jbcmm.Signature#getSignature()
	 * @see #getSignature()
	 * @generated
	 */
	EAttribute getSignature_Signature();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.ClassSignature <em>Class Signature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Class Signature</em>'.
	 * @see nl.utwente.jbcmm.ClassSignature
	 * @generated
	 */
	EClass getClassSignature();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.MethodSignature <em>Method Signature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Method Signature</em>'.
	 * @see nl.utwente.jbcmm.MethodSignature
	 * @generated
	 */
	EClass getMethodSignature();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.FieldTypeSignature <em>Field Type Signature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Field Type Signature</em>'.
	 * @see nl.utwente.jbcmm.FieldTypeSignature
	 * @generated
	 */
	EClass getFieldTypeSignature();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.TypeReference <em>Type Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type Reference</em>'.
	 * @see nl.utwente.jbcmm.TypeReference
	 * @generated
	 */
	EClass getTypeReference();

	/**
	 * Returns the meta object for the reference '{@link nl.utwente.jbcmm.TypeReference#getReferencedClass <em>Referenced Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Referenced Class</em>'.
	 * @see nl.utwente.jbcmm.TypeReference#getReferencedClass()
	 * @see #getTypeReference()
	 * @generated
	 */
	EReference getTypeReference_ReferencedClass();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.TypeReference#getTypeDescriptor <em>Type Descriptor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type Descriptor</em>'.
	 * @see nl.utwente.jbcmm.TypeReference#getTypeDescriptor()
	 * @see #getTypeReference()
	 * @generated
	 */
	EAttribute getTypeReference_TypeDescriptor();

	/**
	 * Returns the meta object for the containment reference list '{@link nl.utwente.jbcmm.TypeReference#getRuntimeInvisibleAnnotations <em>Runtime Invisible Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Runtime Invisible Annotations</em>'.
	 * @see nl.utwente.jbcmm.TypeReference#getRuntimeInvisibleAnnotations()
	 * @see #getTypeReference()
	 * @generated
	 */
	EReference getTypeReference_RuntimeInvisibleAnnotations();

	/**
	 * Returns the meta object for the containment reference list '{@link nl.utwente.jbcmm.TypeReference#getRuntimeVisibleAnnotations <em>Runtime Visible Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Runtime Visible Annotations</em>'.
	 * @see nl.utwente.jbcmm.TypeReference#getRuntimeVisibleAnnotations()
	 * @see #getTypeReference()
	 * @generated
	 */
	EReference getTypeReference_RuntimeVisibleAnnotations();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.MethodDescriptor <em>Method Descriptor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Method Descriptor</em>'.
	 * @see nl.utwente.jbcmm.MethodDescriptor
	 * @generated
	 */
	EClass getMethodDescriptor();

	/**
	 * Returns the meta object for the containment reference list '{@link nl.utwente.jbcmm.MethodDescriptor#getParameterTypes <em>Parameter Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameter Types</em>'.
	 * @see nl.utwente.jbcmm.MethodDescriptor#getParameterTypes()
	 * @see #getMethodDescriptor()
	 * @generated
	 */
	EReference getMethodDescriptor_ParameterTypes();

	/**
	 * Returns the meta object for the containment reference '{@link nl.utwente.jbcmm.MethodDescriptor#getResultType <em>Result Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Result Type</em>'.
	 * @see nl.utwente.jbcmm.MethodDescriptor#getResultType()
	 * @see #getMethodDescriptor()
	 * @generated
	 */
	EReference getMethodDescriptor_ResultType();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.MethodReference <em>Method Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Method Reference</em>'.
	 * @see nl.utwente.jbcmm.MethodReference
	 * @generated
	 */
	EClass getMethodReference();

	/**
	 * Returns the meta object for the containment reference '{@link nl.utwente.jbcmm.MethodReference#getDeclaringClass <em>Declaring Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Declaring Class</em>'.
	 * @see nl.utwente.jbcmm.MethodReference#getDeclaringClass()
	 * @see #getMethodReference()
	 * @generated
	 */
	EReference getMethodReference_DeclaringClass();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.MethodReference#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see nl.utwente.jbcmm.MethodReference#getName()
	 * @see #getMethodReference()
	 * @generated
	 */
	EAttribute getMethodReference_Name();

	/**
	 * Returns the meta object for the containment reference '{@link nl.utwente.jbcmm.MethodReference#getDescriptor <em>Descriptor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Descriptor</em>'.
	 * @see nl.utwente.jbcmm.MethodReference#getDescriptor()
	 * @see #getMethodReference()
	 * @generated
	 */
	EReference getMethodReference_Descriptor();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.FieldReference <em>Field Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Field Reference</em>'.
	 * @see nl.utwente.jbcmm.FieldReference
	 * @generated
	 */
	EClass getFieldReference();

	/**
	 * Returns the meta object for the containment reference '{@link nl.utwente.jbcmm.FieldReference#getDeclaringClass <em>Declaring Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Declaring Class</em>'.
	 * @see nl.utwente.jbcmm.FieldReference#getDeclaringClass()
	 * @see #getFieldReference()
	 * @generated
	 */
	EReference getFieldReference_DeclaringClass();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.FieldReference#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see nl.utwente.jbcmm.FieldReference#getName()
	 * @see #getFieldReference()
	 * @generated
	 */
	EAttribute getFieldReference_Name();

	/**
	 * Returns the meta object for the containment reference '{@link nl.utwente.jbcmm.FieldReference#getDescriptor <em>Descriptor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Descriptor</em>'.
	 * @see nl.utwente.jbcmm.FieldReference#getDescriptor()
	 * @see #getFieldReference()
	 * @generated
	 */
	EReference getFieldReference_Descriptor();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.Field <em>Field</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Field</em>'.
	 * @see nl.utwente.jbcmm.Field
	 * @generated
	 */
	EClass getField();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Field#isPublic <em>Public</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Public</em>'.
	 * @see nl.utwente.jbcmm.Field#isPublic()
	 * @see #getField()
	 * @generated
	 */
	EAttribute getField_Public();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Field#isPrivate <em>Private</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Private</em>'.
	 * @see nl.utwente.jbcmm.Field#isPrivate()
	 * @see #getField()
	 * @generated
	 */
	EAttribute getField_Private();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Field#isProtected <em>Protected</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Protected</em>'.
	 * @see nl.utwente.jbcmm.Field#isProtected()
	 * @see #getField()
	 * @generated
	 */
	EAttribute getField_Protected();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Field#isStatic <em>Static</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Static</em>'.
	 * @see nl.utwente.jbcmm.Field#isStatic()
	 * @see #getField()
	 * @generated
	 */
	EAttribute getField_Static();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Field#isFinal <em>Final</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Final</em>'.
	 * @see nl.utwente.jbcmm.Field#isFinal()
	 * @see #getField()
	 * @generated
	 */
	EAttribute getField_Final();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Field#isVolatile <em>Volatile</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Volatile</em>'.
	 * @see nl.utwente.jbcmm.Field#isVolatile()
	 * @see #getField()
	 * @generated
	 */
	EAttribute getField_Volatile();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Field#isTransient <em>Transient</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Transient</em>'.
	 * @see nl.utwente.jbcmm.Field#isTransient()
	 * @see #getField()
	 * @generated
	 */
	EAttribute getField_Transient();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Field#isSynthetic <em>Synthetic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Synthetic</em>'.
	 * @see nl.utwente.jbcmm.Field#isSynthetic()
	 * @see #getField()
	 * @generated
	 */
	EAttribute getField_Synthetic();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Field#isEnum <em>Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Enum</em>'.
	 * @see nl.utwente.jbcmm.Field#isEnum()
	 * @see #getField()
	 * @generated
	 */
	EAttribute getField_Enum();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Field#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see nl.utwente.jbcmm.Field#getName()
	 * @see #getField()
	 * @generated
	 */
	EAttribute getField_Name();

	/**
	 * Returns the meta object for the containment reference '{@link nl.utwente.jbcmm.Field#getConstantValue <em>Constant Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Constant Value</em>'.
	 * @see nl.utwente.jbcmm.Field#getConstantValue()
	 * @see #getField()
	 * @generated
	 */
	EReference getField_ConstantValue();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Field#isDeprecated <em>Deprecated</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Deprecated</em>'.
	 * @see nl.utwente.jbcmm.Field#isDeprecated()
	 * @see #getField()
	 * @generated
	 */
	EAttribute getField_Deprecated();

	/**
	 * Returns the meta object for the containment reference list '{@link nl.utwente.jbcmm.Field#getRuntimeInvisibleAnnotations <em>Runtime Invisible Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Runtime Invisible Annotations</em>'.
	 * @see nl.utwente.jbcmm.Field#getRuntimeInvisibleAnnotations()
	 * @see #getField()
	 * @generated
	 */
	EReference getField_RuntimeInvisibleAnnotations();

	/**
	 * Returns the meta object for the containment reference list '{@link nl.utwente.jbcmm.Field#getRuntimeVisibleAnnotations <em>Runtime Visible Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Runtime Visible Annotations</em>'.
	 * @see nl.utwente.jbcmm.Field#getRuntimeVisibleAnnotations()
	 * @see #getField()
	 * @generated
	 */
	EReference getField_RuntimeVisibleAnnotations();

	/**
	 * Returns the meta object for the containment reference '{@link nl.utwente.jbcmm.Field#getDescriptor <em>Descriptor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Descriptor</em>'.
	 * @see nl.utwente.jbcmm.Field#getDescriptor()
	 * @see #getField()
	 * @generated
	 */
	EReference getField_Descriptor();

	/**
	 * Returns the meta object for the containment reference '{@link nl.utwente.jbcmm.Field#getSignature <em>Signature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Signature</em>'.
	 * @see nl.utwente.jbcmm.Field#getSignature()
	 * @see #getField()
	 * @generated
	 */
	EReference getField_Signature();

	/**
	 * Returns the meta object for the container reference '{@link nl.utwente.jbcmm.Field#getClass_ <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Class</em>'.
	 * @see nl.utwente.jbcmm.Field#getClass_()
	 * @see #getField()
	 * @generated
	 */
	EReference getField_Class();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.Annotation <em>Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Annotation</em>'.
	 * @see nl.utwente.jbcmm.Annotation
	 * @generated
	 */
	EClass getAnnotation();

	/**
	 * Returns the meta object for the containment reference list '{@link nl.utwente.jbcmm.Annotation#getElementValuePairs <em>Element Value Pairs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Element Value Pairs</em>'.
	 * @see nl.utwente.jbcmm.Annotation#getElementValuePairs()
	 * @see #getAnnotation()
	 * @generated
	 */
	EReference getAnnotation_ElementValuePairs();

	/**
	 * Returns the meta object for the containment reference '{@link nl.utwente.jbcmm.Annotation#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type</em>'.
	 * @see nl.utwente.jbcmm.Annotation#getType()
	 * @see #getAnnotation()
	 * @generated
	 */
	EReference getAnnotation_Type();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.ElementValuePair <em>Element Value Pair</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Element Value Pair</em>'.
	 * @see nl.utwente.jbcmm.ElementValuePair
	 * @generated
	 */
	EClass getElementValuePair();

	/**
	 * Returns the meta object for the containment reference '{@link nl.utwente.jbcmm.ElementValuePair#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see nl.utwente.jbcmm.ElementValuePair#getValue()
	 * @see #getElementValuePair()
	 * @generated
	 */
	EReference getElementValuePair_Value();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.ElementValuePair#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see nl.utwente.jbcmm.ElementValuePair#getName()
	 * @see #getElementValuePair()
	 * @generated
	 */
	EAttribute getElementValuePair_Name();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.ElementValue <em>Element Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Element Value</em>'.
	 * @see nl.utwente.jbcmm.ElementValue
	 * @generated
	 */
	EClass getElementValue();

	/**
	 * Returns the meta object for the containment reference '{@link nl.utwente.jbcmm.ElementValue#getConstantValue <em>Constant Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Constant Value</em>'.
	 * @see nl.utwente.jbcmm.ElementValue#getConstantValue()
	 * @see #getElementValue()
	 * @generated
	 */
	EReference getElementValue_ConstantValue();

	/**
	 * Returns the meta object for the containment reference '{@link nl.utwente.jbcmm.ElementValue#getEnumValue <em>Enum Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Enum Value</em>'.
	 * @see nl.utwente.jbcmm.ElementValue#getEnumValue()
	 * @see #getElementValue()
	 * @generated
	 */
	EReference getElementValue_EnumValue();

	/**
	 * Returns the meta object for the containment reference '{@link nl.utwente.jbcmm.ElementValue#getAnnotationValue <em>Annotation Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Annotation Value</em>'.
	 * @see nl.utwente.jbcmm.ElementValue#getAnnotationValue()
	 * @see #getElementValue()
	 * @generated
	 */
	EReference getElementValue_AnnotationValue();

	/**
	 * Returns the meta object for the containment reference list '{@link nl.utwente.jbcmm.ElementValue#getArrayValue <em>Array Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Array Value</em>'.
	 * @see nl.utwente.jbcmm.ElementValue#getArrayValue()
	 * @see #getElementValue()
	 * @generated
	 */
	EReference getElementValue_ArrayValue();

	/**
	 * Returns the meta object for the containment reference '{@link nl.utwente.jbcmm.ElementValue#getClassValue <em>Class Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Class Value</em>'.
	 * @see nl.utwente.jbcmm.ElementValue#getClassValue()
	 * @see #getElementValue()
	 * @generated
	 */
	EReference getElementValue_ClassValue();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.ElementaryValue <em>Elementary Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Elementary Value</em>'.
	 * @see nl.utwente.jbcmm.ElementaryValue
	 * @generated
	 */
	EClass getElementaryValue();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.BooleanValue <em>Boolean Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Boolean Value</em>'.
	 * @see nl.utwente.jbcmm.BooleanValue
	 * @generated
	 */
	EClass getBooleanValue();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.BooleanValue#isValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see nl.utwente.jbcmm.BooleanValue#isValue()
	 * @see #getBooleanValue()
	 * @generated
	 */
	EAttribute getBooleanValue_Value();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.CharValue <em>Char Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Char Value</em>'.
	 * @see nl.utwente.jbcmm.CharValue
	 * @generated
	 */
	EClass getCharValue();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.CharValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see nl.utwente.jbcmm.CharValue#getValue()
	 * @see #getCharValue()
	 * @generated
	 */
	EAttribute getCharValue_Value();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.ByteValue <em>Byte Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Byte Value</em>'.
	 * @see nl.utwente.jbcmm.ByteValue
	 * @generated
	 */
	EClass getByteValue();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.ByteValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see nl.utwente.jbcmm.ByteValue#getValue()
	 * @see #getByteValue()
	 * @generated
	 */
	EAttribute getByteValue_Value();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.ShortValue <em>Short Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Short Value</em>'.
	 * @see nl.utwente.jbcmm.ShortValue
	 * @generated
	 */
	EClass getShortValue();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.ShortValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see nl.utwente.jbcmm.ShortValue#getValue()
	 * @see #getShortValue()
	 * @generated
	 */
	EAttribute getShortValue_Value();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.IntValue <em>Int Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Int Value</em>'.
	 * @see nl.utwente.jbcmm.IntValue
	 * @generated
	 */
	EClass getIntValue();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.IntValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see nl.utwente.jbcmm.IntValue#getValue()
	 * @see #getIntValue()
	 * @generated
	 */
	EAttribute getIntValue_Value();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.LongValue <em>Long Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Long Value</em>'.
	 * @see nl.utwente.jbcmm.LongValue
	 * @generated
	 */
	EClass getLongValue();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.LongValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see nl.utwente.jbcmm.LongValue#getValue()
	 * @see #getLongValue()
	 * @generated
	 */
	EAttribute getLongValue_Value();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.FloatValue <em>Float Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Float Value</em>'.
	 * @see nl.utwente.jbcmm.FloatValue
	 * @generated
	 */
	EClass getFloatValue();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.FloatValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see nl.utwente.jbcmm.FloatValue#getValue()
	 * @see #getFloatValue()
	 * @generated
	 */
	EAttribute getFloatValue_Value();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.DoubleValue <em>Double Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Double Value</em>'.
	 * @see nl.utwente.jbcmm.DoubleValue
	 * @generated
	 */
	EClass getDoubleValue();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.DoubleValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see nl.utwente.jbcmm.DoubleValue#getValue()
	 * @see #getDoubleValue()
	 * @generated
	 */
	EAttribute getDoubleValue_Value();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.StringValue <em>String Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String Value</em>'.
	 * @see nl.utwente.jbcmm.StringValue
	 * @generated
	 */
	EClass getStringValue();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.StringValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see nl.utwente.jbcmm.StringValue#getValue()
	 * @see #getStringValue()
	 * @generated
	 */
	EAttribute getStringValue_Value();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.EnumValue <em>Enum Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Enum Value</em>'.
	 * @see nl.utwente.jbcmm.EnumValue
	 * @generated
	 */
	EClass getEnumValue();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.EnumValue#getConstName <em>Const Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Const Name</em>'.
	 * @see nl.utwente.jbcmm.EnumValue#getConstName()
	 * @see #getEnumValue()
	 * @generated
	 */
	EAttribute getEnumValue_ConstName();

	/**
	 * Returns the meta object for the containment reference '{@link nl.utwente.jbcmm.EnumValue#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type</em>'.
	 * @see nl.utwente.jbcmm.EnumValue#getType()
	 * @see #getEnumValue()
	 * @generated
	 */
	EReference getEnumValue_Type();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.Method <em>Method</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Method</em>'.
	 * @see nl.utwente.jbcmm.Method
	 * @generated
	 */
	EClass getMethod();

	/**
	 * Returns the meta object for the container reference '{@link nl.utwente.jbcmm.Method#getClass_ <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Class</em>'.
	 * @see nl.utwente.jbcmm.Method#getClass_()
	 * @see #getMethod()
	 * @generated
	 */
	EReference getMethod_Class();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Method#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see nl.utwente.jbcmm.Method#getName()
	 * @see #getMethod()
	 * @generated
	 */
	EAttribute getMethod_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link nl.utwente.jbcmm.Method#getInstructions <em>Instructions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Instructions</em>'.
	 * @see nl.utwente.jbcmm.Method#getInstructions()
	 * @see #getMethod()
	 * @generated
	 */
	EReference getMethod_Instructions();

	/**
	 * Returns the meta object for the reference '{@link nl.utwente.jbcmm.Method#getFirstInstruction <em>First Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>First Instruction</em>'.
	 * @see nl.utwente.jbcmm.Method#getFirstInstruction()
	 * @see #getMethod()
	 * @generated
	 */
	EReference getMethod_FirstInstruction();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Method#isPublic <em>Public</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Public</em>'.
	 * @see nl.utwente.jbcmm.Method#isPublic()
	 * @see #getMethod()
	 * @generated
	 */
	EAttribute getMethod_Public();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Method#isPrivate <em>Private</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Private</em>'.
	 * @see nl.utwente.jbcmm.Method#isPrivate()
	 * @see #getMethod()
	 * @generated
	 */
	EAttribute getMethod_Private();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Method#isProtected <em>Protected</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Protected</em>'.
	 * @see nl.utwente.jbcmm.Method#isProtected()
	 * @see #getMethod()
	 * @generated
	 */
	EAttribute getMethod_Protected();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Method#isStatic <em>Static</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Static</em>'.
	 * @see nl.utwente.jbcmm.Method#isStatic()
	 * @see #getMethod()
	 * @generated
	 */
	EAttribute getMethod_Static();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Method#isFinal <em>Final</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Final</em>'.
	 * @see nl.utwente.jbcmm.Method#isFinal()
	 * @see #getMethod()
	 * @generated
	 */
	EAttribute getMethod_Final();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Method#isSynthetic <em>Synthetic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Synthetic</em>'.
	 * @see nl.utwente.jbcmm.Method#isSynthetic()
	 * @see #getMethod()
	 * @generated
	 */
	EAttribute getMethod_Synthetic();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Method#isSynchronized <em>Synchronized</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Synchronized</em>'.
	 * @see nl.utwente.jbcmm.Method#isSynchronized()
	 * @see #getMethod()
	 * @generated
	 */
	EAttribute getMethod_Synchronized();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Method#isBridge <em>Bridge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bridge</em>'.
	 * @see nl.utwente.jbcmm.Method#isBridge()
	 * @see #getMethod()
	 * @generated
	 */
	EAttribute getMethod_Bridge();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Method#isVarArgs <em>Var Args</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Var Args</em>'.
	 * @see nl.utwente.jbcmm.Method#isVarArgs()
	 * @see #getMethod()
	 * @generated
	 */
	EAttribute getMethod_VarArgs();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Method#isNative <em>Native</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Native</em>'.
	 * @see nl.utwente.jbcmm.Method#isNative()
	 * @see #getMethod()
	 * @generated
	 */
	EAttribute getMethod_Native();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Method#isAbstract <em>Abstract</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Abstract</em>'.
	 * @see nl.utwente.jbcmm.Method#isAbstract()
	 * @see #getMethod()
	 * @generated
	 */
	EAttribute getMethod_Abstract();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Method#isStrict <em>Strict</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Strict</em>'.
	 * @see nl.utwente.jbcmm.Method#isStrict()
	 * @see #getMethod()
	 * @generated
	 */
	EAttribute getMethod_Strict();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Method#isDeprecated <em>Deprecated</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Deprecated</em>'.
	 * @see nl.utwente.jbcmm.Method#isDeprecated()
	 * @see #getMethod()
	 * @generated
	 */
	EAttribute getMethod_Deprecated();

	/**
	 * Returns the meta object for the containment reference list '{@link nl.utwente.jbcmm.Method#getRuntimeInvisibleAnnotations <em>Runtime Invisible Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Runtime Invisible Annotations</em>'.
	 * @see nl.utwente.jbcmm.Method#getRuntimeInvisibleAnnotations()
	 * @see #getMethod()
	 * @generated
	 */
	EReference getMethod_RuntimeInvisibleAnnotations();

	/**
	 * Returns the meta object for the containment reference list '{@link nl.utwente.jbcmm.Method#getRuntimeVisibleAnnotations <em>Runtime Visible Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Runtime Visible Annotations</em>'.
	 * @see nl.utwente.jbcmm.Method#getRuntimeVisibleAnnotations()
	 * @see #getMethod()
	 * @generated
	 */
	EReference getMethod_RuntimeVisibleAnnotations();

	/**
	 * Returns the meta object for the containment reference '{@link nl.utwente.jbcmm.Method#getAnnotationDefaultValue <em>Annotation Default Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Annotation Default Value</em>'.
	 * @see nl.utwente.jbcmm.Method#getAnnotationDefaultValue()
	 * @see #getMethod()
	 * @generated
	 */
	EReference getMethod_AnnotationDefaultValue();

	/**
	 * Returns the meta object for the containment reference '{@link nl.utwente.jbcmm.Method#getDescriptor <em>Descriptor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Descriptor</em>'.
	 * @see nl.utwente.jbcmm.Method#getDescriptor()
	 * @see #getMethod()
	 * @generated
	 */
	EReference getMethod_Descriptor();

	/**
	 * Returns the meta object for the containment reference '{@link nl.utwente.jbcmm.Method#getSignature <em>Signature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Signature</em>'.
	 * @see nl.utwente.jbcmm.Method#getSignature()
	 * @see #getMethod()
	 * @generated
	 */
	EReference getMethod_Signature();

	/**
	 * Returns the meta object for the containment reference list '{@link nl.utwente.jbcmm.Method#getExceptionsCanBeThrown <em>Exceptions Can Be Thrown</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Exceptions Can Be Thrown</em>'.
	 * @see nl.utwente.jbcmm.Method#getExceptionsCanBeThrown()
	 * @see #getMethod()
	 * @generated
	 */
	EReference getMethod_ExceptionsCanBeThrown();

	/**
	 * Returns the meta object for the containment reference list '{@link nl.utwente.jbcmm.Method#getExceptionTable <em>Exception Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Exception Table</em>'.
	 * @see nl.utwente.jbcmm.Method#getExceptionTable()
	 * @see #getMethod()
	 * @generated
	 */
	EReference getMethod_ExceptionTable();

	/**
	 * Returns the meta object for the containment reference list '{@link nl.utwente.jbcmm.Method#getLocalVariableTable <em>Local Variable Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Local Variable Table</em>'.
	 * @see nl.utwente.jbcmm.Method#getLocalVariableTable()
	 * @see #getMethod()
	 * @generated
	 */
	EReference getMethod_LocalVariableTable();

	/**
	 * Returns the meta object for the containment reference list '{@link nl.utwente.jbcmm.Method#getRuntimeInvisibleParameterAnnotations <em>Runtime Invisible Parameter Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Runtime Invisible Parameter Annotations</em>'.
	 * @see nl.utwente.jbcmm.Method#getRuntimeInvisibleParameterAnnotations()
	 * @see #getMethod()
	 * @generated
	 */
	EReference getMethod_RuntimeInvisibleParameterAnnotations();

	/**
	 * Returns the meta object for the containment reference list '{@link nl.utwente.jbcmm.Method#getRuntimeVisibleParameterAnnotations <em>Runtime Visible Parameter Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Runtime Visible Parameter Annotations</em>'.
	 * @see nl.utwente.jbcmm.Method#getRuntimeVisibleParameterAnnotations()
	 * @see #getMethod()
	 * @generated
	 */
	EReference getMethod_RuntimeVisibleParameterAnnotations();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.LocalVariableTableEntry <em>Local Variable Table Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Local Variable Table Entry</em>'.
	 * @see nl.utwente.jbcmm.LocalVariableTableEntry
	 * @generated
	 */
	EClass getLocalVariableTableEntry();

	/**
	 * Returns the meta object for the container reference '{@link nl.utwente.jbcmm.LocalVariableTableEntry#getMethod <em>Method</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Method</em>'.
	 * @see nl.utwente.jbcmm.LocalVariableTableEntry#getMethod()
	 * @see #getLocalVariableTableEntry()
	 * @generated
	 */
	EReference getLocalVariableTableEntry_Method();

	/**
	 * Returns the meta object for the reference '{@link nl.utwente.jbcmm.LocalVariableTableEntry#getStartInstruction <em>Start Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Start Instruction</em>'.
	 * @see nl.utwente.jbcmm.LocalVariableTableEntry#getStartInstruction()
	 * @see #getLocalVariableTableEntry()
	 * @generated
	 */
	EReference getLocalVariableTableEntry_StartInstruction();

	/**
	 * Returns the meta object for the reference '{@link nl.utwente.jbcmm.LocalVariableTableEntry#getEndInstruction <em>End Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>End Instruction</em>'.
	 * @see nl.utwente.jbcmm.LocalVariableTableEntry#getEndInstruction()
	 * @see #getLocalVariableTableEntry()
	 * @generated
	 */
	EReference getLocalVariableTableEntry_EndInstruction();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.LocalVariableTableEntry#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see nl.utwente.jbcmm.LocalVariableTableEntry#getName()
	 * @see #getLocalVariableTableEntry()
	 * @generated
	 */
	EAttribute getLocalVariableTableEntry_Name();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.LocalVariableTableEntry#getIndex <em>Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Index</em>'.
	 * @see nl.utwente.jbcmm.LocalVariableTableEntry#getIndex()
	 * @see #getLocalVariableTableEntry()
	 * @generated
	 */
	EAttribute getLocalVariableTableEntry_Index();

	/**
	 * Returns the meta object for the reference '{@link nl.utwente.jbcmm.LocalVariableTableEntry#getSignature <em>Signature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Signature</em>'.
	 * @see nl.utwente.jbcmm.LocalVariableTableEntry#getSignature()
	 * @see #getLocalVariableTableEntry()
	 * @generated
	 */
	EReference getLocalVariableTableEntry_Signature();

	/**
	 * Returns the meta object for the containment reference '{@link nl.utwente.jbcmm.LocalVariableTableEntry#getDescriptor <em>Descriptor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Descriptor</em>'.
	 * @see nl.utwente.jbcmm.LocalVariableTableEntry#getDescriptor()
	 * @see #getLocalVariableTableEntry()
	 * @generated
	 */
	EReference getLocalVariableTableEntry_Descriptor();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.ExceptionTableEntry <em>Exception Table Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Exception Table Entry</em>'.
	 * @see nl.utwente.jbcmm.ExceptionTableEntry
	 * @generated
	 */
	EClass getExceptionTableEntry();

	/**
	 * Returns the meta object for the container reference '{@link nl.utwente.jbcmm.ExceptionTableEntry#getMethod <em>Method</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Method</em>'.
	 * @see nl.utwente.jbcmm.ExceptionTableEntry#getMethod()
	 * @see #getExceptionTableEntry()
	 * @generated
	 */
	EReference getExceptionTableEntry_Method();

	/**
	 * Returns the meta object for the reference '{@link nl.utwente.jbcmm.ExceptionTableEntry#getStartInstruction <em>Start Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Start Instruction</em>'.
	 * @see nl.utwente.jbcmm.ExceptionTableEntry#getStartInstruction()
	 * @see #getExceptionTableEntry()
	 * @generated
	 */
	EReference getExceptionTableEntry_StartInstruction();

	/**
	 * Returns the meta object for the reference '{@link nl.utwente.jbcmm.ExceptionTableEntry#getEndInstruction <em>End Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>End Instruction</em>'.
	 * @see nl.utwente.jbcmm.ExceptionTableEntry#getEndInstruction()
	 * @see #getExceptionTableEntry()
	 * @generated
	 */
	EReference getExceptionTableEntry_EndInstruction();

	/**
	 * Returns the meta object for the reference '{@link nl.utwente.jbcmm.ExceptionTableEntry#getHandlerInstruction <em>Handler Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Handler Instruction</em>'.
	 * @see nl.utwente.jbcmm.ExceptionTableEntry#getHandlerInstruction()
	 * @see #getExceptionTableEntry()
	 * @generated
	 */
	EReference getExceptionTableEntry_HandlerInstruction();

	/**
	 * Returns the meta object for the containment reference '{@link nl.utwente.jbcmm.ExceptionTableEntry#getCatchType <em>Catch Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Catch Type</em>'.
	 * @see nl.utwente.jbcmm.ExceptionTableEntry#getCatchType()
	 * @see #getExceptionTableEntry()
	 * @generated
	 */
	EReference getExceptionTableEntry_CatchType();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.ControlFlowEdge <em>Control Flow Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Control Flow Edge</em>'.
	 * @see nl.utwente.jbcmm.ControlFlowEdge
	 * @generated
	 */
	EClass getControlFlowEdge();

	/**
	 * Returns the meta object for the container reference '{@link nl.utwente.jbcmm.ControlFlowEdge#getStart <em>Start</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Start</em>'.
	 * @see nl.utwente.jbcmm.ControlFlowEdge#getStart()
	 * @see #getControlFlowEdge()
	 * @generated
	 */
	EReference getControlFlowEdge_Start();

	/**
	 * Returns the meta object for the reference '{@link nl.utwente.jbcmm.ControlFlowEdge#getEnd <em>End</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>End</em>'.
	 * @see nl.utwente.jbcmm.ControlFlowEdge#getEnd()
	 * @see #getControlFlowEdge()
	 * @generated
	 */
	EReference getControlFlowEdge_End();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.UnconditionalEdge <em>Unconditional Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unconditional Edge</em>'.
	 * @see nl.utwente.jbcmm.UnconditionalEdge
	 * @generated
	 */
	EClass getUnconditionalEdge();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.ConditionalEdge <em>Conditional Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Conditional Edge</em>'.
	 * @see nl.utwente.jbcmm.ConditionalEdge
	 * @generated
	 */
	EClass getConditionalEdge();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.ConditionalEdge#isCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Condition</em>'.
	 * @see nl.utwente.jbcmm.ConditionalEdge#isCondition()
	 * @see #getConditionalEdge()
	 * @generated
	 */
	EAttribute getConditionalEdge_Condition();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.SwitchCaseEdge <em>Switch Case Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Switch Case Edge</em>'.
	 * @see nl.utwente.jbcmm.SwitchCaseEdge
	 * @generated
	 */
	EClass getSwitchCaseEdge();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.SwitchCaseEdge#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Condition</em>'.
	 * @see nl.utwente.jbcmm.SwitchCaseEdge#getCondition()
	 * @see #getSwitchCaseEdge()
	 * @generated
	 */
	EAttribute getSwitchCaseEdge_Condition();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.SwitchDefaultEdge <em>Switch Default Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Switch Default Edge</em>'.
	 * @see nl.utwente.jbcmm.SwitchDefaultEdge
	 * @generated
	 */
	EClass getSwitchDefaultEdge();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.ExceptionalEdge <em>Exceptional Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Exceptional Edge</em>'.
	 * @see nl.utwente.jbcmm.ExceptionalEdge
	 * @generated
	 */
	EClass getExceptionalEdge();

	/**
	 * Returns the meta object for the reference '{@link nl.utwente.jbcmm.ExceptionalEdge#getExceptionTableEntry <em>Exception Table Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Exception Table Entry</em>'.
	 * @see nl.utwente.jbcmm.ExceptionalEdge#getExceptionTableEntry()
	 * @see #getExceptionalEdge()
	 * @generated
	 */
	EReference getExceptionalEdge_ExceptionTableEntry();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.Instruction <em>Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Instruction</em>'.
	 * @see nl.utwente.jbcmm.Instruction
	 * @generated
	 */
	EClass getInstruction();

	/**
	 * Returns the meta object for the container reference '{@link nl.utwente.jbcmm.Instruction#getMethod <em>Method</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Method</em>'.
	 * @see nl.utwente.jbcmm.Instruction#getMethod()
	 * @see #getInstruction()
	 * @generated
	 */
	EReference getInstruction_Method();

	/**
	 * Returns the meta object for the reference '{@link nl.utwente.jbcmm.Instruction#getNextInCodeOrder <em>Next In Code Order</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Next In Code Order</em>'.
	 * @see nl.utwente.jbcmm.Instruction#getNextInCodeOrder()
	 * @see #getInstruction()
	 * @generated
	 */
	EReference getInstruction_NextInCodeOrder();

	/**
	 * Returns the meta object for the reference '{@link nl.utwente.jbcmm.Instruction#getPreviousInCodeOrder <em>Previous In Code Order</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Previous In Code Order</em>'.
	 * @see nl.utwente.jbcmm.Instruction#getPreviousInCodeOrder()
	 * @see #getInstruction()
	 * @generated
	 */
	EReference getInstruction_PreviousInCodeOrder();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Instruction#getLinenumber <em>Linenumber</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Linenumber</em>'.
	 * @see nl.utwente.jbcmm.Instruction#getLinenumber()
	 * @see #getInstruction()
	 * @generated
	 */
	EAttribute getInstruction_Linenumber();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Instruction#getIndex <em>Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Index</em>'.
	 * @see nl.utwente.jbcmm.Instruction#getIndex()
	 * @see #getInstruction()
	 * @generated
	 */
	EAttribute getInstruction_Index();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Instruction#getOpcode <em>Opcode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Opcode</em>'.
	 * @see nl.utwente.jbcmm.Instruction#getOpcode()
	 * @see #getInstruction()
	 * @generated
	 */
	EAttribute getInstruction_Opcode();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.Instruction#getHumanReadable <em>Human Readable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Human Readable</em>'.
	 * @see nl.utwente.jbcmm.Instruction#getHumanReadable()
	 * @see #getInstruction()
	 * @generated
	 */
	EAttribute getInstruction_HumanReadable();

	/**
	 * Returns the meta object for the containment reference list '{@link nl.utwente.jbcmm.Instruction#getOutEdges <em>Out Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Out Edges</em>'.
	 * @see nl.utwente.jbcmm.Instruction#getOutEdges()
	 * @see #getInstruction()
	 * @generated
	 */
	EReference getInstruction_OutEdges();

	/**
	 * Returns the meta object for the reference list '{@link nl.utwente.jbcmm.Instruction#getInEdges <em>In Edges</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>In Edges</em>'.
	 * @see nl.utwente.jbcmm.Instruction#getInEdges()
	 * @see #getInstruction()
	 * @generated
	 */
	EReference getInstruction_InEdges();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.FieldInstruction <em>Field Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Field Instruction</em>'.
	 * @see nl.utwente.jbcmm.FieldInstruction
	 * @generated
	 */
	EClass getFieldInstruction();

	/**
	 * Returns the meta object for the containment reference '{@link nl.utwente.jbcmm.FieldInstruction#getFieldReference <em>Field Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Field Reference</em>'.
	 * @see nl.utwente.jbcmm.FieldInstruction#getFieldReference()
	 * @see #getFieldInstruction()
	 * @generated
	 */
	EReference getFieldInstruction_FieldReference();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.SimpleInstruction <em>Simple Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simple Instruction</em>'.
	 * @see nl.utwente.jbcmm.SimpleInstruction
	 * @generated
	 */
	EClass getSimpleInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.IntInstruction <em>Int Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Int Instruction</em>'.
	 * @see nl.utwente.jbcmm.IntInstruction
	 * @generated
	 */
	EClass getIntInstruction();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.IntInstruction#getOperand <em>Operand</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operand</em>'.
	 * @see nl.utwente.jbcmm.IntInstruction#getOperand()
	 * @see #getIntInstruction()
	 * @generated
	 */
	EAttribute getIntInstruction_Operand();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.JumpInstruction <em>Jump Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Jump Instruction</em>'.
	 * @see nl.utwente.jbcmm.JumpInstruction
	 * @generated
	 */
	EClass getJumpInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.LdcInstruction <em>Ldc Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ldc Instruction</em>'.
	 * @see nl.utwente.jbcmm.LdcInstruction
	 * @generated
	 */
	EClass getLdcInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.MethodInstruction <em>Method Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Method Instruction</em>'.
	 * @see nl.utwente.jbcmm.MethodInstruction
	 * @generated
	 */
	EClass getMethodInstruction();

	/**
	 * Returns the meta object for the containment reference '{@link nl.utwente.jbcmm.MethodInstruction#getMethodReference <em>Method Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Method Reference</em>'.
	 * @see nl.utwente.jbcmm.MethodInstruction#getMethodReference()
	 * @see #getMethodInstruction()
	 * @generated
	 */
	EReference getMethodInstruction_MethodReference();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.TypeInstruction <em>Type Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type Instruction</em>'.
	 * @see nl.utwente.jbcmm.TypeInstruction
	 * @generated
	 */
	EClass getTypeInstruction();

	/**
	 * Returns the meta object for the containment reference '{@link nl.utwente.jbcmm.TypeInstruction#getTypeReference <em>Type Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type Reference</em>'.
	 * @see nl.utwente.jbcmm.TypeInstruction#getTypeReference()
	 * @see #getTypeInstruction()
	 * @generated
	 */
	EReference getTypeInstruction_TypeReference();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.VarInstruction <em>Var Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Var Instruction</em>'.
	 * @see nl.utwente.jbcmm.VarInstruction
	 * @generated
	 */
	EClass getVarInstruction();

	/**
	 * Returns the meta object for the reference '{@link nl.utwente.jbcmm.VarInstruction#getLocalVariable <em>Local Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Local Variable</em>'.
	 * @see nl.utwente.jbcmm.VarInstruction#getLocalVariable()
	 * @see #getVarInstruction()
	 * @generated
	 */
	EReference getVarInstruction_LocalVariable();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.VarInstruction#getVarIndex <em>Var Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Var Index</em>'.
	 * @see nl.utwente.jbcmm.VarInstruction#getVarIndex()
	 * @see #getVarInstruction()
	 * @generated
	 */
	EAttribute getVarInstruction_VarIndex();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.IincInstruction <em>Iinc Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Iinc Instruction</em>'.
	 * @see nl.utwente.jbcmm.IincInstruction
	 * @generated
	 */
	EClass getIincInstruction();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.IincInstruction#getIncr <em>Incr</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Incr</em>'.
	 * @see nl.utwente.jbcmm.IincInstruction#getIncr()
	 * @see #getIincInstruction()
	 * @generated
	 */
	EAttribute getIincInstruction_Incr();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.MultianewarrayInstruction <em>Multianewarray Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Multianewarray Instruction</em>'.
	 * @see nl.utwente.jbcmm.MultianewarrayInstruction
	 * @generated
	 */
	EClass getMultianewarrayInstruction();

	/**
	 * Returns the meta object for the containment reference '{@link nl.utwente.jbcmm.MultianewarrayInstruction#getTypeReference <em>Type Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type Reference</em>'.
	 * @see nl.utwente.jbcmm.MultianewarrayInstruction#getTypeReference()
	 * @see #getMultianewarrayInstruction()
	 * @generated
	 */
	EReference getMultianewarrayInstruction_TypeReference();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.MultianewarrayInstruction#getDims <em>Dims</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Dims</em>'.
	 * @see nl.utwente.jbcmm.MultianewarrayInstruction#getDims()
	 * @see #getMultianewarrayInstruction()
	 * @generated
	 */
	EAttribute getMultianewarrayInstruction_Dims();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.SwitchInstruction <em>Switch Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Switch Instruction</em>'.
	 * @see nl.utwente.jbcmm.SwitchInstruction
	 * @generated
	 */
	EClass getSwitchInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.LdcIntInstruction <em>Ldc Int Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ldc Int Instruction</em>'.
	 * @see nl.utwente.jbcmm.LdcIntInstruction
	 * @generated
	 */
	EClass getLdcIntInstruction();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.LdcIntInstruction#getConstant <em>Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Constant</em>'.
	 * @see nl.utwente.jbcmm.LdcIntInstruction#getConstant()
	 * @see #getLdcIntInstruction()
	 * @generated
	 */
	EAttribute getLdcIntInstruction_Constant();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.LdcLongInstruction <em>Ldc Long Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ldc Long Instruction</em>'.
	 * @see nl.utwente.jbcmm.LdcLongInstruction
	 * @generated
	 */
	EClass getLdcLongInstruction();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.LdcLongInstruction#getConstant <em>Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Constant</em>'.
	 * @see nl.utwente.jbcmm.LdcLongInstruction#getConstant()
	 * @see #getLdcLongInstruction()
	 * @generated
	 */
	EAttribute getLdcLongInstruction_Constant();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.LdcFloatInstruction <em>Ldc Float Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ldc Float Instruction</em>'.
	 * @see nl.utwente.jbcmm.LdcFloatInstruction
	 * @generated
	 */
	EClass getLdcFloatInstruction();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.LdcFloatInstruction#getConstant <em>Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Constant</em>'.
	 * @see nl.utwente.jbcmm.LdcFloatInstruction#getConstant()
	 * @see #getLdcFloatInstruction()
	 * @generated
	 */
	EAttribute getLdcFloatInstruction_Constant();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.LdcDoubleInstruction <em>Ldc Double Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ldc Double Instruction</em>'.
	 * @see nl.utwente.jbcmm.LdcDoubleInstruction
	 * @generated
	 */
	EClass getLdcDoubleInstruction();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.LdcDoubleInstruction#getConstant <em>Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Constant</em>'.
	 * @see nl.utwente.jbcmm.LdcDoubleInstruction#getConstant()
	 * @see #getLdcDoubleInstruction()
	 * @generated
	 */
	EAttribute getLdcDoubleInstruction_Constant();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.LdcStringInstruction <em>Ldc String Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ldc String Instruction</em>'.
	 * @see nl.utwente.jbcmm.LdcStringInstruction
	 * @generated
	 */
	EClass getLdcStringInstruction();

	/**
	 * Returns the meta object for the attribute '{@link nl.utwente.jbcmm.LdcStringInstruction#getConstant <em>Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Constant</em>'.
	 * @see nl.utwente.jbcmm.LdcStringInstruction#getConstant()
	 * @see #getLdcStringInstruction()
	 * @generated
	 */
	EAttribute getLdcStringInstruction_Constant();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.LdcTypeInstruction <em>Ldc Type Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ldc Type Instruction</em>'.
	 * @see nl.utwente.jbcmm.LdcTypeInstruction
	 * @generated
	 */
	EClass getLdcTypeInstruction();

	/**
	 * Returns the meta object for the containment reference '{@link nl.utwente.jbcmm.LdcTypeInstruction#getConstant <em>Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Constant</em>'.
	 * @see nl.utwente.jbcmm.LdcTypeInstruction#getConstant()
	 * @see #getLdcTypeInstruction()
	 * @generated
	 */
	EReference getLdcTypeInstruction_Constant();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.GetstaticInstruction <em>Getstatic Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Getstatic Instruction</em>'.
	 * @see nl.utwente.jbcmm.GetstaticInstruction
	 * @generated
	 */
	EClass getGetstaticInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.PutstaticInstruction <em>Putstatic Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Putstatic Instruction</em>'.
	 * @see nl.utwente.jbcmm.PutstaticInstruction
	 * @generated
	 */
	EClass getPutstaticInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.GetfieldInstruction <em>Getfield Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Getfield Instruction</em>'.
	 * @see nl.utwente.jbcmm.GetfieldInstruction
	 * @generated
	 */
	EClass getGetfieldInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.PutfieldInstruction <em>Putfield Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Putfield Instruction</em>'.
	 * @see nl.utwente.jbcmm.PutfieldInstruction
	 * @generated
	 */
	EClass getPutfieldInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.NopInstruction <em>Nop Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Nop Instruction</em>'.
	 * @see nl.utwente.jbcmm.NopInstruction
	 * @generated
	 */
	EClass getNopInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.Aconst_nullInstruction <em>Aconst null Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aconst null Instruction</em>'.
	 * @see nl.utwente.jbcmm.Aconst_nullInstruction
	 * @generated
	 */
	EClass getAconst_nullInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.Iconst_m1Instruction <em>Iconst m1 Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Iconst m1 Instruction</em>'.
	 * @see nl.utwente.jbcmm.Iconst_m1Instruction
	 * @generated
	 */
	EClass getIconst_m1Instruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.Iconst_0Instruction <em>Iconst 0Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Iconst 0Instruction</em>'.
	 * @see nl.utwente.jbcmm.Iconst_0Instruction
	 * @generated
	 */
	EClass getIconst_0Instruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.Iconst_1Instruction <em>Iconst 1Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Iconst 1Instruction</em>'.
	 * @see nl.utwente.jbcmm.Iconst_1Instruction
	 * @generated
	 */
	EClass getIconst_1Instruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.Iconst_2Instruction <em>Iconst 2Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Iconst 2Instruction</em>'.
	 * @see nl.utwente.jbcmm.Iconst_2Instruction
	 * @generated
	 */
	EClass getIconst_2Instruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.Iconst_3Instruction <em>Iconst 3Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Iconst 3Instruction</em>'.
	 * @see nl.utwente.jbcmm.Iconst_3Instruction
	 * @generated
	 */
	EClass getIconst_3Instruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.Iconst_4Instruction <em>Iconst 4Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Iconst 4Instruction</em>'.
	 * @see nl.utwente.jbcmm.Iconst_4Instruction
	 * @generated
	 */
	EClass getIconst_4Instruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.Iconst_5Instruction <em>Iconst 5Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Iconst 5Instruction</em>'.
	 * @see nl.utwente.jbcmm.Iconst_5Instruction
	 * @generated
	 */
	EClass getIconst_5Instruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.Lconst_0Instruction <em>Lconst 0Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lconst 0Instruction</em>'.
	 * @see nl.utwente.jbcmm.Lconst_0Instruction
	 * @generated
	 */
	EClass getLconst_0Instruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.Lconst_1Instruction <em>Lconst 1Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lconst 1Instruction</em>'.
	 * @see nl.utwente.jbcmm.Lconst_1Instruction
	 * @generated
	 */
	EClass getLconst_1Instruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.Fconst_0Instruction <em>Fconst 0Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fconst 0Instruction</em>'.
	 * @see nl.utwente.jbcmm.Fconst_0Instruction
	 * @generated
	 */
	EClass getFconst_0Instruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.Fconst_1Instruction <em>Fconst 1Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fconst 1Instruction</em>'.
	 * @see nl.utwente.jbcmm.Fconst_1Instruction
	 * @generated
	 */
	EClass getFconst_1Instruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.Fconst_2Instruction <em>Fconst 2Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fconst 2Instruction</em>'.
	 * @see nl.utwente.jbcmm.Fconst_2Instruction
	 * @generated
	 */
	EClass getFconst_2Instruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.Dconst_0Instruction <em>Dconst 0Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dconst 0Instruction</em>'.
	 * @see nl.utwente.jbcmm.Dconst_0Instruction
	 * @generated
	 */
	EClass getDconst_0Instruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.Dconst_1Instruction <em>Dconst 1Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dconst 1Instruction</em>'.
	 * @see nl.utwente.jbcmm.Dconst_1Instruction
	 * @generated
	 */
	EClass getDconst_1Instruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.IaloadInstruction <em>Iaload Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Iaload Instruction</em>'.
	 * @see nl.utwente.jbcmm.IaloadInstruction
	 * @generated
	 */
	EClass getIaloadInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.LaloadInstruction <em>Laload Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Laload Instruction</em>'.
	 * @see nl.utwente.jbcmm.LaloadInstruction
	 * @generated
	 */
	EClass getLaloadInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.FaloadInstruction <em>Faload Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Faload Instruction</em>'.
	 * @see nl.utwente.jbcmm.FaloadInstruction
	 * @generated
	 */
	EClass getFaloadInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.DaloadInstruction <em>Daload Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Daload Instruction</em>'.
	 * @see nl.utwente.jbcmm.DaloadInstruction
	 * @generated
	 */
	EClass getDaloadInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.AaloadInstruction <em>Aaload Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aaload Instruction</em>'.
	 * @see nl.utwente.jbcmm.AaloadInstruction
	 * @generated
	 */
	EClass getAaloadInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.BaloadInstruction <em>Baload Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Baload Instruction</em>'.
	 * @see nl.utwente.jbcmm.BaloadInstruction
	 * @generated
	 */
	EClass getBaloadInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.CaloadInstruction <em>Caload Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Caload Instruction</em>'.
	 * @see nl.utwente.jbcmm.CaloadInstruction
	 * @generated
	 */
	EClass getCaloadInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.SaloadInstruction <em>Saload Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Saload Instruction</em>'.
	 * @see nl.utwente.jbcmm.SaloadInstruction
	 * @generated
	 */
	EClass getSaloadInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.IastoreInstruction <em>Iastore Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Iastore Instruction</em>'.
	 * @see nl.utwente.jbcmm.IastoreInstruction
	 * @generated
	 */
	EClass getIastoreInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.LastoreInstruction <em>Lastore Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lastore Instruction</em>'.
	 * @see nl.utwente.jbcmm.LastoreInstruction
	 * @generated
	 */
	EClass getLastoreInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.FastoreInstruction <em>Fastore Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fastore Instruction</em>'.
	 * @see nl.utwente.jbcmm.FastoreInstruction
	 * @generated
	 */
	EClass getFastoreInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.DastoreInstruction <em>Dastore Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dastore Instruction</em>'.
	 * @see nl.utwente.jbcmm.DastoreInstruction
	 * @generated
	 */
	EClass getDastoreInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.AastoreInstruction <em>Aastore Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aastore Instruction</em>'.
	 * @see nl.utwente.jbcmm.AastoreInstruction
	 * @generated
	 */
	EClass getAastoreInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.BastoreInstruction <em>Bastore Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bastore Instruction</em>'.
	 * @see nl.utwente.jbcmm.BastoreInstruction
	 * @generated
	 */
	EClass getBastoreInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.CastoreInstruction <em>Castore Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Castore Instruction</em>'.
	 * @see nl.utwente.jbcmm.CastoreInstruction
	 * @generated
	 */
	EClass getCastoreInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.SastoreInstruction <em>Sastore Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sastore Instruction</em>'.
	 * @see nl.utwente.jbcmm.SastoreInstruction
	 * @generated
	 */
	EClass getSastoreInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.PopInstruction <em>Pop Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pop Instruction</em>'.
	 * @see nl.utwente.jbcmm.PopInstruction
	 * @generated
	 */
	EClass getPopInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.Pop2Instruction <em>Pop2 Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pop2 Instruction</em>'.
	 * @see nl.utwente.jbcmm.Pop2Instruction
	 * @generated
	 */
	EClass getPop2Instruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.DupInstruction <em>Dup Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dup Instruction</em>'.
	 * @see nl.utwente.jbcmm.DupInstruction
	 * @generated
	 */
	EClass getDupInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.Dup_x1Instruction <em>Dup x1 Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dup x1 Instruction</em>'.
	 * @see nl.utwente.jbcmm.Dup_x1Instruction
	 * @generated
	 */
	EClass getDup_x1Instruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.Dup_x2Instruction <em>Dup x2 Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dup x2 Instruction</em>'.
	 * @see nl.utwente.jbcmm.Dup_x2Instruction
	 * @generated
	 */
	EClass getDup_x2Instruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.Dup2Instruction <em>Dup2 Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dup2 Instruction</em>'.
	 * @see nl.utwente.jbcmm.Dup2Instruction
	 * @generated
	 */
	EClass getDup2Instruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.Dup2_x1Instruction <em>Dup2 x1 Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dup2 x1 Instruction</em>'.
	 * @see nl.utwente.jbcmm.Dup2_x1Instruction
	 * @generated
	 */
	EClass getDup2_x1Instruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.Dup2_x2Instruction <em>Dup2 x2 Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dup2 x2 Instruction</em>'.
	 * @see nl.utwente.jbcmm.Dup2_x2Instruction
	 * @generated
	 */
	EClass getDup2_x2Instruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.SwapInstruction <em>Swap Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Swap Instruction</em>'.
	 * @see nl.utwente.jbcmm.SwapInstruction
	 * @generated
	 */
	EClass getSwapInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.IaddInstruction <em>Iadd Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Iadd Instruction</em>'.
	 * @see nl.utwente.jbcmm.IaddInstruction
	 * @generated
	 */
	EClass getIaddInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.LaddInstruction <em>Ladd Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ladd Instruction</em>'.
	 * @see nl.utwente.jbcmm.LaddInstruction
	 * @generated
	 */
	EClass getLaddInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.FaddInstruction <em>Fadd Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fadd Instruction</em>'.
	 * @see nl.utwente.jbcmm.FaddInstruction
	 * @generated
	 */
	EClass getFaddInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.DaddInstruction <em>Dadd Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dadd Instruction</em>'.
	 * @see nl.utwente.jbcmm.DaddInstruction
	 * @generated
	 */
	EClass getDaddInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.IsubInstruction <em>Isub Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Isub Instruction</em>'.
	 * @see nl.utwente.jbcmm.IsubInstruction
	 * @generated
	 */
	EClass getIsubInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.LsubInstruction <em>Lsub Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lsub Instruction</em>'.
	 * @see nl.utwente.jbcmm.LsubInstruction
	 * @generated
	 */
	EClass getLsubInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.FsubInstruction <em>Fsub Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fsub Instruction</em>'.
	 * @see nl.utwente.jbcmm.FsubInstruction
	 * @generated
	 */
	EClass getFsubInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.DsubInstruction <em>Dsub Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dsub Instruction</em>'.
	 * @see nl.utwente.jbcmm.DsubInstruction
	 * @generated
	 */
	EClass getDsubInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.ImulInstruction <em>Imul Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Imul Instruction</em>'.
	 * @see nl.utwente.jbcmm.ImulInstruction
	 * @generated
	 */
	EClass getImulInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.LmulInstruction <em>Lmul Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lmul Instruction</em>'.
	 * @see nl.utwente.jbcmm.LmulInstruction
	 * @generated
	 */
	EClass getLmulInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.FmulInstruction <em>Fmul Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fmul Instruction</em>'.
	 * @see nl.utwente.jbcmm.FmulInstruction
	 * @generated
	 */
	EClass getFmulInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.DmulInstruction <em>Dmul Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dmul Instruction</em>'.
	 * @see nl.utwente.jbcmm.DmulInstruction
	 * @generated
	 */
	EClass getDmulInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.IdivInstruction <em>Idiv Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Idiv Instruction</em>'.
	 * @see nl.utwente.jbcmm.IdivInstruction
	 * @generated
	 */
	EClass getIdivInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.LdivInstruction <em>Ldiv Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ldiv Instruction</em>'.
	 * @see nl.utwente.jbcmm.LdivInstruction
	 * @generated
	 */
	EClass getLdivInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.FdivInstruction <em>Fdiv Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fdiv Instruction</em>'.
	 * @see nl.utwente.jbcmm.FdivInstruction
	 * @generated
	 */
	EClass getFdivInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.DdivInstruction <em>Ddiv Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ddiv Instruction</em>'.
	 * @see nl.utwente.jbcmm.DdivInstruction
	 * @generated
	 */
	EClass getDdivInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.IremInstruction <em>Irem Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Irem Instruction</em>'.
	 * @see nl.utwente.jbcmm.IremInstruction
	 * @generated
	 */
	EClass getIremInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.LremInstruction <em>Lrem Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lrem Instruction</em>'.
	 * @see nl.utwente.jbcmm.LremInstruction
	 * @generated
	 */
	EClass getLremInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.FremInstruction <em>Frem Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Frem Instruction</em>'.
	 * @see nl.utwente.jbcmm.FremInstruction
	 * @generated
	 */
	EClass getFremInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.DremInstruction <em>Drem Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Drem Instruction</em>'.
	 * @see nl.utwente.jbcmm.DremInstruction
	 * @generated
	 */
	EClass getDremInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.InegInstruction <em>Ineg Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ineg Instruction</em>'.
	 * @see nl.utwente.jbcmm.InegInstruction
	 * @generated
	 */
	EClass getInegInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.LnegInstruction <em>Lneg Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lneg Instruction</em>'.
	 * @see nl.utwente.jbcmm.LnegInstruction
	 * @generated
	 */
	EClass getLnegInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.FnegInstruction <em>Fneg Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fneg Instruction</em>'.
	 * @see nl.utwente.jbcmm.FnegInstruction
	 * @generated
	 */
	EClass getFnegInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.DnegInstruction <em>Dneg Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dneg Instruction</em>'.
	 * @see nl.utwente.jbcmm.DnegInstruction
	 * @generated
	 */
	EClass getDnegInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.IshlInstruction <em>Ishl Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ishl Instruction</em>'.
	 * @see nl.utwente.jbcmm.IshlInstruction
	 * @generated
	 */
	EClass getIshlInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.LshlInstruction <em>Lshl Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lshl Instruction</em>'.
	 * @see nl.utwente.jbcmm.LshlInstruction
	 * @generated
	 */
	EClass getLshlInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.IshrInstruction <em>Ishr Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ishr Instruction</em>'.
	 * @see nl.utwente.jbcmm.IshrInstruction
	 * @generated
	 */
	EClass getIshrInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.LshrInstruction <em>Lshr Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lshr Instruction</em>'.
	 * @see nl.utwente.jbcmm.LshrInstruction
	 * @generated
	 */
	EClass getLshrInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.IushrInstruction <em>Iushr Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Iushr Instruction</em>'.
	 * @see nl.utwente.jbcmm.IushrInstruction
	 * @generated
	 */
	EClass getIushrInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.LushrInstruction <em>Lushr Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lushr Instruction</em>'.
	 * @see nl.utwente.jbcmm.LushrInstruction
	 * @generated
	 */
	EClass getLushrInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.IandInstruction <em>Iand Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Iand Instruction</em>'.
	 * @see nl.utwente.jbcmm.IandInstruction
	 * @generated
	 */
	EClass getIandInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.LandInstruction <em>Land Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Land Instruction</em>'.
	 * @see nl.utwente.jbcmm.LandInstruction
	 * @generated
	 */
	EClass getLandInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.IorInstruction <em>Ior Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ior Instruction</em>'.
	 * @see nl.utwente.jbcmm.IorInstruction
	 * @generated
	 */
	EClass getIorInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.LorInstruction <em>Lor Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lor Instruction</em>'.
	 * @see nl.utwente.jbcmm.LorInstruction
	 * @generated
	 */
	EClass getLorInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.IxorInstruction <em>Ixor Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ixor Instruction</em>'.
	 * @see nl.utwente.jbcmm.IxorInstruction
	 * @generated
	 */
	EClass getIxorInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.LxorInstruction <em>Lxor Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lxor Instruction</em>'.
	 * @see nl.utwente.jbcmm.LxorInstruction
	 * @generated
	 */
	EClass getLxorInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.I2lInstruction <em>I2l Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>I2l Instruction</em>'.
	 * @see nl.utwente.jbcmm.I2lInstruction
	 * @generated
	 */
	EClass getI2lInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.I2fInstruction <em>I2f Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>I2f Instruction</em>'.
	 * @see nl.utwente.jbcmm.I2fInstruction
	 * @generated
	 */
	EClass getI2fInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.I2dInstruction <em>I2d Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>I2d Instruction</em>'.
	 * @see nl.utwente.jbcmm.I2dInstruction
	 * @generated
	 */
	EClass getI2dInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.L2iInstruction <em>L2i Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>L2i Instruction</em>'.
	 * @see nl.utwente.jbcmm.L2iInstruction
	 * @generated
	 */
	EClass getL2iInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.L2fInstruction <em>L2f Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>L2f Instruction</em>'.
	 * @see nl.utwente.jbcmm.L2fInstruction
	 * @generated
	 */
	EClass getL2fInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.L2dInstruction <em>L2d Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>L2d Instruction</em>'.
	 * @see nl.utwente.jbcmm.L2dInstruction
	 * @generated
	 */
	EClass getL2dInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.F2iInstruction <em>F2i Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>F2i Instruction</em>'.
	 * @see nl.utwente.jbcmm.F2iInstruction
	 * @generated
	 */
	EClass getF2iInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.F2lInstruction <em>F2l Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>F2l Instruction</em>'.
	 * @see nl.utwente.jbcmm.F2lInstruction
	 * @generated
	 */
	EClass getF2lInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.F2dInstruction <em>F2d Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>F2d Instruction</em>'.
	 * @see nl.utwente.jbcmm.F2dInstruction
	 * @generated
	 */
	EClass getF2dInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.D2iInstruction <em>D2i Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>D2i Instruction</em>'.
	 * @see nl.utwente.jbcmm.D2iInstruction
	 * @generated
	 */
	EClass getD2iInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.D2lInstruction <em>D2l Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>D2l Instruction</em>'.
	 * @see nl.utwente.jbcmm.D2lInstruction
	 * @generated
	 */
	EClass getD2lInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.D2fInstruction <em>D2f Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>D2f Instruction</em>'.
	 * @see nl.utwente.jbcmm.D2fInstruction
	 * @generated
	 */
	EClass getD2fInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.I2bInstruction <em>I2b Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>I2b Instruction</em>'.
	 * @see nl.utwente.jbcmm.I2bInstruction
	 * @generated
	 */
	EClass getI2bInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.I2cInstruction <em>I2c Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>I2c Instruction</em>'.
	 * @see nl.utwente.jbcmm.I2cInstruction
	 * @generated
	 */
	EClass getI2cInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.I2sInstruction <em>I2s Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>I2s Instruction</em>'.
	 * @see nl.utwente.jbcmm.I2sInstruction
	 * @generated
	 */
	EClass getI2sInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.LcmpInstruction <em>Lcmp Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lcmp Instruction</em>'.
	 * @see nl.utwente.jbcmm.LcmpInstruction
	 * @generated
	 */
	EClass getLcmpInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.FcmplInstruction <em>Fcmpl Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fcmpl Instruction</em>'.
	 * @see nl.utwente.jbcmm.FcmplInstruction
	 * @generated
	 */
	EClass getFcmplInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.FcmpgInstruction <em>Fcmpg Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fcmpg Instruction</em>'.
	 * @see nl.utwente.jbcmm.FcmpgInstruction
	 * @generated
	 */
	EClass getFcmpgInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.DcmplInstruction <em>Dcmpl Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dcmpl Instruction</em>'.
	 * @see nl.utwente.jbcmm.DcmplInstruction
	 * @generated
	 */
	EClass getDcmplInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.DcmpgInstruction <em>Dcmpg Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dcmpg Instruction</em>'.
	 * @see nl.utwente.jbcmm.DcmpgInstruction
	 * @generated
	 */
	EClass getDcmpgInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.IreturnInstruction <em>Ireturn Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ireturn Instruction</em>'.
	 * @see nl.utwente.jbcmm.IreturnInstruction
	 * @generated
	 */
	EClass getIreturnInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.LreturnInstruction <em>Lreturn Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lreturn Instruction</em>'.
	 * @see nl.utwente.jbcmm.LreturnInstruction
	 * @generated
	 */
	EClass getLreturnInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.FreturnInstruction <em>Freturn Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Freturn Instruction</em>'.
	 * @see nl.utwente.jbcmm.FreturnInstruction
	 * @generated
	 */
	EClass getFreturnInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.DreturnInstruction <em>Dreturn Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dreturn Instruction</em>'.
	 * @see nl.utwente.jbcmm.DreturnInstruction
	 * @generated
	 */
	EClass getDreturnInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.AreturnInstruction <em>Areturn Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Areturn Instruction</em>'.
	 * @see nl.utwente.jbcmm.AreturnInstruction
	 * @generated
	 */
	EClass getAreturnInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.ReturnInstruction <em>Return Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Return Instruction</em>'.
	 * @see nl.utwente.jbcmm.ReturnInstruction
	 * @generated
	 */
	EClass getReturnInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.ArraylengthInstruction <em>Arraylength Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Arraylength Instruction</em>'.
	 * @see nl.utwente.jbcmm.ArraylengthInstruction
	 * @generated
	 */
	EClass getArraylengthInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.AthrowInstruction <em>Athrow Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Athrow Instruction</em>'.
	 * @see nl.utwente.jbcmm.AthrowInstruction
	 * @generated
	 */
	EClass getAthrowInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.MonitorenterInstruction <em>Monitorenter Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Monitorenter Instruction</em>'.
	 * @see nl.utwente.jbcmm.MonitorenterInstruction
	 * @generated
	 */
	EClass getMonitorenterInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.MonitorexitInstruction <em>Monitorexit Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Monitorexit Instruction</em>'.
	 * @see nl.utwente.jbcmm.MonitorexitInstruction
	 * @generated
	 */
	EClass getMonitorexitInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.BipushInstruction <em>Bipush Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bipush Instruction</em>'.
	 * @see nl.utwente.jbcmm.BipushInstruction
	 * @generated
	 */
	EClass getBipushInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.SipushInstruction <em>Sipush Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sipush Instruction</em>'.
	 * @see nl.utwente.jbcmm.SipushInstruction
	 * @generated
	 */
	EClass getSipushInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.NewarrayInstruction <em>Newarray Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Newarray Instruction</em>'.
	 * @see nl.utwente.jbcmm.NewarrayInstruction
	 * @generated
	 */
	EClass getNewarrayInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.IfeqInstruction <em>Ifeq Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ifeq Instruction</em>'.
	 * @see nl.utwente.jbcmm.IfeqInstruction
	 * @generated
	 */
	EClass getIfeqInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.IfneInstruction <em>Ifne Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ifne Instruction</em>'.
	 * @see nl.utwente.jbcmm.IfneInstruction
	 * @generated
	 */
	EClass getIfneInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.IfltInstruction <em>Iflt Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Iflt Instruction</em>'.
	 * @see nl.utwente.jbcmm.IfltInstruction
	 * @generated
	 */
	EClass getIfltInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.IfgeInstruction <em>Ifge Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ifge Instruction</em>'.
	 * @see nl.utwente.jbcmm.IfgeInstruction
	 * @generated
	 */
	EClass getIfgeInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.IfgtInstruction <em>Ifgt Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ifgt Instruction</em>'.
	 * @see nl.utwente.jbcmm.IfgtInstruction
	 * @generated
	 */
	EClass getIfgtInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.IfleInstruction <em>Ifle Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ifle Instruction</em>'.
	 * @see nl.utwente.jbcmm.IfleInstruction
	 * @generated
	 */
	EClass getIfleInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.If_icmpeqInstruction <em>If icmpeq Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>If icmpeq Instruction</em>'.
	 * @see nl.utwente.jbcmm.If_icmpeqInstruction
	 * @generated
	 */
	EClass getIf_icmpeqInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.If_icmpneInstruction <em>If icmpne Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>If icmpne Instruction</em>'.
	 * @see nl.utwente.jbcmm.If_icmpneInstruction
	 * @generated
	 */
	EClass getIf_icmpneInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.If_icmpltInstruction <em>If icmplt Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>If icmplt Instruction</em>'.
	 * @see nl.utwente.jbcmm.If_icmpltInstruction
	 * @generated
	 */
	EClass getIf_icmpltInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.If_icmpgeInstruction <em>If icmpge Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>If icmpge Instruction</em>'.
	 * @see nl.utwente.jbcmm.If_icmpgeInstruction
	 * @generated
	 */
	EClass getIf_icmpgeInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.If_icmpgtInstruction <em>If icmpgt Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>If icmpgt Instruction</em>'.
	 * @see nl.utwente.jbcmm.If_icmpgtInstruction
	 * @generated
	 */
	EClass getIf_icmpgtInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.If_icmpleInstruction <em>If icmple Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>If icmple Instruction</em>'.
	 * @see nl.utwente.jbcmm.If_icmpleInstruction
	 * @generated
	 */
	EClass getIf_icmpleInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.If_acmpeqInstruction <em>If acmpeq Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>If acmpeq Instruction</em>'.
	 * @see nl.utwente.jbcmm.If_acmpeqInstruction
	 * @generated
	 */
	EClass getIf_acmpeqInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.If_acmpneInstruction <em>If acmpne Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>If acmpne Instruction</em>'.
	 * @see nl.utwente.jbcmm.If_acmpneInstruction
	 * @generated
	 */
	EClass getIf_acmpneInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.GotoInstruction <em>Goto Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Goto Instruction</em>'.
	 * @see nl.utwente.jbcmm.GotoInstruction
	 * @generated
	 */
	EClass getGotoInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.JsrInstruction <em>Jsr Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Jsr Instruction</em>'.
	 * @see nl.utwente.jbcmm.JsrInstruction
	 * @generated
	 */
	EClass getJsrInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.IfnullInstruction <em>Ifnull Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ifnull Instruction</em>'.
	 * @see nl.utwente.jbcmm.IfnullInstruction
	 * @generated
	 */
	EClass getIfnullInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.IfnonnullInstruction <em>Ifnonnull Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ifnonnull Instruction</em>'.
	 * @see nl.utwente.jbcmm.IfnonnullInstruction
	 * @generated
	 */
	EClass getIfnonnullInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.InvokevirtualInstruction <em>Invokevirtual Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Invokevirtual Instruction</em>'.
	 * @see nl.utwente.jbcmm.InvokevirtualInstruction
	 * @generated
	 */
	EClass getInvokevirtualInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.InvokespecialInstruction <em>Invokespecial Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Invokespecial Instruction</em>'.
	 * @see nl.utwente.jbcmm.InvokespecialInstruction
	 * @generated
	 */
	EClass getInvokespecialInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.InvokestaticInstruction <em>Invokestatic Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Invokestatic Instruction</em>'.
	 * @see nl.utwente.jbcmm.InvokestaticInstruction
	 * @generated
	 */
	EClass getInvokestaticInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.InvokeinterfaceInstruction <em>Invokeinterface Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Invokeinterface Instruction</em>'.
	 * @see nl.utwente.jbcmm.InvokeinterfaceInstruction
	 * @generated
	 */
	EClass getInvokeinterfaceInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.NewInstruction <em>New Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>New Instruction</em>'.
	 * @see nl.utwente.jbcmm.NewInstruction
	 * @generated
	 */
	EClass getNewInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.AnewarrayInstruction <em>Anewarray Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Anewarray Instruction</em>'.
	 * @see nl.utwente.jbcmm.AnewarrayInstruction
	 * @generated
	 */
	EClass getAnewarrayInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.CheckcastInstruction <em>Checkcast Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Checkcast Instruction</em>'.
	 * @see nl.utwente.jbcmm.CheckcastInstruction
	 * @generated
	 */
	EClass getCheckcastInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.InstanceofInstruction <em>Instanceof Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Instanceof Instruction</em>'.
	 * @see nl.utwente.jbcmm.InstanceofInstruction
	 * @generated
	 */
	EClass getInstanceofInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.IloadInstruction <em>Iload Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Iload Instruction</em>'.
	 * @see nl.utwente.jbcmm.IloadInstruction
	 * @generated
	 */
	EClass getIloadInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.LloadInstruction <em>Lload Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lload Instruction</em>'.
	 * @see nl.utwente.jbcmm.LloadInstruction
	 * @generated
	 */
	EClass getLloadInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.FloadInstruction <em>Fload Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fload Instruction</em>'.
	 * @see nl.utwente.jbcmm.FloadInstruction
	 * @generated
	 */
	EClass getFloadInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.DloadInstruction <em>Dload Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dload Instruction</em>'.
	 * @see nl.utwente.jbcmm.DloadInstruction
	 * @generated
	 */
	EClass getDloadInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.AloadInstruction <em>Aload Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aload Instruction</em>'.
	 * @see nl.utwente.jbcmm.AloadInstruction
	 * @generated
	 */
	EClass getAloadInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.IstoreInstruction <em>Istore Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Istore Instruction</em>'.
	 * @see nl.utwente.jbcmm.IstoreInstruction
	 * @generated
	 */
	EClass getIstoreInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.LstoreInstruction <em>Lstore Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lstore Instruction</em>'.
	 * @see nl.utwente.jbcmm.LstoreInstruction
	 * @generated
	 */
	EClass getLstoreInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.FstoreInstruction <em>Fstore Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fstore Instruction</em>'.
	 * @see nl.utwente.jbcmm.FstoreInstruction
	 * @generated
	 */
	EClass getFstoreInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.DstoreInstruction <em>Dstore Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dstore Instruction</em>'.
	 * @see nl.utwente.jbcmm.DstoreInstruction
	 * @generated
	 */
	EClass getDstoreInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.AstoreInstruction <em>Astore Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Astore Instruction</em>'.
	 * @see nl.utwente.jbcmm.AstoreInstruction
	 * @generated
	 */
	EClass getAstoreInstruction();

	/**
	 * Returns the meta object for class '{@link nl.utwente.jbcmm.RetInstruction <em>Ret Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ret Instruction</em>'.
	 * @see nl.utwente.jbcmm.RetInstruction
	 * @generated
	 */
	EClass getRetInstruction();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	JbcmmFactory getJbcmmFactory();

} //JbcmmPackage
