/**
 */
package nl.utwente.jbcmm;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Elementary Value</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getElementaryValue()
 * @model
 * @generated
 */
public interface ElementaryValue extends Identifiable {

} // ElementaryValue
