/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Faload Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getFaloadInstruction()
 * @model
 * @generated
 */
public interface FaloadInstruction extends SimpleInstruction {
} // FaloadInstruction
