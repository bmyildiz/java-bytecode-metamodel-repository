/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fconst 0Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getFconst_0Instruction()
 * @model
 * @generated
 */
public interface Fconst_0Instruction extends SimpleInstruction {
} // Fconst_0Instruction
