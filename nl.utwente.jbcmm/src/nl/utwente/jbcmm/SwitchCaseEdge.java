/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Switch Case Edge</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.SwitchCaseEdge#getCondition <em>Condition</em>}</li>
 * </ul>
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getSwitchCaseEdge()
 * @model
 * @generated
 */
public interface SwitchCaseEdge extends ControlFlowEdge {
	/**
	 * Returns the value of the '<em><b>Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition</em>' attribute.
	 * @see #setCondition(int)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getSwitchCaseEdge_Condition()
	 * @model required="true"
	 * @generated
	 */
	int getCondition();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.SwitchCaseEdge#getCondition <em>Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition</em>' attribute.
	 * @see #getCondition()
	 * @generated
	 */
	void setCondition(int value);

} // SwitchCaseEdge
