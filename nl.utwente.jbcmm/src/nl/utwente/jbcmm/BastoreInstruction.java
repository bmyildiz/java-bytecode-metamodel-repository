/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bastore Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getBastoreInstruction()
 * @model
 * @generated
 */
public interface BastoreInstruction extends SimpleInstruction {
} // BastoreInstruction
