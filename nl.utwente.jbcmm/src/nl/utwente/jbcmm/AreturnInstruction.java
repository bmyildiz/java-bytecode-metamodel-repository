/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Areturn Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getAreturnInstruction()
 * @model
 * @generated
 */
public interface AreturnInstruction extends SimpleInstruction {
} // AreturnInstruction
