/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Istore Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIstoreInstruction()
 * @model
 * @generated
 */
public interface IstoreInstruction extends VarInstruction {
} // IstoreInstruction
