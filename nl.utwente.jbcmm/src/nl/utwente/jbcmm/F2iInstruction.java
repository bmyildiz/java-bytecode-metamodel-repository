/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>F2i Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getF2iInstruction()
 * @model
 * @generated
 */
public interface F2iInstruction extends SimpleInstruction {
} // F2iInstruction
