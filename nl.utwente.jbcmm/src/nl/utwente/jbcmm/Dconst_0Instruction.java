/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dconst 0Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getDconst_0Instruction()
 * @model
 * @generated
 */
public interface Dconst_0Instruction extends SimpleInstruction {
} // Dconst_0Instruction
