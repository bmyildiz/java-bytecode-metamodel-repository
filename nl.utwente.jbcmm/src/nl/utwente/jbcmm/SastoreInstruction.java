/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sastore Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getSastoreInstruction()
 * @model
 * @generated
 */
public interface SastoreInstruction extends SimpleInstruction {
} // SastoreInstruction
