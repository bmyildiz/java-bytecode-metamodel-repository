/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unconditional Edge</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getUnconditionalEdge()
 * @model
 * @generated
 */
public interface UnconditionalEdge extends ControlFlowEdge {
} // UnconditionalEdge
