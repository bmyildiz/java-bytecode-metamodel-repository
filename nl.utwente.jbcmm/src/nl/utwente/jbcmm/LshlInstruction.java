/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lshl Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getLshlInstruction()
 * @model
 * @generated
 */
public interface LshlInstruction extends SimpleInstruction {
} // LshlInstruction
