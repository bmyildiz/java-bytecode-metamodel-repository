/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Instanceof Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getInstanceofInstruction()
 * @model
 * @generated
 */
public interface InstanceofInstruction extends TypeInstruction {
} // InstanceofInstruction
