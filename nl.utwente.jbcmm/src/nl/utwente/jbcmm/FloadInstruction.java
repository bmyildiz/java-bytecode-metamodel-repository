/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fload Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getFloadInstruction()
 * @model
 * @generated
 */
public interface FloadInstruction extends VarInstruction {
} // FloadInstruction
