/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Daload Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getDaloadInstruction()
 * @model
 * @generated
 */
public interface DaloadInstruction extends SimpleInstruction {
} // DaloadInstruction
