/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Switch Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getSwitchInstruction()
 * @model
 * @generated
 */
public interface SwitchInstruction extends Instruction {
} // SwitchInstruction
