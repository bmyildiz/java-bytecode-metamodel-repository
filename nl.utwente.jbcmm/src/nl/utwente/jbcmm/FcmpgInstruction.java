/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fcmpg Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getFcmpgInstruction()
 * @model
 * @generated
 */
public interface FcmpgInstruction extends SimpleInstruction {
} // FcmpgInstruction
