/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Putstatic Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getPutstaticInstruction()
 * @model
 * @generated
 */
public interface PutstaticInstruction extends FieldInstruction {
} // PutstaticInstruction
