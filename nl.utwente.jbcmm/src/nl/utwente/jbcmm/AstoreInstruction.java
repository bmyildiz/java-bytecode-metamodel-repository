/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Astore Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getAstoreInstruction()
 * @model
 * @generated
 */
public interface AstoreInstruction extends VarInstruction {
} // AstoreInstruction
