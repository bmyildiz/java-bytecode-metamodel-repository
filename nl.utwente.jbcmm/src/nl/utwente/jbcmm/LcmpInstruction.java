/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lcmp Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getLcmpInstruction()
 * @model
 * @generated
 */
public interface LcmpInstruction extends SimpleInstruction {
} // LcmpInstruction
