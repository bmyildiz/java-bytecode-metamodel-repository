/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dconst 1Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getDconst_1Instruction()
 * @model
 * @generated
 */
public interface Dconst_1Instruction extends SimpleInstruction {
} // Dconst_1Instruction
