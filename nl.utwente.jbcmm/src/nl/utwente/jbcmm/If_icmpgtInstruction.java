/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>If icmpgt Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIf_icmpgtInstruction()
 * @model
 * @generated
 */
public interface If_icmpgtInstruction extends JumpInstruction {
} // If_icmpgtInstruction
