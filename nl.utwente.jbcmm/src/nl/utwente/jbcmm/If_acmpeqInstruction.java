/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>If acmpeq Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIf_acmpeqInstruction()
 * @model
 * @generated
 */
public interface If_acmpeqInstruction extends JumpInstruction {
} // If_acmpeqInstruction
