/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fsub Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getFsubInstruction()
 * @model
 * @generated
 */
public interface FsubInstruction extends SimpleInstruction {
} // FsubInstruction
