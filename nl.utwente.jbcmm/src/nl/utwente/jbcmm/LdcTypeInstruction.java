/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ldc Type Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link nl.utwente.jbcmm.LdcTypeInstruction#getConstant <em>Constant</em>}</li>
 * </ul>
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getLdcTypeInstruction()
 * @model
 * @generated
 */
public interface LdcTypeInstruction extends LdcInstruction {
	/**
	 * Returns the value of the '<em><b>Constant</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constant</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constant</em>' containment reference.
	 * @see #setConstant(TypeReference)
	 * @see nl.utwente.jbcmm.JbcmmPackage#getLdcTypeInstruction_Constant()
	 * @model containment="true" required="true"
	 * @generated
	 */
	TypeReference getConstant();

	/**
	 * Sets the value of the '{@link nl.utwente.jbcmm.LdcTypeInstruction#getConstant <em>Constant</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constant</em>' containment reference.
	 * @see #getConstant()
	 * @generated
	 */
	void setConstant(TypeReference value);

} // LdcTypeInstruction
