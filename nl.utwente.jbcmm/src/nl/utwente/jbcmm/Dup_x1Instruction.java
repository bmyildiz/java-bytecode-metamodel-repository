/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dup x1 Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getDup_x1Instruction()
 * @model
 * @generated
 */
public interface Dup_x1Instruction extends SimpleInstruction {
} // Dup_x1Instruction
