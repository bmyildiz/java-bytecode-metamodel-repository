/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>F2d Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getF2dInstruction()
 * @model
 * @generated
 */
public interface F2dInstruction extends SimpleInstruction {
} // F2dInstruction
