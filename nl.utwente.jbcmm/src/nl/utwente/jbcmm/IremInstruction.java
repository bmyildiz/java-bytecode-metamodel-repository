/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Irem Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIremInstruction()
 * @model
 * @generated
 */
public interface IremInstruction extends SimpleInstruction {
} // IremInstruction
