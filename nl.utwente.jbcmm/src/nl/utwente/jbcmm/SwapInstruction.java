/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Swap Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getSwapInstruction()
 * @model
 * @generated
 */
public interface SwapInstruction extends SimpleInstruction {
} // SwapInstruction
