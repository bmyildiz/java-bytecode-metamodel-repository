/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ishr Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIshrInstruction()
 * @model
 * @generated
 */
public interface IshrInstruction extends SimpleInstruction {
} // IshrInstruction
