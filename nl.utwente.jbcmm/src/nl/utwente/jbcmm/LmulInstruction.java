/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lmul Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getLmulInstruction()
 * @model
 * @generated
 */
public interface LmulInstruction extends SimpleInstruction {
} // LmulInstruction
