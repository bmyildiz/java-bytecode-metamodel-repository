/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lreturn Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getLreturnInstruction()
 * @model
 * @generated
 */
public interface LreturnInstruction extends SimpleInstruction {
} // LreturnInstruction
