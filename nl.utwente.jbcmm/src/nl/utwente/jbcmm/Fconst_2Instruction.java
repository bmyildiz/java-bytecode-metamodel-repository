/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fconst 2Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getFconst_2Instruction()
 * @model
 * @generated
 */
public interface Fconst_2Instruction extends SimpleInstruction {
} // Fconst_2Instruction
