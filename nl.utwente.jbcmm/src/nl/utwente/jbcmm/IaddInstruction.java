/**
 */
package nl.utwente.jbcmm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Iadd Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see nl.utwente.jbcmm.JbcmmPackage#getIaddInstruction()
 * @model
 * @generated
 */
public interface IaddInstruction extends SimpleInstruction {
} // IaddInstruction
